<%@ include file="/common/taglibs.jsp"%>

<menu:useMenuDisplayer name="Velocity" config="navbarMenu.vm" permissions="rolesAdapter">
<div class="collapse navbar-collapse" id="navbar">
<ul class="nav navbar-nav">
    <c:if test="${empty pageContext.request.remoteUser}">
        <li class="active">
            <a href="<c:url value="/login"/>"><fmt:message key="login.title"/></a>
        </li>
    </c:if>
    <menu:displayMenu name="Home"/>
    <menu:displayMenu name="UserMenu"/>
    <menu:displayMenu name="AdminMenu"/>
    <menu:displayMenu name="Logout"/>
    
    
    
    
    
    
    <!--Visit-START-->
    <menu:displayMenu name="VisitMenu"/>
    <!--Visit-END-->
    <!--Attendance-START-->
    <menu:displayMenu name="AttendanceMenu"/>
    <!--Attendance-END-->
    
    
    <!--Group-START-->
    <menu:displayMenu name="GroupMenu"/>
    <!--Group-END-->
    <!--Category-START-->
    <menu:displayMenu name="CategoryMenu"/>
    <!--Category-END-->
    <!--Batch-START-->
    <menu:displayMenu name="BatchMenu"/>
    <!--Batch-END-->
    <!--Contribution-START-->
    <menu:displayMenu name="ContributionMenu"/>
    <!--Contribution-END-->
    <!--Campaign-START-->
    <menu:displayMenu name="CampaignMenu"/>
    <!--Campaign-END-->
    <!--Pledge-START-->
    <menu:displayMenu name="PledgeMenu"/>
    <!--Pledge-END-->
    <!--Tenant-START-->
    <menu:displayMenu name="TenantMenu"/>
    <!--Tenant-END-->
    <!--Talent-START-->
    <menu:displayMenu name="TalentMenu"/>
    <!--Talent-END-->
    
    <!--CustomMeta-START-->
    <menu:displayMenu name="CustomMetaMenu"/>
    <!--CustomMeta-END-->
    <!--GroupRole-START-->
    <menu:displayMenu name="GroupRoleMenu"/>
    <!--GroupRole-END-->
    
    
    <!--Resource-START-->
    <menu:displayMenu name="ResourceMenu"/>
    <!--Resource-END-->
    
    <!--Room-START-->
    <menu:displayMenu name="RoomMenu"/>
    <!--Room-END-->
    <!--EventType-START-->
    <menu:displayMenu name="EventTypeMenu"/>
    <!--EventType-END-->
    <!--Event-START-->
    <menu:displayMenu name="EventMenu"/>
    <!--Event-END-->
    
    
    <!--Message-START-->
    <menu:displayMenu name="MessageMenu"/>
    <!--Message-END-->
</ul>
</div>
</menu:useMenuDisplayer>
