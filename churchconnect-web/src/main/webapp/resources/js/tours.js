/**
 * 
 */

// Define the tour!
var tour = {
	id : "hello-hopscotch",
	steps : [ {
		title : "My Header",
		content : "This is the header of my page.",
		target : "header",
		placement : "bottom"
	}, {
		title : "My content",
		content : "Here is where I put my content.",
		target : document.querySelector("#content p"),
		placement : "bottom"
	} ]
};


var persons = {
	id : "persons-tour",
	steps : [ 
		
		{
			title : "Introduction",
			content : "Use the People list to locate and manage individuals and households. You can locate individuals by browsing the entire list of people; you can also search by name.",
			target : document.querySelector("#msgs"),
			placement : "bottom",
			showPrevButton: false,
	        showCTAButton: true,
	        ctaLabel: "End Tour",
	        onCTA: function () {hopscotch.endTour(false);}
	 	} ,
		
		{
		title : "Return Home",
		content : "You can quickly return back to home dashboard after search for a person.",
		target : document.querySelector("#cmdCancel"),
		placement : "bottom",
		showPrevButton: true,
		showCTAButton: true,
	    ctaLabel: "End Tour",
	    onCTA: function () {hopscotch.endTour(false);}
	}, {
		title : "Add Person",
		content : "You can easily add a person to your organization.",
		target : document.querySelector("#add"),
		placement : "bottom",
		showPrevButton: true,
        showCTAButton: true,
        ctaLabel: "End Tour",
        onCTA: function () {hopscotch.endTour(false);}
 	} ,
 	 {
		title : "Search for a Person",
		content : "You can dynamically search for a person by typing their first name or last name. To view a person information, edit or delete it, click on one of the individuals from the list.",
		target : document.querySelector("#globalFilter"),
		placement : "bottom",
		showPrevButton: true,
        showCTAButton: true,
        ctaLabel: "End Tour",
        onCTA: function () {hopscotch.endTour(false);}
 	} 
	
	]
};
