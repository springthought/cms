PrimeFaces.locales['en_US'].messages['javax.faces.component.UIInput.REQUIRED'] = '{0}: is required.';

window.onerror = function(msg, url, line, col, error) {
	// Note that col & error are new to the HTML 5 spec and may not be
	// supported in every browser. It worked for me in Chrome.
	var extra = !col ? '' : '\ncolumn: ' + col;
	extra += !error ? '' : '\nerror: ' + error;

	// You can view the information in an alert to see things working like this:
	alert("Error: " + msg + "\nurl: " + url + "\nline: " + line + extra);

	// TODO: Report this error via ajax so you can keep track
	// of what pages have JS issues

	var suppressErrorAlert = true;
	// If you return true, then error alerts (like in older versions of
	// Internet Explorer) will be suppressed.
	return suppressErrorAlert;
};

function addRowOnComplete(searchStyle) {

	searchStyle = searchStyle + " .ui-datatable-data tr";

	jQuery(searchStyle).first().find('span.ui-icon-pencil').each(function() {
		jQuery(this).click();
	});

}

function addNoteOnComplete(searchStyle) {

	searchStyle = searchStyle + " .ui-datatable-data tr";
	jQuery(searchStyle).first().find('.note-button-edit').each(function() {
		jQuery(this).click();
	});

}
function isRowSelected() {

	$(".st-delete").removeClass("ui-state-disabled");
	$(".st-delete").attr("disabled", false);
	$("#ipthnIsChecked").val("Yes");

}

function checkAddressField() {
	if ($(".addressSelector").val().trim().length != 0) {
		alert("Address has all values");
	} else {
		alert("Address does not have all values");
	}
}

$(document).ready(function() {
	// Set the unload message whenever any input element get changed.
	//$(':input').on('change', function() {
    $('input:not(:button,:submit),textarea,select').change(function () {
		setConfirmUnload(true);
	});

	// Turn off the unload message whenever a form get submitted properly.
	$('form').on('submit', function() {
		 setConfirmUnload(false);
	});
});

function setConfirmUnload(on) {
    var message = "You have unsaved data. Are you sure to leave the page?";
	window.onbeforeunload = (on) ? function() {
  		return message;
	} : null;
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}

function showHelp(cURL) {

	var win = window.open(cURL, '_blank');
	if (win) {
		// Browser has allowed it to be opened
		win.focus();
	} else {
		// Browser has blocked it
		alert('Please allow popups for website:' + window.location.hostname);
	}
}

// clear all components values to empty for a given form
function clearForm(cform) {
	$(cform).find(':input').not(
			':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
	$(cform).find(':checkbox, :radio').prop('checked', false);
}

// Event and Calendar function
function updateEndTime() {

	PF('endTimeWv').selectValue(PF('startTimeWv').getSelectedValue());
}

function updateOnDayNth() {

	// method not used anymore
	// Perform this is action in the managed bean
	var iDate = 0;
	iDate = PF('startDateWv').getDate();
	PF('endDateWv').setDate(iDate);
	$("#onDayNth_holder").val(iDate);
	$("#onDayNth_holder").change();
}

function test() {

	var sMessage = "IDEA Rock!!!";
	PrimeFaces.info("In common.js test()");
	alert(sMessage);


}

// END

var winRef;

function openWindow(url, target, message) {

	if (!winRef || winRef.closed || typeof (winRef) == 'undefined') {

		winRef = window.open(url, target);

		if (!winRef || winRef.closed || typeof (winRef) == 'undefined') {
			alert(message);
		}

	} else {
		try {
			winRef.document; // if this throws an exception then we have no
			// access to the child window - probably domain
			// change so we open a new window
		} catch (e) {
			winRef = window.open(url, target);
		}

		// IE doesn't allow focus, so I close it and open a new one
		if (navigator.appName == 'Microsoft Internet Explorer') {
			winRef.close();
			winRef = window.open(url, target);
		} else {
			// give it focus for a better user experience
			// 8/23/2018 - @TODO this is not work, need to find soultion that works in all browsers
			winRef.blur();
			setTimeout(winRef.focus(),0);
			
		}
	}
}
