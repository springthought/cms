package com.springthought.webapp.action;

import com.springthought.model.Person;
import com.springthought.service.GenericManager;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

@Component("imageStreamer")
@Scope("session")
public class ImageStreamer extends BasePage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private GenericManager<Person, Long> personManager;
	private static final String IMAGE_FILE_NAME = "headoutline_2.png";
	//private static final String IMAGE_FILE_NAME = "headoutline_1.png";

	@Autowired
	public void setPersonManager(
			@Qualifier("personManager") GenericManager<Person, Long> personManager) {
		this.personManager = personManager;
	}


	public DefaultStreamedContent getPersonImage() {


		if (getFacesContext().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		}

		DefaultStreamedContent dbImage = new DefaultStreamedContent();

		String parm = getFacesContext().getExternalContext()
				.getRequestParameterMap().get("personId");


		if ( StringUtils.isNotBlank( parm ) ) {

			Long personID = Long.valueOf(parm);
			Person person = personManager.get(personID);

			try {
				if (person.getPersonBin() != null
						&& person.getPersonBin().getContents() != null) {
					dbImage = new DefaultStreamedContent(person.getPersonBin()
							.getInputstream(), person.getPersonBin()
							.getContentType(), person.getPersonBin()
							.getFileName());
				} else {
					dbImage = generateDefaultImage(IMAGE_FILE_NAME);

				}
			} catch (IOException e) {
				log.error("Unabble to retrive image", e);
				throw new FacesException("Error in writing image.", e);
			}

		}else{

			dbImage = generateDefaultImage( IMAGE_FILE_NAME );

		}

		return dbImage;
	}

	private DefaultStreamedContent generateDefaultImage(String imageFile) {

		DefaultStreamedContent personImage;

		ServletContext servletContext = (ServletContext) FacesContext
				.getCurrentInstance().getExternalContext().getContext();

		String defaultImageFile = servletContext.getRealPath("")
				+ File.separator + "resources" + File.separator + "images"
				+ File.separator + imageFile;

		try {

			personImage = new DefaultStreamedContent(new FileInputStream(
					new File(defaultImageFile)), "image/png", imageFile);

		} catch (Exception e) {
			log.fatal(e);
			throw new FacesException("Error in writing file image.", e);
		}
		return personImage;
	}

}
