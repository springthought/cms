
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Connect;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("connectList")
@Scope("view")
public class ConnectList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 705592816451423676L;
    private String query;
    private GenericManager<Connect, Long> connectManager;
    private Connect selectedConnect = new Connect();
    private boolean checked;

    @Autowired
    public void setConnectManager(@Qualifier("connectManager") GenericManager<Connect, Long> connectManager) {
	this.connectManager = connectManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public void ConnectList() {
	setSortColumn("connectionId"); // sets the default sort column
    }

    public List<Connect> getConnections() {
	try {
	    return connectManager.search(query, Connect.class);
	} catch (SearchException se) {
	    addError(se.getMessage());
	    return sort(connectManager.getAll());
	}
    }

    public String search() {
	return "success";
    }

    public Connect getSelectedConnect() {
	return selectedConnect;
    }

    public void setSelectedConnect(Connect connect) {
	this.selectedConnect = connect;
    }

    public void delete(ActionEvent event) {

	try {
	    connectManager.remove(selectedConnect.getConnectId());
	    addMessage("connect.deleted");
	} catch (DataIntegrityViolationException e) {

	    DataIntegrityViolationExceptionHandler(e);

	} catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }
}
