package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.Person;
import com.springthought.model.Role;
import com.springthought.model.User;
import com.springthought.service.GenericManager;
import com.springthought.service.RoleManager;
import com.springthought.service.SecurityContextManager;
import com.springthought.service.UserExistsException;
import com.springthought.util.ConvertUtil;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * JSF Page class to handle editing a user with a form.
 *
 * @author mraible
 */

@Scope("view")
@Component("inviteUserForm")
public class InviteUserForm extends BasePage implements Serializable {

    private static final long serialVersionUID = -1141119853856863204L;
    private RoleManager roleManager;


    private String id;
    private User user = new User();
    private Map<String, String> availableRoles;
    private String[] userRoles;

    private Person selectedPerson;

    private String u_value;
    private String p_value;

    private String from;


    @Autowired
    SecurityContextManager securityContext;
    private Role role;


    private GenericManager<Person, Long> personManager;

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    List<Person> people = new ArrayList<Person>(0);


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        initUser(user);
    }

    public String getU_value() {
        return u_value;
    }

    public void setU_value(String u_value) {
        this.u_value = u_value;
    }

    public String getP_value() {
        return p_value;
    }

    public void setP_value(String p_value) {
        this.p_value = p_value;
    }

    private void initUser(User user) {

        if (user.getUsername() != null) {
            user.setConfirmPassword("");
            user.setPassword("");
            user.setUsername("");
        }
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    @Autowired
    public void setRoleManager(@Qualifier("roleManager") RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    public String add() {

        user = new User();
        user.setEnabled(true);
        user.addRole(new Role(Constants.USER_ROLE));

        return "editProfile";
    }

    public String cancel() {
        if (log.isDebugEnabled()) {
            log.debug("Entering 'cancel' method");
        }

        if (!"list".equals(getParameter("from"))) {
            return "mainMenu";
        } else {
            return "cancel";
        }
    }

    public void setFrom(String from) {
        this.from = from;
    }


    public String edit() {

        return "editProfile";
    }


    public void save(ActionEvent event) throws IOException {
        this.save();
    }

    public String save() throws IOException {

        // workaround for plain ol' HTML input tags that don't seem to set
        // properties on the managed bean

        // setUserRoles(getRequest().getParameterValues("userForm:userRoles"));

        // String roles[] = this.getUserRoles();

        // ArrayList<String> alRoles = new
        // ArrayList<String>(Arrays.asList(roles));

        // for (String srole : alRoles) {
        // / log.info(srole);
        // }

        boolean isNew = (user.getId() == null || user.getId() == 0);
        String decryptedPassword = user.getPassword();
        user.setVerified(true);
		
		
/*

		for (int i = 0; (userRoles != null) && (i < userRoles.length); i++) {
			String roleName = userRoles[i];
			user.addRole(roleManager.getRole(roleName));
		}
*/

//        user.addRole(roleManager.get(getRole().getId()));

        Integer originalVersion = user.getVersion();


        String userAgent = getRequest().getHeader("User-Agent");
        // For some reason, IE causes version to be 0. Set it to null so test
        // will pass.
        if (userAgent != null && userAgent.contains("MSIE")
                && user.getVersion() == 0) {
            log.debug("Detected IE, setting version and id to null");
            user.setId(null);
            user.setVersion(null);
        }

        try {
            user = userManager.saveUser(user);

        } catch (AccessDeniedException ade) {
            log.fatal(ade.getMessage());
            getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        } catch (ConstraintViolationException | DataIntegrityViolationException e) {
            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
            log.fatal(e);
            return "";
        } catch (javax.validation.ConstraintViolationException e) {
            ConstraintViolationExceptionHandler(e);
            log.fatal(e.getMessage(), e);
            return "";
        } catch (UserExistsException e) {
            addError("errors.existing.user", new Object[]{user.getUsername(),
                    user.getEmail()});
            user.setVersion(originalVersion);
            log.fatal( e );
            return null;
        }
        addMessage("user.updated.byAdmin", user.getFullName());
        return null;
    }


    public String getFrom() {
        return this.from;
    }

    // Form Controls ==========================================================
    @SuppressWarnings("unchecked")
    public Map<String, String> getAvailableRoles() {
        if (availableRoles == null) {
            @SuppressWarnings("rawtypes")
            List roles = (List) getServletContext().getAttribute(
                    Constants.AVAILABLE_ROLES);
            availableRoles = ConvertUtil.convertListToMap(roles);
        }

        return availableRoles;
    }

    public List<Role> getRoles() {
        List<Role> roles = roleManager.getAllDistinct();
        return roles;

    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return this.role;
    }


    public String[] getUserRoles() {

        userRoles = new String[user.getRoles().size()];

        int i = 0;

        if (userRoles.length > 0) {
            for (Role role : user.getRoles()) {
                userRoles[i] = role.getName();
                i++;
            }
        }

        return userRoles;
    }

    public void setUserRoles(String[] userRoles) {
        this.userRoles = userRoles;
    }


    @PostConstruct
    public void initialization() throws IOException {

        id = Optional.ofNullable(getParameter("id")).orElse(null);

        if (id != null) {
            try {
                log.debug("loaded user, id is: " + id);
                user = userManager.getUserByUsername(id.toString());
                initUser(user);
            } catch (UsernameNotFoundException | ObjectRetrievalFailureException e) {
                getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
                throw new AccessDeniedException("403 returned");
            } catch (Exception e) {
                log.fatal(e);
            }
        } else {
            getResponse().sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }


    public Person getSelectedPerson() {
        return selectedPerson;
    }

    public void setSelectedPerson(Person selectedPerson) {
        log.info(selectedPerson);
        this.selectedPerson = selectedPerson;
    }
}
