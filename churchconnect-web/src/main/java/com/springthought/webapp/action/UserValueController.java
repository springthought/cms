package com.springthought.webapp.action;

import com.springthought.Constants.UserValueType;
import com.springthought.model.UserValue;
import com.springthought.service.UserValueManager;
import com.springthought.util.FacesUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component("userValueController")
@Scope("session")
public class UserValueController extends BasePage implements Serializable {

	private static final long serialVersionUID = 7939394506124504540L;
	private UserValueManager userValueManger = null;
	private List<UserValue> selectedUservalues = new ArrayList<UserValue>();
	private UserValue selectedUservalue = null;
	private List<UserValue> userValueList = null;
	private List<UserValue> contactLabelList = null;
	private UserValue selectedContactLabel = null;
	DataTable dataTable;
	private Boolean checked = false;
	private String postDeleteUpdate = "";


	@Autowired
	public void setUserValueManager(
			@Qualifier("userValueManager") UserValueManager manager) {
		this.userValueManger = manager;
	}

	public List<UserValue> getMemberTypes() {

		if (userValueList == null) {
			userValueList = this.userValueManger
					.getUserValueByType( UserValueType.CLASSIFICATION );
		}
		return userValueList;
	}

	/**
	 *
	 * @return
	 */
	public List<UserValue> getContactValues() {

		if (contactLabelList == null) {
			contactLabelList = this.userValueManger
					.getUserValueByType( UserValueType.CONTACT );
		}


		return contactLabelList;
	}

	public void onTabChange( TabChangeEvent event ){

		log.info("Tab = " + event.getTab().getTitle());

	}
	/**
	 * @return the selectedUservalues
	 */
	public List<UserValue> getSelectedUservalues() {
		return selectedUservalues;
	}

	/**
	 * @param selectedUservalues
	 *            the selectedUservalues to set
	 */
	public void setSelectedUservalues(List<UserValue> selectedUservalues) {
		this.selectedUservalues = selectedUservalues;
	}

	public void onEdit(RowEditEvent event) {

		UserValue uservalue = (UserValue) event.getObject();

		boolean isNew = (uservalue.getUservalueId() == null || uservalue
				.getUservalueId() == 0);

		userValueManger.save(uservalue);

		String key = (isNew) ? "userValueDetial.added" : "userValueDetial.updated";

		addMessage(key);

		selectedUservalue = null;
		checked = false;

	}

	public void onCancel(RowEditEvent event) {
		addMessage("errors.cancel");
		selectedUservalue = null;
		checked = false;
	}

	public void checkBoxSelect(SelectEvent event) {

		log.info("Number of items selected :=" + selectedUservalues.size());

	}

	public void checkBoxUnselect(UnselectEvent event) {

		log.info("Number of items selected :=" + selectedUservalues.size());

	}

	public void delete() {
/**
 		List<UserValue> values = selectedUservalues;

		for (UserValue uv : values) {
			userValueManger.remove(uv.getUservalueId());
		}

		selectedUservalues.removeAll(values);
		selectedUservalue = null;
		userValueList = null;
*
 */

		userValueManger.remove(selectedUservalue.getUservalueId());
		selectedUservalue = null;
		userValueList = null;
		checked = false;
		addMessage("userValueDetial.deleted");

	}

	public void addUserValueClassfication() {

		UserValue uv = new UserValue();

		uv.setValue_Type(UserValueType.CLASSIFICATION);
		uv.setName("[New classification]");
		uv.setDescr("[New description]");

		// should save row to the database and retrieve primary id from saved
		// recored.
		uv.setUservalueId(0L);
		userValueList.add(0, uv);
		selectedUservalues.add(uv);

		// dataTable.setRowIndex(dataTable.getRowCount());

		FacesUtils.addInfoMessage("Entry added");

	}

	/**
	 * @return the dataTable
	 */
	public DataTable getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable
	 *            the dataTable to set
	 */
	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the checked
	 */
	public Boolean getChecked() {
		return this.checked;
	}

	/**
	 * @param checked
	 *            the checked to set
	 */
	public void setChecked(Boolean checked) {
		log.info("Checked value = " + checked.toString());
		this.checked = checked;
	}

	public void radioSelected(SelectEvent event) {
		checked = true;
	}

	/**
	 * @return the selectedUservalue
	 */
	public UserValue getSelectedUservalue() {
		return selectedUservalue;
	}

	/**
	 * @param selectedUservalue
	 *            the selectedUservalue to set
	 */
	public void setSelectedUservalue(UserValue selectedUservalue) {
		this.selectedUservalue = selectedUservalue;
	}

	/**
	 * @return the postDeleteUpdate
	 */
	public String getPostDeleteUpdate() {
		return postDeleteUpdate;
	}

	/**
	 * @param postDeleteUpdate the postDeleteUpdate to set
	 */
	public void setPostDeleteUpdate(String postDeleteUpdate) {
		this.postDeleteUpdate = postDeleteUpdate;
	}

	/**
	 * @return the selectedContactLabel
	 */
	public UserValue getSelectedContactLabel() {
		return selectedContactLabel;
	}

	/**
	 * @param selectedContactLabel the selectedContactLabel to set
	 */
	public void setSelectedContactLabel(UserValue selectedContactLabel) {
		this.selectedContactLabel = selectedContactLabel;
	}

	/**
	 * @return the contactLabelList
	 */
	public List<UserValue> getContactLabelList() {
		return contactLabelList;
	}

	/**
	 * @param contactLabelList the contactLabelList to set
	 */
	public void setContactLabelList(List<UserValue> contactLabelList) {
		this.contactLabelList = contactLabelList;
	}


}
