
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.EventType;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("eventTypeList")
@Scope("view")
public class EventTypeList extends BasePage implements Serializable {
    private String query;
    private GenericManager<EventType, Long> eventTypeManager;
    private EventType selectedEventType = new EventType();
    private boolean checked;

    @Autowired
    public void setEventTypeManager(@Qualifier("eventTypeManager") GenericManager<EventType, Long> eventTypeManager) {
	this.eventTypeManager = eventTypeManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public EventTypeList() {
	setSortColumn("eventTypeId"); // sets the default sort column
    }

    public List<EventType> getEventTypes() {
	try {
	    return eventTypeManager.search(query, EventType.class);
	} catch (SearchException se) {
	    addError(se.getMessage());
	    return sort(eventTypeManager.getAll());
	}
    }

    public String search() {
	return "success";
    }

    public EventType getSelectedEventType() {
	return selectedEventType;
    }

    public void setSelectedEventType(EventType eventType) {
	this.selectedEventType = eventType;
    }

    public void delete(ActionEvent event) {

	EventType eventType = eventTypeManager.get(selectedEventType.getEventTypeId());

	try {
	    eventTypeManager.remove(eventType.getEventTypeId());
	    addMessage("eventType.deleted");

	} catch (DataIntegrityViolationException e) {

	    DataIntegrityViolationExceptionHandler(e);

	} catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }
}
