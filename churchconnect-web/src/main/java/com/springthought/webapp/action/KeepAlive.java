package com.springthought.webapp.action;


import com.springthought.util.FacesUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;



@Scope("session")
@Component("keepAlive")
public class KeepAlive extends BasePage implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public void ping() {

	HttpServletRequest request = (HttpServletRequest) FacesUtils.getExternalContext().getRequest();
		request.getSession();
		log.info("Ping");
	}

}
