package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.CategoryType;
import com.springthought.model.Category;
import com.springthought.service.GenericManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.UUID;

@Component("categoryForm")
@Scope("view")
public class CategoryForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private GenericManager<Category, Long> categoryManager;
    private Category category = new Category();
    private Long categoryId;
    private String from;
    private String template = "/templates/mainMenu.xhtml";
    private CategoryType type;




    /**
     * @return the template
     */

    public String getTemplate() {
        return this.template;

    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * @return the categoryId
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
        this.edit();
    }

    @Autowired
    public void setCategoryManager(@Qualifier("categoryManager") GenericManager<Category, Long> categoryManager) {
        this.categoryManager = categoryManager;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }

    public String delete() {
        categoryManager.remove(category.getCategoryId());
        addMessage("category.deleted");

        return "categories";
    }

    public String edit() {

        this.categoryId = getParameter("categoryId") == null ? 0L : new Long(getParameter("categoryId"));

        if (this.categoryId != null && this.categoryId != 0) {
            category = categoryManager.get(this.categoryId);
        } else {
            category = new Category();
        }

        return "categoryForm";
    }

    public String save() {
        boolean isNew = (category.getCategoryId() == null || category.getCategoryId() == 0);

        try {

            category = categoryManager.save(category);

            String key = (isNew) ? "category.added" : "category.updated";
            addMessage(key);

            if (isNew) {
                return "categories";
            } else {
                return "categories";
            }

        } catch (ConstraintViolationException e) {

            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

        return "";
    }

    public Constants.CategoryType[] getCategoryTypeValues() {
        return Constants.CategoryType.values();
    }

    /*
     * public SelectItem[] getCategoryValues() {
     *
     * SelectItem[] items = new SelectItem[CategoryType.values().length]; int i
     * = 0; for (CategoryType c : CategoryType.values()) { items[i++] = new
     * SelectItem(c, c.getValue()); } return items; }
     */
    public void save(ActionEvent event) {
        this.save();
    }

    public void close(ActionEvent event) {
        PrimeFaces.current().dialog().closeDynamic(new Category());
        //RequestContext.getCurrentInstance().closeDialog(new Category());
    }

    public void saveAndClose(ActionEvent event) {

        if (!this.save().isEmpty()) {
            PrimeFaces.current().dialog().closeDynamic(category);
            //RequestContext.getCurrentInstance().closeDialog(category);
        }
    }


    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    public String go() {

        String action = "categories";

        switch (type) {
            case FUND:
                action = "fundCategories";
                break;

            default:
                action = "categories";
                break;
        }

        return action;
    }

    @PostConstruct
    private void initialization() {

        //Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String stype;

        from = getParameter("from") == null ? "" : getParameter("from");

        stype = getParameter("type") == null ? "" : getParameter("type");

        type = StringUtils.isBlank(stype) ? CategoryType.valueOf("MINISTRY") : CategoryType.valueOf(stype);


        this.categoryId = getParameter("categoryId") == null ? 0L : new Long(getParameter("categoryId"));

        if (this.categoryId != null && this.categoryId != 0) {
            category = categoryManager.get(this.categoryId);
        } else {
            category = new Category();
        }

        if (from.equals("maintform")) {
            this.template = "/templates/mainNoMenu.xhtml";
        }

    }
}