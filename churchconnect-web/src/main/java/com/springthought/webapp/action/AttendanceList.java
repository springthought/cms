
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Attendance;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("attendanceList")
@Scope("view")
public class AttendanceList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String query;
    private GenericManager<Attendance, Long> attendanceManager;
    private Attendance selectedAttendance = new Attendance();
    private boolean checked;

    @Autowired
    public void setAttendanceManager(
	    @Qualifier("attendanceManager") GenericManager<Attendance, Long> attendanceManager) {
	this.attendanceManager = attendanceManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public AttendanceList() {
	setSortColumn("attendanceId"); // sets the default sort column
    }

    public List<Attendance> getAttendances() {
	try {
	    return attendanceManager.search(query, Attendance.class);
	} catch (SearchException se) {
	    addError(se.getMessage());
	    return sort(attendanceManager.getAll());
	}
    }

    public String search() {
	return "success";
    }

    public Attendance getSelectedAttendance() {
	return selectedAttendance;
    }

    public void setSelectedAttendance(Attendance attendance) {
	this.selectedAttendance = attendance;
    }

    public void delete(ActionEvent event) {

	Attendance attendance = attendanceManager.get(selectedAttendance.getAttendanceId());
	attendance.removeAttendancesFromPeople();


	try {
	    attendanceManager.remove(attendance);
	    addMessage("attendance.deleted");

	} catch (DataIntegrityViolationException e) {
	    DataIntegrityViolationExceptionHandler(e);

	} catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }
}
