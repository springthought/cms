package com.springthought.webapp.action;

import com.springthought.model.Batch;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("batchList")
@Scope("view")
public class BatchList extends BasePage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7559890559465395672L;
    private String query;
    private GenericManager<Batch, Long> batchManager;
    private Batch selectedBatch = new Batch();
    private boolean checked;
    List<Batch> batches = null;

    @Autowired
    public void setBatchManager(@Qualifier("batchManager") GenericManager<Batch, Long> batchManager) {
        this.batchManager = batchManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public BatchList() {
        setSortColumn("batchId"); // sets the default sort column
    }

    @SuppressWarnings("unchecked")
    public List<Batch> getBatches() {
        return sort(batchManager.getAllDistinct());
    }

    public String search() {
        return "success";
    }

    public Batch getSelectedBatch() {
        return selectedBatch;
    }

    public void setSelectedBatch(Batch batch) {
        this.selectedBatch = batch;
    }

    public void delete(ActionEvent event) {

        Batch batch = batchManager.get(selectedBatch.getBatchId());
        batch.removeAllContributions();


        try {
            batchManager.remove(batch.getBatchId());
            addMessage("batch.deleted");
        } catch (DataIntegrityViolationException e) {

            DataIntegrityViolationExceptionHandler(e);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }
    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @SuppressWarnings("unchecked")
    @PostConstruct
    private void initilization() {

    }
}