
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Fund;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;



@Component("fundList")
@Scope("request")
public class FundList extends BasePage implements Serializable {
    private String query;
    private GenericManager<Fund, Long> fundManager;
	private Fund selectedFund = new Fund();
    private boolean checked;

    @Autowired
    public void setFundManager(@Qualifier("fundManager") GenericManager<Fund, Long> fundManager) {
        this.fundManager = fundManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public FundList() {
        setSortColumn("fundId"); // sets the default sort column
    }

    public List<Fund> getFunds() {
        try {
            return fundManager.search(query, Fund.class);
        } catch (SearchException se) {
            addError(se.getMessage());
            return sort(fundManager.getAll());
        }
    }

    public String search() {
        return "success";
    }

    public Fund getSelectedFund() {
        return selectedFund;
    }

    public void setSelectedFund(Fund fund) {
        this.selectedFund = fund;
    }

	public void delete( ActionEvent event ){
    	fundManager.remove( selectedFund.getFundId());
	  	addMessage("fund.deleted");
	  	checked = false;
    }


    public void radioSelected(SelectEvent event) {checked = true;}

	public boolean isChecked() {return checked;}

	public void setChecked(boolean checked) {this.checked = checked;}
}
