package com.springthought.webapp.action;

import com.springthought.model.Location;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.UUID;

@Component("locationForm")
@Scope("request")
public class LocationForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1115404518892250160L;
    private GenericManager<Location, Long> locationManager;
    private Location location = new Location();
    private Long locationId;

    @Autowired
    public void setlocationManager(@Qualifier("locationManager") GenericManager<Location, Long> locationManager) {
	this.locationManager = locationManager;
    }

    public Location getLocation() {
	return location;
    }

    public void setLocation(Location location) {

	this.location = location;
    }

    public void setlocationId(Long locationId) {
	this.locationId = locationId;
    }

    public String delete() {
	locationManager.remove(location.getLocationId());
	addMessage("location.deleted");

	return "locations";
    }

    public String edit() {

	return "locationForm";
    }

    public String save() {

	boolean isNew = (location.getLocationId() == null || location.getLocationId() == 0);

	try {

	    locationManager.save(location);
	    String key = (isNew) ? "location.added" : "location.updated";
	    addMessage(key);

	    if (isNew) {
		return "locations";
	    } else {
		return "locations";
	    }
	} catch (ConstraintViolationException e) {
	    DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
	}

	catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new Object[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

	return "";
    }

    public void save(ActionEvent event) {
	this.save();
    }

    @PostConstruct
    private void initilization() {

	locationId = getParameter("locationId") == null ? 0L : new Long(getParameter("locationId"));
	if (locationId != null && locationId != 0) {
	    location = locationManager.get(locationId);
	} else {
	    location = new Location();
	}

    }

}
