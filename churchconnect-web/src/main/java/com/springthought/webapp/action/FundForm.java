package com.springthought.webapp.action;

import com.springthought.model.Fund;
import com.springthought.service.GenericManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;
import java.io.Serializable;



@Component("fundForm")
//@Scope("request")
@RequestScoped
public class FundForm extends BasePage implements Serializable {
    private GenericManager<Fund, Long> fundManager;
    private Fund fund = new Fund();
    private Long fundId;


    @Autowired
    public void setFundManager(@Qualifier("fundManager") GenericManager<Fund, Long> fundManager) {
        this.fundManager = fundManager;
    }

    public Fund getFund() {
        return fund;
    }

    public void setFund(Fund fund) {
        this.fund = fund;
    }

    public void setFundId(Long fundId) {
        this.fundId = fundId;
    }


    public String delete() {

        fundManager.remove(fund.getFundId());
        addMessage("fund.deleted");

        return "funds";
    }

    public String edit() {
        // Workaround for not being able to set the id using #{param.id} when using Spring-configured managed-beans
        if (fundId == null) {
            fundId = new Long(getParameter("fundId"));
        }
        // Comparison to zero (vs. null) is required with MyFaces 1.2.2, not with previous versions
        if (fundId != null && fundId != 0) {
            fund = fundManager.get(fundId);
        } else {
            fund = new Fund();
        }

        return "fundForm";
    }

    public String save() {
        boolean isNew = (fund.getFundId() == null || fund.getFundId() == 0);
        fundManager.save(fund);

        String key = (isNew) ? "fund.added" : "fund.updated";
        addMessage(key);

	    if (isNew) {
            return "funds";
        } else {
            return "funds";
        }
    }


   public void save( ActionEvent event ){	this.save(); }

}
