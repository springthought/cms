/**
 *
 */
package com.springthought.webapp.action;

import com.springthought.Constants.ExportOption;
import com.springthought.model.ReportParamHolder;
import com.springthought.model.SystemParamDef;
import com.springthought.model.SystemReportDef;
import com.springthought.service.GenericManager;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import java.util.*;

/**
 * @author eairrick
 *
 */
@Component("reportForm")
@Scope("view")
public class ReportForm extends AbstractReportBean {

	/**
	 *
	 */
	private static final long serialVersionUID = 8611497149211131829L;
	private String reportFormat = "";
	private String mode;
	private final String COMPILE_FILE_NAME = "Persons";
	private List<ReportParamHolder> paramHolders = new ArrayList<ReportParamHolder>(
			0);
	private GenericManager<SystemReportDef, Long> systemReportDefManager;
	private SystemReportDef currentReportDef = null;

	public String getReportFormat() {
		return reportFormat;
	}

	public void setReportFormat(String reportFormat) {
		this.reportFormat = reportFormat;
	}

	public GenericManager<SystemReportDef, Long> getSystemReportDefManager() {
		return systemReportDefManager;
	}

	@Autowired
	public void setSystemReportDefManager(
			@Qualifier("systemReportDefManager") GenericManager<SystemReportDef, Long> systemReportDefManager) {

		this.systemReportDefManager = systemReportDefManager;
	}

	@Override
	protected String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	public String execute() {

		try {

			Map<String, Object> reportParameters = new HashMap<String, Object>();

			for (ReportParamHolder holder : paramHolders) {

				switch (holder.getSystemParamDef().getDataType()) {

				case DATE:

					reportParameters.put(holder.getSystemParamDef()
							.getMetaData(), holder.getDateValue());
					break;

				case CHECKBOXMENU:

					reportParameters.put(
							holder.getSystemParamDef().getMetaData(),
							new ArrayList<String>(Arrays.asList(holder
									.getSelectedItems())));
					break;
				case INPUTTEXT:
					reportParameters.put(holder.getSystemParamDef()
							.getMetaData(), holder.getTextValue());
					break;

				case SPINNER:
					reportParameters.put(holder.getSystemParamDef()
							.getMetaData(), holder.getNumberValue());
					break;

				case CHECKBOX:
					reportParameters.put(holder.getSystemParamDef()
							.getMetaData(), holder.getBooleanValue());
					break;

				case SELECTMENU:
					reportParameters.put(holder.getSystemParamDef()
							.getMetaData(), holder.getTextValue());
					break;

				default:
					break;
				}

			}

			reportParameters.put("rTenantId", currentUserTenantContext());

			reportParameters.put("REPORT_LOCALE", new Locale("es"));

			reportParameters.put("REPORT_TIME_ZONE",
					java.util.TimeZone.getTimeZone(getTimezone()));

			reportParameters.put("REPORT_RESOURCE_BUNDLE",
					java.util.ResourceBundle.getBundle(
							"ApplicationResources_es", new Locale("es")));

			setReportParameters(reportParameters);

			super.prepareReport(currentReportDef.getReportFileName());

		} catch (Exception e) {

			UUID guid = UUID.randomUUID();
			addError("errors.generalApplicationError",
					new Object[] { e.getMessage(), guid.toString() });
			log.error(guid.toString(), e);
		}

		return null;
	}

	public ExportOption[] getReportFormats() {
		return ExportOption.values();
	}

	public void reportFormatTypeChange(AjaxBehaviorEvent event) {

	}

	public List<ReportParamHolder> getParamHolders() {
		return paramHolders;
	}

	public void setParamHolders(List<ReportParamHolder> paramHolders) {
		this.paramHolders = paramHolders;
	}

	public SystemReportDef getCurrentReportDef() {
		return currentReportDef;
	}

	public void setCurrentReportDef(SystemReportDef currentReportDef) {
		this.currentReportDef = currentReportDef;
	}

	public String getReportname() {
		return getText(currentReportDef.getName());

	}

	public String getReportDescr() {
		return getText(currentReportDef.getDescr());
	}

	private void buildParamHolders(Long reportID) {

		paramHolders = new ArrayList<ReportParamHolder>(0);

		try {
			currentReportDef = systemReportDefManager.get(reportID);

			for (SystemParamDef param : currentReportDef.getSystemParamDefs()) {
				paramHolders.add(new ReportParamHolder(param));
			}

		} catch (ObjectRetrievalFailureException e) {

			currentReportDef = null;
			log.error(e);

		}

	}

	public void destinationReportPage(ActionEvent event) {

		mode = (String) event.getComponent().getAttributes().get("mode");

		if (NumberUtils.isNumber(mode)) {
			buildParamHolders(Long.parseLong(mode));
		}

	}

	@PostConstruct
	private void initilization() {
		// buildParamHolders(-1L);

	}

}
