package com.springthought.webapp.action;


import com.springthought.model.User;
import com.springthought.webapp.util.RequestUtil;
import org.springframework.mail.MailException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;

/**
 * Managed Bean to send password hints to registered users.
 *
 * <p>
 * <a href="PasswordHint.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Component("passwordHint")
public class PasswordHint extends BasePage {

    private String username;
    /**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}


	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	public void execute(ActionEvent event ) {


        // ensure that the username has been sent
        if (username == null || "".equals(username)) {
            log.warn("Username not specified, notifying user that it's a required field.");

            addError("errors.required", getText("user.username"));

        } else if (username.endsWith(".jsf")) {
            username = username.substring(0, username.indexOf(".jsf"));
        }

        if (log.isDebugEnabled()) {
            log.debug("Processing Password Hint...");
        }

        // look up the user's information
        try {
            User user = userManager.getUserByUsername(username);

            StringBuffer msg = new StringBuffer();
            msg.append("Your password hint is: ").append(user.getPasswordHint());
            msg.append("\n\nLogin at: ").append(RequestUtil.getAppURL(getRequest()));

            message.setTo(user.getEmail());
            String subject = '[' + getText("webapp.name") + "] " + getText("user.passwordHint");
            message.setSubject(subject);
            message.setText(msg.toString());
            mailEngine.send(message);
            addMessage("login.passwordHint.sent",
                       new Object[] { username, user.getEmail() });

        } catch (UsernameNotFoundException e) {
            log.warn(e.getMessage());
            // If exception is expected do not rethrow
            addError("login.passwordHint.error", username);
        } catch (MailException me) {
            addError(me.getCause().getLocalizedMessage());
        }


    }
}

