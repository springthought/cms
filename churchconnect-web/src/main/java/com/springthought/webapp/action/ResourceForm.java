package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.ResourceType;
import com.springthought.model.Resource;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.UUID;

@Component("resourceForm")
@Scope("view")
public class ResourceForm extends BasePage implements Serializable {
    /**
    *
    */
    private static final long serialVersionUID = 2062664675910542568L;
    private GenericManager<Resource, Long> resourceManager;
    private Resource resource = new Resource();
    private Long resourceId;

    @Autowired
    public void setResourceManager(@Qualifier("resourceManager") GenericManager<Resource, Long> resourceManager) {
	this.resourceManager = resourceManager;
    }

    public Resource getResource() {
	return resource;
    }

    public void setResource(Resource resource) {
	this.resource = resource;
    }

    public void setResourceId(Long resourceId) {
	this.resourceId = resourceId;
    }

    public String delete() {
	resourceManager.remove(resource.getResourceId());
	addMessage("resource.deleted");

	return "resources";
    }

    public String edit() {

	return "resourceForm";
    }

    public String save() {

	boolean isNew = (resource.getResourceId() == null || resource.getResourceId() == 0);

	try {

	    resourceManager.save(resource);
	    String key = (isNew) ? "resource.added" : "resource.updated";
	    addMessage(key);

	    if (isNew) {
		return "resources";
	    } else {
		return "resources";
	    }
	} catch (ConstraintViolationException e) {

	    DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
	}

	catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new Object[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

	return "";
    }

    public void save(ActionEvent event) {
	this.save();
    }

    public ResourceType[] getResouceValues() {
	return Constants.ResourceType.values();
    }

    @PostConstruct
    private void initilization() {

	resourceId = getParameter("resourceId") == null ? 0L : new Long(getParameter("resourceId"));

	if (resourceId != null && resourceId != 0) {
	    resource = resourceManager.get(resourceId);
	} else {
	    resource = new Resource();
	}

    }

}
