package com.springthought.webapp.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component("myInformationForm")
@Scope("request")
public class MyInformationForm extends BasePage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5552927078273873334L;

    public void myInformation() {
	getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), "null",
		goToPersnonInfo());
    }

    private String goToPersnonInfo() {
	return "/views/personForm.xhtml?faces-redirect=true&includeViewParams=true";
    }

}
