package com.springthought.webapp.action;

import com.springthought.model.Household;
import com.springthought.model.Person;
import com.springthought.service.GenericManager;
import com.springthought.service.SecurityContextManager;
import com.springthought.service.impl.SecurityContextManagerImpl;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component("householdList")
@Scope("view")
public class HouseholdList extends BasePage implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7027448820189097076L;
	private String query;
	private GenericManager<Household, Long> householdManager;
	private Household selectedHousehold = new Household();
	private boolean checked;
	private ArrayList<Person> persons = new ArrayList<Person>();
	private SecurityContextManager contextManager = new SecurityContextManagerImpl();

	@Autowired
	public void setHouseholdManager(
			@Qualifier("householdManager") GenericManager<Household, Long> householdManager) {
		this.householdManager = householdManager;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	public HouseholdList() {
		setSortColumn("householdId"); // sets the default sort column
	}

	@SuppressWarnings("unchecked")
	public List<Household> getHouseholds() {

		return sort(householdManager.getAllDistinct());

	}

	/**
	 * @return the list of persons contained within a household Use to populate
	 *         the household row expansion.
	 */
	public ArrayList<Person> getPersons() {
		return persons;
	}

	/**
	 * @param persons
	 *            the persons to set
	 */
	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
	}

	public String search() {
		return "success";
	}

	public Household getSelectedHousehold() {
		return selectedHousehold;
	}

	public void setSelectedHousehold(Household household) {
		this.selectedHousehold = household;
	}

	public void delete(ActionEvent event) {
		householdManager.remove(selectedHousehold.getHouseholdId());
		addMessage("household.deleted");
		checked = false;
	}

	public void radioSelected(SelectEvent event) {
		checked = true;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void onRowToggle(ToggleEvent event) {

		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Row State " + event.getVisibility(), "Model:"
						+ ((Household) event.getData()).getName());

		FacesContext.getCurrentInstance().addMessage(null, msg);

		Set<Person> members = ((Household) event.getData()).getPersons();

		this.persons.clear();

		for (Person member : members) {
			this.persons.add(member);
			log.info(member.toString());
		}

	}

	public void test(ActionEvent event ){

		try {
			Long t = contextManager.retriveTenantContext();

		} catch (Exception e) {
			log.equals(e);

		}

	}
}
