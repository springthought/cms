package com.springthought.webapp.action;

import com.springthought.model.Batch;
import com.springthought.model.Category;
import com.springthought.model.Contribution;
import com.springthought.model.Person;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import com.springthought.util.ConvertUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.donut.DonutChartDataSet;
import org.primefaces.model.charts.donut.DonutChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Component("contributionForm")
@Scope("view")
public class ContributionForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private GenericManager<Contribution, Long> contributionManager;
    private GenericManager<Batch, Long> batchManager;
    private GenericManager<Person, Long> personManager;

    private Batch batch = new Batch();
    private Long batchId;
    private Contribution selectedContribution = new Contribution();
    private Contribution entryContribution = new Contribution();
    private ArrayList<Person> persons;
    private Person person;
    private Boolean rowselected = false;
    private Long contributionId;
    private Boolean showBatchDetails = false;

    private BigDecimal actualBatchTotal;

    private DonutChartModel donutModel;
    private ArrayList<FundModel> model;

    @Autowired
    public void setContributionManager(
            @Qualifier("contributionManager") GenericManager<Contribution, Long> contributionManager) {
        this.contributionManager = contributionManager;
    }

    public GenericManager<Contribution, Long> getContributionManager() {
        return contributionManager;
    }

    @Autowired
    public void setBatchManager(@Qualifier("batchManager") GenericManager<Batch, Long> batchManager) {
        this.batchManager = batchManager;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    public DataService getDataService() {
        return dataService;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public ContributionForm() {


    }

    public Contribution getSelectedContribution() {
        return selectedContribution;
    }

    public void setSelectedContribution(Contribution selectedContribution) {
        this.selectedContribution = selectedContribution;
    }

    public Contribution getEntryContribution() {
        return entryContribution;
    }

    public void setEntryContribution(Contribution entryContribution) {
        this.entryContribution = entryContribution;
    }

    public BigDecimal getActualBatchTotal() {
        return actualBatchTotal;
    }

    public void setActualBatchTotal(BigDecimal actualBatchTotal) {
        this.actualBatchTotal = actualBatchTotal;
    }

    public Boolean getRowselected() {
        return rowselected;
    }

    public void setRowselected(Boolean rowselected) {
        this.rowselected = rowselected;
    }

    public Batch getBatch() {
        calcActualBatchTotal();
        return batch;
    }

    public void setBatch(Batch batch) {
        this.batch = batch;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public ArrayList<Person> getPersons() {
        return persons;
    }

    public void setPersons(ArrayList<Person> persons) {
        this.persons = persons;
    }

    public String delete() {
        addMessage("contribution.deleted");
        return "contributions";
    }

    public String edit() {
        return "contributionForm";
    }

    public String save() {

        boolean isNew = (batch.getBatchId() == null || batch.getBatchId() == 0);

        try {
            batchManager.save(batch);
            // TODO update save/add message
            String key = (isNew) ? "contribution.added" : "contribution.updated";
            addMessage(key);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();

            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});

            log.error(guid.toString(), e);

            return "";
        }

        // TODO update screen navigation
        if (isNew) {
            return "batches";
        } else {
            return "batches";
        }
    }

    public void save(ActionEvent event) {
        this.save();
    }

    public Object handleCloseDialog() {
        this.selectedContribution = new Contribution();
        return null;

    }

    public void deleteContribution(ActionEvent event) {

        batch.getContributions().remove(selectedContribution);
        selectedContribution.setAmount(selectedContribution.getAmount().multiply(new BigDecimal(-1)));
        sumContributions(selectedContribution);
        refreshModel();
        addMessage("contribution.deleted");

    }

    public void updateContribution(ActionEvent event) {

        int idx = batch.getContributions().indexOf(selectedContribution);
        batch.getContributions().set(idx, selectedContribution);
        addMessage("contribution.updated");


    }

    public char getPersonGroup(Person person) {
        return (person == null ? '\0' : person.getFirstName().charAt(0));
    }

    public List<Person> completePerson(String query) {

        List<Person> allPersons = this.persons;
        List<Person> filteredPersons = new ArrayList<Person>();

        for (Person person : allPersons) {
            if (person.getName().toLowerCase().contains(query.toLowerCase())) {
                filteredPersons.add(person);
            }
        }

        return filteredPersons;
    }

    public void onRowSelect(SelectEvent event) {
        this.rowselected = true;
    }

    public void onRowUnselect(UnselectEvent event) {
        this.rowselected = false;
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
    }


    public void reinitContribution(ActionEvent event) {

        entryContribution.setBatch(batch);
        entryContribution.setVersion(0);
        batch.getContributions().add(entryContribution);

        //recalc chart counts
        sumContributions(entryContribution);
        //refresh chart model after the recalc
        refreshModel();


        Category category = entryContribution.getCategory();

        entryContribution = new Contribution();
        entryContribution.setUID(UUID.randomUUID().toString());
        entryContribution.setCategory(category);
        entryContribution.setAmount(BigDecimal.ZERO);

        addMessage("contribution.added");

    }

    public void onRowEdit(RowEditEvent event) {
    }

    private void calcActualBatchTotal() {

        float sum = 0;

        for (Contribution contribution : batch.getContributions()) {
            sum += contribution.getAmount().floatValue();
        }
        actualBatchTotal = new BigDecimal(sum);
    }

    /**
     *
     */
    @SuppressWarnings("unchecked")
    @PostConstruct
    private void initialization() {


        if (batchId == null) {
            batchId = getParameter("batchId") == null ? -0L : new Long(getParameter("batchId"));
        }

        if (batchId != null && batchId != 0) {
            batch = batchManager.get(batchId);
            batch.getContributions().size();
        } else {

            batch.setBatchTotal(BigDecimal.ZERO);
        }
        this.setSortColumn("name");
        this.persons = (ArrayList<Person>) sort(personManager.getAllDistinct());
        this.createDonutModel();


    }


    private void createDonutModel() {

        model = new ArrayList<FundModel>(0);
        donutModel = new DonutChartModel();
        getBatch().getContributions().forEach(contribution -> sumContributions(contribution));
        refreshModel();

    }

    private void refreshModel() {

        ChartData data = new ChartData();
        DonutChartDataSet dataSet = new DonutChartDataSet();

        List<Number> values = new ArrayList<>();
        List<String> bgColors = new ArrayList<>();
        List<String> labels = new ArrayList<>();


        if (model != null) {
            model.stream().filter(item -> item.getTotal().compareTo(BigDecimal.ZERO) == 1).forEach(item -> {
                values.add(item.getTotal());
                bgColors.add(ConvertUtil.hex2Rgb(item.getCategory().getColor()));
                labels.add(item.getCategory().getName());
            });
        }

        dataSet.setData(values);
        dataSet.setBackgroundColor(bgColors);
        data.addChartDataSet(dataSet);
        data.setLabels(labels);

        donutModel.setData(data);
    }

    private void sumContributions(Contribution contribution) {


        FundModel fundModelItem = model.stream()
                .filter(m -> m.getCategory().getName().equals(contribution.getCategory().getName()))
                .findFirst()
                .orElse(null);


        if (fundModelItem != null) {
            fundModelItem.setTotal(fundModelItem.getTotal().add(contribution.getAmount()));
        } else {
            model.add(new FundModel(contribution.getCategory(), contribution.getAmount()));

        }
    }

    /**
     * Project: framework Method : setContributionId Return : void Params : @param l
     * Author : eairrick
     */
    public void setContributionId(long l) {
        this.contributionId = l;

    }

    class FundModel {

        Category fund;
        BigDecimal total;

        public FundModel(Category fund, BigDecimal total) {
            this.fund = fund;
            this.total = total;
        }

        public Category getCategory() {
            return fund;
        }

        public void setCategory(Category fund) {
            this.fund = fund;
        }

        public BigDecimal getTotal() {
            return total;
        }

        public void setTotal(BigDecimal total) {
            this.total = total;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FundModel fundModel = (FundModel) o;
            return Objects.equals(fund, fundModel.fund) &&
                    Objects.equals(getTotal(), fundModel.getTotal());
        }

        @Override
        public int hashCode() {
            return Objects.hash(fund, getTotal());
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("fund", fund)
                    .toString();
        }
    }


    public void toggleBatchDetails() {
        setShowBatchDetails(!getShowBatchDetails());
    }

    public Boolean getShowBatchDetails() {
        return showBatchDetails;
    }

    public void setShowBatchDetails(Boolean showBatchDetails) {
        this.showBatchDetails = showBatchDetails;
    }

    public DonutChartModel getDonutModel() {
        return donutModel;
    }

    public void setDonutModel(DonutChartModel donutModel) {
        this.donutModel = donutModel;
    }
}