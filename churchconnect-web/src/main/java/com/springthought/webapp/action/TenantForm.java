package com.springthought.webapp.action;

import com.springthought.model.Tenant;
import com.springthought.service.GenericManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.PreRenderViewEvent;
import java.io.Serializable;

@Component("tenantForm")
@RequestScoped
//@Scope("request")
public class TenantForm extends BasePage implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private GenericManager<Tenant, Long> tenantManager;
	private Tenant tenant = new Tenant();
	private Long tenantId;

	@Autowired
	public void setTenantManager(
			@Qualifier("tenantManager") GenericManager<Tenant, Long> tenantManager) {
		this.tenantManager = tenantManager;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String delete() {
		tenantManager.remove(tenant.getTenantId());
		addMessage("tenant.deleted");

		return "tenants";
	}

	public String edit() {
		// Workaround for not being able to set the id using #{param.id} when
		// using Spring-configured managed-beans
		if (tenantId == null) {
			tenantId = getParameter("tenantId") == null ? 0L : new Long(
					getParameter("tenantId"));
		}
		// Comparison to zero (vs. null) is required with MyFaces 1.2.2, not
		// with previous versions
		if (tenantId != null && tenantId != 0) {
			tenant = tenantManager.get(tenantId);
		} else {
			tenantId = this.currentUserTenantContext();
			tenant = tenantManager.get(tenantId);
		}

		return "tenantForm";
	}

	public String save() {
		boolean isNew = (tenant.getTenantId() == null || tenant.getTenantId() == 0);
		tenantManager.save(tenant);

		String key = (isNew) ? "tenant.added" : "tenant.updated";
		addMessage(key);

		if (isNew) {
			return "tenants";
		} else {
			return "tenants";
		}
	}

	public void loadTenant(PreRenderViewEvent event) {

		log.debug("?");

		if (this.tenantId == null || this.tenantId ==0L ) {
			edit();
		}
	}

	public void save(ActionEvent event) {
		this.save();
	}

}
