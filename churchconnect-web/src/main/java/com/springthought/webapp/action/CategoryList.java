package com.springthought.webapp.action;

import com.springthought.Constants.CategoryType;
import com.springthought.model.Category;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("categoryList")
@Scope("view")
public class CategoryList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7232937000394322816L;
    private String query;
    private GenericManager<Category, Long> categoryManager;
    private Category selectedCategory = new Category();
    private boolean checked;
    private CategoryType type;
    private DataService dataService;
    private String pageHeader = "Undefined";
    private String columnHeader = "Undefined";
    private String pageTitle = "Undefined";


    private String itemDetail = "Undfined";
    private String from;

    @Autowired
    public void setCategoryManager(@Qualifier("categoryManager") GenericManager<Category, Long> categoryManager) {
        this.categoryManager = categoryManager;
    }

    @Autowired
    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    public DataService getDataService() {
        return dataService;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public CategoryList() {
        setSortColumn("name"); // sets the default sort column
    }

    @SuppressWarnings("unchecked")
    public List<Category> getCategories() {

        if (type != null) {
            return sort(dataService.getCategoryByType(type));
        } else {
            return sort(categoryManager.getAllDistinct());
        }
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }


    public String getPageHeader() {
        return pageHeader;
    }

    public void setPageHeader(String pageHeader) {
        this.pageHeader = pageHeader;
    }

    public String getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }


    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }

    public String search() {
        return "success";
    }

    public void delete(ActionEvent event) {

        Category category = categoryManager.get(selectedCategory.getCategoryId());
        category.removeGroups();

        try {
            categoryManager.remove(category.getCategoryId());
            addMessage("category.deleted");

        } catch (DataIntegrityViolationException e) {

            DataIntegrityViolationExceptionHandler(e);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @PostConstruct
    private void initilization() {

        String stype = null;

        if (type == null) {
            stype = getParameter("type");
        }

        from = getParameter("from") == null ? "" : getParameter("from");

        type = StringUtils.isBlank(stype) ? CategoryType.valueOf("MINISTRY") : CategoryType.valueOf(stype);

        switch (type) {

            case CLASS:
                break;
            case TALENT:
                break;
            case MINISTRY:
                setPageTitle(getText("categoryList.title"));
                setPageHeader(getText("categoryList.heading"));
                setColumnHeader(getText("category.category_id"));
                break;
            case GIFT:
                break;
            case FUND:
                setPageTitle(getText("  fundList.title"));
                setPageHeader(getText("fundList.heading"));
                setColumnHeader(getText("fund.fundId"));
                setItemDetail(getText("fundDetail.title"));
                break;
            case EVENT:
                break;
            default:
                setPageTitle(getText("categoryDetail.title"));
                setPageHeader(getText("categoryDetail.heading"));
                setColumnHeader(getText("category.category_id"));
                break;
        }


    }


    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getItemDetail() {
        return itemDetail;
    }

    public void setItemDetail(String itemDetail) {
        this.itemDetail = itemDetail;
    }



}