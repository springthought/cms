
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Visit;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;


@Component("visitList")
@Scope("view")
public class VisitList extends BasePage implements Serializable {
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String query;
    private GenericManager<Visit, Long> visitManager;
	private Visit selectedVisit = new Visit();
    private boolean checked;

    @Autowired
    public void setVisitManager(@Qualifier("visitManager") GenericManager<Visit, Long> visitManager) {
        this.visitManager = visitManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public VisitList() {
        setSortColumn("visitId"); // sets the default sort column
    }

    public List<Visit> getVisits() {
        try {
            return visitManager.search(query, Visit.class);
        } catch (SearchException se) {
            addError(se.getMessage());
            return sort(visitManager.getAll());
        }
    }

    public String search() {
        return "success";
    }

    public Visit getSelectedVisit() {
        return selectedVisit;
    }

    public void setSelectedVisit(Visit visit) {
        this.selectedVisit = visit;
    }

	public void delete( ActionEvent event ){
    	visitManager.remove( selectedVisit.getVisitId());
	  	addMessage("visit.deleted");
	  	checked = false;
    }


    public void radioSelected(SelectEvent event) {checked = true;}

	public boolean isChecked() {return checked;}

	public void setChecked(boolean checked) {this.checked = checked;}
}
