package com.springthought.webapp.action;

import com.springthought.model.Campaign;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.UUID;

@Component("campaignForm")
@Scope("view")
public class CampaignForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6366720425756614968L;
    private GenericManager<Campaign, Long> campaignManager;
    private Campaign campaign = new Campaign();
    private Long campaignId;
    private DataService dataService;

    @Autowired
    public void setCampaignManager(
            @Qualifier("campaignManager") GenericManager<Campaign, Long> campaignManager) {
        this.campaignManager = campaignManager;
    }

    @Autowired
    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    public DataService getDataService() {
        return dataService;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public String delete() {
        campaignManager.remove(campaign.getCampaignId());
        addMessage("campaign.deleted");

        return "campaigns";
    }

    public String edit() {
        campaignId = getParameter("campaignId") == null ? 0L : new Long(
                getParameter("campaignId"));
        if (campaignId != null && campaignId != 0) {
            campaign = campaignManager.get(campaignId);
        } else {
            campaign = new Campaign();
        }

        return "campaignForm";
    }

    public Object handleCloseDialog() {
        this.campaign = new Campaign();
        return null;

    }


    public String save() {
        PrimeFaces.current().ajax().addCallbackParam("valid", true);

        boolean isNew = (campaign.getCampaignId() == null || campaign
                .getCampaignId() == 0);

        try {

            campaignManager.save(campaign);
            String key = (isNew) ? "campaign.added" : "campaign.updated";
            addMessage(key);
            campaign = new Campaign();

        } catch (ConstraintViolationException e) {

            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));

        } catch (Exception e) {

            UUID guid = UUID.randomUUID();
            PrimeFaces.current().ajax().addCallbackParam("valid", false);

            addError("errorAddCampaignKey", "errors.generalApplicationError",
                    new Object[]{e.getMessage(), guid.toString()});

            log.error(guid.toString(), e);

            return "";
        }

        if (isNew) {
            return "campaigns";
        } else {
            return "campaigns";
        }

    }

    public void save(ActionEvent event) {
        this.save();
    }

    public void onRowEdit(RowEditEvent event) {
        this.campaign = (Campaign) event.getObject();
        //this.save();
    }

    public void onCellEdit(CellEditEvent event) {

    }
}
