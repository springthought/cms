
package com.springthought.webapp.action;

import com.springthought.model.Resource;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("resourceList")
@Scope("view")
public class ResourceList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -2425137961797006520L;
    private String query;
    private GenericManager<Resource, Long> resourceManager;
    private Resource selectedResource = new Resource();
    private boolean checked;

    @Autowired
    public void setResourceManager(@Qualifier("resourceManager") GenericManager<Resource, Long> resourceManager) {
        this.resourceManager = resourceManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public ResourceList() {
        setSortColumn("resourceId"); // sets the default sort column
    }

    public List<Resource> getResources() {
        return sort(resourceManager.getAllDistinct());
    }

    public String search() {
        return "success";
    }

    public Resource getSelectedResource() {
        return selectedResource;
    }

    public void setSelectedResource(Resource resource) {
        this.selectedResource = resource;
    }

    public void delete(ActionEvent event) {

        Resource resource = resourceManager.get(selectedResource.getResourceId());

        try {
            resourceManager.remove(resource.getResourceId());
            addMessage("resource.deleted");

        } catch (DataIntegrityViolationException e) {

            DataIntegrityViolationExceptionHandler(e);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
