package com.springthought.webapp.action;

import com.springthought.model.User;
import com.springthought.webapp.listener.UserCounterListener;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Scope("session")
@Component("activeUserList")
@ManagedBean
@SessionScoped
public class ActiveUserList extends BasePage implements Serializable {
    private static final long serialVersionUID = -2725378005612769815L;
    private List<User> filteredUsers;
    private User selectedUser = new User();
    
    /*
     * 
     */
    public List<User> getFilteredUsers() {
		return filteredUsers;
	}

	public void setFilteredUsers(List<User> filteredUsers) {
		this.filteredUsers = filteredUsers;
	}

	public ActiveUserList() {
        setSortColumn("username");
    }
    
    public List<User> getUsers() {
        
    	Long tenantId = currentUserTenantContext();
    	
    	Set tenantUsers = new HashSet<User>();
    	Set users = (Set) getServletContext().getAttribute(UserCounterListener.USERS_KEY);

    	
    	//filter out users that don't belong the same tenantid
    	for (Object user : users) {
    	  	 if ( ((User)user).getTenantId() == tenantId ){
    			 tenantUsers.add( user );
			 }
		}
        
        if (! tenantUsers.isEmpty()) {
            return sort(new ArrayList<User>( tenantUsers ));
        } else {
            return new ArrayList<User>(0);
        }
        
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }
}
