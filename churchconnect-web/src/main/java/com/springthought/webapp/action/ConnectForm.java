package com.springthought.webapp.action;

import com.springthought.Constants.ConnectAction;
import com.springthought.Constants.ConnectStatus;
import com.springthought.Constants.ConnectTabs;
import com.springthought.Constants.Status;
import com.springthought.model.*;
import com.springthought.service.GenericManager;
import com.springthought.util.DateUtil;
import com.springthought.util.Queries;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Component("connectForm")
@Scope("view")
public class ConnectForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7444417105877392447L;
    private boolean checked;
    boolean newEntry;
    private Connect connect = new Connect();
    private Long connectId;
    private GenericManager<Connect, Long> connectManager;
    private ConnectTabs currentTab = ConnectTabs.tbAssign;
    private String query;
    private Connect selectedConnect = new Connect();
    private String tab;
    private Long iD;
    private boolean show = true;
    private String from;
    private TabView connectTabView;
    private int ALL_CONNECTIONS_TAB = 2;

    public String complete(ActionEvent event) {

        this.getConnect().setStatus(ConnectStatus.COMP);

        String retval = save();

        if (retval.equals("connections")) {
            this.setShow(false);
        }
        return retval;

    }

    /**
     * @param query
     * @return
     */
    public List<Person> completePerson(String query) {

        List<Person> allPersons = dataService.getPersons();

        if (query.isEmpty()) {
            return allPersons;
        }

        List<Person> filteredPersons = new ArrayList<Person>();

        for (Person person : allPersons) {
            if (person.getName().toLowerCase().startsWith(query.toLowerCase())) {
                filteredPersons.add(person);
            }
        }
        return filteredPersons;
    }

    public void ConnectList() {
        setSortColumn("connectionId"); // sets the default sort column
    }

    public void delete(ActionEvent event) {
        connectManager.remove(selectedConnect.getConnectId());
        addMessage("connect.deleted");
        checked = false;
    }

    public String edit() {
        return "connectForm";
    }

    public List<Connect> getCompletedConnections() {

        List<Connect> Connections = connectManager.getAll().stream()
                .filter(c -> c.getStatus().equals(ConnectStatus.COMP)).collect(Collectors.toList());

        return Connections;

    }

    public Connect getConnect() {
        return connect;
    }

    public ConnectAction[] getConnectActions() {
        return ConnectAction.values();
    }

    private Connect getConnectionByUUID(Long uuid) {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.CONNECT_ID_PARM, uuid);

        List<Connect> connect = connectManager.findByNamedQuery(Queries.CONNECT_BY_UUID, queryParams);

        return (connect.size() != 0 ? connect.get(0) : null);

    }

    public List<Connect> getConnections() {

        List<Connect> connections = null;

        switch (currentTab) {

            case tbMyConnection:

                User user = currentUser();

                connections = connectManager.getAll().stream()
                        .filter(c -> c.getStatus() != ConnectStatus.COMP
                                && c.getAssignee().getPersonId().equals(user.getPerson().getPersonId()))
                        .collect(Collectors.toList());
                break;
            case tbConnecitons:
                connections = connectManager.getAll().stream().filter(c -> c.getStatus() != ConnectStatus.COMP)
                        .collect(Collectors.toList());
                break;
            case tbConnected:
                connections = connectManager.getAll().stream().filter(c -> c.getStatus().equals(ConnectStatus.COMP))
                        .collect(Collectors.toList());
                break;
            default:
                connections = new ArrayList<Connect>(0);
                break;
        }

        return connections;
    }

    public TabView getConnectTabView() {
        return connectTabView;
    }

    public ConnectTabs getCurrentTab() {
        return currentTab;
    }

    public String getFrom() {
        return from;
    }

    public Long getiD() {
        return iD;
    }

    public char getPersonGroup(Person person) {
        return (person == null ? '\0' : person.getFirstName().charAt(0));
    }

    public String getQuery() {
        return query;
    }

    public Connect getSelectedConnect() {
        return selectedConnect;
    }

    public String getTab() {
        return tab;
    }

    @PostConstruct
    private void initialization() {

        connectId = getParameter("connectId") == null ? 0L : new Long(getParameter("connectId"));
        from = getParameter("from") == null ? new String("") : getParameter("from");
        iD = getParameter("id") == null ? 0L : new Long(getParameter("id"));

        if (from.equals("topbar")) {
            currentTab = ConnectTabs.tbMyConnection;
        } else {
            currentTab = ConnectTabs.tbAssign;
        }

        // locate by uuid if it's being passed in from the get url string
        if (iD != null && iD != 0) {

            connect = getConnectionByUUID(iD);

            if (connect == null) {

                setShow(false);
                addError("connect.notFound");

            } else if (connect.getStatus().equals(ConnectStatus.COMP)) {

                setShow(false);

                addMessage("connect.complete", new String[]{connect.getAssignee().getName(),
                        DateUtil.convertDateToString(connect.getCompleteOnDate())});

            }

        } else if (connectId != null && connectId != 0) {
            connect = connectManager.get(connectId);
        } else {
            connect = new Connect();
        }

    }

    public boolean isChecked() {
        return checked;
    }

    public boolean isNewEntry() {
        return (connect.getConnectId() == null || connect.getConnectId() == 0);
    }

    public boolean isShow() {
        return show;
    }

    public void onTabChange(TabChangeEvent event) {
        currentTab = ConnectTabs.valueOf(event.getTab().getId());
        tab = currentTab.getLabel();
    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    public String save() {

        boolean isNew = isNewEntry();

        if (isNew) {
            connect.setuUID(UUID.randomUUID().getLeastSignificantBits());
        }

        connect = connectManager.save(connect);

        Optional<TabView> tbv = Optional.ofNullable(getConnectTabView());

        if (tbv.isPresent()) {
            tbv.get().setActiveIndex(ALL_CONNECTIONS_TAB);
            currentTab = ConnectTabs.valueOf("tbConnecitons");
        }
        if (isNew) {

            connect.setuUID(UUID.randomUUID().getLeastSignificantBits());
            StatusResult results = sendAssignment();
            if (results.getStatus().equals(Status.ERR)) {
                addError(results.getResultMessage());
            } else {
                addMessage("connect.connectAssignment", connect.getAssignee().getName());
            }

        }

        String key = (isNew) ? "connect.added" : "connect.updated";
        addMessage(key);

        return "connections";
    }

    public void save(ActionEvent event) {
        this.save();
    }

    public String search() {
        return "success";
    }

    private StatusResult sendAssignment() {

        String app_url = pageURL();
        MessageTemplateSupplier supplier = new MessageTemplateSupplier();
        Message msg = new Message();
        msg.setName(getText("connectMessage.assignment"));

        StringBuilder builder = new StringBuilder();

        builder.append(connect.getAssignee().getFirstName() + ", " + getText("connectMessage.assignedBy") + "<b>"
                + DateUtil.convertDateToString(connect.getCompleteDate()) + "</b>.<p/>");

        builder.append(getText("connect.instructions") + ":" + connect.getInstructions() + "<p/>"
                + getText("connectMessage.typeOfConnection") + ": <b>" + connect.getAction().getLabel() + "</b><p/>"
                + getText("person.individual") + ": <b>" + connect.getIndividual().getName() + ".</b><p/>");

        builder.append(getText("connectMessage.after") + "," + "<a href=" + app_url + "/connect/"
                + connect.getuUID().toString() + ">" + getText("connectMessage.enterNotes") + "</a> "
                + getText("connectMessage.markedComplete"));

        msg.setMessage(builder.toString());

        supplier.setPerson(connect.getAssignee());
        supplier.setMessage(msg);

        log.info("Applicaiton URL:" + app_url);

        supplier.setApplicationURL("");

        supplier.setSender(null);

        StatusResult statusResult = this.sendPersonMessage(supplier, "assignmentMailTemplate.vm");

        return statusResult;

    }


    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void setConnect(Connect connect) {
        this.connect = connect;
    }

    public void setConnectId(Long connectId) {
        this.connectId = connectId;
    }

    @Autowired
    public void setConnectManager(@Qualifier("connectManager") GenericManager<Connect, Long> connectManager) {
        this.connectManager = connectManager;
    }

    public void setConnectTabView(TabView connectTabView) {
        this.connectTabView = connectTabView;
    }

    public void setCurrentTab(ConnectTabs currentTab) {
        this.currentTab = currentTab;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setiD(Long iD) {
        this.iD = iD;
    }

    public void setNewEntry(boolean newEntry) {
        this.newEntry = newEntry;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setSelectedConnect(Connect connect) {
        this.selectedConnect = connect;
    }

    public void setShow(boolean show) {
        this.show = show;
    }
}
