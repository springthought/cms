package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.RepeatOption;
import com.springthought.Constants.WeekOption;
import com.springthought.model.Event;
import com.springthought.model.Group;
import com.springthought.model.LabelValue;
import com.springthought.service.GenericManager;
import com.springthought.util.DateUtil;
import com.springthought.util.EventDateRepeaterUtil;
import io.lamma.DayOfWeek;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.primefaces.PrimeFaces;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import scala.collection.concurrent.Debug;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component("eventForm")
@Scope("view")
public class EventForm extends BasePage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Boolean showMore = new Boolean(false);

	private Event event = new Event();
	private Long eventId = null;

	private Event eventOccurrence = new Event();
	private Long occurrenceId = null;

	private DualListModel<Group> groupLists = new DualListModel<Group>(new ArrayList<Group>(0),
			new ArrayList<Group>(0));

	private GenericManager<Group, Long> groupManager;
	private GenericManager<Event, Long> eventManager;

	// private GenericManager<EventOccurrence, Long> eventOccurrenceManager;

	private String from = "";
	private List<io.lamma.Date> currentEventOccurrences = new ArrayList<io.lamma.Date>(0);
	private ArrayList<Date> dateList = new ArrayList<Date>(0);

	private String duration = "";

	private String template = "/templates/mainMenu.xhtml";

	private String mode = Constants.EDIT_MODE_ONE;

	private ArrayList<Integer> maxRepeatEvery = new ArrayList<Integer>(0);

	private Date startTimeHolder = new Date();
	private Date endTimeHolder = new Date();

	/*
	 * private EventContext eventContext = new EventContext(); private
	 * EventCalendarState eventCalendarState = new EventCalendarState(); private
	 * EventListState eventListState = new EventListState();
	 */

	public EventForm() {

		super();

		for (int i = 1; i < Constants.EVENT_MAX_REPEATS_EVERY + 1; i++) {
			maxRepeatEvery.add(Integer.parseInt("" + i));
		}

	}

	public void calcDuration(AjaxBehaviorEvent e) {

		Calendar startTime = Calendar.getInstance();
		Calendar endTime = Calendar.getInstance();

		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();

		startTime.setTime(getStartTimeHolder());
		endTime.setTime(getEndTimeHolder());

		startDate.setTime(event.getStartDate());
		endDate.setTime(event.getEndDate());

		startDate.set(Calendar.HOUR, startTime.get(Calendar.HOUR));
		startDate.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE));
		startDate.set(Calendar.AM_PM, startTime.get(Calendar.AM_PM));

		endDate.set(Calendar.HOUR, endTime.get(Calendar.HOUR));
		endDate.set(Calendar.MINUTE, endTime.get(Calendar.MINUTE));
		endDate.set(Calendar.AM_PM, endTime.get(Calendar.AM_PM));

		DateTime startDateTime = new DateTime(startDate);
		DateTime endDateTime = new DateTime(endDate);

		Duration dur = new Duration(startDateTime, endDateTime);

		this.event.setStartDate(startDateTime.toDate());
		this.event.setEndDate(endDateTime.toDate());
		this.event.setDuration(dur.getStandardMinutes());

		setDuration(dur.getStandardMinutes() + " " + getText("eventDetail.duration"));

	}

	public void checkReoccurrence() {

		boolean isNew = (event.getEventId() == null || event.getEventId() == 0);

		if (event.getRepeat() != Constants.RepeatOption.NO && !isNew) {

			PrimeFaces.current().executeScript("PF('eventEditDlgInfoWv').show()");
		}

		updateDayOfWeek(event.getStartDate());

	}

	public void close(ActionEvent event) {
		// RequestContext.getCurrentInstance().closeDialog(null);
	}

	private boolean daily() {

		currentEventOccurrences = EventDateRepeaterUtil.daily(dateConverter.convertDate(this.event.getStartDate()),
				dateConverter.convertDate(this.event.getRepeatUntilDate()), this.event.isWeekDaysOnly(),
				this.event.getRepeatEvery());

		return currentEventOccurrences.size() <= Constants.MAX_EVENT_OCCURRENCE;
	}

	public String delete() {

		try {

			eventManager.remove(event.getEventId());
			addMessage("event.deleted");

			if (this.from.equals("calendarform")) {
				navigateToPage("/views/calendarForm.xhtml?faces-redirect=true&startDate=" + (new Date()).getTime());
				return null;
			} else {
				return "events";
			}

		} catch (DataIntegrityViolationException e) {

			DataIntegrityViolationExceptionHandler(e);

		} catch (Exception e) {
			UUID guid = UUID.randomUUID();
			addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
			log.error(guid.toString(), e);
		}

		return null;
	}

	public String edit() {
		return "eventForm";
	}

	public void editMode(String mode) {
		setMode(mode);
	}

	/**
	 * Project: framework Method : generateEvents Return : List<EventOccurrence>
	 * Params : @return Author : eairrick
	 *
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 *
	 */
	private Set<Event> generateEvents() throws IllegalAccessException, InvocationTargetException {

		Set<Event> eventOccurs = new HashSet<Event>(0);

		for (io.lamma.Date date : currentEventOccurrences) {

			Date compStartDate = dateConverter.convertDateTo(date, event.getStartDate());

			Date compEndDate = dateConverter.convertDateTo(date, event.getEndDate());

			Event eo = new Event();

			BeanUtils.copyProperties(event, eo);

			if (event.getRepeat() == RepeatOption.NO) {
				// set until date to start since it's not reoccurring
				event.setRepeatUntilDate(event.getStartDate());

			} else {
				// add override for reocurring meetings here.
			}

			eo.setEventId(null);
			eo.setVersion(null);
			eo.setParent(false);
			eo.setEditable(true);

			eo.setSetStartDate(compStartDate);
			eo.setSetEndDate(compEndDate);
			eo.setStartDate(compStartDate);
			eo.setEndDate(compEndDate);

			eventOccurs.add(eo);

			eo.setEventParent(this.event);

		}

		return eventOccurs;
	}

	public ArrayList<Date> getDateList() {
		return dateList;
	}

	public ArrayList<LabelValue> getDayValues() {

		int max = retriveMonthMaxDays(this.event.getStartDate()) + 1;

		ArrayList<LabelValue> days = new ArrayList<LabelValue>(0);

		for (int i = 1; i < max; i++) {

			LabelValue value = new LabelValue();

			value.setLabel(String.valueOf(i));
			value.setValue(String.valueOf(i));

			days.add(value);
		}

		return days;

	}

	public String getDuration() {
		return duration;
	}

	public Date getEndTimeHolder() {
		return endTimeHolder;
	}

	public Event getEvent() {

		return event;
	}

	/**
	 * @return the eventId
	 */
	public Long getEventId() {
		return eventId;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return this.from;
	}

	/**
	 * @return the groupList
	 */
	public DualListModel<Group> getGroupLists() {
		return groupLists;
	}

	public ArrayList<Integer> getMaxRepeatEvery() {
		return maxRepeatEvery;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	public RepeatOption[] getRepeatValues() {
		return Constants.RepeatOption.values();
	}

	public Boolean getShowMore() {
		return showMore;
	}

	public Date getStartTimeHolder() {
		return startTimeHolder;
	}

	public Constants.EventStatus[] getStatusValues() {
		return Constants.EventStatus.values();
	}

	public String getTemplate() {
		return template;
	}

	/**
	 *
	 * Project: framework Method : getWeekDays Return : ArrayList<LabelValue> Params
	 * : @return Author : eairrick
	 *
	 */
	public ArrayList<LabelValue> getWeekDays() {

		io.lamma.DayOfWeek[] weekdays = io.lamma.DayOfWeek.values();

		ArrayList<LabelValue> weekdayList = new ArrayList<LabelValue>(0);

		for (DayOfWeek dayNum : weekdays) {

			LabelValue value = new LabelValue();
			value.setLabel(StringUtils.capitalize(DayOfWeek.of(dayNum.n()).name().toLowerCase()));
			value.setValue(String.valueOf(dayNum.n()));
			weekdayList.add(value);
		}

		return weekdayList;
	}

	public WeekOption[] getWeekValues() {
		return Constants.WeekOption.values();
	}

	public void handleAllDayEvent(AjaxBehaviorEvent e) {

		Object obj = ((SelectBooleanCheckbox) e.getComponent()).getValue();
		Boolean oN = Boolean.parseBoolean(obj.toString());

		if (oN) {
			startTimeHolder = new DateTime(2199, 1, 1, 12, 0, 0, 0).toDate();
			endTimeHolder = new DateTime(2199, 1, 1, 12, 0, 0, 0).toDate();
		} else {
			startTimeHolder = new DateTime(2199, 1, 1, 8, 0, 0, 0).toDate();
			endTimeHolder = new DateTime(2199, 1, 1, 9, 0, 0, 0).toDate();
		}

	}

	/**
	 * Creates a brand new Event; called after new inline event is created Need due
	 * to scope of the manage been being a "View" state.
	 *
	 * @param event
	 */

	public Object handleCloseDialog() {
		this.event = new Event();
		return null;

	}

	public void handleDateSelect(AjaxBehaviorEvent event) {

		handleDateSelect(new SelectEvent(event.getComponent(), event.getBehavior(),
				((org.primefaces.component.calendar.Calendar) event.getSource()).getValue()));
	}

	public void handleDateSelect(org.primefaces.event.SelectEvent event) {

		this.event.setEndDate((Date) event.getObject());
		this.calcDuration(null);
		updateDayOfWeek((Date) event.getObject());

	}

	public void initEvent(ActionEvent event) {

		this.event = (Event) event.getComponent().getAttributes().get("event");

		// need to reload object if not a new event
		if (this.getEvent().getEventId() != null && this.getEvent().getEventId() != 0) {
			this.event = eventManager.get(this.event.getEventId());
			this.event.getGroups().size();
		}

	}


	@PostConstruct
	private void initialization() {

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		dateFormat.setTimeZone(TimeZone.getTimeZone(this.getTimezone()));

		Date eventDate = null;

		this.from = getParameter("from") == null ? "" : getParameter("from");

		this.mode = getParameter("mode") == null ? Constants.EDIT_MODE_ONE : getParameter("mode");

		String strdate = getParameter("date") == null
				? dateFormat.format(DateUtil.setMinute(DateUtil.setHour((new Date()), 8), 0))
				: getParameter("date");

		try {
			eventDate = dateFormat.parse(strdate);
		} catch (ParseException e) {
			log.error(e.getMessage(), e);
		}

		if (this.eventId == null) {

			this.eventId = getParameter("eventId") == null ? 0L : new Long(getParameter("eventId"));
		}

		if (from.equals("calendarform")) {
			// template = "/templates/mainNoMenu.xhtml";
			// eventContext.setState(eventCalendarState);
		} else {
			// eventContext.setState(eventListState);
		}

		// eventContext.doAction();

		// Comparison to zero (vs. null) is required with MyFaces 1.2.2, not
		// with previous versions
		if (this.eventId != null && this.eventId != 0) {

			event = eventManager.get(eventId);

			startTimeHolder = new DateTime(2199, 1, 1, DateUtil.getHour(event.getStartDate()),
					DateUtil.getMinute(event.getStartDate()), 0, 0).toDate();

			endTimeHolder = new DateTime(2199, 1, 1, DateUtil.getHour(event.getEndDate()),
					DateUtil.getMinute(event.getEndDate()), 0, 0).toDate();

		} else {
			event = new Event(eventDate, eventDate);

			startTimeHolder = new DateTime(2199, 1, 1, DateUtil.getHour(eventDate), DateUtil.getMinute(eventDate), 0, 0)
					.toDate();
			endTimeHolder = new DateTime(2199, 1, 1, DateUtil.getHour(eventDate), DateUtil.getMinute(eventDate), 0, 0)
					.toDate();
		}

		List<Group> groups = this.groupManager.getAllDistinct();
		groups.removeAll(event.getGroups()); // remove selected from available
		// list.
		groupLists = new DualListModel<Group>(groups, event.getGroups());
		DateTime dt = new DateTime(2199, 1, 1, 0, 0, 0, 0);

		for (int m = 0; m < 1470; m += 30) {
			DateTime adjDt = dt.plus(Period.minutes(m));
			dateList.add(adjDt.toDate());
		}

	}

	private boolean isUpdateableOccurrence(Event occurrence) {

		if (!this.from.equals("calendarform")) {
			return occurrence.isParent();
		} else {
			return occurrence.getEventId().equals(this.eventId);
		}

	}

	/**
	 * Project: framework Method : validate Return : void Params : Author : eairrick
	 *
	 */
	private boolean isValid() {

		boolean valid = true;

		currentEventOccurrences = new ArrayList<io.lamma.Date>(0);

		switch (event.getRepeat()) {
		case DAILY:
			valid = daily();
			break;
		case WEEKLY:
			valid = weekly();
			break;
		/*
		 * case BIWEEKLY: valid = biWeekly(); break;
		 */
		case MONTHLY:
			valid = monthly();
			break;
		case YEARLY:
			valid = yearly();
			break;
		default:
			// repeat is No; still need to create the original occurrence.
			currentEventOccurrences.add(dateConverter.convertDate(this.event.getStartDate()));

			// set until date to start since it's not reoccurring
			event.setRepeatUntilDate(event.getStartDate());
			event.setRepeatEvery(null);
			break;
		}

		return valid;
	}

	/*
	 * private boolean biWeekly() {
	 *
	 * currentEventOccurrences =
	 * EventDateRepeaterUtil.biWeekly(dateConverter.convertDate(this.event.
	 * getStartDate()), dateConverter.convertDate(this.event.getRepeatUntilDate()));
	 *
	 * return currentEventOccurrences.size() <= Constants.MAX_EVENT_OCCURRENCE;
	 *
	 * }
	 */

	private boolean monthly() {

		if (event.getMonthYearOption() == 0) {

			Integer nThDay = event.getDayNth();
			currentEventOccurrences = EventDateRepeaterUtil.nthDayMonthly(
					dateConverter.convertDate(this.event.getStartDate()),
					dateConverter.convertDate(this.event.getRepeatUntilDate()), nThDay, this.event.getRepeatEvery());

		} else {

			WeekOption weekOption = event.getWeekOccurrence();

			Integer dayNum = event.getWeekDayNum();

			switch (weekOption) {
			case FIRST:
				currentEventOccurrences = EventDateRepeaterUtil.firstDayMonthly(
						dateConverter.convertDate(this.event.getStartDate()),
						dateConverter.convertDate(this.event.getRepeatUntilDate()), DayOfWeek.of(dayNum),
						this.event.getRepeatEvery());
				break;
			case LAST:
				currentEventOccurrences = EventDateRepeaterUtil.lastDayMonthly(
						dateConverter.convertDate(this.event.getStartDate()),
						dateConverter.convertDate(this.event.getRepeatUntilDate()), DayOfWeek.of(dayNum),
						this.event.getRepeatEvery());
			default:
				currentEventOccurrences = EventDateRepeaterUtil.nthMonthly(
						dateConverter.convertDate(this.event.getStartDate()),
						dateConverter.convertDate(this.event.getRepeatUntilDate()), weekOption.n(),
						DayOfWeek.of(dayNum), this.event.getRepeatEvery());
				break;

			}
		}
		return currentEventOccurrences.size() <= Constants.MAX_EVENT_OCCURRENCE;

	}

	public void onTransfer(TransferEvent event) {

		StringBuilder builder = new StringBuilder();

		for (Object item : event.getItems()) {
			builder.append(((Group) item).getName()).append("<br />");
		}

		FacesMessage msg = new FacesMessage();

		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		msg.setSummary("Items Transferred");
		msg.setDetail(builder.toString());

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	private void PersistGroups() {
		event.getGroups().retainAll(this.getGroupLists().getTarget());

		for (Group selectedGroup : this.getGroupLists().getTarget()) {

			if (event.getGroups().indexOf(selectedGroup) == -1) {
				event.getGroups().add(selectedGroup);
			}
		}
	}

	public int retriveMonthMaxDays(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 *
	 * Project: framework Method : edit Return : String Params : @return Author :
	 * eairrick
	 *
	 *
	 * public String edit() {
	 *
	 * if (eventId == null) { eventId = getParameter("eventId") == null ? 0L : new
	 * Long( getParameter("eventId")); }
	 *
	 * if (eventId != null && eventId != 0) { event = eventManager.get(eventId); }
	 * else { event = new Event(); }
	 *
	 * return "eventForm"; }
	 */
	public String save() {

		try {
			prepareSave();
		} catch (DataIntegrityViolationException e) {
			log.error(e);
			return "";
		} catch (ConstraintViolationException e) {
			DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
			log.error(e);
			return "";
		} catch (Exception e) {
			UUID guid = UUID.randomUUID();
			addError("errors.generalApplicationError", new Object[] { e.getMessage(), guid.toString() });
			log.error(guid.toString(), e);
			return "";
		}

		if (!isValid()) {
			addError("errors.event.exceeded", Constants.MAX_EVENT_OCCURRENCE.toString());
			return "";
		}

		boolean isNew = (event.getEventId() == null || event.getEventId() == 0);

		if (isNew) {
			try {
				event.getEventOccurrences().addAll(generateEvents());
			} catch (Exception e) {
				UUID guid = UUID.randomUUID();
				addError("errors.generalApplicationError", new Object[] { e.getMessage(), guid.toString() });
				log.fatal(guid.toString(), e);
				return "";
			}
		} else {

			if (this.mode.equals(Constants.EDIT_MODE_ONE)) {
				updateSingleEvent();
			}

		}

		// PersistGroups();

		try {

			this.event = eventManager.save(event);

			String key = (isNew) ? "event.added" : "event.updated";

			addMessage(key);

			if (isNew) {
				return "eventForm";
			} else {
				return "eventForm";
			}

		} catch (ConstraintViolationException e) {

			DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
		}

		catch (Exception e) {
			UUID guid = UUID.randomUUID();
			addError("errors.generalApplicationError", new Object[] { e.getMessage(), guid.toString() });
			log.error(guid.toString(), e);
		}

		return "";
	}

	private void prepareSave() throws ConstraintViolationException, DataIntegrityViolationException, Exception {

		if (this.mode.equals(Constants.EDIT_MODE_SERIES)) {

			try {
				eventManager.remove(event.getParentId());
				event.setEventId(null);
				event.setEventParent(null);
				event.setParent(true);
				event.setEditable(false);
			} catch (ConstraintViolationException e) {
				throw e;
			} catch (Exception e) {

				throw e;
			}
		}

		calcDuration(null);
		updateDayOfWeek(event.getStartDate());
	}

	public void save(ActionEvent event) {
		this.save();
	}

	public void saveAndClose(ActionEvent event) {

		if (!this.save().isEmpty()) {
			navigateToPage(
					"/views/calendarForm.xhtml?faces-redirect=true&startDate=" + getEvent().getStartDate().getTime());
		}
	}

	public void selectionChanged(AjaxBehaviorEvent event) {
		this.event.setRepeatUntilDate(this.event.getEndDate());
	}

	public void setDateList(ArrayList<Date> dateList) {
		this.dateList = dateList;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	void setEndTimeHolder(Date endTimeHolder) {
		this.endTimeHolder = endTimeHolder;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	@Autowired
	public void setEventManager(@Qualifier("eventManager") GenericManager<Event, Long> eventManager) {
		this.eventManager = eventManager;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @param groupList the groupList to set
	 */
	public void setGroupLists(DualListModel<Group> groupList) {
		this.groupLists = groupList;
	}

	@Autowired
	public void setGroupManager(@Qualifier("groupManager") GenericManager<Group, Long> groupManager) {
		this.groupManager = groupManager;
	}

	public void setMaxRepeatEvery(ArrayList<Integer> maxRepeatEvery) {
		this.maxRepeatEvery = maxRepeatEvery;
	}

	/**
	 * @param mode the mode to set
	 */
	private void setMode(String mode) {
		this.mode = mode;
	}

	public void setShowMore(Boolean showMore) {
		this.showMore = showMore;
	}

	public void setStartTimeHolder(Date startTimeHolder) {
		this.startTimeHolder = startTimeHolder;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public void showMoreOptions(ActionEvent event) {
		showMore = !showMore;
	}

	/**
	 *
	 * Updates weekend number and week in months fields in the data model and update
	 * corresponding fields in the UI.
	 *
	 *
	 */

	private void updateDayOfWeek(Date date) {

		Calendar cal = Calendar.getInstance();

		cal.setTime(date);

		SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE");

		Debug.log("Day of week:" + dateFormat.format(cal.getTime()));

		Debug.log("Day of Week in Month:" + WeekOption.of(cal.get(Calendar.DAY_OF_WEEK_IN_MONTH)));

		event.setWeekDayNum(DateUtil.retrieveDayOfWeek(cal.get(Calendar.DAY_OF_WEEK)).n());

		event.setWeekOccurrence(WeekOption.of(cal.get(Calendar.DAY_OF_WEEK_IN_MONTH)));

		event.setDayNth(cal.get(Calendar.DAY_OF_MONTH));

	}

	private void updateMultipleEvents() throws IllegalAccessException, InvocationTargetException,
			ConstraintViolationException, DataIntegrityViolationException {

		Event eventParent = eventManager.get(event.getParentId());

		BeanUtils.copyProperties(event, eventParent, Constants.EVENT_PARENT_EXCLUSION_PROPS);

		eventManager.remove(event.getParentId());

		// eventParent.getEventOccurrences().forEach(e -> e.removeAllAssociations());
		// eventParent.removeAllEventOccurrences();

		eventParent.getEventOccurrences().addAll(generateEvents());

		eventParent = eventManager.save(eventParent);

		Optional<Event> match = eventParent.getEventOccurrences().stream().filter(e -> e == this.event).findFirst();

		if (match.isPresent()) {
			setEvent(match.get());
		}
	}

	/**
	 * Project: framework Method : updateSingleEvent Return : void Params : Author :
	 * eairrick
	 *
	 */
	private void updateSingleEvent() {

		for (Event eventOccurr : event.getEventOccurrences()) {

			if (this.isUpdateableOccurrence(eventOccurr)) {

				eventOccurr.setDescr(event.getDescr());
				eventOccurr.setStartDate(event.getStartDate());
				eventOccurr.setEndDate(event.getEndDate());
				eventOccurr.setTitle(event.getTitle());

				this.eventOccurrence = eventOccurr;

				break;
			}

		}

	}

	private boolean weekly() {

		currentEventOccurrences = EventDateRepeaterUtil.weekly(dateConverter.convertDate(this.event.getStartDate()),
				dateConverter.convertDate(this.event.getRepeatUntilDate()), this.event.getRepeatEvery());

		return currentEventOccurrences.size() <= Constants.MAX_EVENT_OCCURRENCE;

	}

	private boolean yearly() {

		if (event.getMonthYearOption() == 0) {

			currentEventOccurrences = EventDateRepeaterUtil.yearly(dateConverter.convertDate(this.event.getStartDate()),
					dateConverter.convertDate(this.event.getRepeatUntilDate()), this.event.getRepeatEvery());

		} else {

			WeekOption weekOption = event.getWeekOccurrence();

			Integer dayNum = event.getWeekDayNum();

			switch (weekOption) {

			case FIRST:
				currentEventOccurrences = EventDateRepeaterUtil.firstDayYearly(
						dateConverter.convertDate(this.event.getStartDate()),
						dateConverter.convertDate(this.event.getRepeatUntilDate()), DayOfWeek.of(dayNum),
						this.event.getRepeatEvery());
				break;
			case LAST:
				currentEventOccurrences = EventDateRepeaterUtil.lastDayYearly(
						dateConverter.convertDate(this.event.getStartDate()),
						dateConverter.convertDate(this.event.getRepeatUntilDate()), DayOfWeek.of(dayNum),
						this.event.getRepeatEvery());
			default:
				currentEventOccurrences = EventDateRepeaterUtil.nthYearly(
						dateConverter.convertDate(this.event.getStartDate()),
						dateConverter.convertDate(this.event.getRepeatUntilDate()), weekOption.n(),
						DayOfWeek.of(dayNum), this.event.getRepeatEvery());

				break;

			}
		}

		return currentEventOccurrences.size() <= Constants.MAX_EVENT_OCCURRENCE;

	}

}
