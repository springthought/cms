
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Pledge;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;


@Component("pledgeList")
@Scope("view")
public class PledgeList extends BasePage implements Serializable {
    private String query;
    private GenericManager<Pledge, Long> pledgeManager;
	private Pledge selectedPledge = new Pledge();
    private boolean checked;

    @Autowired
    public void setPledgeManager(@Qualifier("pledgeManager") GenericManager<Pledge, Long> pledgeManager) {
        this.pledgeManager = pledgeManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public PledgeList() {
        setSortColumn("pledgeId"); // sets the default sort column
    }

    public List<Pledge> getPledges() {
        try {
            return pledgeManager.search(query, Pledge.class);
        } catch (SearchException se) {
            addError(se.getMessage());
            return sort(pledgeManager.getAll());
        }
    }

    public String search() {
        return "success";
    }

    public Pledge getSelectedPledge() {
        return selectedPledge;
    }

    public void setSelectedPledge(Pledge pledge) {
        this.selectedPledge = pledge;
    }

	public void delete( ActionEvent event ){
    	pledgeManager.remove( selectedPledge.getPledgeId());
	  	addMessage("pledge.deleted");
	  	checked = false;
    }


    public void radioSelected(SelectEvent event) {checked = true;}

	public boolean isChecked() {return checked;}

	public void setChecked(boolean checked) {this.checked = checked;}
}
