package com.springthought.webapp.action;

import com.springthought.model.Batch;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.UUID;

@Component("batchForm")
@Scope("view")
public class BatchForm extends BasePage implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private GenericManager<Batch, Long> batchManager;
    private Batch batch = new Batch();
    private Long batchId;


    @Autowired
    public void setBatchManager(@Qualifier("batchManager") GenericManager<Batch, Long> batchManager) {
	this.batchManager = batchManager;
    }

    public Batch getBatch() {
	return batch;
    }

    public void setBatch(Batch batch) {
	this.batch = batch;
    }

    public void setBatchId(Long batchId) {
	this.batchId = batchId;
    }

    public String delete() {
	batchManager.remove(batch.getBatchId());
	addMessage("batch.deleted");

	return "batches";
    }

    public String edit() {

	return "batchForm";
    }

    public String save() {

	PrimeFaces.current().ajax().addCallbackParam("validationFailed", false);

	boolean isNew = (batch.getBatchId() == null || batch.getBatchId() == 0);

	try {

	    batchManager.save(batch);
	    String key = (isNew) ? "batch.added" : "batch.updated";
	    addMessage(key);

	} catch (ConstraintViolationException e) {

	    DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));

	} catch (DataIntegrityViolationException e) {

	    DataIntegrityViolationExceptionHandler(e);

	} catch (Exception e) {

	    PrimeFaces.current().ajax().addCallbackParam("validationFailed", true);
	    UUID guid = UUID.randomUUID();

	    addError("errorAddBatchKey", "errors.generalApplicationError",
		    new Object[] { e.getMessage(), guid.toString() });

	    log.error(guid.toString(), e);

	    return "";
	}

	batch = new Batch();

	if (isNew) {
	    return "batches";
	} else {
	    return "batches";
	}

    }

    public void save(ActionEvent event) {
	this.save();
    }

    public void onRowEdit(RowEditEvent event) {
	this.batch = (Batch) event.getObject();
	this.save();
    }

    public void onCellEdit(CellEditEvent event) {

    }

	public Object handleCloseDialog() {
		this.batch = new Batch();
		return null;

	}

    @PostConstruct
    private void initilization() {

	batchId = getParameter("batchId") == null ? 0L : new Long(getParameter("batchId"));

	if (batchId != null && batchId != 0) {
	    batch = batchManager.get(batchId);
	} else {
	    batch = new Batch();
	}

    }

}
