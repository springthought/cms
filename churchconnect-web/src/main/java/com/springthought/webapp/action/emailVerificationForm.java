package com.springthought.webapp.action;

import com.springthought.model.User;
import com.springthought.util.Queries;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@Component("emailVerificationForm")
@Scope("request")
public class emailVerificationForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7444417105877392447L;
    private boolean show = true;

    public boolean isShow() {
 	return show;
     }

     public void setShow(boolean show) {
 	this.show = show;
     }

    private User getUserByConfirmationID(Long id) {

	HashMap<String, Object> queryParams = new HashMap<String, Object>();
	queryParams.put(Queries.USER_CONFIRM_ID_PARM, id);

	List<User> user = userManager.findByNamedQuery(Queries.USER_BY_CONFIRM_ID, queryParams);

	return (user.size() != 0 ? user.get(0) : null);

    }

    @PostConstruct
    private void initialization() {

	Long iD = getParameter("id") == null ? 0L : new Long(getParameter("id"));

	if (iD != null && iD != 0) {

	    User user = getUserByConfirmationID(iD);

	    if (user == null) {
		setShow(false);
		addError("signup.emailconfirmationNotFound", new String[] { Long.toString(iD) });
	    } else {
		setShow(true);
		addMessage("signup.emailverified", new String[] { user.getEmail() });
		user.setVerified(true);
		userManager.save(user);

	    }

	}
    }


}
