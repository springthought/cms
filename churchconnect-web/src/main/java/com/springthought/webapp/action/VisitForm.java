package com.springthought.webapp.action;

import com.springthought.model.Visit;
import com.springthought.service.GenericManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;



@Component("visitForm")
@Scope("request")
public class VisitForm extends BasePage implements Serializable {
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private GenericManager<Visit, Long> visitManager;
    private Visit visit = new Visit();
    private Long visitId;


    @Autowired
    public void setVisitManager(@Qualifier("visitManager") GenericManager<Visit, Long> visitManager) {
        this.visitManager = visitManager;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }

    public void setVisitId(Long visitId) {
        this.visitId = visitId;
    }


    public String delete() {
        visitManager.remove(visit.getVisitId());
        addMessage("visit.deleted");

        return "visits";
    }

    public String edit() {
        // Workaround for not being able to set the id using #{param.id} when using Spring-configured managed-beans
        // if (visitId == null) {
            visitId =  getParameter("visitId")==null? 0L : new Long( getParameter("visitId") );

        //}
        // Comparison to zero (vs. null) is required with MyFaces 1.2.2, not with previous versions
        if (visitId != null && visitId != 0) {
            visit = visitManager.get(visitId);
        } else {
            visit = new Visit();
        }

        return "visitForm";
    }

    public String save() {
        boolean isNew = (visit.getVisitId() == null || visit.getVisitId() == 0);

        visitManager.save(visit);

        String key = (isNew) ? "visit.added" : "visit.updated";
        addMessage(key);

	    if (isNew) {
            return "visits";
        } else {
            return "visits";
        }
    }


   public void save( ActionEvent event ){	this.save(); }

}
