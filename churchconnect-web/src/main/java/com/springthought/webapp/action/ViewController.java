/**
 *
 */
package com.springthought.webapp.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;

/**
 * @author kinseye
 *
 */
@Component("viewController")
@Scope("session")
public class ViewController extends BasePage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5648919136617431564L;
	private String currentPage;

	/**
	 * @param destinationPage
	 *            the destinationPage to set
	 */
	public String destinationPage() {

		this.log.info("Navigating To Page:" + currentPage);
		return currentPage;
	}

	@PostConstruct
	public void init() {

	}

	/**
	 * @return the currentPage
	 */
	public String getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage the currentPage to set
	 */
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

}
