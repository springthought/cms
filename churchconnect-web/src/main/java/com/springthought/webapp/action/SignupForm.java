package com.springthought.webapp.action;

import com.springthought.Constants.Status;
import com.springthought.model.*;
import com.springthought.service.GenericManager;
import com.springthought.service.PasswordEncryptor;
import com.springthought.service.RoleManager;
import com.springthought.util.FacesUtils;
import com.springthought.util.Queries;
import com.springthought.webapp.util.RequestUtil;
import org.apache.commons.lang.WordUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * JSF Page class to handle signing up a new user.
 *
 * @author mraible
 */

@Scope("view")
@Component("signupForm")
public class SignupForm extends BasePage implements Serializable {

    private static final long serialVersionUID = 3524937486662786265L;
    public String resetEmail;
    private User user = new User();
    private Tenant tenant = new Tenant();
    private RoleManager roleManager;
    private Person person = new Person();
    private GenericManager<Person, Long> personManager;
    private GenericManager<Tenant, Long> tenantManager;
    @Autowired
    private PasswordEncryptor passwordEncryptor;
    private String from;
    private Long id;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getResetEmail() {
        return resetEmail;
    }

    public void setResetEmail(String resetEmail) {
        this.resetEmail = resetEmail;
    }

    @Autowired
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;

    }

    @Autowired
    public void setTenantManager(@Qualifier("tenantManager") GenericManager<Tenant, Long> tenantManager) {
        this.tenantManager = tenantManager;

    }

    public void changePassword(ActionEvent actionevent) throws IOException {

        passwordEncryptor.encrypt(user);

        try {
            user = userManager.save(user);
        } catch (AccessDeniedException ade) {
            // thrown by UserSecurityAdvice configured in aop:advisor
            // userManagerSecurity
            log.warn(ade.getMessage());
            getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

        addMessage("passwordChange.successfully");


    }

    public void signUp(ActionEvent event) throws Exception {

        String decryptedPassword = user.getPassword();

        initializeUser();

        try {
            tenant = tenantManager.save(tenant);
            person.getUser().setTenantId(tenant.getTenantId());
            logUserIn();
            person = personManager.save(person);
            logUserOut();

        } catch (AccessDeniedException ade) {
            // thrown by UserSecurityAdvice configured in aop:advisor
            // userManagerSecurity
            log.warn(ade.getMessage());
            getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
            // } catch (UserExistsException e) {
            // addError("errors.existing.user", new Object[] {
            // user.getUsername(), user.getEmail() });
            //
            // // redisplay the unencrypted passwords
            // user.setPassword(user.getConfirmPassword());
            // return null;
        } catch (ConstraintViolationException e) {

            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
            return;

        }

        /*
         * getSession().setAttribute(Constants.REGISTERED, Boolean.TRUE);
         *
         * // log user in automatically
         *
         * UsernamePasswordAuthenticationToken auth = new
         * UsernamePasswordAuthenticationToken(user.getUsername(),
         * user.getConfirmPassword(), user.getAuthorities());
         * auth.setDetails(user);
         *
         * SecurityContextHolder.getContext().setAuthentication(auth);
         *
         * // Send an account information e-mail
         * message.setSubject(getText("signup.email.subject"));
         */

        StatusResult result = sendVerificationMessage(user, decryptedPassword);
        addMessage("user.registered");

        if (result.getStatus().equals(Status.ERR)) {
            addError(result.getResultMessage());
            return;
        }

        return;
    }

    private void initializeUser() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.NAME_PARM, "ROLE_ADMIN");
        queryParams.put(Queries.TENANT_ID_PARM, 0L);
        List<Role> roles = roleManager.findByNamedQuery(Queries.ROLE_BY_TENANT, queryParams);

        person.setUser(user);
        person.getUser().setPerson(person);
        person.getUser().setEnabled(true);
        //person.getUser().setRoles(new HashSet<Role>(roles));
        person.setUser(passwordEncryptor.encrypt(user));
        person.getUser().setLastName(person.getLastName());
        person.getUser().setFirstName(person.getFirstName());
        person.getUser().setEmail(person.getEmail());
        person.getUser().setUsername(person.getEmail());
        person.getUser().setVerified(false);
        person.getUser().setConfirmationId(UUID.randomUUID().getLeastSignificantBits());
    }

    private void logUserOut() {
        FacesUtils.getHttpSession(false)
                .removeAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);
    }

    private void logUserIn() {

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user,
                user.getConfirmPassword(), user.getAuthorities());

        auth.setDetails(user);

        SecurityContextHolder.getContext().setAuthentication(auth);

        ((HttpSession) Faces.getExternalContext().getSession(false)).setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                SecurityContextHolder.getContext());

//        Faces.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
//
//        FacesUtils.getHttpSession(false).setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
//                SecurityContextHolder.getContext());

    }

    public void defaultUserName(ActionEvent event) {
        user.setUsername(WordUtils.capitalize(person.getFirstName().trim()) + "."
                + WordUtils.capitalize(person.getLastName().trim()));
    }

    public void resend(ActionEvent event) {

        StatusResult result = resendVerficationEmail(getCurrentUser());

        if (result.getStatus().equals(Status.ERR)) {
            addError(result.getResultMessage());
        } else {
            addMessage("signup.unVerifyResent", getCurrentUser().getEmail());
        }
    }

    private StatusResult sendVerificationMessage(User user, String password) {

        MessageTemplateSupplier supplier = new MessageTemplateSupplier();
        Message msg = new Message();

        msg.setName(getText("signup.email.subject"));

        StringBuilder builder = new StringBuilder();

        builder.append("<p><b>" + getText("signup.email.message") + "</b></p>");
        builder.append("<p><b>" + "User Name:" + user.getUsername() + "</b></p>");
        builder.append("<p><b>" + "Password:" + password + "</b></p></p>");

        builder.append("<a href=" + prop.getProperty("app.url") + "/verify/"
                + user.getConfirmationId().toString().trim() + ">" + getText("signup.emailverification") + "</a>");

        msg.setMessage(builder.toString());

        supplier.setPerson(person);
        supplier.setMessage(msg);

        log.info("Applicaiton URL:" + this.prop.getProperty("app.url"));

        supplier.setApplicationURL(RequestUtil.getAppURL(getRequest()));
        supplier.setSender(null);

        return this.sendPersonMessage(supplier, "assignmentMailTemplate.vm");

    }

    private StatusResult resendVerficationEmail(User user) {

        MessageTemplateSupplier supplier = new MessageTemplateSupplier();
        Message msg = new Message();
        Person person = new Person();

        person.setEmail(user.getEmail());
        person.setLastName(user.getLastName());
        person.setFirstName(user.getFirstName());

        msg.setName(getText("signup.resend.verification.email.subject"));

        StringBuilder builder = new StringBuilder();

        builder.append("<a href=" + prop.getProperty("app.url") + "/verify/"
                + user.getConfirmationId().toString().trim() + ">" + getText("signup.emailverification") + "</a>");

        msg.setMessage(builder.toString());

        supplier.setPerson(person);
        supplier.setMessage(msg);

        supplier.setApplicationURL(RequestUtil.getAppURL(getRequest()));
        supplier.setSender(null);

        return this.sendPersonMessage(supplier, "assignmentMailTemplate.vm");

    }

    public String getUnVerifyMessage() {
        return getText("signup.unVerifyMessage", getCurrentUser().getEmail());
    }

    public void resetPassWord(ActionEvent actionevent) {

        User user = loadUser(resetEmail);

        if (user != null) {
            StatusResult result = sendReset(user);
            if (result.getStatus().equals(Status.ERR)) {
                addError(result.getResultMessage());
            } else {
                addMessage("passwordReset.resetPasswordMessage");
            }
        } else {
            addMessage("passwordReset.resetPasswordMessage");
        }
    }

    private User loadUser(String email) {
        User user = null;

        try {
            user = userManager.getUserByEmail(resetEmail);
        } catch (UsernameNotFoundException e) {
            user = null;
        }
        return user;
    }

    private StatusResult sendReset(User user) {

        MessageTemplateSupplier supplier = new MessageTemplateSupplier();
        Message msg = new Message();
        Person person = new Person();

        person.setEmail(user.getEmail());
        person.setLastName(user.getLastName());
        person.setFirstName(user.getFirstName());

        msg.setName(getText("webapp.name") + " " + getText("passwordReset.title"));

        StringBuilder builder = new StringBuilder();

        builder.append("<p>" + getText("passwordReset.emailLine1") + "<p>");

        builder.append("<a href=" + prop.getProperty("app.url") + "/reset/" + user.getConfirmationId().toString().trim()
                + ">" + getText("passwordReset.resetEmailLink") + "</a>");

        builder.append("<p>" + getText("passwordReset.emailLine2") + "<p>");

        msg.setMessage(builder.toString());

        supplier.setPerson(person);
        supplier.setMessage(msg);

        supplier.setApplicationURL(RequestUtil.getAppURL(getRequest()));
        supplier.setSender(null);

        return this.sendPersonMessage(supplier, "assignmentMailTemplate.vm");

    }

    private User getUserByConfirmationID(Long id) {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.USER_CONFIRM_ID_PARM, id);

        List<User> user = userManager.findByNamedQuery(Queries.USER_BY_CONFIRM_ID, queryParams);

        return (user.size() != 0 ? user.get(0) : null);

    }

    @PostConstruct
    private void initilization() {

        if (from == null) {
            from = getParameter("from") == null ? "login" : getParameter("from");
        }

        if (id == null) {
            id = getParameter("id") == null ? -0L : new Long(getParameter("id"));
        }

        if (from.equals("reset")) {
            this.user = getUserByConfirmationID(id);
        }
    }

}
