package com.springthought.webapp.action;

import com.springthought.model.*;
import com.springthought.service.GenericManager;
import com.springthought.service.LookupManager;
import com.springthought.util.FacesUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.*;

@Component("groupMaintForm")
@Scope("view")
public class GroupMaintForm extends BasePage implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private GenericManager<Group, Long> groupManager;
    private GenericManager<Person, Long> personManager;
    private GenericManager<Category, Long> categoryManager;
    private GenericManager<GroupRole, Long> groupRoleManager;
    private GenericManager<GroupMember, Long> groupMemberManager;

    /**
     * @return the groupRoleManager
     */
    public GenericManager<GroupRole, Long> getGroupRoleManager() {
	return groupRoleManager;
    }

    private LookupManager lookupManager;
    /*
     * Category selected in category auto complete
     */
    private Category selectedCategory = new Category();

    /*
     * Person selected in available auto complete
     */
    private Person selectedPerson;

    private Group group = new Group();
    private Long groupId;

    private Group selectedGroup = new Group();
    private GroupMember selectedGroupMember = null;
    private boolean checked;
    private boolean memberChecked;
    private List<GroupRole> groupRoles;
    private GroupRole selectedRole = null;

    @Autowired
    public void setGroupManager(@Qualifier("groupManager") GenericManager<Group, Long> groupManager) {
	this.groupManager = groupManager;
    }

    @Autowired
    public void setCategoryManager(@Qualifier("categoryManager") GenericManager<Category, Long> categoryManager) {
	this.categoryManager = categoryManager;
    }

    @Autowired
    public void setGroupRoleManager(@Qualifier("groupRoleManager") GenericManager<GroupRole, Long> groupRoleManager) {
	this.groupRoleManager = groupRoleManager;
    }

    /**
     * @return the categoryManager
     */
    public GenericManager<Category, Long> getCategoryManager() {
	return categoryManager;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
	this.personManager = personManager;
    }

    /**
     * @return the personManager
     */
    public GenericManager<Person, Long> getPersonManager() {
	return personManager;
    }

    @Autowired
    public void setLookupManager(@Qualifier("lookupManager") LookupManager lookupManager) {
	this.lookupManager = lookupManager;
    }

    @Autowired
    public void setGroupMemberManager(
	    @Qualifier("groupMemberManager") GenericManager<GroupMember, Long> groupMemberManager) {
	this.groupMemberManager = groupMemberManager;
    }

    public Group getGroup() {
	return group;
    }

    public void setGroup(Group group) {
	this.group = group;
    }

    public void setGroupId(Long groupId) {
	this.groupId = groupId;
    }

    /**
     * @return the selectedPerson
     */
    public Person getSelectedPerson() {
	return selectedPerson;
    }

    /**
     * @param selectedPerson
     *            the selectedPerson to set
     */
    public void setSelectedPerson(Person selectedPerson) {
	this.selectedPerson = selectedPerson;
    }

    /**
     * @return the selectedCategory
     */
    public Category getSelectedCategory() {
	return selectedCategory;
    }

    /**
     * @param selectedCategory
     *            the selectedCategory to set
     */
    public void setSelectedCategory(Category selectedCategory) {

	this.selectedCategory = selectedCategory;

	// if (this.selectedCategory.getGroups().size() > 0) {
	// this.selectedGroup = this.selectedCategory.getGroups().get(0);
	// }

    }

    /**
     * @return the selectedGroupMember
     */
    public GroupMember getSelectedGroupMember() {
	return selectedGroupMember;
    }

    /**
     * @param selectedGroupMember
     *            the selectedGroupMember to set
     */
    public void setSelectedGroupMember(GroupMember selectedGroupMember) {
	this.selectedGroupMember = selectedGroupMember;
    }

    /**
     * @return the selectedRole
     */
    public GroupRole getSelectedRole() {
	return selectedRole;
    }

    /**
     * @param selectedRole
     *            the selectedRole to set
     */
    public void setSelectedRole(GroupRole selectedRole) {
	this.selectedRole = selectedRole;
    }

    public String delete() {
	groupManager.remove(group.getGroupId());
	addMessage("group.deleted");

	return "groups";
    }

    public String edit() {
	// Workaround for not being able to set the id using #{param.id} when
	// using Spring-configured managed-beans
	// if (groupId == null) {
	groupId = getParameter("groupId") == null ? 0L : new Long(getParameter("groupId"));

	// }
	// Comparison to zero (vs. null) is required with MyFaces 1.2.2, not
	// with previous versions
	if (groupId != null && groupId != 0) {
	    group = groupManager.get(groupId);
	} else {
	    group = new Group();
	}

	return "groupForm";
    }

    public String save() {

	boolean isNew = (this.selectedGroupMember.getGroupMemberId() == null
		|| this.selectedGroupMember.getGroupMemberId() == 0);

	try {

	    // this.selectedGroupMember =
	    // groupMemberManager.save(this.selectedGroupMember);

	    String key = (isNew) ? "groupMember.added" : "groupMember.updated";

	    if (isNew) {
		return "groups";
	    } else {
		addMessage(key);
		return "groups";
	    }

	} catch (ConstraintViolationException e) {

	    DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
	}

	catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new Object[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

	return "groups";
    }

    public void save(ActionEvent event) {
	this.save();
    }

    /**
     * 
     * Project: framework Method : selectCategory Return : void Params : @param
     * event Author : eairrick stores the category selected, via Category
     * AutoComplete
     */
    public void selectCategory(SelectEvent event) {
	this.selectedCategory = (Category) event.getObject();
    }

    /**
     * 
     * Project: framework Method : selectPerson Return : void Params : @param
     * event Author : eairrick
     * 
     */
    public void selectPerson(SelectEvent event) {
	log.debug("Person Selected = " + event.toString());
	this.setSelectedPerson((Person) event.getObject());
    }

    /**
     * 
     * Project: framework Method : completeCategory Return : List
     * <Category> Params : @param query Params : @return Author : eairrick
     * 
     */
    public List<Category> completeCategory(String query) {

	List<Category> allCategories = categoryManager.getAllDistinct();

	if (query.isEmpty()) {
	    return allCategories;
	}

	List<Category> filteredCategories = new ArrayList<Category>();

	for (Category category : allCategories) {
	    if (category.getName().toLowerCase().startsWith(query.toLowerCase())) {
		filteredCategories.add(category);
	    }
	}

	return filteredCategories;
    }

    public List<Person> completePerson(String query) {

	List<Person> allPersons = personManager.getAllDistinct();

	if (query.isEmpty()) {
	    return allPersons;
	}

	List<Person> filteredPersons = new ArrayList<Person>();

	for (Person person : allPersons) {
	    if (person.getName().toLowerCase().startsWith(query.toLowerCase())) {
		filteredPersons.add(person);
	    }
	}
	return filteredPersons;
    }

    public List<GroupRole> getGroupRoles() {
	return groupRoles;
    }

    /**
     * 
     * Project: framework Method : addCategory Return : void Params : @param
     * event Author : eairrick
     * 
     */
    public void addCategory(ActionEvent event) {
	this.showViewDialog("categoryForm", 650, 450);
    }

    public void addGroup(ActionEvent event) {
	this.showViewDialog("groupForm", 650, 450);
    }

    public void addPerson(ActionEvent event) {
	this.showViewDialog("/sections/person/quickAdd", 650, 350);

    }

    private void showViewDialog(String view, int width, int height) {

	Map<String, List<String>> params = new HashMap<String, List<String>>();

	List<String> parmList = new ArrayList<String>();
	List<String> catList = new ArrayList<String>();

	parmList.add("maintform");
	catList.add(Long.toString(this.selectedCategory.getCategoryId()));
	params.put("from", parmList);
	params.put("categoryId", catList);

	Map<String, Object> options = new HashMap<String, Object>();
	options.put("modal", true);
	options.put("closable", false);
	options.put("resizable", false);
	options.put("draggable", true);
	options.put("contentHeight", height);
	options.put("contentWidth", width);
	options.put("height", height);

	PrimeFaces.current().dialog().openDynamic(view, options, params);

    }

    /**
     * 
     * Project: framework Method : handleCategoryReturn Return : void Params
     * : @param event Author : eairrick
     * 
     */

    public void handleCategoryReturn(SelectEvent event) {

	Category category = (Category) event.getObject();

	if (category != null && (category.getCategoryId() != null || category.getCategoryId() != 0)) {
	    this.selectedCategory = category;
	    this.addMessage("category.groupMaintForm.added", new Object[] { this.selectedCategory.getName() });

	}

    }

    /**
     * 
     * Project: framework Method : handleGroupReturn Return : void Params
     * : @param event Author : eairrick
     * 
     */

    public void handleGroupReturn(SelectEvent event) {

	Group group = (Group) event.getObject();

	if (group != null && group.getGroupId() != null) {
	    this.selectedGroup = group;

	    // reload category with child groups
	    this.selectedCategory = categoryManager.get(this.selectedCategory.getCategoryId());

	    this.addMessage("group.groupMaintForm.added", new Object[] { this.selectedGroup.getName() });
	}
    }

    public void handlePersonReturn(SelectEvent event) {

	Person person = (Person) event.getObject();

	if (person.getPersonId() != null) {
	    this.selectedPerson = (Person) event.getObject();
	    addMemberToGroup();
	}
    }

    /**
     * @return the selectedGroup
     */
    public Group getSelectedGroup() {
	return selectedGroup;
    }

    /**
     * @param selectedGroup
     *            the selectedGroup to set
     */
    public void setSelectedGroup(Group selectedGroup) {
	this.selectedGroup = selectedGroup;
    }

    public List<Group> getGroups() {
	return this.selectedCategory.getGroups();
    }

    /**
     * @return the checked
     */
    public boolean isChecked() {
	return checked;
    }

    /**
     * @param checked
     *            the checked to set
     */
    public void setChecked(boolean checked) {
	this.checked = checked;
    }

    /**
     * @return the memberChecked
     */
    public boolean isMemberChecked() {
	return memberChecked;
    }

    /**
     * @param memberChecked
     *            the memberChecked to set
     */
    public void setMemberChecked(boolean memberChecked) {
	this.memberChecked = memberChecked;
    }

    public void delete(ActionEvent event) {
	groupManager.remove(selectedGroup.getGroupId());
	addMessage("group.deleted");
	checked = false;
    }

    public void radioSelected(SelectEvent event) {
	this.selectedGroup = (Group) event.getObject();
	checked = true;
    }

    public void radioSelectedMember(SelectEvent event) {
	this.memberChecked = true;
    }

    public void addMemberToGroup() {

	GroupMember member = new GroupMember();

	if (selectedGroup.getGroupId() == null || selectedGroup.getGroupId() == 0L) {
	    selectedGroup = ((GroupForm) FacesUtils.getManagedBean("groupForm")).getGroup();
	}

	member.setPerson(selectedPerson);
	member.setGroup(this.selectedGroup);
	member.setGroupRole(this.selectedRole);
	member.setActive(true);
	member.setVersion(0);

	if (doesMemberExistAlready(member)) {
	    addError("groupMember.exist", new String[] { member.getPerson().getName(), this.selectedGroup.getName() });
	} else {

	    getSelectedGroup().getGroupMembers().add(0, member);
	    selectedGroupMember = member;
	    addMessage("groupMembers.add", new String[] { member.getPerson().getName(), this.selectedGroup.getName() });
	    this.save();
	}

    }

    public void addMemberToGroup(ActionEvent event) {
	this.addMemberToGroup();
    }

    private boolean doesMemberExistAlready(GroupMember member) {

	boolean exist = false;
	for (GroupMember gm : this.getSelectedGroup().getGroupMembers()) {
	    if (gm.getPerson().getPersonId() == member.getPerson().getPersonId()) {
		exist = true;
		break;
	    }
	}
	return exist;
    }

    public void onCellEdit(CellEditEvent event) {

	Object oldValue = event.getOldValue();
	Object newValue = event.getNewValue();

	if (newValue != null && !newValue.equals(oldValue)) {
	    // this.save();
	}
    }

    public void rowEdit(RowEditEvent event) {
	GroupMember member = (GroupMember) event.getObject();
	this.selectedGroupMember = member;
	log.debug("Group Member = " + member.getPerson().getName());
	this.save();

    };

    @PostConstruct
    private void initScreen() {

	List<Category> cats = categoryManager.getAllDistinct();

	if (cats.size() > 0) {
	    this.setSelectedCategory(cats.get(0));
	}

	groupRoles = groupRoleManager.getAllDistinct();

//	selectedRole = Iterables.find(groupRoles, new Predicate<GroupRole>() {
//	    @Override
//	    public boolean apply(GroupRole arg) {
//		return arg.getGrouproleId().equals(-3L);
//	    }
//	}, null);

    }
}