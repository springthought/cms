
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Location;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("locationList")
@Scope("view")
public class LocationList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6919116168741270334L;
    /**
     *
     */

    private String query;
    private GenericManager<Location, Long> locationManager;

    private Location selectedLocation = new Location();
    private boolean checked;

    @Autowired
    public void setLocationManager(@Qualifier("locationManager") GenericManager<Location, Long> locationManager) {
	this.locationManager = locationManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public LocationList() {
	setSortColumn("locationId"); // sets the default sort column
    }

    public List<Location> getLocations() {
	try {
	    return locationManager.search(query, Location.class);
	} catch (SearchException se) {
	    addError(se.getMessage());
	    return sort(locationManager.getAll());
	}
    }

    public String search() {
	return "success";
    }

    public Location getSelectedLocation() {
	return selectedLocation;
    }

    public void setSelectedLocation(Location location) {
	this.selectedLocation = location;
    }

    public void delete(ActionEvent event) {


	Location location = locationManager.get(selectedLocation.getLocationId());

	try {
	    locationManager.remove(location.getLocationId());
		addMessage("location.deleted");

	} catch (DataIntegrityViolationException e) {

	    DataIntegrityViolationExceptionHandler(e);

	} catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}
    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }
}
