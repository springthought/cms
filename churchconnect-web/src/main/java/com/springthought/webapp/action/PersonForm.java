package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.CategoryType;
import com.springthought.Constants.CodeListType;
import com.springthought.Constants.FamilyStatusType;
import com.springthought.model.*;
import com.springthought.service.*;
import com.springthought.util.ConvertUtil;
import com.springthought.util.GeoCoder;
import com.springthought.util.Queries;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.hibernate.StaleObjectStateException;
import org.hibernate.exception.ConstraintViolationException;
import org.imgscalr.Scalr;
import org.primefaces.PrimeFaces;
import org.primefaces.component.tabview.TabView;
import org.primefaces.component.wizard.Wizard;
import org.primefaces.event.*;
import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.*;
import org.primefaces.model.map.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


@Component("personForm")
@Scope("view")
public class PersonForm extends BasePage implements Serializable {

    private static final long serialVersionUID = -3332606155399517059L;
    private static final String IMAGE_FILE_NAME = "headoutline_1.png";
    private static final Object TAB_LOGIN_ID = "tbLogin";
    private final String TAB_ATTENDANCE_ID = "tbAttendance";
    private final String TAB_CLASSES_ID = "tbClasses";
    private final String TAB_FAMILY_ID = "tbFamily";
    private final String TAB_CONTRIBUTIONS = "tbContributions";
    private GenericManager<Person, Long> personManager;
    private GenericManager<Household, Long> householdManager;
    private GenericManager<CustomMeta, Long> customMetaManager;
    private GenericManager<Contribution, Long> contributionManager;
    private GenericManager<Batch, Long> batchManager;
    private LookupManager lookupManager;
    private Contribution selectedContribution;
    private Person person = new Person();
    private Batch batch = new Batch();
    private Long personId;
    private DefaultStreamedContent dbImage = null;
    private ByteArrayContent picture = null;
    private DefaultStreamedContent personImage = null;
    private NativeUploadedFile uploadedFile;
    private CroppedImage croppedImage;
    private String defaultImageFile = null;
    private String savedImageFile = null;
    private Wizard wizard = null;
    private String step = "main";


    private GeoCoder geocoder = new GeoCoder();
    //private GeoCoder geocoder = null;
    private String center = "36.890257,30.707417";
    private MapModel markerModel;
    private MapModel simpleModel;
    private Person selectedPerson = new Person();
    private String selectedRelationship;
    private Household selectedHousehold;
    private Contribution entryContribution = new Contribution();
    private Pledge selectedPledge;

    private boolean addressComplete = false;
    private boolean disableButtons = true;
    private boolean required = false;
    private boolean rowselected = false;

    private List<Household> households;
    private Household newHousehold = new Household();
    private List<CustomField> fields = new ArrayList<CustomField>(0);

    private Category groupMemberCategory = new Category();
    private String from;
    private GroupMember selectedGroupMember;
    private GroupMember initGroupMember = new GroupMember();
    private Pledge initPledge = new Pledge();

    private String template = "/templates/mainMenu.xhtml";
    private GenericManager<Category, Long> categoryManager;
    private GenericManager<GroupMember, Long> groupMemberManager;
    private String currentTab = "";
    private ArrayList<LabelValue> usStates = new ArrayList<LabelValue>(0);

    private boolean requiredFamily = false;

    private GenericManager<PersonBin, Long> personBinManager;
    private List<Person> people;

    private List<Household> familyIDs = new ArrayList<Household>(0);
    private ArrayList<Person> familyMembersRemoved = new ArrayList<Person>(0);


    private ArrayList<Person> tempFamilyMembers = new ArrayList<Person>(0);

    private GenericManager<Attendance, Long> attendanceManager;

    private RoleManager roleManager;
    private Map<String, String> availableRoles;
    private String[] userRoles;

    public String getUrlinclude() {
        return Urlinclude;
    }

    public void setUrlinclude(String urlinclude) {
        Urlinclude = urlinclude;
    }

    private String Urlinclude = "/sections/person/contact.xhtml";


    private FileLink selectedFileLink;

    @Autowired
    private PasswordEncryptor passwordEncryptor;

    @Autowired
    private SecurityContextManager securityContext;

    private TabView tabViewFileLink;

    @Autowired
    public void setRoleManager(@Qualifier("roleManager") RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    @Override
    public DataService getDataService() {

        DataService dataService = super.getDataService();

        return dataService;
    }

    public void addClassMember(ActionEvent event) {
        addGroupToCollection();
    }


    /**
     * @param event
     */
    public void addFamily(ActionEvent event) {


        if (!this.newHousehold.getName().trim().isEmpty()) {
            PrimeFaces.current().ajax().addCallbackParam("valid", true);

            // Validate the name is unique
            HashMap<String, Object> queryParams = new HashMap<String, Object>();
            queryParams.put(Queries.HOUSEHOLD_NAME, this.newHousehold.getName().trim().toLowerCase());
            List<Household> search = householdManager.findByNamedQuery(Queries.HOUSEHOLD_FIND_BY_NAME, queryParams);

            if (search.size() != 0) {
                PrimeFaces.current().ajax().addCallbackParam("valid", false);
                addError("errors.existing.household", this.newHousehold.getName().trim());

            }

        } else {
            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            addError("errors.required", getText("household.householdName"));

        }

    }

    /**
     * @param event
     */
    public void addPledge(ActionEvent event) {

        boolean retval = initPledge.getPledgeAmt() != null && initPledge.getCampaign() != null;

        PrimeFaces.current().ajax().addCallbackParam("valid", retval);

        if (retval) {
            initPledge.setPerson(getPerson());
            this.getPerson().getPledges().add(initPledge);
            selectedPledge = initPledge;
            initPledge = new Pledge();
        }

    }

    public void removePledge(ActionEvent event) {
        person.getPledges().remove(selectedPledge);
        selectedPledge = null;

    }


    public void addGroupMember(ActionEvent event) {
        addGroupToCollection();
    }

    private void addGroupToCollection() {

        selectedGroupMember = initGroupMember;
        selectedGroupMember.setPerson(getPerson());

        int idx = person.getGroupMembers().indexOf(selectedGroupMember);

        if (idx == -1) {
            person.getGroupMembers().add(0, selectedGroupMember);
        } else {
            person.getGroupMembers().set(idx, selectedGroupMember);
        }
        initGroupMember = new GroupMember();
    }

    /**
     * @param event
     */

    public void addPersonToExistingFamily(SelectEvent event) {

        Object object = event.getObject();

        if (selectedHousehold != null) {
            try {
                Household household = householdManager.get(selectedHousehold.getHouseholdId());
                household.getPersons().size();
                household.getPersons().add(person);
                person.setHousehold(household);
                //selectedHousehold = null;
            } catch (Exception e) {
                UUID guid = UUID.randomUUID();
                String msg = getText("errors.family.unable.load", selectedHousehold.getName());
                addError("errors.generalApplicationError", new Object[]{msg, guid.toString()});
                log.error(guid.toString(), e);
            }
        }
    }

    /**
     * @param event
     */
    public void removePersonFromExistingFamily(UnselectEvent event) {

        Object object = event.getObject();
        System.out.println(object.toString());

    }

    public void addPersonToFamily(ActionEvent event) {
        selectedPerson.setRelationship(selectedRelationship);
        selectedPerson.setEmail(selectedPerson.getEmail().isEmpty() ? null : selectedPerson.getEmail());
        tempFamilyMembers.add(selectedPerson);
        selectedPerson = new Person();
        selectedRelationship = null;

    }

    public void flushFamilyMember(ActionEvent event) {

        try {

            if (person.getHousehold().getHouseholdId() == null || person.getHousehold().getHouseholdId() == 0) {
            /*    Household family = householdManager.save(person.getHousehold());
                person.setHousehold(family);*/
            }
            tempFamilyMembers.forEach(member -> {
                member.setHousehold(person.getHousehold());
                person.getHousehold().getPersons().add(member);
            });

            person.getHousehold().getPersons().forEach(person1 -> System.out.println(person1.getFirstName()));
            //person.getHousehold().getPersons().forEach(person1 -> System.out.println(person1.getHousehold().getName()));

            addMessage("person.addToFamily",
                    new String[]{String.valueOf(tempFamilyMembers.size()), person.getHousehold().getName()});

            tempFamilyMembers = new ArrayList<Person>(0);


        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            String msg = getText("errors.family.unable.create", selectedHousehold.getName());
            addError("errors.generalApplicationError", new Object[]{msg, guid.toString()});
            log.fatal(guid.toString(), e);
        }

    }

    public boolean isEmptyTempFamilyMembers() {
        return (tempFamilyMembers.isEmpty());
    }

    public List<Group> getAllGroupClasses() {
        return this.getDataService().getAllGroupClasses(currentUserTenantContext());
    }

    public void clearSelection(ActionEvent event) {
        this.selectedGroupMember = null;
    }

    public void close(ActionEvent event) {

        PrimeFaces.current().dialog().closeDynamic(new Person());
        //RequestContext.getCurrentInstance().closeDialog(new Person());
    }


    public String onFlowProcess(FlowEvent event) {
        return this.step;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public Wizard getWizard() {
        return wizard;
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
    }


    public Object handleCloseContribDialog() {
        entryContribution = new Contribution();
        return null;
    }


    public Object handleCloseClassDialog() {
        initGroupMember = new GroupMember();
        return null;
    }

    public Object handleClosePledgeDialog() {
        initPledge = new Pledge();
        return null;
    }

    public Object handleCloseAddMemberDialog() {
        tempFamilyMembers = new ArrayList<Person>(0);
        step = "main";
        return null;
    }


    public List<Household> completeHousehold(String query) {

        List<Household> suggestions = new ArrayList<Household>();

        for (Household h : households) {

            if (h.getName().toLowerCase().startsWith(query.toLowerCase()))
                suggestions.add(h);
        }

        return suggestions;
    }

    public ArrayList<Person> getTempFamilyMembers() {
        return tempFamilyMembers;
    }

    public void setTempFamilyMembers(ArrayList<Person> tempFamilyMembers) {
        this.tempFamilyMembers = tempFamilyMembers;
    }

    public List<Person> completePeople(String query) {

        List<Person> suggestions = new ArrayList<Person>();
        for (Person p : people) {
            if (p.getName().toLowerCase().contains(query.toLowerCase()))
                if (p.getHousehold() == null) {
                    suggestions.add(p);
                }
        }
        return suggestions;
    }

    /**
     * @param event
     */
    public void convertAddress(ActionEvent event) {

        markerModel = new DefaultMapModel();

        LatLng latlng = geocoder.retrieveAddressLatitudeAndLongitude(this.person.getAddress());

        if (geocoder.getStatus().equals(Constants.GeocodingStatusCode.OK)) {
            markerModel.addOverlay(new Marker(latlng, person.getName(), person));
            person.getAddress().setLatitude(latlng.getLat());
            person.getAddress().setLongitude(latlng.getLng());
            this.center = latlng.getLat() + ", " + latlng.getLng();
            PrimeFaces.current().ajax().addCallbackParam("valid", true);
            PrimeFaces.current().ajax().addCallbackParam("msg", Constants.GeocodingStatusCode.OK.toString());
        } else {
            UUID guid = UUID.randomUUID();
            String msg = getText("errors.generalApplicationError", new Object[]{geocoder.getStatus(), guid.toString()});
            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            PrimeFaces.current().ajax().addCallbackParam("msg", msg);
        }
    }

    public void createHousehold(ActionEvent event) {

        Household family = new Household();

        family.setAddress(person.getAddress());
        family.setName(person.getName().trim() + " " + getText("persontab.family"));
        family.setStatus(FamilyStatusType.ACTIVE);
        family.setStatusDate(new Date());
        family.setAddress(person.getAddress());

        person.setRelationship("HH");
        person.setHeadofHouse(true);
        person.setHousehold(family);
        person.getHousehold().getPersons().add(person);

        this.households.add(family);

    }

    /**
     * Project: framework Method : cropImage Return : void Params : @param event
     * Author : Eairrick L. Kinsey Crops uploaded image and delete temps files.
     */

    public void crop() {
        this.crop(new ActionEvent(null));
    }

    public void crop(ActionEvent event) {

        deleteTempFile();

        savedImageFile = generateTempFileName();

        try (FileImageOutputStream imageOutput = new FileImageOutputStream(new File(savedImageFile));) {
            imageOutput.write(croppedImage.getBytes(), 0, croppedImage.getBytes().length);
            // imageOutput.close();
        } catch (Exception e) {
            log.fatal(e);
            throw new FacesException("Error in writing image.", e);
        }

        addMessage("imageDialog.cropped");
    }

    public String delete() {
        personManager.remove(person.getPersonId());
        addMessage("person.deleted");

        return "persons";
    }

    public void deleteContribution(ActionEvent event) {
        person.getContributions().remove(selectedContribution);
    }

    /**
     * Project: framework Method : deleteTempFile Return : void Params : Author
     * : eairrick
     */
    private void deleteTempFile() {
        try {

            if (savedImageFile != null) {
                Boolean fileDeleted = Files.deleteIfExists(new File(savedImageFile).toPath());
                log.debug(savedImageFile + " deleted : " + fileDeleted);
            }
        } catch (IOException e) {
            log.fatal("Unable to delete file:" + savedImageFile, e);
            throw new FacesException("Error in writing image.", e);
        }

        savedImageFile = null;

    }


    public void disableRequired() {
        this.required = false;
    }

    /**
     * Project: framework Method : done Return : void Params : @param event
     * Author : eairrick
     */
    public void done(ActionEvent event) {

        try {

            byte[] contents = Files.readAllBytes(new File(savedImageFile).toPath());

            if (person.getPersonBin() == null) {
                PersonBin bin = new PersonBin();
                person.setPersonBin(bin);
                bin.setPerson(person);
            }

            person.getPersonBin().setContents(contents);
            person.getPersonBin().setSize(Long.valueOf(contents.length));
            person.getPersonBin().setContentType(uploadedFile.getContentType());
            person.getPersonBin().setFileName(uploadedFile.getFileName());

        } catch (IOException e) {
            log.fatal("Unable to read temp file into byte array for DAO persistants", e);
            throw new FacesException("Error in writing image.", e);
        } finally {
            deleteTempFile();
        }

    }

    /**
     * Project: framework Method : edit Return : String Params : @return Author
     * : eairrick
     */
    @SuppressWarnings("unchecked")
    public String edit() {

        return "personForm";
    }

    public void editGroupMemberRow(RowEditEvent event) {
        // not needed
    }

    public void enableRequired() {
        this.required = true;
    }

    public void eventSelected(OverlaySelectEvent evt) {
        Overlay overlay = evt.getOverlay();
        this.selectedPerson = (Person) overlay.getData();
    }

    /**
     * Project: framework Method : generateDefaultImage Return :
     * DefaultStreamedContent Params : @return Author : eairrick
     */

    private DefaultStreamedContent generateDefaultImage() {

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
                .getContext();

        defaultImageFile = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "images"
                + File.separator + IMAGE_FILE_NAME;

        try {

            personImage = new DefaultStreamedContent(new FileInputStream(new File(defaultImageFile)), "image/png",
                    IMAGE_FILE_NAME);

        } catch (Exception e) {
            log.fatal(e);
            throw new FacesException("Error in writing file image.", e);
        }
        return personImage;
    }

    private ByteArrayContent generateDefaultPicture() throws FacesException {

        ByteArrayContent pic = null;

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
                .getContext();

        defaultImageFile = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "images"
                + File.separator + IMAGE_FILE_NAME;

        try {

            pic = new ByteArrayContent(Files.readAllBytes(Paths.get(defaultImageFile)), "image/png", IMAGE_FILE_NAME);

        } catch (Exception e) {
            log.fatal(e);
            throw new FacesException("Error in writing file image.", e);
        }
        return pic;
    }

    /**
     * Project: framework Method : generateImage Return : DefaultStreamedContent
     * Params : @return Author : eairrick
     */
    private DefaultStreamedContent generateImage() {

        try {
            if (person.getPersonBin() != null && person.getPersonBin().getContents() != null) {
                dbImage = new DefaultStreamedContent(person.getPersonBin().getInputstream(),
                        person.getPersonBin().getContentType(), person.getPersonBin().getFileName());
            } else {
                dbImage = generateDefaultImage();

            }
        } catch (IOException e) {
            log.fatal(e);
            throw new FacesException("Error in writing image.", e);
        }

        return dbImage;

    }

    /**
     * Project: framework Method : generateTempFileName Return : Object Params
     * : @return Author : eairrick
     */
    private String generateTempFileName() {

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
                .getContext();

        return servletContext.getRealPath("") + File.separator + "resources" + File.separator + "images"
                + File.separator + "temp" + File.separator + UUID.randomUUID().toString().replace("-", "") + "."
                + FilenameUtils.getExtension(uploadedFile.getFileName());

    }

    public String getCenter() {
        return center;
    }

    public GenericManager<Contribution, Long> getContributionManager() {
        return contributionManager;
    }

    /**
     * cropped Image is the image stored to the PersonBinDAO.
     *
     * @return the croppedImage
     */
    public CroppedImage getCroppedImage() {
        return croppedImage;
    }

    /**
     * @return the currentTab
     */
    public String getCurrentTab() {
        return currentTab;
    }

    /**
     * Project: framework Method : getDefaultImagePath Return : String Params
     * : @return Author : eairrick
     */
    public String getDefaultImagePath() {

        String imageLocation = savedImageFile == null ? "/resources/images/" + IMAGE_FILE_NAME
                : "/resources/images/temp/" + FilenameUtils.getName(savedImageFile);
        log.info("DefaultImagePath = " + imageLocation);
        return imageLocation;

    }

    public Contribution getEntryContribution() {
        return entryContribution;
    }

    @SuppressWarnings("unchecked")
    public List<LabelValue> getEthnicGroup() {
        this.setAscending(true);
        return sort(lookupManager.getSystemValueByCodeListType(CodeListType.ENTHIC));
    }

    /**
     * @return the fields
     */
    public List<CustomField> getFields() {
        return getPerson().getCustomFields();
    }

    public String getFrom() {
        return from;
    }

    public StreamedContent getFile() {

        DefaultStreamedContent content = new DefaultStreamedContent(
                new ByteArrayInputStream(selectedFileLink.getContents()), selectedFileLink.getContentType(),
                selectedFileLink.getName());

        return content;

    }

    @SuppressWarnings("unchecked")
    public List<LabelValue> getGenders() {
        List<LabelValue> values = lookupManager.getSystemValueByCodeListType(CodeListType.GENDER);
        return values;
    }

    public void handleFileDialogClose(ActionEvent event) {
        setSelectedFileLink(null);
    }

    public void removeFile(ActionEvent event) {
        boolean isRemoved = person.getFileLinks().remove(this.selectedFileLink);
        log.debug("Succesfully removed " + selectedFileLink.getName() + ":" + isRemoved);
    }

    /**
     * @return the groupMemberCategory
     */
    public Category getGroupMemberCategory() {
        return groupMemberCategory;
    }

    public char getHouseholdGroup(Household household) {
        return (person == null ? '\0' : household.getName().charAt(0));
    }

    /**
     * @return the households
     */
    public List<Household> getHouseholds() {
        return households;
    }

    /**
     * Project: framework Method : getImageUploaded Return : Boolean Params
     * : @return Author : eairrick
     */

    public Boolean getImageUploaded() {
        return savedImageFile != null;
    }

    /**
     * @return the initGroupMember
     */
    public GroupMember getInitGroupMember() {
        return initGroupMember;
    }

    @SuppressWarnings("unchecked")
    public List<LabelValue> getLifeStages() {
        this.setAscending(true);
        return sort(lookupManager.getSystemValueByCodeListType(CodeListType.STAGEOFLIFE));
    }

    @SuppressWarnings("unchecked")
    public List<LabelValue> getMaritalStatues() {
        this.setAscending(true);
        return sort(lookupManager.getSystemValueByCodeListType(CodeListType.MARITAL));
    }

    /**
     * @return the markerModel
     */
    public MapModel getMarkerModel() {
        return markerModel;
    }

    @SuppressWarnings("unchecked")
    public List<LabelValue> getMemberTypes() {
        this.setAscending(true);
        return sort(lookupManager.getSystemValueByCodeListType(CodeListType.STATUS));
    }

    /**
     * @return the newHousehold
     */
    public Household getNewHousehold() {
        return newHousehold;
    }

    public List<Person> getPeople() {
        return people;
    }

    public Person getPerson() {
        return person;
    }

    public char getPersonGroup(Person person) {
        return (person == null ? '\0' : person.getFirstName().charAt(0));
    }

    public List<GroupMember> getPersonGroupClasses() {

        List<GroupMember> gm = person.getGroupMembers().stream()
                .filter(m -> m.getGroup().getCategory().getCategoryType() == CategoryType.CLASS)
                .collect(Collectors.toList());

        return gm;

    }

    public List<GroupMember> getPersonGroups() {

        List<GroupMember> gm = person.getGroupMembers().stream()
                .filter(m -> m.getGroup().getCategory().getCategoryType() != CategoryType.CLASS)
                .collect(Collectors.toList());

        return gm;

    }

    public StreamedContent getPersonImage() {
        return generateImage();
    }

    public StreamedContent getPersonPicture() {

        if (person.getPersonBin() != null && person.getPersonBin().getContents() != null) {

            picture = new ByteArrayContent(person.getPersonBin().getContents(), person.getPersonBin().getContentType(),
                    person.getPersonBin().getFileName());

        } else {

            picture = generateDefaultPicture();

        }

        return picture;
    }

    @SuppressWarnings("unchecked")
    public List<LabelValue> getRelationships() {
        this.setAscending(true);
        return sort(lookupManager.getSystemValueByCodeListType(CodeListType.RELATIONSHIP));
    }

    public Contribution getSelectedContribution() {
        return selectedContribution;
    }

    public GroupMember getSelectedGroupMember() {
        return selectedGroupMember;
    }

    public Household getSelectedHousehold() {
        return selectedHousehold;
    }

    /**
     * @return the selectedPerson
     */
    public Person getSelectedPerson() {
        return selectedPerson;
    }

    public String getSelectedRelationship() {
        return selectedRelationship;
    }

    /**
     * @return the simpleModel
     */
    public MapModel getSimpleModel() {
        return simpleModel;
    }

    public String getTemplate() {
        return template;
    }

    public ArrayList<LabelValue> getUsStates() {
        return usStates;
    }

    public Pledge getSelectedPledge() {
        return selectedPledge;
    }

    public void setSelectedPledge(Pledge selectedPledge) {
        this.selectedPledge = selectedPledge;
    }

    /**
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) {

        uploadedFile = (NativeUploadedFile) event.getFile();
        saveUploadedImageToDisk();
        addMessage("uploadForm.sucessfull.upload", uploadedFile.getFileName());
        disableButtons = false;

    }

    public void handlePersonFileUpload(FileUploadEvent event) {

        FileLink fileLink = new FileLink(event.getFile());

        if (selectedFileLink == null) {
            person.getFileLinks().add(fileLink);
            addMessage("fupload", "uploadForm.sucessfull.upload", event.getFile().getFileName());
        } else {

            if (person.getFileLinks().remove(this.selectedFileLink)) {
                person.getFileLinks().add(fileLink);
                addMessage("fupload", "uploadForm.updated.upload", event.getFile().getFileName());
            }
        }

    }

    /**
     *
     */
    public void initCustomField() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CUSTOMMETA_CHECK_EXISTS_PERSONID_PARM, this.getPerson().getPersonId());
        queryParams.put(Queries.CUSTOMMETA_CHECK_EXISTS_USAGE_PARM, Constants.CustomFieldType.PERSON.getValue());

        List<CustomMeta> customMetas = this.customMetaManager.findByNamedQuery(Queries.CUSTOMMETA_CHECK_EXISTS,
                queryParams);

        for (CustomMeta customMeta : customMetas) {

            CustomField customField = new CustomField();
            customField.setPerson(getPerson());
            customField.setCustomFieldId(0L);
            customField.setCustomMeta(customMeta);
            customField.setUID(customMeta.getUID());

            switch (customMeta.getDataType()) {
                case Constants.DATA_TYPE_TEXT:
                case Constants.DATA_TYPE_LIST:
                    customField.setTextValue(customMeta.getDefaultValue());
                    break;
                case Constants.DATA_TYPE_NUMBER:
                    customField.setNumberValue(customMeta.getDefaultValueNumber());
                    break;
                case Constants.DATA_TYPE_DATE:
                    customField.setDateValue((customMeta.getDefaultValueDate() == null ? new Date() : customMeta.getDefaultValueDate()));
                    break;
                default:
                    customField.setBooleanValue(customMeta.getDefaultValueBoolean());
                    break;
            }
            getPerson().getCustomFields().add(customField);
        }

    }

    /**
     * Initialized Person Model for all tabs.
     *
     * @param event
     */
    public void initialize(ActionEvent event) {

        retrieveClassCategory();
        prepareUserEdit();
        initUser();
    }

    @PostConstruct
    private void initialization() {

        from = getParameter("from") == null ? "" : getParameter("from");
        personId = getParameter("personId") == null ? 0L : new Long(getParameter("personId"));

        // verify that the user has access to the passed in person id
        // this will avoid elevated privilege hacks.
        if (from.equals("self") && currentUser().getPerson().getPersonId().compareTo(personId) != 0) {
            throw new AccessDeniedException("403 returned");
        }

        if (personId != null && personId != 0) {
            try {
                person = personManager.get(personId);
                fetchLazyCollections(person);
            } catch (Exception e) {
                log.fatal(e.getMessage(), e);
            }
        } else {
            person = new Person();
            person.setPersonBin(new PersonBin());
            person.setNotes(new HashSet<Note>(0));
            person.setFirstName("");
            person.setLastName("");
        }

        initCustomField();
        retrieveClassCategory();
        prepareUserEdit();
        initUser();
        initFormData();


        if (from.equals("maintform")) {
            this.template = "/templates/mainNoMenu.xhtml";
        }

        for (String code : Constants.listOfUSStatesCode) {
            usStates.add(new LabelValue(Constants.mapOfUSStates.get(code), code));
        }

        Collections.sort(usStates, (s1, s2) -> s2.getValue().compareTo(s1.getValue()));
        Collections.reverse(usStates);

    }

    private void initFormData() {

        if (households == null) {
            households = householdManager.getAllDistinct();
            households.size();
        }

        if (people == null) {
            people = personManager.getAllDistinct();
            people.size();
        }

        familyIDs = new ArrayList<Household>(0);
        familyMembersRemoved = new ArrayList<Person>(0);


    }

    private static void fetchLazyCollections(Person person) {

        if (Optional.ofNullable(person.getHousehold()).isPresent()) {
            person.getHousehold().getPersons().size();
        }
        person.getAttendances().size();
        person.getIndiviualConnections().size();
        person.getContributions().size();
        person.getGroupMembers().size();
        person.getPledges().size();
        person.getNotes().size();
    }

    /**
     * Convenience method for view templates to check if the user is logged in
     * with RememberMe (cookies).
     *
     * @return true/false - false if user interactively logged in.
     */
    public boolean isRememberMe() {
        if (person.getUser() != null && person.getUser().getId() == null)
            return false; // check for add()

        AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
        SecurityContext ctx = SecurityContextHolder.getContext();

        if (ctx != null) {
            Authentication auth = ctx.getAuthentication();
            return resolver.isRememberMe(auth);
        }
        return false;
    }

    /**
     *
     */
    private void initUser() {

	/*if (person.getUser() == null) {

	    person.setUser(new User());
	    person.getUser().setPerson(person);
	    person.getUser().setEnabled(true);

	    person.getUser().addRole(new Role(Constants.USER_ROLE));
	    person.getUser().setUsername(WordUtils.capitalize(person.getFirstName().trim()) + "."
		    + WordUtils.capitalize(person.getLastName().trim()));

	}*/
    }

    private void prepareUserEdit() {

        /*
         * // if a user's id is passed in if (id != null) { log.debug(
         * "Editing user, id is: " + id); // lookup the user using that id user
         * = userManager.getUser(id); } else { user =
         * userManager.getUserByUsername(request.getRemoteUser()); }
         */

        if (person.getUser() != null && person.getUser().getUsername() != null) {

            person.getUser().setConfirmPassword(person.getUser().getPassword());

            if (isRememberMe()) {
                // if user logged in with remember me, display a warning that
                // they can't change passwords
                log.debug("checking for remember me login...");
                log.trace("User '" + person.getUser().getUsername() + "' logged in with cookie");
                addMessage("userProfile.cookieLogin");
            }
        }

    }

    /**
     * @return the addressComplete
     */
    public boolean isAddressComplete() {

        addressComplete = !person.getAddress().getAddress().trim().isEmpty()
                && !person.getAddress().getCity().trim().isEmpty()
                && !person.getAddress().getPostalCode().trim().isEmpty()
                && !person.getAddress().getProvince().trim().isEmpty();

        return addressComplete;
    }

    public boolean isDisableButtons() {
        return disableButtons;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    public boolean isRequiredFamily() {
        return requiredFamily;
    }

    public boolean isRowselected() {
        return rowselected;
    }

    public void onRowEdit(RowEditEvent event) {

        Person p = (Person) event.getObject();
        addMessage("person.relationshipUpdate", " " + p.getName());
/*

        if (p != person) {
            personManager.save(p);
        }
*/
    }

    public void onRowSelect(SelectEvent event) {
        this.rowselected = true;
    }

    public void populateGroups(AjaxBehaviorEvent event) {
        initGroupMember.setGroup(null);

    }

    public void populateGroups(String s) {
        log.info(s);
    }

    public void processContribution(ActionEvent event) {


        selectedContribution = entryContribution;
        selectedContribution.setPerson(person);
        person.getContributions().add(selectedContribution);
        batch.setDirty(true);
        entryContribution = new Contribution();

    }

    public void removeFamily(ActionEvent event) {

        addMessage("household.deleteFamily", person.getHousehold().getName());

        Long familyID = person.getHousehold().getHouseholdId();
        Household family = person.getHousehold();


        ArrayList<Person> people = new ArrayList<Person>(person.getHousehold().getPersons());

        for (Person p : people) {
            family.getPersons().remove(p);
            p.setHousehold(null);

//           @todo Not sure if I need this save; remove collection and references should be enough
            if (!p.equals(this.person)) {
                personManager.save(p);
            }
        }

        checkFamilyForDeletion(family);

    }

    public void removeGroupMember(ActionEvent event) {
        person.getGroupMembers().remove(selectedGroupMember);
        this.selectedGroupMember = null;


    }

    /**
     * Project: framework Method : removeImage Return : void Params : @param
     * event Author : eairrick
     */
    public void removeImage(ActionEvent event) {

        if (getPerson().getPersonBin() != null) {
            getPerson().getPersonBin().setContents(null);
            getPerson().setPersonBin(null);
            generateImage();
        }
        deleteTempFile();
        addMessage("imageDialog.removed");
        disableButtons = true;
    }

    public void removeMember(Person p) {

        addMessage("person.removedFromFamily", new String[]{p.getName(), p.getHousehold().getName()});

        boolean deleted = person.getHousehold().getPersons().remove(p);
        Household family = person.getHousehold();


        // check to see if you are removing the current person from family
        checkFamilyForDeletion(family);

        if (p.equals(person) || (p.getPersonId() == null || p.getPersonId() == 0)) {
            person.setHousehold(null);
        } else {
            p.setHousehold(null);
            familyMembersRemoved.add(p);
        }

    }

    /**
     * Mark family for deletion.
     *
     * @param family
     */
    private void checkFamilyForDeletion(Household family) {

        if (family != null && !family.hasFamilyMember()) {
            familyIDs.add(family);

            boolean delete = households.remove(family);

            log.info("delete family :" + family.getName() + " = " + delete);

            Household found = households.stream()
                    .peek(p -> System.out.println("filter=" + p.getHouseholdId()))
                    .filter(household -> household.getHouseholdId().equals(family.getHouseholdId()))
                    .findFirst()
                    .orElse(null);

            log.info(found);

        }
    }

    public void required(Boolean on) {
        setRequiredFamily(on);
    }

    /**
     * @param event
     */
    public void resized(ActionEvent event) {

        deleteTempFile();
        saveUploadedImageToDisk(240, 200);
        addMessage("imageDialog.resized");

    }

    private void retrieveClassCategory() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.CATEGORY_TYPE_PARM, CategoryType.CLASS.toString());
        List<Category> categories = categoryManager.findByNamedQuery(Queries.CATEGORY_FIND_BY_TYPE, queryParams);
        setGroupMemberCategory(categories.get(0));

    }

    public TabView getTabViewFileLink() {
        return tabViewFileLink;
    }

    public void setTabViewFileLink(TabView tabViewFileLink) {
        this.tabViewFileLink = tabViewFileLink;
    }

    /**
     * Project: framework Method : save Return : String Params : @return Author
     * : eairrick
     *
     * @throws IOException
     */

    public String save() {


        boolean isNew = (person.getPersonId() == null || person.getPersonId() == 0);

        boolean isNewUser = (person.getUser() != null
                && (person.getUser().getId() == null || person.getUser().getId() == 0));

        String savePassword = "";

        try {

            /**
             * Batch Total is only > 0 if a contribution has been added or update during the view session; thus I can use this field as a dirty flag.
             */
            if (batch.isDirty()) {
                batch = batchManager.save(batch);
                person.getContributions().forEach(contribution -> {
                    if (contribution.getBatch() == null) {
                        contribution.setBatch(batch);
                    }
                });

            }


            // check to see if we have an household

            if (person.getHousehold() != null) {
                // keep house house address in sync w/ primary member aka head
                // of house
                if (Optional.ofNullable(person.getHeadofHouse()).isPresent()) {
                    person.getHousehold().setAddress(person.getAddress());
                    person.getHousehold().setEmail(person.getEmail());
                    person.getHousehold().setPhoneNumber(ObjectUtils.firstNonNull(getPerson().getPhoneNumber(), getPerson().getMobileNumber(), getPerson().getBusinessNumber()));
                }
                person.setHousehold(householdManager.save(person.getHousehold()));
            } else {
                person = personManager.save(person);
            }

            processFamily();
            fetchLazyCollections(person);
            initFormData();

            String key = (isNew) ? "person.added" : "person.updated";
            addMessage(key);


            if (isNew) {
                return "persons";
            } else {
                return "persons";
            }

        } catch (AccessDeniedException ade) {
            // thrown by UserSecurityAdvice configured in aop:advisor
            // userManagerSecurity
            log.warn(ade.getMessage());
            try {
                getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
            } catch (IOException e) {
                UUID guid = UUID.randomUUID();
                addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
                log.fatal(e);
            }
            return "";
        } catch (StaleObjectStateException | HibernateOptimisticLockingFailureException e) {
            UUID guid = UUID.randomUUID();
            addError(getText("errors.optimisticLock"));
            log.fatal(guid.toString(), e);
            return "";

        } catch (InvalidDataAccessApiUsageException e) {

            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.fatal(guid.toString(), e);
            return "";

        } catch (ConstraintViolationException | DataIntegrityViolationException e) {
            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
            return "";

        } catch (javax.validation.ConstraintViolationException e) {
            ConstraintViolationExceptionHandler(e);
            log.fatal(e.getMessage(), e);
            return "";

        } catch (Exception e) {

            addError("errors.existing.user",
                    new Object[]{person.getName(), person.getEmail()});
            log.fatal(e.getMessage(), e);
            return "";
        }

    }

    private void processFamily() {

        familyMembersRemoved.forEach(member -> personManager.save(member));
        familyIDs.forEach(f -> {householdManager.remove(f);});
        households = null; //null to get fresh copy in initFormData();
        people = null;
    }


    public void save(ActionEvent event) {
        this.save();
    }

    public void saveAndClose(ActionEvent event) {

        this.person.setPersonBin(new PersonBin());

        if (!this.save().isEmpty()) {
            PrimeFaces.current().dialog().closeDynamic(this.person);
        }
    }

    /**
     * Project: framework Method : saveUploadedImageToDisk Return : void Params
     * : Author : eairrick
     */
    private void saveUploadedImageToDisk() {
        saveUploadedImageToDisk(240, 200);
    }

    /**
     * Project: framework Method : saveUploadedImageToDisk Return : void Author
     * : eairrick
     */
    private void saveUploadedImageToDisk(Integer width, Integer height) {

        savedImageFile = generateTempFileName();

        try {

            BufferedImage image = Scalr.resize(ImageIO.read(uploadedFile.getInputstream()), width, height);

            log.info("Image Width = " + image.getWidth());
            log.info("Image Height = " + image.getHeight());
            log.info("Writing temporary image file " + savedImageFile + " to file system");

            ImageIO.write(image, FilenameUtils.getExtension(uploadedFile.getFileName()), new File(savedImageFile));

        } catch (IOException e) {
            log.fatal(e);
            throw new FacesException("Error in writing image.", e);

        }
    }

    public void selectGroupMember(SelectEvent event) {
        this.selectedGroupMember = (GroupMember) event.getObject();
    }

    /**
     * @param addressComplete the addressComplete to set
     */
    public void setAddressComplete(boolean addressComplete) {
        this.addressComplete = addressComplete;
    }

    @Autowired
    public void setAttendanceManager(
            @Qualifier("attendanceManager") GenericManager<Attendance, Long> attendanceManager) {
        this.attendanceManager = attendanceManager;
    }

    @Autowired
    public void setCategoryManager(@Qualifier("categoryManager") GenericManager<Category, Long> categoryManager) {
        this.categoryManager = categoryManager;
    }

    @Autowired
    public void setContributionManager(
            @Qualifier("contributionManager") GenericManager<Contribution, Long> contributionManager) {
        this.contributionManager = contributionManager;
    }

    /**
     * @param croppedImage the croppedImage to set
     */
    public void setCroppedImage(CroppedImage croppedImage) {
        this.croppedImage = croppedImage;
    }

    @Autowired
    public void setCustomMetaManager(
            @Qualifier("customMetaManager") GenericManager<CustomMeta, Long> customMetaManager) {
        this.customMetaManager = customMetaManager;
    }

    public void setDisableButtons(boolean disableButtons) {
        this.disableButtons = disableButtons;
    }

    public void setEntryContribution(Contribution entryContribution) {
        this.entryContribution = entryContribution;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(List<CustomField> fields) {
        getPerson().setCustomFields(fields);
    }

    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @param groupMemberCategory the groupMemberCategory to set
     */
    public void setGroupMemberCategory(Category groupMemberCategory) {
        this.groupMemberCategory = groupMemberCategory;
    }

    @Autowired
    public void setGroupMemberManager(
            @Qualifier("groupMemberManager") GenericManager<GroupMember, Long> groupMemberManager) {
        this.groupMemberManager = groupMemberManager;
    }

    @Autowired
    public void setHouseholdManager
            (@Qualifier("householdManager") GenericManager<Household, Long> householdManager) {
        this.householdManager = householdManager;
    }

    /**
     * @param households the households to set
     */
    public void setHouseholds(List<Household> households) {
        this.households = households;
    }

    /**
     * @param initGroupMember the initGroupMember to set
     */
    public void setInitGroupMember(GroupMember initGroupMember) {
        this.initGroupMember = initGroupMember;
    }

    @Override
    @Autowired
    public void setLookupManager(@Qualifier("lookupManager") LookupManager lookupManager) {
        this.lookupManager = lookupManager;
    }

    /**
     * @param markerModel the markerModel to set
     */
    public void setMarkerModel(MapModel markerModel) {
        this.markerModel = markerModel;
    }

    /**
     * @param newHousehold the newHousehold to set
     */
    public void setNewHousehold(Household newHousehold) {
        this.newHousehold = newHousehold;
    }

    public Pledge getInitPledge() {
        return initPledge;
    }

    public void setInitPledge(Pledge initPledge) {
        this.initPledge = initPledge;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Autowired
    public void setPersonBinManager
            (@Qualifier("personBinManager") GenericManager<PersonBin, Long> personBinManager) {
        this.personBinManager = personBinManager;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    @Autowired
    public void setBatchManager(@Qualifier("batchManager") GenericManager<Batch, Long> batchManager) {
        this.batchManager = batchManager;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    public void setRequiredFamily(boolean requiredFamily) {
        this.requiredFamily = requiredFamily;
    }

    public void setRowselected(boolean rowselected) {
        this.rowselected = rowselected;
    }

    public void setSelectedContribution(Contribution selectedContribution) {
        this.selectedContribution = selectedContribution;
    }

    public void setSelectedGroupMember(GroupMember selectedGroupMember) {
        this.selectedGroupMember = selectedGroupMember;
    }

    public void setSelectedHousehold(Household selectedHousehold) {
        this.selectedHousehold = selectedHousehold;
    }

    /**
     * @param selectedPerson the selectedPerson to set
     */
    public void setSelectedPerson(Person person) {
        this.selectedPerson = person;
    }

    public void setSelectedRelationship(String selectedRelationship) {
        this.selectedRelationship = selectedRelationship;
    }

    /**
     * @param simpleModel the simpleModel to set
     */
    public void setSimpleModel(MapModel simpleModel) {
        this.simpleModel = simpleModel;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public void setUsStates(ArrayList<LabelValue> usStates) {
        this.usStates = usStates;
    }

    public FileLink getSelectedFileLink() {
        return selectedFileLink;
    }

    public void setSelectedFileLink(FileLink selectedFileLink) {

        this.selectedFileLink = selectedFileLink;
        /*
         * this.getTabViewFileLink()
         * .setActiveIndex((selectedFileLink.getResourceType().equals(Constants.
         * FileLinkType.FILE)) ? 0 : 1);
         */

    }

    public void tabOnChange(TabChangeEvent event) {

        String tabID = event.getTab().getId();

        if (tabID.equals(TAB_ATTENDANCE_ID)) {
        }

        if (tabID.equals(TAB_CLASSES_ID)) {
            retrieveClassCategory();
        }

        if (tabID.endsWith(TAB_CONTRIBUTIONS)) {

        }


    }

    public void turnButtonsOff(ActionEvent event) {
        disableButtons = true;
    }

    // Form Controls ==========================================================
    @SuppressWarnings("unchecked")
    public Map<String, String> getAvailableRoles() {
        if (availableRoles == null) {
            List roles = (List) getServletContext().getAttribute(Constants.AVAILABLE_ROLES);
            availableRoles = ConvertUtil.convertListToMap(roles);
        }

        return availableRoles;
    }

    public String[] getUserRoles() {

        if (person.getUser() == null) {
            return null;
        }

        userRoles = new String[person.getUser().getRoles().size()];

        int i = 0;

        if (userRoles.length > 0) {
            for (Role role : person.getUser().getRoles()) {
                userRoles[i] = role.getName();
                i++;
            }
        }

        return userRoles;

    }

    public void setUserRoles(String[] userRoles) {
        this.userRoles = userRoles;
    }


    public Object handleCloseGroupDialog() {
        initGroupMember = new GroupMember();
        return null;
    }


}
