package com.springthought.webapp.action;

import com.springthought.model.EventType;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.UUID;

@Component("eventTypeForm")
@Scope("view")
public class EventTypeForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7712738978395410285L;
    private GenericManager<EventType, Long> eventTypeManager;
    private EventType eventType = new EventType();
    private Long eventTypeId;

    @Autowired
    public void setEventTypeManager(@Qualifier("eventTypeManager") GenericManager<EventType, Long> eventTypeManager) {
	this.eventTypeManager = eventTypeManager;
    }

    public EventType getEventType() {
	return eventType;
    }

    public void setEventType(EventType eventType) {
	this.eventType = eventType;
    }

    public void setEventTypeId(Long eventTypeId) {
	this.eventTypeId = eventTypeId;
    }

    public String delete() {
	eventTypeManager.remove(eventType.getEventTypeId());
	addMessage("eventType.deleted");

	return "eventTypes";
    }

    public String edit() {

	return "eventTypeForm";
    }

    public String save() {
	boolean isNew = (eventType.getEventTypeId() == null || eventType.getEventTypeId() == 0);

	try {
	    eventTypeManager.save(eventType);

	    String key = (isNew) ? "eventType.added" : "eventType.updated";
	    addMessage(key);

	    if (isNew) {
		return "eventTypes";
	    } else {
		return "eventTypes";
	    }
	} catch (ConstraintViolationException e) {

	    DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
	}

	catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new Object[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

	return "";
    }

    public void save(ActionEvent event) {
	this.save();
    }

    @PostConstruct
    private void initilization() {
	eventTypeId = getParameter("eventTypeId") == null ? 0L : new Long(getParameter("eventTypeId"));

	if (eventTypeId != null && eventTypeId != 0) {
	    eventType = eventTypeManager.get(eventTypeId);
	} else {
	    eventType = new EventType();
	}

    }

}
