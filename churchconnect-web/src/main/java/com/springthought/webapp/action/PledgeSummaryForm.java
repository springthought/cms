package com.springthought.webapp.action;

import com.springthought.model.Aggregate;
import com.springthought.model.Campaign;
import com.springthought.model.Pledge;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import com.springthought.webapp.jsf.chart.PledgeSummaryBarChart;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.chart.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component("pledgeSummaryForm")
@Scope("view")
public class PledgeSummaryForm extends BasePage implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -386242130707616137L;
	private String query;
	private GenericManager<Pledge, Long> pledgeManager;
	private Pledge selectedPledge = new Pledge();
	private boolean checked;
	private GenericManager<Campaign, Long> campaignManager;
	private GenericManager<Aggregate, Long> aggregateManager;
	private DataService dataService;
	private List<PledgeSummaryBarChart> pledgeCharts = new ArrayList<PledgeSummaryBarChart>(
			0);

	private DonutChartModel donutModel1;
	private DonutChartModel donutModel2;
	private BarChartModel animatedModel2;

	private Long campaignId;
	private String from = "summary";

	public BarChartModel getAnimatedModel2() {
		return animatedModel2;
	}

	public void setAnimatedModel2(BarChartModel animatedModel2) {
		this.animatedModel2 = animatedModel2;
	}

	@Autowired
	public void setPledgeManager(
			@Qualifier("pledgeManager") GenericManager<Pledge, Long> pledgeManager) {
		this.pledgeManager = pledgeManager;
	}

	@Autowired
	public void setCampaignManager(
			@Qualifier("campaignManager") GenericManager<Campaign, Long> campaignManager) {
		this.campaignManager = campaignManager;
	}

	@Autowired
	public void setDataService(DataService dataService) {
		this.dataService = dataService;
	}

	@Autowired
	public void setAggregateManager(
			@Qualifier("aggregateManager") GenericManager<Aggregate, Long> aggregateManager) {
		this.aggregateManager = aggregateManager;
	}

	public DataService getDataService() {
		return dataService;
	}

	public PledgeSummaryForm() {
		setSortColumn("name"); // sets the default sort column
	}

	@SuppressWarnings("unchecked")
	public List<Pledge> getPledges() {
		return sort(pledgeManager.getAllDistinct());
	}

	@SuppressWarnings("unchecked")
	public List<Campaign> getCampaigns() {
		return sort(campaignManager.getAllDistinct());
	}

	public Pledge getSelectedPledge() {
		return selectedPledge;
	}

	public void setSelectedPledge(Pledge pledge) {
		this.selectedPledge = pledge;
	}

	public List<PledgeSummaryBarChart> getPledgeCharts() {
		return pledgeCharts;
	}

	public void setPledgeCharts(List<PledgeSummaryBarChart> pledgeCharts) {
		this.pledgeCharts = pledgeCharts;
	}

	public void delete(ActionEvent event) {
		pledgeManager.remove(selectedPledge.getPledgeId());
		addMessage("pledge.deleted");
		checked = false;
	}

	public void radioSelected(SelectEvent event) {
		checked = true;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	@PostConstruct
	public void initialization() {
		createModels();
		createDataList();
	}

	public DonutChartModel getDonutModel1() {
		return donutModel1;
	}

	public DonutChartModel getDonutModel2() {
		return donutModel2;
	}

	private void createModels() {

		donutModel1 = initDonutModel();
		donutModel1.setTitle("Donut Chart");
		donutModel1.setLegendPosition("w");

		donutModel2 = initDonutModel();
		donutModel2.setTitle("Custom Options");
		// donutModel2.setLegendPosition("e");
		donutModel2.setSliceMargin(1);
		donutModel2.setShowDataLabels(true);
		donutModel2.setDataFormat("value");
		donutModel2.setShadow(false);
		donutModel2.setLegendPlacement(LegendPlacement.INSIDE);
		donutModel2.setSeriesColors("E1E1E1,55F775");

		animatedModel2 = initBarModel();
		animatedModel2.setTitle("Buidling Fund");
		animatedModel2.setAnimate(true);
		animatedModel2.setLegendPosition("ne");
		animatedModel2.setSeriesColors("5e4fa2,3288bd,66c2a5");
		Axis yAxis = animatedModel2.getAxis(AxisType.Y);
		yAxis.setMin(0);
		yAxis.setMax(200);

	}

	private DonutChartModel initDonutModel() {
		DonutChartModel model = new DonutChartModel();

		Map<String, Number> circle1 = new LinkedHashMap<String, Number>();
		circle1.put("Remaining %", 75);
		circle1.put("Received %", 25);
		model.addCircle(circle1);
		return model;
	}

	private BarChartModel initBarModel() {

		BarChartModel model = new BarChartModel();

		ChartSeries campaign = new ChartSeries();
		campaign.setLabel("Campaign");
		campaign.set("2004", 120);

		ChartSeries pledges = new ChartSeries();
		pledges.setLabel("Pledges");
		pledges.set("YTD", 100);

		ChartSeries received = new ChartSeries();
		received.setLabel("Received");
		received.set("YTD", 80);

		model.addSeries(campaign);
		model.addSeries(pledges);
		model.addSeries(received);

		return model;
	}

	/**
	 * Project: framework Method : createDateList Return : void Params : Author
	 * : eairrick
	 *
	 */
	private void createDataList() {

		List<Campaign> campaigns = campaignManager.getAllDistinct();
		pledgeCharts = new ArrayList<PledgeSummaryBarChart>(0);

		for (Campaign campaign : campaigns) {
			PledgeSummaryBarChart chart = new PledgeSummaryBarChart(campaign,
					aggregateManager);
			pledgeCharts.add(chart);
		}

	}

	public String go() {
		return "/views/pledgeForm.xhtml?faces-redirect=true&includeViewParams=true";
	}

	public void itemSelect(ItemSelectEvent event) {

		campaignId = getParameter("campaignId") == null ? -0L : new Long(
				getParameter("campaignId"));

		/*
		Integer seriesIndex = event.getSeriesIndex();

		BarChart chart = (BarChart) event.getSource();

		BarChartModel bModel = (BarChartModel) chart.getModel();

		Map<Object, Number> cData = bModel.getSeries().get(seriesIndex)
				.getData();*/


		if (campaignId != null && campaignId !=0 ){
			getFacesContext().getApplication().getNavigationHandler()
					.handleNavigation(getFacesContext(), "null", go());

		}
		/*switch (event.getSeriesIndex()) {
		case 0:

			break;
		case 1:
			getFacesContext().getApplication().getNavigationHandler()
					.handleNavigation(getFacesContext(), "null", go());
			break;
		default:
			break;
		}
*/
	}

	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
}
