
package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.Permission;
import com.springthought.model.Role;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Component("roleList")
@Scope("view")
public class RoleList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7473550871344529835L;
    private String query;
    private GenericManager<Role, Long> roleManager;
    private Role selectedRole = new Role();
    private boolean checked;
    private String copedLabel;

    @Autowired
    public void setRoleManager(@Qualifier("roleManager") GenericManager<Role, Long> roleManager) {
        this.roleManager = roleManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public RoleList() {
        setSortColumn("id"); // sets the default sort column
    }

    @SuppressWarnings("unchecked")
    public List<Role> getRoles() {
        return sort(roleManager.getAllDistinct());
    }

    public String search() {
        return "success";
    }

    public Role getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Role role) {
        this.selectedRole = role;
    }

    public void delete(ActionEvent event) {

        Role role = roleManager.get(selectedRole.getId());
        role.removeAllPermissions();

        try {
            roleManager.remove(role);
            addMessage("role.deleted");
        } catch (DataIntegrityViolationException e) {

            DataIntegrityViolationExceptionHandler(e);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }
    }

    public void copy(ActionEvent event) {



        //need to move to a common route.
        Role copy = new Role();

        copy.setLabel(getCopiedLabel());
        copy.setName(Constants.ROLE_PREFIX + "_" + getCopiedLabel().toUpperCase().trim().replace(" ", ""));

        copy.setTenantId(getCurrentUser().getTenantId());
        copy.setDescription(selectedRole.getDescription());
        copy.setBasic(false);
        copy.setCreateDate(new Date());
        copy.setCreatedByUser(getCurrentUser().getUsername());
        copy.setUpdatedByUser(getCurrentUser().getUsername());
        copy.setLastUpdate(new Date());

        for (Permission pm : selectedRole.getPermissions()) {
            Permission permission = new Permission(pm.getName());
            permission.setPermissionCategory(pm.getPermissionCategory());
            permission.setDescr(pm.getDescr());
            permission.setPermissionId(pm.getPermissionId());
            copy.getPermissions().add(permission);
        }
        //need to move to a common route.


        try {
            selectedRole = roleManager.save(copy);
            addMessage("role.copied");
            checked = true;
            PrimeFaces.current().ajax().addCallbackParam("valid", true);
            setCopiedLabel("");
        } catch (ConstraintViolationException e) {

            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("inptxtName", "errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            log.error(e);

        }

    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getCopiedLabel() {
        return copedLabel;
    }

    public void setCopiedLabel(String label) {
        this.copedLabel = label;
    }

    @PostConstruct
    private void initilization() {

    }
}