package com.springthought.webapp.action;

import com.springthought.model.Campaign;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component("campaignList")
@Scope("view")
public class CampaignList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6875627740535314798L;
    private String query;
    private GenericManager<Campaign, Long> campaignManager;
    private Campaign selectedCampaign = new Campaign();
    private boolean checked;
    private ArrayList<Campaign> campaigns = new ArrayList<Campaign>(0);

    @Autowired
    public void setCampaignManager(
            @Qualifier("campaignManager") GenericManager<Campaign, Long> campaignManager) {
        this.campaignManager = campaignManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public CampaignList() {
        setSortColumn("campaignId"); // sets the default sort column
        setAscending(false);
    }

    @SuppressWarnings("unchecked")
    public List<Campaign> getCampaigns() {
        return sort(campaignManager.getAllDistinct());
    }

    public String search() {
        return "success";
    }

    public Campaign getSelectedCampaign() {
        return selectedCampaign;
    }

    public void setSelectedCampaign(Campaign campaign) {
        this.selectedCampaign = campaign;
    }

    public void delete(ActionEvent event) {

        Campaign campaign = campaignManager.get(selectedCampaign.getCampaignId());
        campaign.removePledges();

        try {
            campaignManager.remove(campaign);
            addMessage("campaign.deleted");

        } catch (DataIntegrityViolationException e) {

            DataIntegrityViolationExceptionHandler(e);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    /**
     *
     * Project: framework
     * Method : initilization
     * Return : void
     * Params :
     * Author : eairrick
     *

     private void initilization() {

     campaigns = new ArrayList<Campaign>(0);
     List<Campaign> workCampaigns = campaignManager.getAllDistinct();

     for (Campaign campaign : workCampaigns) {
     campaignManager.getSession().evict(campaign);
     campaigns.add(campaign);
     }
     campaigns = (ArrayList<Campaign>) sort(campaigns);
     }

     */
}