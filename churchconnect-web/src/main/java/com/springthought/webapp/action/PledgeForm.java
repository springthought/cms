package com.springthought.webapp.action;

import com.springthought.model.Aggregate;
import com.springthought.model.Campaign;
import com.springthought.model.Person;
import com.springthought.model.Pledge;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import com.springthought.util.Queries;
import org.hibernate.StaleObjectStateException;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.optionconfig.title.Title;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;


@Component("pledgeForm")
@Scope("view")
public class PledgeForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 2003568708787874746L;
    private GenericManager<Pledge, Long> pledgeManager;
    private GenericManager<Campaign, Long> campaignManager;
    private GenericManager<Person, Long> personManager;
    private GenericManager<Aggregate, Long> aggregateManager;
    private BigDecimal actualPledgeTotal = BigDecimal.ZERO;
    private BigDecimal pledgesGivenTotal = BigDecimal.ZERO;
    private Pledge selectedPledge = new Pledge();
    private Pledge entryPledge = new Pledge();
    private Campaign campaign = new Campaign();
    private Campaign currentCampaign = new Campaign();
    private Long campaignId;
    private ArrayList<Person> persons = new ArrayList<Person>(0);
    private String UID = UUID.randomUUID().toString();
    private boolean rowselected;
    HashMap<String, Object> queryParams = new HashMap<String, Object>();
    private DataService dataService;
    private String from = null;
    private boolean isDirty;
    private BarChartModel barModel;

    private boolean showPledgeDetails;


    private Pledge pledge = new Pledge(); //not needed temp added for unit test
    private Long pledgeId; //not needed temp added for unit test

    public boolean isRowselected() {
        return rowselected;
    }

    public void setRowselected(boolean rowselected) {
        this.rowselected = rowselected;
    }

    @Autowired
    public void setPledgeManager(@Qualifier("pledgeManager") GenericManager<Pledge, Long> pledgeManager) {
        this.pledgeManager = pledgeManager;
    }

    @Autowired
    public void setCampaignManager(@Qualifier("campaignManager") GenericManager<Campaign, Long> campaignManager) {
        this.campaignManager = campaignManager;
    }

    @Autowired
    public void setAggregateManager(@Qualifier("aggregateManager") GenericManager<Aggregate, Long> aggregateManager) {
        this.aggregateManager = aggregateManager;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    public BigDecimal getActualPledgeTotal() {
        return actualPledgeTotal;
    }

    @Autowired
    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    public DataService getDataService() {
        return dataService;
    }

    public BigDecimal getPledgesGivenTotal() {

        queryParams.put(Queries.CONTRIBUTION_SUM_BY_CATEGORY_ID_PARM, campaign.getCategory());

        List<Aggregate> agg = aggregateManager.findByNamedQuery(Queries.CONTRIBUTION_SUM_BY_CATEGORY, queryParams);

        pledgesGivenTotal = (BigDecimal) ((agg.get(0) == null) ? BigDecimal.ZERO : agg.get(0));

        return pledgesGivenTotal;
    }

    public void setPledgesGivenTotal(BigDecimal pledgesGivenTotal) {
        this.pledgesGivenTotal = pledgesGivenTotal;
    }

    public Pledge getSelectedPledge() {
        return selectedPledge;
    }

    public void setSelectedPledge(Pledge selectedPledge) {
        this.selectedPledge = selectedPledge;
    }

    public Pledge getEntryPledge() {
        return entryPledge;
    }

    public void setEntryPledge(Pledge entryPledge) {
        this.entryPledge = entryPledge;
    }

    public Campaign getCampaign() {
        this.calcActualPledgeTotal();
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public Pledge getPledge() {
        return pledge;
    }

    public void setPledge(Pledge pledge) {
        this.pledge = pledge;
    }

    public Long getPledgeId() {
        return pledgeId;
    }

    public void setPledgeId(Long pledgeId) {
        this.pledgeId = pledgeId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Campaign getCurrentCampaign() {
        return currentCampaign;
    }

    public void setCurrentCampaign(Campaign currentCampaign) {
        this.currentCampaign = currentCampaign;
    }

    public String delete() {
        try {
            pledgeManager.remove(selectedPledge.getPledgeId());
            addMessage("pledge.deleted");
        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

        return "pledges";
    }

    public String edit() {
        return "pledgeForm";
    }

    public String save() {

        try {

            boolean isNew = (campaign.getCampaignId() == null || campaign.getCampaignId() == 0);

            campaignManager.save(campaign);

            String key = (isNew) ? "pledge.added" : "pledge.updated";
            addMessage(key);
            isDirty = false;

            if (isNew) {
                return "campaigns";
            } else {
                return "campaigns";
            }

        } catch (StaleObjectStateException e) {

            UUID guid = UUID.randomUUID();
            addError(getText("errors.optimisticLock"));
            log.error(guid.toString(), e);
            return "";

        } catch (InvalidDataAccessApiUsageException e) {

            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
            return "";

        } catch (ConstraintViolationException e) {
            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
            return "";

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
            return "";

        }
    }

    public void save(ActionEvent event) {
        this.save();
    }

    public void deletePledge(ActionEvent event) {
        boolean deleted = campaign.getPledges().remove(selectedPledge);
        addMessage("pledge.deleted");
    }

    public void onRowSelect(SelectEvent event) {
        this.rowselected = true;
    }

    public void onRowEdit(RowEditEvent event) {
        this.isDirty = true;
    }

    private void calcActualPledgeTotal() {
        float sum = 0;
        for (Pledge pledge : campaign.getPledges()) {
            sum += pledge.getPledgeAmt().floatValue();
        }
        actualPledgeTotal = new BigDecimal(sum);
    }

    public List<Person> completePerson(String query) {

        List<Person> allPersons = this.persons;
        List<Person> filteredPersons = new ArrayList<Person>();

        for (Person person : allPersons) {
            if (person.getName().toLowerCase().contains(query.toLowerCase())) {
                filteredPersons.add(person);
            }
        }

        return filteredPersons;
    }

    public char getPersonGroup(Person person) {
        return (person == null ? '\0' : person.getFirstName().charAt(0));
    }

    public void reinitPledge(ActionEvent event) {

        entryPledge.setCampaign(campaign);
        entryPledge.setVersion(0);
        campaign.getPledges().add(entryPledge);
        entryPledge = new Pledge();
        entryPledge.setUID(UUID.randomUUID().toString());
        entryPledge.setPledgeAmt(BigDecimal.ZERO);
        isDirty = true;

    }

    public void updatePledgeEntry(ActionEvent event){

        Pledge pledgeItem = campaign.getPledges().stream()
                .filter(p -> p.getUID().equals(entryPledge.getUID()))
                .findFirst()
                .orElse(null);

        if( pledgeItem!=null ){
            pledgeItem = entryPledge;
            entryPledge = new Pledge();
            addMessage(getText("pledge.updated"));
        }else {
            addError(getText("errors.pledge.not.found", entryPledge));
        }

    }

    public void campaignChange(AjaxBehaviorEvent event) {


        if (isDirty) {
            PrimeFaces.current().executeScript("PF('savePledgeDlgWv').show()");
            //context.execute("PF('savePledgeDlgWv').show()");
        } else {
            switchCampaign("0");
        }

    }

    public void switchCampaign(String save) {

        if (save.equals("1")) {
            this.save();
        }
        campaignId = currentCampaign.getCampaignId();
        initilization();

    }

    @SuppressWarnings("unchecked")
    @PostConstruct
    private void initilization() {

        if (from == null) {
            from = getParameter("from") == null ? "menu" : getParameter("from");
        }

        if (campaignId == null) {
            campaignId = getParameter("campaignId") == null ? -0L : new Long(getParameter("campaignId"));
        }

        if (campaignId != null && campaignId != 0) {

            campaign = campaignManager.get(campaignId);
            currentCampaign = campaign;
            campaign.getPledges().size();
            this.buildBarModel();


        } else {
            campaign = dataService.getCampaigns().get(0);
        }

        this.setSortColumn("name");
        this.persons = (ArrayList<Person>) sort(personManager.getAllDistinct());


    }

    public String go() {
        return (from.equalsIgnoreCase("summary") ? "pledgeSummary" : "campaigns");
    }


    public void togglePledgeDetails() {
        setShowPledgeDetails(!getShowPledgeDetails());
    }



    public BarChartModel getBarModel() {
        this.buildBarModel();
        return barModel;
    }


    public boolean getShowPledgeDetails() {
        return showPledgeDetails;
    }

    public void setShowPledgeDetails(boolean showPledgeDetails) {
        this.showPledgeDetails = showPledgeDetails;
    }


    public void setBarModel(BarChartModel barModel) {
        this.barModel = barModel;
    }

    public void buildBarModel() {
        barModel = new BarChartModel();
        ChartData data = new ChartData();

        BarChartDataSet barDataSet = new BarChartDataSet();
        barDataSet.setLabel(getText("pledgeDetail.goalAmount"));


        List<Number> values = new ArrayList<>();


        values.add(this.getCampaign().getGoalAmt().intValue());
        values.add(this.getActualPledgeTotal().intValue());
        values.add(this.getPledgesGivenTotal().intValue());
        values.add((this.getCampaign().getGoalAmt().subtract(this.getPledgesGivenTotal())).intValue());

        barDataSet.setData(values);

        List<String> bgColor = new ArrayList<>();
        bgColor.add("rgba(255, 205, 86, 0.2)");
        bgColor.add("rgba(75, 192, 192, 0.2)");
        bgColor.add("rgba(54, 162, 235, 0.2)");
        bgColor.add("rgba(153, 102, 255, 0.2)");
        barDataSet.setBackgroundColor(bgColor);

        List<String> borderColor = new ArrayList<>();
        borderColor.add("rgb(255, 205, 86)");
        borderColor.add("rgb(75, 192, 192)");
        borderColor.add("rgb(54, 162, 235)");
        borderColor.add("rgb(153, 102, 255)");
        barDataSet.setBorderColor(borderColor);
        barDataSet.setBorderWidth(1);

        data.addChartDataSet(barDataSet);

        List<String> labels = new ArrayList<>();
        labels.add(getText("pledgeDetail.goalAmount"));
        labels.add(getText("pledgeDetail.pledgeTotal"));
        labels.add(getText("pledgeDetail.actualPledges"));
        labels.add(getText("pledgeDetail.difference"));
        data.setLabels(labels);
        barModel.setData(data);

        //Options
        BarChartOptions options = new BarChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);

        Title title = new Title();
        title.setDisplay(true);
        title.setText(this.getCampaign().getName());
        options.setTitle(title);

        barModel.setOptions(options);
    }
}
