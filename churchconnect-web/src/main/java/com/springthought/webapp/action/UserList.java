package com.springthought.webapp.action;

import com.springthought.model.Person;
import com.springthought.model.User;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Scope("view")
@Component("userList")
//@ManagedBean
//@SessionScoped
public class UserList extends BasePage implements Serializable {

    private static final long serialVersionUID = 972359310602744018L;
    private User selectedUser = new User();

    private boolean checked;
    private GenericManager<Person, Long> personManager;


    List<Person> people = new ArrayList<Person>(0);

    private String query;

    public void setQuery(String query) {

        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    public UserList() {
        setSortColumn("username");
    }

    @SuppressWarnings("rawtypes")
    public List getUsers() {
        List<User> users = userManager.getAllDistinct();
        return sort(users);
    }

    public String search() {
        return "success";
    }

    public User getSelectedUser() {

        return selectedUser;
    }

    public void setSelectedUser(User user) {
        this.selectedUser = user;
    }

    public void delete(ActionEvent event) {

        User user = userManager.get(selectedUser.getId());
        user.removePerson();
        user.removeRoles();
        user.removeNotifications();

        try {
            if (!user.getUsername().equals(getPrincipalUserName())) {
                userManager.remove(selectedUser.getId());
                addMessage("user.deleted");
            } else {
                addError("user.cannot.delete.signed.account", selectedUser.getUsername());
            }
        } catch (DataIntegrityViolationException e) {

            DataIntegrityViolationExceptionHandler(e);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }


    }


    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    @PostConstruct
    private void initialization() {
        people = personManager.getAllDistinct();
    }



    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }
}

