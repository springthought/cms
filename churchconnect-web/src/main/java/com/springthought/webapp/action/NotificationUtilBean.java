package com.springthought.webapp.action;

import com.springthought.Constants.NotificationType;
import com.springthought.Constants.Priority;
import com.springthought.model.Notification;
import com.springthought.model.Person;
import com.springthought.model.User;
import com.springthought.service.GenericManager;
import com.springthought.service.UserManager;
import com.springthought.util.DateUtil;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component("notificationUtilBean")
public class NotificationUtilBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3090391075931144536L;
    private GenericManager<Person, Long> personManager;
    private UserManager userManager;
    private final Log log = LogFactory.getLog(getClass());
    private final int SHOW_BIRTHDAYS_OCCURRING_NEXT_DAYS = 25;
    private final int SHOW_JOINED_OCCURRING_NEXT_DAYS = 25;
    private final int SHOW_ANNIVERSARY_OCCURRING_NEXT_DAYS = 25;
    private Session session = null;
    private ArrayList<Notification> notifications = new ArrayList<Notification>(0);

    private Long tenantId = null;
    private User user = null;

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
	this.personManager = personManager;

    }

    // public void setPersonManager(GenericManager<Person, Long> personManager)
    // {
    // this.personManager = personManager;
    // }

    @Autowired
    public void setUserManager(@Qualifier("userManager") UserManager userManager) {
	this.userManager = userManager;
    }

    public static void main(String[] args) throws Exception {

	String[] location = { "classpath:/applicationContext-resources.xml", "classpath:/applicationContext-dao.xml",
		"classpath*:/applicationContext.xml", "classpath:**/applicationContext*.xml",
		"classpath:/applicationContext-service.xml" };

	try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(location)) {

	    NotificationUtilBean p = context.getBean(NotificationUtilBean.class);

	    User user = new User();
	    user.setId(-1L);
	    user.setTenantId(1L);

	    p.processDates(user);
	}
    }

    private Date bringDateCurrent(Date date) {

	Date convertdate = null;

	if (date != null) {
	    DateTime datetime = new DateTime(date);
	    convertdate = new DateTime((new DateTime()).getYear(), datetime.getMonthOfYear(), datetime.getDayOfMonth(),
		    0, 0).toDate();
	}

	return convertdate;
    }

    public ArrayList<Notification> processDates(User user) throws Exception {

	try {

	    this.session = personManager.getSessionFactory().openSession();
	    this.tenantId = user.getTenantId();
	    this.user = userManager.getUser(String.valueOf(user.getId()));

	    // session.beginTransaction();

	    String hql = "delete from Notification n where n.user.id= :userId";

	    Integer count = session.createQuery(hql).setLong("userId", user.getId()).executeUpdate();

	    processBirthDate();
	    processAnniversaryDate();
	    processJoinedDate();

	    // session.getTransaction().commit();

	} catch (Exception e) {
	    log.error(e);
	    throw e;

	} finally {

	    if (session != null && session.isOpen()) {
		session.close();
	    }

	}

	return this.notifications;

    }

    @SuppressWarnings("unchecked")
    private void processBirthDate() throws Exception {

	String hql = MessageFormat.format(
		"from Person as p where month(p.{0}) >= (month(current_date()) - 1) and tenant_id = {1}", "birthDate",
		tenantId);

	Query query = session.createQuery(hql);
	List<Person> persons = query.list();

	Interval interval = new Interval(new DateTime().minusMonths(1),
		new DateTime().plusDays(SHOW_BIRTHDAYS_OCCURRING_NEXT_DAYS));

	List<Person> people = persons.stream()
		.filter(p -> interval.contains(new DateTime(bringDateCurrent(p.getBirthDate()))))
		.collect(Collectors.toList());

	generateNotification(people, NotificationType.BIRTHDAY, this.user);

    }

    @SuppressWarnings("unchecked")
    private void processJoinedDate() throws Exception {

	String hql = MessageFormat.format(
		"from Person as p where month(p.{0}) >= month(current_date()) and tenant_id = {1}", "dateJoined",
		tenantId);

	Query query = session.createQuery(hql);
	List<Person> persons = query.list();

	Interval interval = new Interval(new DateTime().minusMonths(1),
		new DateTime().plusDays(SHOW_JOINED_OCCURRING_NEXT_DAYS));

	List<Person> people = persons.stream()
		.filter(p -> interval.contains(new DateTime(bringDateCurrent(p.getDateJoined()))))
		.collect(Collectors.toList());

	generateNotification(people, NotificationType.JOINED, this.user);

    }

    @SuppressWarnings("unchecked")
    private void processAnniversaryDate() throws Exception {

	String hql = MessageFormat.format(
		"from Person as p where month(p.{0}) >= month(current_date()) and tenant_id = {1}", "anniversaryDate",
		tenantId);

	Query query = session.createQuery(hql);
	List<Person> persons = query.list();

	Interval interval = new Interval(new DateTime().minusMonths(1),
		new DateTime().plusDays(SHOW_ANNIVERSARY_OCCURRING_NEXT_DAYS));

	List<Person> people = persons.stream()
		.filter(p -> interval.contains(new DateTime(bringDateCurrent(p.getAnniversaryDate()))))
		.collect(Collectors.toList());

	generateNotification(people, NotificationType.ANNIVERSARY, this.user);

    }

    private void generateNotification(List<Person> people, NotificationType type, User user) {

	for (Person person : people) {

	    Notification notification = new Notification();
	    notification.setPerson(person);
	    notification.setNotificationType(type);
	    notification.setName(type.toString());
	    notification.setPerson(person);
	    notification.setUser(user);

	    notification = updatePriorityDescr(notification, type, person);

	    notifications.add(notification);
	}

    }

    private Notification updatePriorityDescr(Notification notification, NotificationType type, Person person) {

	LocalDate today = new DateTime().toLocalDate();
	LocalDate currentDate;

	switch (type) {
	case BIRTHDAY:

	    currentDate = new DateTime(person.getBirthDate()).toLocalDate();

	    if (currentDate.compareTo(today) == 0) {

		notification.setPriority(Priority.MEDIUM);
		notification.setKey("notification.birthdayToday");
		notification.setParm1(WordUtils.capitalize(person.getName()));

	    } else if (currentDate.compareTo(today) < 0) {

		notification.setPriority(Priority.HIGH);
		notification.setKey("notification.birthdayWas");
		notification.setParm1(WordUtils.capitalize(person.getName()));
		notification.setParm2(DateUtil.getDateTime("MMMM dd", currentDate.toDate()));

	    } else {

		notification.setPriority(Priority.LOW);
		notification.setKey("notification.birthdayIs");
		notification.setParm1(WordUtils.capitalize(person.getName()));
		notification.setParm2(String.valueOf(Days.daysBetween(today, currentDate).getDays()));
	    }

	    break;
	case JOINED:
	    currentDate = new DateTime(person.getBirthDate()).toLocalDate();
	    break;
	case ANNIVERSARY:
	    currentDate = new DateTime(person.getBirthDate()).toLocalDate();
	    break;
	default:
	    notification.setPriority(Priority.NONE);
	    break;

	}
	return notification;
    }

}
