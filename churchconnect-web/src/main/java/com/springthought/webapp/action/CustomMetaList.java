package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.CodeListType;
import com.springthought.Constants.CustomFieldType;
import com.springthought.dao.SearchException;
import com.springthought.model.CustomMeta;
import com.springthought.model.CustomMetaValue;
import com.springthought.model.LabelValue;
import com.springthought.service.GenericManager;
import com.springthought.service.LookupManager;
import com.springthought.util.Queries;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Component("customMetaList")
@Scope("view")
public class CustomMetaList extends BasePage implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String query;
    private GenericManager<CustomMeta, Long> customMetaManager;
    private GenericManager<CustomMetaValue, Long> customMetaValueManager;
    private CustomMeta selectedCustomMeta = new CustomMeta();
    private CustomMetaValue selectedCustomMetaValue = new CustomMetaValue();
    private List<CustomMetaValue> customMetaValueList = null;
    private List<CustomMetaValue> selectedCustomMetaValues = new ArrayList<CustomMetaValue>();
    private boolean checked;
    private boolean checkedValue;
    private String postDeleteUpdate = "";

    // attempt to use primefaces collection helper class
    private CustomMetaValue customFieldValue = new CustomMetaValue();
    private List<CustomMetaValue> customFieldValues = new ArrayList<CustomMetaValue>();
    private boolean addMode = false;

    /**
     * @return the showText
     */

    private LookupManager lookupManager;

    List<LabelValue> dataTypes;

    private SelectItem[] dataTypeOptions;
    private SelectItem[] assignToOptions;
    private CustomMeta editCustomMeta = new CustomMeta();

    public CustomMetaList() {
	setSortColumn("customMetaId"); // sets the default sort column
    }


    public void addCustomFieldValue(ActionEvent event) {

        setAddMode(true);

        customFieldValue = new CustomMetaValue();
        customFieldValue.setListLabel(getText("customMeta.newValue"));
        customFieldValue.setDescr(getText("customMeta.newDescription"));
        customFieldValue.setValueListId(0L);
        customFieldValue.setCustomMeta(getEditCustomMeta());
        editCustomMeta.getCustomMetaValues().add(customFieldValue);

        log.debug(customFieldValue);

    }
    public void addCustomMeta(ActionEvent event) {

	selectedCustomMeta = new CustomMeta();
	selectedCustomMeta.setDataType(Constants.DATA_TYPE_TEXT);
	selectedCustomMeta.setCustomMetaId(0L);
	editCustomMeta = selectedCustomMeta;

    }

    /**
     * @tod: refactor and move to BasePage class.
     */
    private SelectItem[] createFilterOptions(List<LabelValue> labelValues) {

	SelectItem[] options = new SelectItem[labelValues.size() + 1];
	options[0] = new SelectItem("", getText("button.selectOne"));

	for (int i = 0; i < labelValues.size(); i++) {
	    options[i + 1] = new SelectItem(labelValues.get(i).getValue(), labelValues.get(i).getLabel());
	}

	return options;
    }

    public void delete(ActionEvent event) {
	customMetaManager.remove(selectedCustomMeta.getCustomMetaId());
	addMessage("customMeta.deleted");
	checked = false;
    }

    public void deleteValue() {

	boolean deleted = editCustomMeta.getCustomMetaValues().remove(selectedCustomMetaValue);

	System.out.println(deleted);

	// customMetaValueList = null;
	// selectedCustomMetaValue = null; // not sure why I am doing this.
	checkedValue = false;

	addMessage("userValueDetial.deleted");

    }

    public SelectItem[] getAssignToOptions() {
	return assignToOptions;
    }

    /**
     * @return the customFieldValue
     */
    public CustomMetaValue getCustomFieldValue() {

	return customFieldValue;
    }

    /**
     * @return the customFieldValues
     */
    public Set<CustomMetaValue> getCustomFieldValues() {

	return editCustomMeta.getCustomMetaValues();
    }

    @SuppressWarnings("unchecked")
    public List<CustomMeta> getCustomMetas() {
	try {
	    return sort(customMetaManager.getAllDistinct());
	} catch (SearchException se) {
	    addError(se.getMessage());
	    return null;
	}

    }

    public List<CustomMetaValue> getCustomMetaValues() {

	if (customMetaValueList == null) {

	    HashMap<String, Object> queryParams = new HashMap<String, Object>();

	    queryParams.put(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID_PARM, editCustomMeta.getCustomMetaId());

	    customMetaValueList = customMetaValueManager.findByNamedQuery(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID,
		    queryParams);

	}

	return customMetaValueList;

    }

    public SelectItem[] getDataTypeOptions() {
	return dataTypeOptions;
    }

    public List<LabelValue> getDataTypes() {
	return this.dataTypes;
    }

    /**
     * @return the editCustomMeta
     */
    public CustomMeta getEditCustomMeta() {
	return editCustomMeta;
    }

    /**
     * @return the postDeleteUpdate
     */
    public String getPostDeleteUpdate() {
	return postDeleteUpdate;
    }

    public String getQuery() {
	return query;
    }

    public CustomMeta getSelectedCustomMeta() {
	return selectedCustomMeta;
    }

    /**
     * @return the selectedCustomMetaValue
     */
    public CustomMetaValue getSelectedCustomMetaValue() {
	return selectedCustomMetaValue;
    }

    @PostConstruct
    private void init() {
	if (this.dataTypes == null) {

	    // this.setAscending(true);
	    this.dataTypes = lookupManager.getSystemValueByCodeListType(CodeListType.DATATYPE);

	    this.dataTypeOptions = createFilterOptions(this.dataTypes);

	    ArrayList<LabelValue> assignTo = new ArrayList<LabelValue>();
	    assignTo.add(new LabelValue(CustomFieldType.HOUSEHOLD.getValue(), CustomFieldType.HOUSEHOLD.toString()));
	    assignTo.add(new LabelValue(CustomFieldType.PERSON.getValue(), CustomFieldType.PERSON.toString()));
	    this.assignToOptions = createFilterOptions(assignTo);

	}
    }

    /**
     * 
     * Project: framework Method : initCustomFieldValues Return : void Params :
     * Author : eairrick
     * 
     * Loads collection of Custom Field Value on Edit() and Add()
     */
    private void initCustomFieldValues() {

	HashMap<String, Object> queryParams = new HashMap<String, Object>();

	queryParams.put(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID_PARM, editCustomMeta.getCustomMetaId());

	customFieldValues = customMetaValueManager.findByNamedQuery(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID, queryParams);

    }

    /**
     * @return the addMode
     */
    public boolean isAddMode() {
	return addMode;
    }

    public boolean isChecked() {
	return checked;
    }

    /**
     * @return the checkedValue
     */
    public boolean isCheckedValue() {
	return checkedValue;
    }

    public void onCancel(RowEditEvent event) {

	CustomMetaValue cmv = (CustomMetaValue) event.getObject();

	/**
	 * Not going to remove the object from the collection I have a strange
	 * visual effect where the 2nd row is doupled. and a shadow record
	 * exist. Need to post an issue.
	 * 
	 * if (isAddMode()) { editCustomMeta.getCustomMetaValues().remove(cmv);
	 * }
	 */

	checkedValue = false;
	addMessage("errors.cancel");
	setAddMode(false);

    }

    public void onEdit(RowEditEvent event) {

	setAddMode(false);

	CustomMetaValue value = (CustomMetaValue) event.getObject();

	boolean isNew = (value.getValueListId() == null || value.getValueListId() == 0);

	String key = (isNew) ? "customMeta.valueAdded" : "customMeta.valueUpdated";

	addMessage(key);

	selectedCustomMetaValue = null;
	checkedValue = false;

    }

    public void radioSelected(SelectEvent event) {
	selectedCustomMeta = (CustomMeta) event.getObject();
	checked = true;
    }

    public void radioSelectedValue(SelectEvent event) {
	this.setSelectedCustomMetaValue((CustomMetaValue) event.getObject());
	checkedValue = true;
    }

    public void refresh(ActionEvent e) {

    }

    private void save(CustomMeta field) {

	boolean isNew = (field.getCustomMetaId() == null || field.getCustomMetaId() == 0);

	customMetaManager.save(field);

	String key = (isNew) ? "customMeta.added" : "customMeta.updated";

	addMessage(key);

    }

    public String search() {
	return "success";
    }

    public void selectionChanged(AjaxBehaviorEvent event) {
	// need to clear fields of previously select fields.
	customMetaValueList = null;
	getCustomMetaValues();
    }

    /**
     * @param addMode
     *            the addMode to set
     */
    public void setAddMode(boolean addMode) {
	this.addMode = addMode;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }

    /**
     * @param checkedValue
     *            the checkedValue to set
     */
    public void setCheckedValue(boolean checkedValue) {
	this.checkedValue = checkedValue;
    }

    /**
     * @param customFieldValue
     *            the customFieldValue to set
     */
    public void setCustomFieldValue(CustomMetaValue customFieldValue) {
	this.customFieldValue = customFieldValue;
    }

    /**
     * @param customFieldValues
     *            the customFieldValues to set
     */
    public void setCustomFieldValues(List<CustomMetaValue> customFieldValues) {

	editCustomMeta.setCustomMetaValues(null);
	// this.customFieldValues = customFieldValues;
    }

    @Autowired
    public void setCustomMetaManager(
	    @Qualifier("customMetaManager") GenericManager<CustomMeta, Long> customMetaManager) {
	this.customMetaManager = customMetaManager;
    }

    @Autowired
    public void setCustomMetaValueManager(
	    @Qualifier("customMetaValueManager") GenericManager<CustomMetaValue, Long> customMetaValueManager) {
	this.customMetaValueManager = customMetaValueManager;
    }

    /**
     * @param editCustomMeta
     *            the editCustomMeta to set
     */
    public void setEditCustomMeta(CustomMeta editCustomMeta) {
	this.editCustomMeta = editCustomMeta;
	initCustomFieldValues();
    }

    @Autowired
    public void setLookupManager(@Qualifier("lookupManager") LookupManager lookupManager) {
	this.lookupManager = lookupManager;
    }

    /**
     * @param postDeleteUpdate
     *            the postDeleteUpdate to set
     */
    public void setPostDeleteUpdate(String postDeleteUpdate) {
	this.postDeleteUpdate = postDeleteUpdate;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public void setSelectedCustomMeta(CustomMeta customMeta) {
	this.selectedCustomMeta = customMeta;
    }

    /**
     * @param selectedCustomMetaValue
     *            the selectedCustomMetaValue to set
     */
    public void setSelectedCustomMetaValue(CustomMetaValue selectedCustomMetaValue) {
	this.selectedCustomMetaValue = selectedCustomMetaValue;
    }



}