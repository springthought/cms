package com.springthought.webapp.action;

import com.springthought.model.Person;
import org.primefaces.event.SelectEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component("menuSearch")
@Scope("session")
public class menuSearch extends BasePage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8240817772982180244L;
    private List<Person> people;
    private Person selectedPerson;
    private Long personId;
    private String supportURL = "https://springthought.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&screenshot=no";

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public List<Person> completePeople(String query) {

        List<Person> suggestions = new ArrayList<Person>();

        for (Person p : people) {
            if (p.getName().toLowerCase().contains(query.toLowerCase()))
                suggestions.add(p);
        }
        return suggestions;
    }

    public char fetchPersonGroup(Person person) {
        return (person == null ? '\0' : person.getFirstName().charAt(0));
    }

    @PostConstruct
    private void initialization() {
        people = getDataService().getPersons();
    }

    public Person getSelectedPerson() {
        return selectedPerson;
    }

    public void setSelectedPerson(Person selectedPerson) {
        this.selectedPerson = selectedPerson;
    }

    public void onItemSelect(SelectEvent event) {
        personId = ((Person) event.getObject()).getPersonId();
        getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), "null", go());
    }

    private String go() {
        return "/views/personForm.xhtml?faces-redirect=true&includeViewParams=true";
    }

    public String getSupportURL() {
        return supportURL;
    }

    public void setSupportURL(String supportURL) {
        this.supportURL = supportURL;
    }
}
