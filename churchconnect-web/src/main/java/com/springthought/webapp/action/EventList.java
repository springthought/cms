
package com.springthought.webapp.action;

import com.springthought.model.Event;
import com.springthought.service.GenericManager;
import com.springthought.util.Queries;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("eventList")
@Scope("view")
public class EventList extends BasePage implements Serializable {

	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	private String query;
	private GenericManager<Event, Long> eventManager;
	private Event selectedEvent = new Event();
	private boolean checked;

	@Autowired
	public void setEventManager(@Qualifier("eventManager") GenericManager<Event, Long> eventManager) {
		this.eventManager = eventManager;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	public EventList() {
		setSortColumn("eventId"); // sets the default sort column
	}

	@SuppressWarnings("unchecked")
	public List<Event> getEvents() {
		return sort(eventManager.findByNamedQuery(Queries.EVENT_IS_PARENT, null));

	}

	public String search() {
		return "success";
	}

	public Event getSelectedEvent() {
		return selectedEvent;
	}

	public void setSelectedEvent(Event event) {
		this.selectedEvent = event;
	}

	public void delete(ActionEvent event) {

		try {

			Event e = eventManager.get(selectedEvent.getEventId());

			if (e.isParent() == null || e.isParent()) {
				e.removeAllEventOccurrences();
			}

			e.removeAllGroups();
			e.removeAttendance();
			e.removeResource();
			e.removeLocation();

			eventManager.remove(e.getEventId());
			addMessage("event.deleted");

		} catch (DataIntegrityViolationException e) {
			DataIntegrityViolationExceptionHandler(e);
		} catch (Exception e) {
			UUID guid = UUID.randomUUID();
			addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
			log.error(guid.toString(), e);
		}

	}

	public void radioSelected(SelectEvent event) {
		checked = true;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
