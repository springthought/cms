/**
 *
 */
package com.springthought.webapp.action;

import biweekly.ICalVersion;
import biweekly.ICalendar;
import biweekly.io.TzUrlDotOrgGenerator;
import biweekly.io.text.ICalWriter;
import biweekly.property.RawProperty;
import com.springthought.Constants;
import com.springthought.Constants.RepeatOption;
import com.springthought.model.Event;
import com.springthought.model.EventType;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import com.springthought.service.ICalBuilderManager;
import com.springthought.util.DateUtil;
import com.springthought.util.EventDateRepeaterUtil;
import com.springthought.util.Queries;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author eairrick
 *
 */

@Component("calendarForm")
@Scope("view")
public class CalendarForm extends BasePage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String I_CALENDAR_NAME = "X-WR-CALNAME";

    // private ScheduleModel model = new DefaultScheduleModel();
    private ScheduleModel model;

    private GenericManager<Event, Long> eventManager;
    private DefaultScheduleEvent selectedEvent = new DefaultScheduleEvent();
    private Event selectedCalendarEvent = new Event();
    private String mode = Constants.EDIT_MODE_ONE;
    private String id = null;

    private ICalBuilderManager iCalBuilder = null;

    private List<EventType> eventTypeFilters = new ArrayList<EventType>();

    private DataService dataService;
    private Date initialDate;

    @Override
    @Autowired
    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    @Autowired
    public void setiCalService(ICalBuilderManager iCalBuilder) {
        this.iCalBuilder = iCalBuilder;
    }

    /**
     * @return the model
     */
    @Autowired
    public void setEventManager(@Qualifier("eventManager") GenericManager<Event, Long> eventManager) {
        this.eventManager = eventManager;
    }

    public ScheduleModel getModel() {
        return model;
    }

    public DefaultScheduleEvent getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(DefaultScheduleEvent selectedEvent) {
        this.selectedEvent = selectedEvent;
        this.id = getSelectedEvent().getId();

    }

    public CalendarForm() {
        super();
    }

    /**
     * @param model
     *                  the model to set
     */
    public void setModel(ScheduleModel model) {
        this.model = model;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void editMode(String mode) {
        setMode(mode);
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public List<EventType> getEventTypeFilters() {
        return eventTypeFilters;
    }

    public void setEventTypeFilters(List<EventType> eventTypeFilter) {
        this.eventTypeFilters = eventTypeFilter;
    }

    public Event getSelectedCalendarEvent() {
        return selectedCalendarEvent;
    }

    public void setSelectedCalendarEvent(Event selectedCalendarEvent) {
        this.selectedCalendarEvent = selectedCalendarEvent;
    }

    public String save() {

        return "calenderForm";
    }

    private void saveEvent(Event event) {

        try {
            eventManager.save(event);
            String key = "event.updated";
            addMessage(key);

        } catch (ConstraintViolationException e) {

            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

    }

    public String delete() {
        addMessage("calendar.deleted");
        return "calenderForm";
    }

    public void timezoneChangedListener() {
        System.out.println("timezoneChangedListener");
    }

    public void onEventSelect(SelectEvent selectEvent) {
        setSelectedEvent((DefaultScheduleEvent) selectEvent.getObject());

        selectedCalendarEvent = (Event) selectedEvent.getData();

        if (selectedCalendarEvent != null) {
            setMode(selectedCalendarEvent.getRepeat().equals(RepeatOption.NO) ? Constants.EDIT_MODE_ONE
                    : Constants.EDIT_MODE_SERIES);
        }
    }

    public void onDateSelect(SelectEvent selectEvent) {

        setSelectedEvent(new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject()));

        Event event = new Event((Date) selectEvent.getObject(), (Date) selectEvent.getObject());

        setInitialDate(event.getStartDate());
        getSelectedEvent().setData(event);
        setMode(Constants.EDIT_MODE_ONE);

    }

    public void addEvent(AjaxBehaviorEvent event) throws ParseException {

        onDateSelect(new SelectEvent(event.getComponent(), event.getBehavior(),
                DateUtil.setHour(DateUtil.getToday().getTime(), 8)));

    }

    public void exportCalendar(ActionEvent event) {

        try (ICalWriter icalWriter = new ICalWriter(getResponse().getOutputStream(), ICalVersion.V2_0);) {

            // signal JSF that it doesn't need to render the response
            getFacesContext().responseComplete();

            icalWriter.getTimezoneInfo().setGenerator(new TzUrlDotOrgGenerator(true));

            icalWriter.getTimezoneInfo().setDefaultTimeZone(TimeZone.getTimeZone(getTimezone()));

            ICalendar ical = iCalBuilder.generateICalendar(getBrowserLocale(), retriveParentEvents());

            RawProperty prop = new RawProperty(I_CALENDAR_NAME, Constants.ICALENDAR_NAME);

            ical.addProperty(prop);

            // obtains ServletContext
            ServletContext context = getServletContext();

            // gets MIME type of the file
            String mimeType = context.getMimeType(Constants.ICALENDAR_FILE_NAME);
            if (mimeType == null) {
                // set to binary type if MIME mapping not found
                mimeType = "application/octet-stream";
            }

            log.debug("MIME type: " + mimeType);

            // modifies response

            getResponse().setContentType(mimeType);

            // forces download
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", Constants.ICALENDAR_FILE_NAME);

            getResponse().setHeader(headerKey, headerValue);

            icalWriter.write(ical);

        } catch (IOException e) {
            log.fatal(e.getMessage(), e);
        } finally {
            try {
                getResponse().getOutputStream().close();
            } catch (IOException e) {
                log.fatal(e.getMessage(), e);
            }
        }
    }

    /**
     *
     * Project: framework Method : exportCalendar Return : void Params : Author :
     * eairrick
     *
     *
     * public void exportCalendar() {
     *
     * // reads input file from an absolute path String filePath =
     * "C:/temp/book.pdf"; File downloadFile = new File(filePath);
     *
     * FileInputStream inStream = null; try { inStream = new
     * FileInputStream(downloadFile); } catch (FileNotFoundException e) {
     * e.printStackTrace(); }
     *
     * // if you want to use a relative path to context root: String relativePath =
     * getServletContext().getRealPath(""); System.out.println("relativePath = " +
     * relativePath);
     *
     * // obtains ServletContext ServletContext context = getServletContext();
     *
     * // gets MIME type of the file String mimeType =
     * context.getMimeType(filePath); if (mimeType == null) { // set to binary type
     * if MIME mapping not found mimeType = "application/octet-stream"; }
     * System.out.println("MIME type: " + mimeType);
     *
     * // modifies response
     *
     * getResponse().setContentType(mimeType); getResponse().setContentLength((int)
     * downloadFile.length()); getFacesContext().responseComplete();
     *
     * // forces download String headerKey = "Content-Disposition"; String
     * headerValue = String.format("attachment; filename=\"%s\"",
     * downloadFile.getName()); getResponse().setHeader(headerKey, headerValue);
     *
     * // obtains response's output stream OutputStream outStream = null; try {
     * outStream = getResponse().getOutputStream(); byte[] buffer = new byte[4096];
     * int bytesRead = -1;
     *
     * while ((bytesRead = inStream.read(buffer)) != -1) { outStream.write(buffer,
     * 0, bytesRead); } } catch (IOException e) {
     *
     * e.printStackTrace(); } finally {
     *
     * try { inStream.close(); outStream.flush(); outStream.close(); // not sure if
     * I should close the response // output stream may cause issues
     *
     * } catch (IOException e) { e.printStackTrace(); }
     *
     * }
     *
     * }
     */
    public void onEventMove(ScheduleEntryMoveEvent event) {

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved",
                "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta() + ", Start Date: "
                        + dateFormat.format(event.getScheduleEvent().getStartDate()) + ", End Date: "
                        + dateFormat.format(event.getScheduleEvent().getEndDate()));

        getFacesContext().addMessage(null, message);

        saveEvent(updateEvent(event.getScheduleEvent()));

    }

    public void onEventResize(ScheduleEntryResizeEvent event) {

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event time changed",
                "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta() + ", Start Date: "
                        + dateFormat.format(event.getScheduleEvent().getStartDate()) + ", End Date: "
                        + dateFormat.format(event.getScheduleEvent().getEndDate()));

        getFacesContext().addMessage(null, message);

        saveEvent(updateEvent(event.getScheduleEvent()));

    }

    private Event updateEvent(ScheduleEvent schdlEvent) {

        Event scheduleEvent = (Event) schdlEvent.getData();

        scheduleEvent.setStartDate(scheduleEvent.getStartDate());

        scheduleEvent.setEndDate(scheduleEvent.getEndDate());

        return scheduleEvent;
    }

    public void handleSelectedEvent(String mode) {

        Event objEvent = (Event) selectedEvent.getData();

        if (mode.equals(Constants.CALENDAR_DETAILS)) {

            navigateToPage("/views/eventForm.xhtml?faces-redirect=true&from=calendarform" + "&mode=" + this.getMode()
                    + "&eventId=" + objEvent.getEventId());
        } else {

            String id = objEvent.getAttendance() != null ? objEvent.getAttendance().getAttendanceId().toString() : "0";

            String uri = getRequest().getRequestURI();

            StringBuffer rURL = getRequest().getRequestURL();

            String host = rURL.substring(0, rURL.indexOf(uri));

            String url = host + getServletContext().getContextPath()
                    + "/views/attendanceForm.xhtml?faces-redirect=true&from=calendarform" + "&attendanceId=" + id
                    + "&eventId=" + objEvent.getEventId();

            /*@TODO: Determine URL to call this from the browser client; however go through the JSF servlet context
             * Current URL bypasses the JSF servlet.
             *
             * String command = "openWindow('" + url + "','" +
             * getText("attendanceDetail.title") + "','" + getText("errors.popup.blocker") +
             * "')";
             */

            String command = "window.open('" + url + "','" + getText("attendanceDetail.title") + "')";

            PrimeFaces.current().executeScript(command);

        }

    }

    @SuppressWarnings("unchecked")
    public void viewEvent() {

        Long id = null;

        Map<String, List<String>> params = new HashMap<String, List<String>>();

        List<String> parmFrom = new ArrayList<String>();
        List<String> parmEvent = new ArrayList<String>();
        List<String> parmEditMode = new ArrayList<String>();
        List<String> parmDate = new ArrayList<String>();

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        dateFormat.setTimeZone(TimeZone.getTimeZone(this.getTimezone()));

        parmDate.add(dateFormat.format(((Event) getSelectedEvent().getData()).getStartDate()));

        if (getMode().equalsIgnoreCase(Constants.EDIT_MODE_ONE)) {
            id = ((Event) getSelectedEvent().getData()).getEventId();
        } else {
            id = ((Event) getSelectedEvent().getData()).getParentId();
        }

        parmFrom.add("calendarform");
        parmEvent.add(id != null ? String.valueOf(id) : "0");
        parmEditMode.add(getMode());

        params.put("from", parmFrom);
        params.put("eventId", parmEvent);
        params.put("mode", parmEditMode);
        params.put("date", parmDate);

        Map<String, Object> options = new HashMap<String, Object>();

        options.put("modal", true);
        options.put("closable", false);
        options.put("resizable", true);
        options.put("draggable", true);

        options.put("contentHeight", 550);
        options.put("height", 550);

        options.put("contentWidth", 700);
        options.put("width", 700);

        PrimeFaces.current().dialog().openDynamic("eventForm", options, params);

    }

    private void selectEventMode() {

        PrimeFaces.current().executeScript("PF('eventEditDlgWv').show()");

    }

    public void handleReturn(SelectEvent event) {

        Event returnedEvent = (Event) event.getObject();

        if (returnedEvent != null) {

            try {
                returnedEvent.setId(this.id);
                BeanUtils.copyProperties(getSelectedEvent(), returnedEvent);
                getSelectedEvent().setData(returnedEvent);
                this.model.updateEvent(getSelectedEvent());
            } catch (IllegalAccessException e) {
                log.error(e.getMessage(), e);
            } catch (InvocationTargetException e) {
                log.error(e.getMessage(), e);
            }

        }
    }

    private ArrayList<Event> retriveParentEvents() {

        return (ArrayList<Event>) eventManager.findByNamedQuery(Queries.EVENT_IS_PARENT, null);

    }

    public void filterCalendar(ActionEvent event) {

        for (EventType type : this.getEventTypeFilters()) {
            System.out.println("Event Type = " + type.getName());
        }
    }

    @PostConstruct
    private void initilization() {

        /*
         * DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
         * dateFormat.setTimeZone(TimeZone.getTimeZone(this.getTimezone()));
         *
         * String startDate = getParameter("startDate") == null ?
         * dateFormat.format(DateUtil.setMinute(DateUtil.setHour((new Date()), 8), 0)) :
         * getParameter("startDate");
         */

        Long startDate = getParameter("startDate") == null ? new Long(new Date().getTime())
                : Long.parseLong(getParameter("startDate"));

        initialDate = new Date(startDate);

        eventTypeFilters = dataService.getEventTypes();

        model = new LazyScheduleModel() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void loadEvents(Date start, Date end) {

                clear();

                HashMap<String, Object> queryParams = new HashMap<String, Object>();

                queryParams.put(Queries.EVENT_START_PARM, start);

                ArrayList<Event> events = (ArrayList<Event>) eventManager
                        .findByNamedQuery(Queries.EVENT_IS_PARENT_IN_RANGE, queryParams);

                for (Event event : events) {

                    for (Event ocurr : event.getEventOccurrences()) {

                        if (DateUtil.isDateWithinRange(ocurr.getStartDate(), start, end)
                                && eventTypeFilters.contains(ocurr.getEventType())) {

                            Date startDte = EventDateRepeaterUtil.convertTz(ocurr.getStartDate(),
                                    TimeZone.getTimeZone(getTimezone()));

                            Date endDte = EventDateRepeaterUtil.convertTz(ocurr.getEndDate(),
                                    TimeZone.getTimeZone(getTimezone()));

                            DefaultScheduleEvent schedEvent = new DefaultScheduleEvent(ocurr.getTitle(), startDte,
                                    endDte, event.isAllDay());

                            schedEvent.setData(ocurr);

                            schedEvent.setStyleClass(event.getEventType().getStyleClass());

                            schedEvent.setDescription(ocurr.getDescription());

                            model.addEvent(schedEvent);
                        }

                    }

                }

            }
        };

    }
}
