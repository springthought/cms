package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.Person;
import com.springthought.model.Role;
import com.springthought.model.User;
import com.springthought.service.GenericManager;
import com.springthought.service.RoleManager;
import com.springthought.service.SecurityContextManager;
import com.springthought.service.UserExistsException;
import com.springthought.util.ConvertUtil;
import com.springthought.webapp.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.MailException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * JSF Page class to handle editing a user with a form.
 *
 * @author mraible
 */

@Scope("view")
@Component("userForm")
public class UserForm extends BasePage implements Serializable {

    private static final long serialVersionUID = -1141119853856863204L;
    private RoleManager roleManager;
    private String id;
    private User user = new User();
    private Map<String, String> availableRoles;
    private String[] userRoles;
    private Person selectedPerson;
    private String mode ="USER";

    private GenericManager<Person, Long> personManager;

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    List<Person> people = new ArrayList<Person>(0);


    private String from;


    @Autowired
    SecurityContextManager securityContext;
    private Role role;

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        initUser(user);
    }

    private void initUser(User user) {
        if (user.getUsername() != null) {
            user.setConfirmPassword(user.getPassword());
            setRole(user.getRoles().stream().findFirst().orElse(new Role()));
        }
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    @Autowired
    public void setRoleManager(@Qualifier("roleManager") RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    public String add() {

        user = new User();
        user.setEnabled(true);
        user.addRole(new Role(Constants.USER_ROLE));

        return "editProfile";
    }

    public String cancel() {
        if (log.isDebugEnabled()) {
            log.debug("Entering 'cancel' method");
        }

        if (!"list".equals(getParameter("from"))) {
            return "mainMenu";
        } else {
            return "cancel";
        }
    }

    public void setFrom(String from) {
        this.from = from;
    }


    public String edit() {

        return "editProfile";
    }

    /**
     * Convenience method for view templates to check if the user is logged in
     * with RememberMe (cookies).
     *
     * @return true/false - false if user interactively logged in.
     */
    public boolean isRememberMe() {
        if (user != null && user.getId() == null)
            return false; // check for add()

        AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
        SecurityContext ctx = SecurityContextHolder.getContext();

        if (ctx != null) {
            Authentication auth = ctx.getAuthentication();
            return resolver.isRememberMe(auth);
        }
        return false;
    }

    public void save(ActionEvent event) throws IOException {
        this.save();
    }

    public String save() throws IOException {



        boolean isNew = (user.getId() == null || user.getId() == 0);
        String decryptedPassword = user.getPassword();
		
		

        user.addRole(roleManager.get(getRole().getId()));

        Integer originalVersion = user.getVersion();


        String userAgent = getRequest().getHeader("User-Agent");
        // For some reason, IE causes version to be 0. Set it to null so test
        // will pass.
        if (userAgent != null && userAgent.contains("MSIE")
                && user.getVersion() == 0) {
            log.debug("Detected IE, setting version and id to null");
            user.setId(null);
            user.setVersion(null);
        }

        try {
            if (user.hasPerson()) {
                selectedPerson = personManager.save(selectedPerson);
            }else {
                user = userManager.saveUser(user);
            }
        } catch (AccessDeniedException ade) {
            // thrown by UserSecurityAdvice configured in aop:advisor
            // userManagerSecurity
            log.warn(ade.getMessage());
            getResponse().sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        } catch (UserExistsException e) {
            addError("errors.existing.user", new Object[]{user.getUsername(),
                    user.getEmail()});

            // reset the version # to what was passed in
            user.setVersion(originalVersion);
            return "editProfile";
        }

        if (getPrincipalUserName().equals(user.getUsername())) {
            // add success messages; if current logged in user match the rec you are saving.
            addMessage("user.saved");
            return null;
        } else {
            // add success messages
            //if ("".equals(getParameter("userForm:version")))
            if (isNew) {
                addMessage("user.added", user.getFullName());

                if (user.isEmailUserNameAndPassword()) {
                    try {

                        String key = this.mode.equals("INVITE") ?  "members.createyouraccount" :  "newuser.email.message";
                        String uri =this.mode.equals("INVITE") ?  "/invite/"+user.getUsername() :  "";
                        this.templateName =   this.mode.equals("INVITE") ?  "invitationTemplate.vm" : "mailTemplate.vm";


                        sendUserMessage(
                                this.currentUser(),
                                getText(key, new String[]{currentUser().getFullName(), decryptedPassword}),
                                RequestUtil.getAppURL(getRequest()) + uri, getText("webapp.name") + " " + getText("button.account"));


                    } catch (MailException me) {
                        addError(me.getCause().getLocalizedMessage());
                    }
                }
                return "/admin/users"; // return to list screen
            } else {
                addMessage("user.updated.byAdmin", user.getFullName());
                return "/admin/users";
                //return "editProfile"; // return to current page
            }
        }
    }

    public void sendInvite() throws IOException {

        this.mode = "INVITE";

        Long uuid = UUID.randomUUID().getLeastSignificantBits();

        user = new User();
        user.setEnabled(true);
        user.setInvitationDate(new Date());
        user.setEmail(selectedPerson.getEmail());
        user.setFirstName(selectedPerson.getFirstName());
        user.setLastName(selectedPerson.getLastName());
        //@todo need to let user select there own user name on sign up
        user.setUsername(uuid.toString());
        //@todo change to let user enter there own password v/s a system generated password
        user.setPassword(uuid.toString());
        selectedPerson.setUser(user);
        user.setPerson(selectedPerson);

        role = getRoles().stream()
                .filter(r -> Constants.MEMBER_ROLE.equals(r.getName()))
                .findFirst()
                .orElse(null);

        save();
        return;

    }


    public Object handleCloseDialog() {
        this.user = new User();
        this.showMore = false;
        this.setRole(null);
        return null;
    }


    public String getFrom() {
        return this.from;
    }

    // Form Controls ==========================================================
    @SuppressWarnings("unchecked")
    public Map<String, String> getAvailableRoles() {
        if (availableRoles == null) {
            @SuppressWarnings("rawtypes")
            List roles = (List) getServletContext().getAttribute(
                    Constants.AVAILABLE_ROLES);
            availableRoles = ConvertUtil.convertListToMap(roles);
        }

        return availableRoles;
    }

    public List<Role> getRoles() {
        List<Role> roles = roleManager.getAllDistinct();
        return roles;

    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return this.role;
    }


    public String[] getUserRoles() {

        userRoles = new String[user.getRoles().size()];

        int i = 0;

        if (userRoles.length > 0) {
            for (Role role : user.getRoles()) {
                userRoles[i] = role.getName();
                i++;
            }
        }

        return userRoles;
    }

    public void setUserRoles(String[] userRoles) {
        this.userRoles = userRoles;
    }


    @PostConstruct
    public void initialization() {

        HttpServletRequest request = getRequest();

        id = getParameter("id");
        from = Optional.ofNullable(getParameter("from")).orElse("");

        // if a user's id is passed in

//        if ( ! getFrom().equals("add")) {
        if (id != null) {
            log.debug("Editing user, id is: " + id);
            // lookup the user using that id
            user = userManager.getUser(id);
            initUser(user);
        } else {
            //user = userManager.getUserByUsername(request.getRemoteUser());
            user = new User();
            setRole(null);
        }
//        }


        if (user.getUsername() != null) {

            initUser(user);

            if (isRememberMe()) {
                // if user logged in with remember me, display a warning that
                // they can't change passwords
                log.debug("checking for remember me login...");
                log.trace("User '" + user.getUsername()
                        + "' logged in with cookie");
                addMessage("userProfile.cookieLogin");
            }
        }

        people = personManager.getAllDistinct();

    }


    public Person getSelectedPerson() {
        return selectedPerson;
    }

    public void setSelectedPerson(Person selectedPerson) {
        log.info(selectedPerson);
        this.selectedPerson = selectedPerson;
    }
}
