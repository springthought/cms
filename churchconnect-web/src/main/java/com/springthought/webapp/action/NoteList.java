package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Note;
import com.springthought.service.GenericManager;
import org.primefaces.PrimeFaces;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Component("noteList")
@Scope("view")
public class NoteList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1105393695970773354L;
    private String query;
    private GenericManager<Note, Long> noteManager;
    private Note selectedNote = new Note();
    private List<Note> notes = new ArrayList<Note>();
    private List<Note> deletedNotes = new ArrayList<Note>();
    private boolean checked;
    private Note editNote = new Note(getText("note.setTitle"));


    @Autowired
    @Qualifier("personForm")
    private PersonForm personForm;

    public NoteList() {
        setSortColumn("noteId"); // sets the default sort column
    }

    /**
     * Project: framework Method : add Return : void Params : @param event
     * Author : eairrick
     */
    public void add(ActionEvent event) {
        Note note = new Note();
        note.setName(getText("note.setTitle"));
        note.setPerson(this.personForm.getPerson());
        personForm.getPerson().getNotes().add(note);
    }

    public Object handleCloseNoteDialog() {
        editNote = new Note(getText("note.setTitle"));
        return null;
    }

    /**
     * Project: framework Method : applyEdit Return : void Params : @param event
     * Author : eairrick
     */

    public void applyEdit(ActionEvent event) {


        if (!this.editNote.getName().trim().isEmpty()) {

            PrimeFaces.current().ajax().addCallbackParam("valid", true);

            if (!editNote.isNew()) {
                this.editNote.setLastModifiedDate(new Date());
            } else {
                this.editNote.setLastModifiedDate(new Date());
                this.editNote.setCreateDate(new Date());
            }



            /**build reference**/
            this.editNote.setPerson(this.personForm.getPerson());
            personForm.getPerson().getNotes().add(this.editNote);



            /** Force HTML java script call**/
            //PrimeFaces.current().executeScript("PF('wvNote.saveHTML()';");



        } else {
            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            addError("noteList.titleRequired");
        }

    }

    public void delete(ActionEvent event) {

        Boolean found = personForm.getPerson().getNotes().remove(selectedNote);

        if (found) {
            this.deletedNotes.add(selectedNote);
            addMessage("note.deleted");
        }



    }

    /**
     * @return the editNote
     */
    public Note getEditNote() {
        return editNote;
    }

    public List<Note> getNotes() {

        if (notes.isEmpty()) {

            try {

                this.notes = new ArrayList<Note>(personForm.getPerson()
                        .getNotes());

            } catch (SearchException se) {
                addError(se.getMessage());
            }
        }
        return notes;
    }

    public Set<Note> getNoteset() {

        return personForm.getPerson().getNotes();
    }

    /**
     * @return the personForm
     */
    public PersonForm getPersonForm() {
        return personForm;
    }

    public String getQuery() {
        return query;
    }

    public Note getSelectedNote() {
        return selectedNote;
    }

    public void handleClose(CloseEvent event) {

        facesContext = FacesContext.getCurrentInstance();

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
                event.getComponent().getId() + " closed",
                "So you don't like nature?");

        facesContext.addMessage(null, message);

        log.debug(this.editNote.getNotes());
        log.debug(this.editNote.getName());

    }



    public boolean isChecked() {
        return checked;
    }

    public void radioSelected(SelectEvent event) {
        checked = true;
    }

    public void save(ActionEvent event) {

        for (Note note : this.notes) {
            log.debug(note.getName() + " - " + note.getNotes());
        }
    }

    public String search() {
        return "success";
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void setEditNote(Note note) {
        this.editNote = note;
    }

    @Autowired
    public void setNoteManager(
            @Qualifier("noteManager") GenericManager<Note, Long> noteManager) {
        this.noteManager = noteManager;
    }

    /**
     * @param personForm the personForm to set
     */
    public void setPersonForm(PersonForm personForm) {
        this.personForm = personForm;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setSelectedNote(Note note) {
        this.selectedNote = note;
    }
}
