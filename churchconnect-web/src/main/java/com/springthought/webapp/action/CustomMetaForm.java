package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.CodeListType;
import com.springthought.Constants.CustomFieldType;
import com.springthought.model.CustomMeta;
import com.springthought.model.CustomMetaValue;
import com.springthought.model.LabelValue;
import com.springthought.service.GenericManager;
import com.springthought.util.Queries;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Component("customMetaForm")
@Scope("view")
public class CustomMetaForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -7533235926357592157L;
    /**
     *
     */

    private GenericManager<CustomMeta, Long> customMetaManager;
    private CustomMeta customMeta = new CustomMeta();
    private Long customMetaId;
    private List<LabelValue> dataTypes;
    private SelectItem[] dataTypeOptions;
    private SelectItem[] assignToOptions;
    private boolean checkedValue;
    private boolean addMode = false;
    private CustomMetaValue customFieldValue = new CustomMetaValue();


    private GenericManager<CustomMetaValue, Long> customMetaValueManager;

    private List<CustomMetaValue> customFieldValues = new ArrayList<CustomMetaValue>();

    private CustomMeta selectedCustomMeta = new CustomMeta();

    private CustomMetaValue selectedCustomMetaValue = new CustomMetaValue();

    public void applyEdit(ActionEvent event) {


        if (customMeta.getName().trim().isEmpty()) {

            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            addError("customMetaDetail.LabelIsRequired");

        } else if (customMeta.getDataType().equals(Constants.DATA_TYPE_NUMBER)
                && customMeta.getMinValue() > customMeta.getMaxValue()) {

            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            addError("errors.min.greathan.max", new Object[]{customMeta.getMinValue(), customMeta.getMaxValue()});

        } else if (customMeta.getDataType().equals(Constants.DATA_TYPE_LIST)
                && CollectionUtils.isEmpty(customMeta.getCustomMetaValues())) {

            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            addError("errors.customfield.valuesRequired");

        } else {

            try {

                save();
                PrimeFaces.current().ajax().addCallbackParam("valid", true);

            } catch (ConstraintViolationException e) {

                DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));

            }

        }
    }

    private SelectItem[] createFilterOptions(List<LabelValue> labelValues) {

        SelectItem[] options = new SelectItem[labelValues.size() + 1];
        options[0] = new SelectItem("", getText("button.selectOne"));

        for (int i = 0; i < labelValues.size(); i++) {
            options[i + 1] = new SelectItem(labelValues.get(i).getValue(), labelValues.get(i).getLabel());
        }

        return options;
    }

    public String delete() {
        customMetaManager.remove(customMeta.getCustomMetaId());
        addMessage("customMeta.deleted");

        return "customMetas";
    }

    public String edit() {

        return "customMetaForm";
    }

    public void saveListValue(ActionEvent event) {

        CustomMetaValue customMetaValue = customMeta.getCustomMetaValues().stream().filter(mv -> mv.equals(selectedCustomMetaValue)).findFirst().orElse(null);

        if (customMetaValue == null) {
            getSelectedCustomMetaValue().setCustomMeta(getCustomMeta());
            customMeta.getCustomMetaValues().add(getSelectedCustomMetaValue());
        } else {
            customMetaValue = getSelectedCustomMetaValue();
        }

    }

    public void deleteListValue(ActionEvent event) {

        boolean deleted = customMeta.getCustomMetaValues().remove(selectedCustomMetaValue);
        selectedCustomMetaValue = new CustomMetaValue();
        addMessage("userValueDetail.deleted");

    }

    public Object handleCloseDialog() {
        selectedCustomMetaValue = new CustomMetaValue();
        return null;
    }

    public SelectItem[] getAssignToOptions() {
        return assignToOptions;
    }

    public Set<CustomMetaValue> getCustomFieldValues() {
        return customMeta.getCustomMetaValues();
    }

    public CustomMeta getCustomMeta() {
        return customMeta;
    }

    public SelectItem[] getDataTypeOptions() {
        return dataTypeOptions;
    }

    public List<LabelValue> getDataTypes() {
        return dataTypes;
    }

    public CustomMeta getSelectedCustomMeta() {
        return selectedCustomMeta;
    }

    public CustomMetaValue getSelectedCustomMetaValue() {
        return selectedCustomMetaValue;
    }

    private void initCustomFieldValues() {


        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID_PARM, customMeta.getCustomMetaId());
        setCustomFieldValues(
                customMetaValueManager.findByNamedQuery(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID, queryParams));

    }

    @PostConstruct
    private void initialization() {

        customMetaId = getParameter("customMetaId") == null ? 0L : new Long(getParameter("customMetaId"));
        if (customMetaId != null && customMetaId != 0) {
            customMeta = customMetaManager.get(customMetaId);
            initCustomFieldValues();

        } else {
            customMeta = new CustomMeta();
        }

        if (this.dataTypes == null) {

            // this.setAscending(true);
            this.dataTypes = lookupManager.getSystemValueByCodeListType(CodeListType.DATATYPE);

            this.dataTypeOptions = createFilterOptions(this.dataTypes);

            ArrayList<LabelValue> assignTo = new ArrayList<LabelValue>();
            assignTo.add(new LabelValue(CustomFieldType.HOUSEHOLD.getValue(), CustomFieldType.HOUSEHOLD.toString()));
            assignTo.add(new LabelValue(CustomFieldType.PERSON.getValue(), CustomFieldType.PERSON.toString()));
            this.assignToOptions = createFilterOptions(assignTo);

        }

    }

    public void processSelectionChange(AjaxBehaviorEvent event) {
        log.debug(event.getSource());
    }


    public boolean isDataTypeBoolean() {
        return customMeta.getDataType().equals("BL");
    }

    public boolean isDataTypeDate() {
        return customMeta.getDataType().equals("DT");
    }

    public boolean isDataTypeList() {
        return customMeta.getDataType().equals("LT");
    }

    public boolean isDataTypeNumber() {
        return customMeta.getDataType().equals("NM");
    }

    public boolean isDataTypeText() {
        return customMeta.getDataType().equals("TX");
    }

    public boolean isCheckedValue() {
        return checkedValue;
    }

    public boolean isAddMode() {
        return addMode;
    }

    public void setAddMode(boolean addMode) {
        this.addMode = addMode;
    }

    public void setCheckedValue(boolean checkedValue) {
        this.checkedValue = checkedValue;
    }

    public String save() {
        boolean isNew = (customMeta.getCustomMetaId() == null || customMeta.getCustomMetaId() == 0);

        customMeta = customMetaManager.save(customMeta);

        String key = (isNew) ? "customMeta.added" : "customMeta.updated";
        addMessage(key);

        if (isNew) {
            return "customMetas";
        } else {
            return "customMetas";
        }
    }

    public void save(ActionEvent event) {
        this.save();
    }

    public void setAssignToOptions(SelectItem[] assignToOptions) {
        this.assignToOptions = assignToOptions;
    }

    public void setCustomFieldValues(List<CustomMetaValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    public void setCustomMeta(CustomMeta customMeta) {
        this.customMeta = customMeta;
    }

    public void setCustomMetaId(Long customMetaId) {
        this.customMetaId = customMetaId;
    }

    @Autowired
    public void setCustomMetaManager(
            @Qualifier("customMetaManager") GenericManager<CustomMeta, Long> customMetaManager) {
        this.customMetaManager = customMetaManager;
    }

    @Autowired
    public void setCustomMetaValueManager(
            @Qualifier("customMetaValueManager") GenericManager<CustomMetaValue, Long> customMetaValueManager) {
        this.customMetaValueManager = customMetaValueManager;
    }

    public void setDataTypeOptions(SelectItem[] dataTypeOptions) {
        this.dataTypeOptions = dataTypeOptions;
    }

    public void setDataTypes(List<LabelValue> dataTypes) {
        this.dataTypes = dataTypes;
    }

    public void setSelectedCustomMeta(CustomMeta customMeta) {
        this.selectedCustomMeta = customMeta;
    }

    /**
     * @param selectedCustomMetaValue the selectedCustomMetaValue to set
     */
    public void setSelectedCustomMetaValue(CustomMetaValue selectedCustomMetaValue) {
        this.selectedCustomMetaValue = selectedCustomMetaValue;
    }

    public void updateMaxValue(AjaxBehaviorEvent event) {
        customMeta.setMaxValue(customMeta.getMinValue() + 1);
    }

}