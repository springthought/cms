package com.springthought.webapp.action;

import com.springthought.model.Group;
import com.springthought.service.GenericManager;
import org.apache.commons.dbcp.BasicDataSource;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("groupList")
@Scope("view")
public class GroupList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String query;
    private GenericManager<Group, Long> groupManager;
    private Group selectedGroup = new Group();
    private boolean checked;

    @Autowired
    public void setGroupManager(@Qualifier("groupManager") GenericManager<Group, Long> groupManager) {
	this.groupManager = groupManager;
	BasicDataSource dbcp = new BasicDataSource();

    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public GroupList() {
	setSortColumn("groupId"); // sets the default sort column
    }

    public List<Group> getGroups() {
	return groupManager.getAllDistinct();
    }

    public String search() {
	return "success";
    }

    public Group getSelectedGroup() {
	return selectedGroup;
    }

    public void setSelectedGroup(Group group) {
	this.selectedGroup = group;
    }

    public void delete(ActionEvent event) {

	Group group = groupManager.get(selectedGroup.getGroupId());

	group.removeAllEvents();
	group.removeAllGroupMembers();
	group.removeCategory();

	try {
	    groupManager.remove(group.getGroupId());
	    addMessage("group.deleted", group.getName());

	} catch (DataIntegrityViolationException e) {

	    DataIntegrityViolationExceptionHandler(e);

	} catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }
}
