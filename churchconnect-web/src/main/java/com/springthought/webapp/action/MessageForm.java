package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.SayVerbLanguage;
import com.springthought.Constants.SayVerbVoiceOption;
import com.springthought.model.Message;
import com.springthought.service.GenericManager;
import com.springthought.util.ConvertUtil;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.event.FlowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.UUID;

@Component("messageForm")
@Scope("view")
public class MessageForm extends BasePage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -681714817258376750L;
    private final String FIRST_STEP = "format";
    private final String LAST_STEP = "preview";
    private GenericManager<Message, Long> messageManager;
    private Message message = new Message();
    private Long messageId;
    private String currentStep = "format";
    private boolean skipToSave = false;

    @Autowired
    public void setMessageManager(@Qualifier("messageManager") GenericManager<Message, Long> messageManager) {
        this.messageManager = messageManager;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public boolean isSkipToSave() {
        return skipToSave;
    }

    public void setSkipToSave(boolean skipToSave) {
        this.skipToSave = skipToSave;
    }

    public String getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }

    public String delete() {
        messageManager.remove(message.getMessageId());
        addMessage("message.deleted");

        return "messages";
    }

    public String edit() {

        return "messageForm";
    }

    public String save() {

        try {

            if (message.isPhone()) {

                TwiMLResponse twiml = new TwiMLResponse();

                Say say = new Say(ConvertUtil.html2text(message.getMessage()));
                say.setVoice(message.getVoice().toString().toLowerCase());
                say.setLanguage(message.getLang().toString().toLowerCase());

                twiml.append(say);

                message.setVerbal(twiml.toXML());

            } else {
                message.setVerbal(null);
            }


            boolean isNew = (message.getMessageId() == null || message.getMessageId() == 0);
            messageManager.save(message);

            String key = (isNew) ? "message.added" : "message.updated";
            addMessage(key);

            if (isNew) {
                return "messages";
            } else {
                return "messages";
            }
        } catch (ConstraintViolationException e) {

            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

        return "";

    }

    public void save(ActionEvent event) {
        this.save();
    }

    public SayVerbVoiceOption[] getVoices() {
        return Constants.SayVerbVoiceOption.values();
    }

    public SayVerbLanguage[] getlanguages() {

        return Constants.SayVerbLanguage.values();

    }

    public String onFlowProcess(FlowEvent event) {

        if (isSkipToSave()) {
            setCurrentStep(LAST_STEP);
            return LAST_STEP;
        } else {
            setCurrentStep(event.getNewStep());
            return event.getNewStep();
        }

    }

    @PostConstruct
    private void initialization() {

        if (messageId == null) {
            messageId = getParameter("messageId") == null ? 0L : new Long(getParameter("messageId"));

        }

        if (messageId != null && messageId != 0) {
            message = messageManager.get(messageId);
        } else {
            message = new Message();
        }

    }
}
