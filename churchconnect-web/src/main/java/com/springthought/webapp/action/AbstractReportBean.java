/**
 *
 */
package com.springthought.webapp.action;

import com.springthought.Constants.ExportOption;
import com.springthought.dao.Database;
import com.springthought.util.ReportConfigUtil;
import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractReportBean extends BasePage implements
		Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7382305791656528192L;

	private ExportOption exportOption;
	private final String COMPILE_DIR = "/resources/reports/jasper/";
	private String compileFileName = "";// name of your compiled report
	private String message;
	private Map<String, Object> reportParameters = new HashMap<String, Object>();

	public AbstractReportBean() {
		super();
		setExportOption(ExportOption.PDF);
	}

	protected void prepareReport(String reportName) throws Exception {

		compileFileName = reportName;

		ServletContext context = getServletContext();

		HttpServletResponse response = getResponse();

		ReportConfigUtil.compileReport(context, getCompileDir(),
				getCompileFileName());

		File reportFile = new File(ReportConfigUtil.getJasperFilePath(context,
				getCompileDir(), getCompileFileName() + ".jasper"));

		try (Connection conn = Database.getConnection()) {

			JasperPrint jasperPrint = ReportConfigUtil.fillReport(reportFile,
					getReportParameters(), conn);

			if (getExportOption().equals(ExportOption.EXCEL)) {
				ReportConfigUtil.exportReportAsExcel(jasperPrint, response);
			} else if (getExportOption().equals(ExportOption.RTF)) {
				ReportConfigUtil.exportReportAsRTF(jasperPrint, response);
			} else {
				ReportConfigUtil.exportReportAsPDF(jasperPrint, response);
			}

			getFacesContext().responseComplete();

		} catch (Exception e) {
			log.error(e);
			throw e;
		}

	}

	public ExportOption getExportOption() {
		return exportOption;
	}

	public void setExportOption(ExportOption exportOption) {
		this.exportOption = exportOption;
	}

	protected Map<String, Object> getReportParameters() {
		return reportParameters;
	}

	protected void setReportParameters(Map<String, Object> reportParameters) {
		this.reportParameters = reportParameters;
	}

	protected String getCompileDir() {
		return COMPILE_DIR;
	}

	protected abstract String getCompileFileName();

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
