package com.springthought.webapp.action;

import com.springthought.model.GroupRole;
import com.springthought.service.GenericManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;

@Component("groupRoleForm")
@Scope("request")
public class GroupRoleForm extends BasePage implements Serializable {

    private static final long serialVersionUID = 1L;
    private GenericManager<GroupRole, Long> groupRoleManager;
    private GroupRole groupRole = new GroupRole();
    private Long grouproleId;

    @Autowired
    public void setGroupRoleManager(@Qualifier("groupRoleManager") GenericManager<GroupRole, Long> groupRoleManager) {
	this.groupRoleManager = groupRoleManager;
    }

    public GroupRole getGroupRole() {
	return groupRole;
    }

    public void setGroupRole(GroupRole groupRole) {
	this.groupRole = groupRole;
    }

    public void setGrouproleId(Long grouproleId) {
	this.grouproleId = grouproleId;
    }

    public String delete() {
	groupRoleManager.remove(groupRole.getGrouproleId());
	addMessage("groupRole.deleted");

	return "groupRoles";
    }

    public String edit() {
	// Workaround for not being able to set the id using #{param.id} when
	// using Spring-configured managed-beans
	// if (grouproleId == null) {
	grouproleId = getParameter("grouproleId") == null ? 0L : new Long(getParameter("grouproleId"));

	// }
	// Comparison to zero (vs. null) is required with MyFaces 1.2.2, not
	// with previous versions
	if (grouproleId != null && grouproleId != 0) {
	    groupRole = groupRoleManager.get(grouproleId);
	} else {
	    groupRole = new GroupRole();
	}

	return "groupRoleForm";
    }

    public String save() {

	boolean isNew = (groupRole.getGrouproleId() == null || groupRole.getGrouproleId() == 0);

	groupRoleManager.save(groupRole);

	String key = (isNew) ? "groupRole.added" : "groupRole.updated";
	addMessage(key);

	if (isNew) {
	    return "groupRoles";
	} else {
	    return "groupRoles";
	}
    }

    public void save(ActionEvent event) {
	this.save();
    }

}
