package com.springthought.webapp.action;

import com.springthought.ClassMatcher;
import com.springthought.Constants;
import com.springthought.Constants.DataImportType;
import com.springthought.model.*;
import com.springthought.service.CSVFileImporter;
import com.springthought.service.GenericManager;
import com.springthought.service.impl.CSVFileImporterImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import java.io.*;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;


@Component("importDataForm")
@Scope("view")
public class ImportDataForm extends BasePage implements Serializable {

    private static final long serialVersionUID = -4985497578849837927L;
    private final CSVFileImporter fileImporter = new CSVFileImporterImpl();
    private final NameResolverVisitor visitor = new NameResolverVisitorImpl();
    private GenericManager<Person, Long> personManager;
    private DataImportType typeOfImport = DataImportType.PEOPLE;
    private ArrayList<Object> dataRows;
    private double progress = 0;
    private String progressInformation = "";
    private ClassMatcher match;
    private boolean showErrorMessageDetails = false;
    private Batch batch = new Batch();
    private ArrayList<LabelValue> errorMessages = new ArrayList<>(0);


    public GenericManager<Person, Long> getPersonManager() {
        return personManager;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    public void handleFileUpload(FileUploadEvent event) {

        UploadedFile file = event.getFile();
        setErrorMessages(new ArrayList<>(0));
        ArrayList<String> invalidHeaders = new ArrayList<String>(0);


        try {
            invalidHeaders = validateHeaders(file, getTypeOfImport().getModelObject());
            setShowErrorMessageDetails(invalidHeaders.size() > 0);
            if (invalidHeaders.size() == 0) {
                importDataRows(file);
            } else {

                setProgressInformation(getText("importData.invalid.headers", String.valueOf(invalidHeaders.size())));
                invalidHeaders.stream().forEach(h -> {
                    LabelValue label = new LabelValue();
                    label.setValue(h);
                    label.setLabel(getText("importData.invalid.header", h));
                    getErrorMessages().add(label);
                });

                PrimeFaces.current().ajax().addCallbackParam("valid", false);

            }

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            buildErrorLog(new Integer("1"), getText("importData.invalid.file.format"));
            setShowErrorMessageDetails(true);
            log.fatal(e);
        }


    }


    private ArrayList<String> validateHeaders(UploadedFile file, Object modelObject) throws IOException {

        setProgressInformation(getText("importData.validating.headers"));
        ArrayList<String> headers = new ArrayList<>(0);

        try (BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputstream()))) {
            headers = fileImporter.validateHeadersInObjectField(in, modelObject);
            setProgressInformation(getText("importData.header.validation.successful", file.getFileName()));
        } catch (IOException e) {
            throw e;
        }
        return headers;
    }

    public void processFileUploadValidate() {

        boolean retval = false;

        if ((dataRows.get(0) instanceof NameResolvable) || (dataRows.get(0) instanceof Category)) {
            if (objectInitialization(dataRows)) {
                retval = validateDataRows();
            }
        } else {
            retval = validateDataRows();
        }

        PrimeFaces.current().ajax().addCallbackParam("valid", retval);

    }

    /**
     *
     * @param dataRows
     * @return
     */
    private boolean objectInitialization(ArrayList<Object> dataRows) {

        if (dataRows.get(0) instanceof ITenant) {
            dataRows.stream().forEach(d -> ((ITenant) d).setTenantId(currentUserTenantContext()));
        }

        for (Object object : dataRows) {

            ClassMatcher.match().with(Group.class, this::handleGroup)
                    .with(Contribution.class, this::handleContribution)
                    .with(Category.class, this::handleCategory)
                    .fallthrough(i -> log.fatal("Unknown type [" + i + "] called to actor, cannot route"))
                    .exec(object);
        }

        setShowErrorMessageDetails(getErrorMessages().size() > 0);
        return !getShowErrorMessageDetails();

    }

    /**
     * Specal handling for Contribution
     *
     * @param contribution
     */
    private void handleContribution(Contribution contribution) {
        if (!visitor.visit(contribution, getPersonManager().getSession())) {
            writeErrorMessage(dataRows.indexOf(contribution));
        } else {
            if (contribution.getBatch() == null) {
                contribution.setBatch(batch);
                batch.getContributions().add(contribution);
            }
        }
    }

    /**
     * Special handling for Groups
     *
     * @param group
     */
    private void handleGroup(Group group) {

        if (!visitor.visit(group, getPersonManager().getSession())) {
            writeErrorMessage(dataRows.indexOf(group));
        }
    }

    /**
     * Special handling for Category
     * Cateogry have categorytypes
     *
     * @param category
     */
    private void handleCategory(Category category) {

        switch (typeOfImport) {
            case CATEGORIES:
                category.setCategoryType(Constants.CategoryType.MINISTRY);
                break;
            case FUNDS:
                category.setCategoryType(Constants.CategoryType.FUND);
                break;
        }
    }

    private void writeErrorMessage(int i) {
        Integer lineNo = i + 1;
        visitor.getErrorMessages().forEach(m -> buildErrorLog(lineNo, getText(m.getLabel(), new String[]{m.getValue(), lineNo.toString()})));
    }


    private void buildErrorLog(Integer lineNo, String errorMessage) {
        LabelValue label = new LabelValue();
        label.setValue(lineNo.toString());
        label.setLabel(errorMessage);
        getErrorMessages().add(label);
    }

    public void processFileUploadSave() {

        if (getShowErrorMessageDetails()) {
            String msg = getText("importData.validation.failed", Integer.toString(getErrorMessages().size()));
            setProgressInformation(msg);
        } else {

            setProgress(0);

            int total = dataRows.size();
            int cnt = 0;

            if (dataRows.get(0) instanceof Contribution) {
                try {
                    Batch sBatch = (Batch) ((GenericManager) personManager).save(batch);
                    dataRows.stream().forEach(c -> ((Contribution) c).setBatch(sBatch));
                    dataRows = new ArrayList<>(0);
                    dataRows.addAll(sBatch.getContributions());
                } catch (Exception e) {
                    log.fatal(e);
                    addError(e.getMessage());
                }
            }

            for (Object p : dataRows) {

                BaseObject baseObject = (BaseObject) p;
                ++cnt;
                setProgress(calculatePercentage(cnt, total));
                setProgressInformation(getText("importData.importing.data", new String[]{"" + cnt, "" + total, getText("importData.people")}));
                baseObject.setCreatedByUser(getTypeOfImport().name());

                try {
                    ((GenericManager) personManager).save(baseObject);
                } catch (Exception e) {
                    UUID guid = UUID.randomUUID();
                    addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
                    buildErrorLog(Integer.valueOf(cnt + 1), getText("importData.unable.save.data", (new String[]{Integer.valueOf(cnt + 1).toString(), e.getMessage()})));
                    setShowErrorMessageDetails(true);
                    log.error(guid.toString(), e);
                }

                pause(50);
            }

            setProgressInformation(getText("importData.import.successful", new String[]{"" + cnt, getTypeOfImportName()}));
        }
    }

    public void startFileUpload() {
        setProgressInformation(getText("importData.uploading"));
        pause(1000);
    }


    private ArrayList<Object> importDataRows(UploadedFile file) {

        dataRows = new ArrayList<>(0);
        Object modelObject = getTypeOfImport().getModelObject();

        try (BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputstream()))) {
            dataRows = (ArrayList<Object>) fileImporter.loadCSVFileIntoObject(modelObject, in);
            PrimeFaces.current().ajax().addCallbackParam("valid", true);
        } catch (IOException e) {
            PrimeFaces.current().ajax().addCallbackParam("valid", false);
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.fatal(e);
        }

        return dataRows;
    }


    private boolean validateDataRows() {

        setProgress(0);

        int total = dataRows.size();
        int cnt = 0;


        for (Object p : dataRows) {

            ++cnt;
            setProgress(calculatePercentage(cnt, total));
            setProgressInformation(getText("importData.validating.data", new String[]{"" + cnt, "" + total}));

            Set<ConstraintViolation<Object>> constraintViolations = ((GenericManager) personManager).validate(p);

            if (constraintViolations.size() > 0) {
                for (ConstraintViolation<Object> contraints : constraintViolations) {
                    Integer lineNo = cnt;
                    buildErrorLog(lineNo, contraints.getRootBeanClass().getSimpleName() +
                            " " + WordUtils.capitalize(contraints.getPropertyPath().toString()) + " " + contraints.getMessage() + " line # " + lineNo);
                }
            }

            pause(50);
        }

        setShowErrorMessageDetails(getErrorMessages().size() > 0);
        return !getShowErrorMessageDetails();
    }


    public String getTypeOfImportName() {
        return StringUtils.capitalize(getTypeOfImport().name().toLowerCase());
    }

    public double calculatePercentage(double obtained, double total) {
        return obtained * 100 / total;
    }

    private void pause(Integer millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public DataImportType getTypeOfImport() {
        return typeOfImport;
    }

    public void setTypeOfImport(DataImportType typeOfImport) {
        this.typeOfImport = typeOfImport;
    }


    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }


    public void handleCloseDialog(CloseEvent event) {

        setProgressInformation("");
        setErrorMessages(new ArrayList<>(0));
        setShowErrorMessageDetails(false);
        setProgress(0D);
        setBatch(new Batch());
    }

    public Batch getBatch() {
        return batch;
    }

    public void setBatch(Batch batch) {
        this.batch = batch;
    }

    public String getProgressInformation() {
        return progressInformation;
    }

    public void setProgressInformation(String progressInformation) {
        this.progressInformation = progressInformation;
    }

    public ArrayList<LabelValue> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(ArrayList<LabelValue> errorMessages) {
        this.errorMessages = errorMessages;
    }


    public boolean getShowErrorMessageDetails() {
        return showErrorMessageDetails;
    }

    public void setShowErrorMessageDetails(boolean showErrorMessageDetails) {
        this.showErrorMessageDetails = showErrorMessageDetails;
    }

    public StreamedContent getFileTemplate() {

        StreamedContent fileTemplate = null;

        try {

            InputStream stream = this.getClass().getClassLoader().getResourceAsStream(getTypeOfImport().getFileTemplate());

            if (stream == null) {
                throw new FileNotFoundException("Unable to find :" + getTypeOfImport().getFileTemplate());
            } else {
                fileTemplate = new DefaultStreamedContent(stream, "application/csv",
                        getTypeOfImport().getFileTemplate());
            }
        } catch (Exception e) {

            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);

        }

        return fileTemplate;
    }
}
