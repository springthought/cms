
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.GroupRole;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("groupRoleList")
@Scope("view")
public class GroupRoleList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String query;
    private GenericManager<GroupRole, Long> groupRoleManager;
    private GroupRole selectedGroupRole = new GroupRole();
    private boolean checked;

    @Autowired
    public void setGroupRoleManager(@Qualifier("groupRoleManager") GenericManager<GroupRole, Long> groupRoleManager) {
	this.groupRoleManager = groupRoleManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public GroupRoleList() {
	setSortColumn("grouproleId"); // sets the default sort column
    }

    public List<GroupRole> getGroupRoles() {
	try {
	    return groupRoleManager.search(query, GroupRole.class);
	} catch (SearchException se) {
	    addError(se.getMessage());
	    return sort(groupRoleManager.getAll());
	}
    }

    public String search() {
	return "success";
    }

    public GroupRole getSelectedGroupRole() {
	return selectedGroupRole;
    }

    public void setSelectedGroupRole(GroupRole groupRole) {
	this.selectedGroupRole = groupRole;
    }

    public void delete(ActionEvent event) {

	GroupRole groupRole = groupRoleManager.get(selectedGroupRole.getGrouproleId());

	try {
	    groupRoleManager.remove(groupRole.getGrouproleId());
	    addMessage("groupRole.deleted");

	} catch (DataIntegrityViolationException e) {
	    DataIntegrityViolationExceptionHandler(e);
	} catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }
}
