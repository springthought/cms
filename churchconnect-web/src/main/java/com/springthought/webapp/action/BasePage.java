package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.Status;
import com.springthought.model.*;
import com.springthought.service.*;
import com.springthought.util.DateConverter;
import com.springthought.util.FacesUtils;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.NullComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.omnifaces.util.Faces;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;


public class BasePage {

    protected final Log log = LogFactory.getLog(getClass());
    protected UserManager userManager;
    protected MailEngine mailEngine;
    protected SimpleMailMessage message;
    protected String templateName = "mailTemplate.vm";
    protected FacesContext facesContext;
    protected String sortColumn;
    protected boolean ascending;
    protected boolean nullsAreHigh;
    protected String helpKey = new String();
    protected DateConverter dateConverter = new DateConverter();
    protected String timezone;
    protected Locale browserLocale;
    protected DataService dataService;
    protected Properties prop;
    protected LookupManager lookupManager;
    protected Map<String, String> constraintMsgKeyMap = new HashMap<String, String>();
    protected Boolean showMore = new Boolean(false);


    @Autowired
    SecurityContextManager securityContext;

    public BasePage() {

        String timeZone = getCookie(Constants.COOKIE_TIME_ZONE);
        Resource resource;

        setTimezone((StringUtils.isNotBlank(timeZone)) ? timeZone : TimeZone.getDefault().getID());
        resource = new ClassPathResource("/springthoughtapp.properties");

        try {
            prop = PropertiesLoaderUtils.loadProperties(resource);
        } catch (Exception e) {
            log.error("Unable to load application property file", e);
        }

        resource = new ClassPathResource("/constraint-mapping.properties");

        try {
            Properties p = PropertiesLoaderUtils.loadProperties(resource);
            constraintMsgKeyMap.putAll(p.entrySet().stream()
                    .collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString())));
        } catch (IOException e) {
            log.error("Unable to load application property file", e);

        }

    }

    protected String getPrincipalUserName() {

        if (getAuthentication().isAuthenticated()) {
            return getAuthentication().getName();
        } else {
            return getAuthentication().getPrincipal().toString();
        }

    }

    protected void addError(String key) {
        addError(key, null);
    }

    protected void addError(String key, Object arg) {
        addError(null, key, arg);
    }

    /*
     * ?faces-redirect=true
     */

    protected void navigateToPage(String sURL) {
        getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), "null", sURL);
    }

    @SuppressWarnings("unchecked")
    protected void addError(String clientId, String key, Object arg) {

        List<String> errors = (List) getSession().getAttribute("errors");

        if (errors == null) {
            errors = new ArrayList<String>();
        }

        // if key contains a space, don't look it up, it's likely a raw message
        if (key.contains(" ") && arg == null) {
            errors.add(key);
        } else {
            errors.add(getText(key, arg));
        }

        getSession().setAttribute("errors", errors);

        FacesUtils.addErrorMessage(clientId, getText(key, arg));

    }

    protected void addMessage(String key) {
        addMessage(key, null);
    }

    @SuppressWarnings("unchecked")
    protected void addMessage(String key, Object arg) {

        addMessage(null, key, arg);

    }

    public Boolean getShowMore() {
        return showMore;
    }

    public void setShowMore(Boolean showMore) {
        this.showMore = showMore;
    }

    public void toogleShowMore(ActionEvent event) {

        showMore = !showMore;
    }

    protected void addMessage(String clientId, String key, Object arg) {

        List<String> messages = (List) getSession().getAttribute("messages");

        if (messages == null) {
            messages = new ArrayList<String>();
        }

        messages.add(getText(key, arg));
        getSession().setAttribute("messages", messages);

        // Add message to JSF context
        FacesUtils.addInfoMessage(clientId, getText(key, arg));
    }

    public User currentUser() {

        return securityContext.retrivePrincipal();

    }

    public User getCurrentUser() {
        return currentUser();
    }

    /**
     * Project: framework Method : currentUserTenantContext Return : Long Params
     * : @return Author : eairrick returns the current users tenant context
     */
    public Long currentUserTenantContext() {
        return securityContext.retriveTenantContext();
    }

    public Object getBean(String beanName) {
        Object bean = null;
        FacesContext fc = getFacesContext();

        if (fc != null) {
            ELContext elContext = fc.getELContext();
            bean = elContext.getELResolver().getValue(elContext, null, beanName);
        }

        return bean;
    }

    public Locale getBrowserLocale() {
        browserLocale = getFacesContext().getExternalContext().getRequestLocale();
        return browserLocale;
    }

    public ResourceBundle getBundle() {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        log.info("Bundlee=" + ResourceBundle.getBundle(getBundleName(), getRequest().getLocale(), classLoader));

        return ResourceBundle.getBundle(getBundleName(), getRequest().getLocale(), classLoader);
    }

    public String getBundleName() {

        log.info("BundleName=" + getFacesContext().getApplication().getMessageBundle());

        return getFacesContext().getApplication().getMessageBundle();

    }


    /**
     * Convenience method to get the Configuration HashMap from the servlet context.
     *
     * @return the user's populated form from the session
     */
    protected Map getConfiguration() {
        Map config = (HashMap) getServletContext().getAttribute(Constants.CONFIG);

        // so unit tests don't puke when nothing's been set
        if (config == null) {
            return new HashMap();
        }

        return config;
    }

    /**
     * Project: framework Method : getCookie
     *
     * @param name
     * @return :
     * @author eairrick
     */
    public String getCookie(String name) {

        HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();

        if (request != null) {

            Cookie cookie = null;
            Cookie[] userCookies = request.getCookies();

            if (userCookies != null && userCookies.length > 0) {
                for (int i = 0; i < userCookies.length; i++) {
                    if (userCookies[i].getName().equals(name)) {
                        cookie = userCookies[i];
                        return cookie.getValue();
                    }
                }
            }
        }
        return null;
    }

    /**
     * returns hostname, port, context path of page.
     */
    protected String pageURL() {
        StringBuffer URL = getRequest().getRequestURL();
        String servletPath = getRequest().getServletPath();
        String pageURL = URL.substring(0, URL.indexOf(servletPath));
        return pageURL;
    }


    public Map getCountries() {
        CountryModel model = new CountryModel();
        return model.getCountries(getRequest().getLocale());
    }

    public DataService getDataService() {
        return dataService;
    }

    public FacesContext getFacesContext() {
        return Faces.getContext();
        //return FacesContext.getCurrentInstance();
    }

    /**
     * @return the helpKey
     */
    public String getHelpMessage() {
        return getText(helpKey);
    }

    public LookupManager getLookupManager() {
        return lookupManager;
    }

    // Convenience methods ====================================================
    public String getParameter(String name) {
        return getRequest().getParameter(name);
    }

    /**
     * Servlet API Convenience method
     *
     * @return HttpServletRequest from the FacesContext
     */
    protected HttpServletRequest getRequest() {
        return (HttpServletRequest)Faces.getContext().getExternalContext().getRequest();
        //return (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
    }

    /**
     * Servlet API Convenience method
     *
     * @return HttpServletResponse from the FacesContext
     */
    protected HttpServletResponse getResponse() {
        return (HttpServletResponse) getFacesContext().getExternalContext().getResponse();
    }

    /**
     * Servlet API Convenience method
     *
     * @return the ServletContext form the FacesContext
     */
    protected ServletContext getServletContext() {
        return (ServletContext) getFacesContext().getExternalContext().getContext();
    }

    /**
     * Servlet API Convenience method
     *
     * @return the current user's session
     */
    protected HttpSession getSession() {
        return getRequest().getSession();
    }

    // The following methods are used by t:dataTable for sorting.
    public String getSortColumn() {
        return sortColumn;
    }

    public String getText(String key) {
        String message;

        try {
            message = getBundle().getString(key);
        } catch (java.util.MissingResourceException mre) {
            log.warn("Missing key for '" + key + "'");
            return "???" + key + "???";
        }

        return message;
    }

    public String getText(String key, Object arg) {
        if (arg == null) {
            return getText(key);
        }

        MessageFormat form = new MessageFormat(getBundle().getString(key));

        if (arg instanceof String) {
            return form.format(new Object[]{arg});
        } else if (arg instanceof Object[]) {
            return form.format(arg);
        } else {
            log.error("arg '" + arg + "' not String or Object[]");

            return "";
        }
    }

    public SecurityContextManager getSecurityContext() {
        return securityContext;
    }

    public void setSecurityContext(SecurityContextManager securityContext) {
        this.securityContext = securityContext;
    }

    public String getTimezone() {
        return timezone;
    }

    /**
     * Convenience method for unit tests.
     *
     * @return boolean indicator of an "errors" attribute in the session
     */
    public boolean hasErrors() {
        return (getSession().getAttribute("errors") != null);
    }

    public boolean isAscending() {
        return ascending;
    }


    protected void ConstraintViolationExceptionHandler(javax.validation.ConstraintViolationException e) {

        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();

        if (constraintViolations.size() > 0) {
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                addError((constraintViolation.getRootBeanClass().getSimpleName() +
                        " " + WordUtils.capitalize(constraintViolation.getPropertyPath().toString()) + " " + constraintViolation.getMessage()));
            }
        }
    }

    protected void DataIntegrityViolationExceptionHandler(DataIntegrityViolationException ex) {

        String constraintName = null;
        String message = null;
        UUID guid = UUID.randomUUID();

        if ((ex.getCause() != null) && (ex.getCause() instanceof ConstraintViolationException)) {

            String constraint =  ((ConstraintViolationException) ex.getCause()).getConstraintName();

            constraintName = Optional.ofNullable(constraint).orElse(ex.getCause().getMessage());

            String key = constraintMsgKeyMap.get( Optional.ofNullable(constraintName).orElse(ex.getCause().getMessage()));

            message = (key != null) ? getText(key) : getText("errors.constraint.mapping", constraintName);

            if (constraintName.equals("UQ_DUPLICATE_ENTRY")) {
                addError("msgDuplicate", "errors.general", message);
                addError("errors.general", message);
            } else {
                addError("errors.general", message);
            }
        } else {
            // TODO move 'for key' and 'for field' to bundle
            message = ex.getMostSpecificCause().getMessage().replace("for key", "for field");
            addError("msgDuplicate", "errors.general", message);
            addError("errors.general", message);
        }

        log.fatal(guid.toString(), ex);
        PrimeFaces.current().ajax().addCallbackParam("validationFailed", true);

    }

    protected StatusResult sendPersonMessage(MessageTemplateSupplier supplier, String template) {

        Person person = supplier.getPerson();

        StatusResult result = new StatusResult();
        log.debug("sending e-mail to user [" + person.getEmail() + "]...");

        try {
            if (person.getEmail() == null || person.getEmail().isEmpty()) {
                throw new MessagingException(person.getName() + getText("errors.blankemail"));
            }
            mailEngine.sendHTMLMessage(new String[]{person.getEmail()}, supplier, template);
        } catch (MessagingException e) {
            result.setStatus(Status.ERR);
            result.setResultMessage(e.getMessage());
            return result;
        }

        result.setStatus(Status.SUC);
        result.setResultMessage(Constants.COMMUNICATION_SUCCESS);

        return result;
    }

    /**
     * Convenience message to send messages to users, includes app URL as footer.
     *
     * @param user the user to send the message to
     * @param msg  the message to send
     * @param url  the application's URL
     */

    protected void sendUserMessage(User user, String msg, String url) {
        sendUserMessage(user, msg, url, getText("webapp.name"));
    }

    protected void sendUserMessage(User user, String msg, String url, String subject) {

        if (log.isDebugEnabled()) {
            log.debug("sending e-mail to user [" + user.getEmail() + "]...");
        }

        try {

            Message message = new Message();
            message.setMessage(msg);
            message.setEmail(true);
            message.setSubject(subject);
            message.setTitle( getText("members.create.your.account.title"));
            message.setSecondMessage(getText("members.trouble.with.link"));


            MessageTemplateSupplier suppler = new MessageTemplateSupplier();

            suppler.setApplicationURL(url);
            suppler.setMessage(message);
            suppler.setSender(new InternetAddress(getCurrentUser().getEmail()));

            mailEngine.sendHTMLMessage(new String[]{user.getEmail()}, suppler, templateName);

        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new String[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

    }

    public void setAscending(boolean ascending) {
        this.ascending = ascending;
    }

    @Autowired
    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    /**
     * @param helpKey the helpKey to set
     */
    public void setHelpKey(String helpKey) {
        this.helpKey = helpKey;
    }

    @Autowired
    public void setLookupManager(@Qualifier("lookupManager") LookupManager lookupManager) {
        this.lookupManager = lookupManager;
    }

    @Autowired
    public void setMailEngine(MailEngine mailEngine) {
        this.mailEngine = mailEngine;
    }

    @Autowired
    public void setMessage(SimpleMailMessage message) {
        this.message = message;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Autowired
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * Sort list according to which column has been clicked on.
     *
     * @param list the java.util.List to sort
     * @return ordered list
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    protected List sort(List list) {
        Comparator comparator = new BeanComparator(sortColumn, new NullComparator(nullsAreHigh));
        if (!ascending) {
            comparator = new ReverseComparator(comparator);
        }
        Collections.sort(list, comparator);
        return list;
    }

    @SuppressWarnings("unused")
    protected boolean hasAnyRole(String splitString) {

        boolean founded = false;

        String delims = ",";
        StringTokenizer token = new StringTokenizer(splitString, delims);
        while (token.hasMoreElements()) {

            Permission permission = new Permission((String) token.nextElement());

            if (currentUser() != null) {
                founded = currentUser().getAuthorities().contains(permission);
            }

            if (founded) {
                break;
            }

        }


        return founded;

    }

    private Authentication getAuthentication() {
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        return authentication;
    }

}
