/**
 * 
 */
package com.springthought.webapp.action;

import com.springthought.State;

/**
 * @author eairrick
 * 
 */
public class EventContext implements State {

	private State eventState;

	public void setState(State state) {
		this.eventState = state;
	}

	public State getState() {
		return this.eventState;
	}

	@Override
	public void doAction() {
		this.eventState.doAction();
	}

	/* (non-Javadoc)
	 * @see com.springthought.State#loadAction()
	 */
	@Override
	public void loadAction() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.springthought.State#saveAction()
	 */
	@Override
	public void saveAction() {
		// TODO Auto-generated method stub
		
	}

}
