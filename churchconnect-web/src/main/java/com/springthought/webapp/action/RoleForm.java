package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.Permission;
import com.springthought.model.PermissionCategory;
import com.springthought.model.Role;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

@Component("roleForm")
@Scope("view")
public class RoleForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -4390877387662894736L;
    private GenericManager<Role, Long> roleManager;
    private GenericManager<PermissionCategory, Long> permissionCategoryManager;

    private Role role = new Role();
    private Long id;

    private TreeNode rootTreeModel;
    private TreeNode[] selectedNodes;

    @Autowired
    public void setRoleManager(@Qualifier("roleManager") GenericManager<Role, Long> roleManager) {
        this.roleManager = roleManager;
    }

    @Autowired
    public void setPermissionCategoryManager(
            @Qualifier("permissionCategoryManager") GenericManager<PermissionCategory, Long> permissionCategoryManager) {
        this.permissionCategoryManager = permissionCategoryManager;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String delete() {
        roleManager.remove(role.getId());
        addMessage("role.deleted");

        return "roles";
    }

    public String edit() {

        return "roleForm";
    }

    public String save() {

        if (getSelectedNodeCount() == 0) {
            addError("errors.profile.select.permission", getRole().getLabel());
            return null;
        }

        ArrayList<Permission> savedPermissions = cachedPermission();
        processPermissionDeletes(savedPermissions);
        processAdditions(savedPermissions);

        role.setName(Constants.ROLE_PREFIX + "_" + role.getLabel().toUpperCase().trim().replace(" ", ""));

        boolean isNew = (role.getId() == null || role.getId() == 0);

        try {

            this.role = roleManager.save(role);
            String key = (isNew) ? "role.added" : "role.updated";
            addMessage(key);

            if (isNew) {
                return "roles";
            } else {
                return "roles";
            }
        } catch (DataIntegrityViolationException e) {
            DataIntegrityViolationExceptionHandler(e);
        } catch (ConstraintViolationException e) {
            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }

        return null;

    }

    private void processAdditions(ArrayList<Permission> p) {

        for (Permission permission : p) {
            if (!getRole().getPermissions().contains(permission)) {
                getRole().getPermissions().add(permission);
                log.debug("Add permission: " + permission.getName() + " to Role:" + getRole().getName());
            }
        }

    }

    private void processPermissionDeletes(ArrayList<Permission> p) {

        ArrayList<Permission> itemsToRemove = new ArrayList<Permission>(0);

        for (Permission permission : getRole().getPermissions()) {
            if (!p.contains(permission)) {
                itemsToRemove.add(permission);
                log.debug("Removing permission: " + permission.getName() + " from Role: " + getRole().getName());
            }
        }

        if (!itemsToRemove.isEmpty()) {
            getRole().getPermissions().removeAll(itemsToRemove);
        }

    }

    private ArrayList<Permission> cachedPermission() {

        ArrayList<Permission> p = new ArrayList<Permission>(0);

        for (TreeNode level1Node : getRootTreeModel().getChildren()) {
            for (TreeNode level2Node : level1Node.getChildren()) {
                if (level2Node.isSelected()) {
                    Permission savedPermission = (Permission) level2Node.getData();
                    p.add(savedPermission);
                }
            }
        }
        return p;
    }

    public void save(ActionEvent event) {
        this.save();
    }

    @PostConstruct
    private void initilization() {

        id = getParameter("id") == null ? 0L : new Long(getParameter("id"));
        if (id != null && id != 0) {
            role = roleManager.get(id);
        } else {
            role = new Role();
        }

        buildPermissionModel();

    }

    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }

    public void setSelectedNodes(TreeNode[] selectedNodes) {
        this.selectedNodes = selectedNodes;
    }

    public TreeNode getRootTreeModel() {
        return rootTreeModel;
    }

    public void setRootTreeModel(TreeNode rootTreeModel) {
        this.rootTreeModel = rootTreeModel;
    }

    public void nodeSelect(NodeSelectEvent event) {

        Object data = event.getTreeNode().getData();

        if (data instanceof Permission) {
            if (((Permission) event.getTreeNode().getData()).getName().equals(Constants.ADMIN_ROLE)) {
                selecteAllNodes();
            }
        } else if (data instanceof PermissionCategory) {
            if (((PermissionCategory) event.getTreeNode().getData()).getName().equals(Constants.ADMIN_CATEGORY)) {
                selecteAllNodes();
            }
        }

    }

    private void selecteAllNodes() {
        for (TreeNode treeNode : getRootTreeModel().getChildren()) {
            treeNode.setSelected(true);
        }

    }

    public long getSelectedNodeCount() {

        AtomicLong count = new AtomicLong(0);

        getRootTreeModel().getChildren().forEach(node -> {
            count.getAndAdd(node.getChildren().stream().filter(c -> c.isSelected()).count());
        });

        return count.get();
    }

    public void nodeUnselect(NodeUnselectEvent event) {
        // event.getTreeNode().setSelected(false);
    }

    private void buildPermissionModel() {

        List<PermissionCategory> categories = permissionCategoryManager.getAllDistinct();

        CheckboxTreeNode pseudoNode = new CheckboxTreeNode(
                new PermissionCategory(getText("roleDetail.permission"), getText("roleDetail.permission"), true), null);

        pseudoNode.setType(Constants.TREE_NODE_TYPE_PSEUDO);

        setRootTreeModel(pseudoNode);

        for (PermissionCategory permissionCategory : categories) {
            TreeNode level2node = new CheckboxTreeNode(permissionCategory, getRootTreeModel());
            for (Permission permission : permissionCategory.getPermissions()) {
                if (!permission.getName().equals(Constants.USER_ROLE)) {
                    TreeNode level3node = new CheckboxTreeNode(permission, level2node);
                    level3node.setSelected(isSelected(permission));
                }
            }
        }
    }

    private boolean isSelected(Permission permission) {
        return role.getPermissions().contains(permission);
    }
}