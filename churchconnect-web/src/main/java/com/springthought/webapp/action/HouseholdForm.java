package com.springthought.webapp.action;

import com.springthought.model.Household;
import com.springthought.model.Person;
import com.springthought.service.HouseholdManager;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultUploadedFile;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component("householdForm")
@Scope("session")
public class HouseholdForm extends BasePage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8815839563670515150L;
	private static final String IMAGE_FILE_NAME = "Personal-information64.png";

	// private GenericManager<Household, Long> householdManager;
	private HouseholdManager householdManager;
	private Household household = new Household();
	private Long householdId;
	private DefaultUploadedFile file;
	private DefaultStreamedContent householdImage = null;
	private DefaultStreamedContent dbImage = null;
	private String imageFileName = "";
	private String currentPanel = "LANDING";
	List<Person> members = new ArrayList<Person>();
	List<Person> deletedMembers = new ArrayList<Person>();

	private Person selectedMember;

	/**
	 *
	 @Autowired public void setHouseholdManager(
	 * @Qualifier("householdManager") GenericManager<Household, Long>
	 *                                householdManager) { this.householdManager
	 *                                = (HouseholdManager) householdManager; }
	 */

	@Autowired
	public void setHouseholdManager(
			@Qualifier("householdManager") HouseholdManager householdManager) {
		this.householdManager = householdManager;
	}

	public Household getHousehold() {
		return household;
	}

	public void setHousehold(Household household) {
		this.household = household;
	}

	public void setHouseholdId(Long householdId) {
		this.householdId = householdId;
	}

	public StreamedContent getHouseholdPicture() {
		return generateImage();
	}

	public String delete() {
		householdManager.remove(household.getHouseholdId());
		addMessage("household.deleted");

		return "households";
	}

	public String edit() {
		// Workaround for not being able to set the id using #{param.id} when
		// using Spring-configured managed-beans
		// if (householdId == null) {
		householdId = getParameter("householdId") == null ? 0L : new Long(
				getParameter("householdId"));

		dbImage = null;
		selectedMember = null;

		// }
		// Comparison to zero (vs. null) is required with MyFaces 1.2.2, not
		// with previous versions
		if (householdId != null && householdId != 0) {

			household = householdManager.get(householdId);

			// Load work copy of the the household member list.
			members.addAll(household.getPersons());

		} else {

			household = new Household();
			// initialize children objects.
		}

		return "householdForm";
	}

	public List<Person> getMembers() {
		// TODO: refactor not to recreate objects on each get.
		return members;
	}

	/**
	 * @param members
	 *            the members to set
	 */
	public void setMembers(List<Person> members) {
		log.info(members);
		this.members = members;
	}

	public String save() {

		boolean isNew = (household.getHouseholdId() == null || household
				.getHouseholdId() == 0);

		HashSet<Person> hs = new HashSet<Person>(members);

		Boolean found = household.getPersons().retainAll(hs);

		household = householdManager.saveHousehold( household );

		String key = (isNew) ? "household.added" : "household.updated";

		addMessage(key);

		if (isNew) {
			return "households";
		} else {
			return "households";
		}

	}

	public void save(ActionEvent event) {
		this.save();
	}

	public void handleFileUpload(FileUploadEvent event) {

		file = (DefaultUploadedFile) event.getFile();


		addMessage("uploadForm.sucessfull.upload", file.getFileName());

	}

	private DefaultStreamedContent generateImage() {

	    /**
	     *


		try {
			if (householdBin != null && householdBin.getContents() != null) {
				dbImage = new DefaultStreamedContent(
						householdBin.getInputstream(),
						householdBin.getContentType(),
						householdBin.getFileName());
			} else {
				dbImage = generateDefaultImage();

			}
		} catch (IOException e) {
			log.fatal(e);
			throw new FacesException("Error in writing image.", e);
		}

		log.info("HouseHoldBin Primary Id:" + householdBin.getId());
   */
		return null;
	}

	private DefaultStreamedContent generateDefaultImage() {


		// if (householdImage == null) {

		ServletContext servletContext = (ServletContext) FacesContext
				.getCurrentInstance().getExternalContext().getContext();

		String newFileName = servletContext.getRealPath("") + File.separator
				+ "resources" + File.separator + "images" + File.separator
				+ IMAGE_FILE_NAME;

		try {

			householdImage = new DefaultStreamedContent(new FileInputStream(
					new File(newFileName)), "image/png", IMAGE_FILE_NAME);

		} catch (Exception e) {
			log.fatal(e);
			throw new FacesException("Error in writing file image.", e);
		}
		// }

		return householdImage;
	}

	/**
	 * @return the imageFileName
	 */
	public String getImageFileName() {
		return imageFileName;
	}

	/**
	 * @param imageFileName
	 *            the imageFileName to set
	 */
	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	/**
	 * @return the currentPanel
	 */
	public String getCurrentPanel() {
		return currentPanel;
	}

	/**
	 * @param currentPanel
	 *            the currentPanel to set
	 */
	public void setCurrentPanel(String currentPanel) {
		this.currentPanel = currentPanel;
	}

	/**
	 *
	 */

	public void removeMember(ActionEvent event) {

		addMessage(
				"household.removeMember",
				new String[] {
						selectedMember.getFirstName() + " "
								+ selectedMember.getLastName(),
						household.getName() });

		// remove from collection working copy
		Boolean found = members.remove(selectedMember);

		if ( found ){ this.deletedMembers.add(selectedMember);}

		selectedMember = null;
	}

	/**
	 * @return the selectedMember
	 */
	public Person getSelectedMember() {
		return selectedMember;
	}

	/**
	 * @param selectedMember
	 *            the selectedMember to set
	 */
	public void setSelectedMember(Person selectedMember) {
		this.selectedMember = selectedMember;
	}

}
