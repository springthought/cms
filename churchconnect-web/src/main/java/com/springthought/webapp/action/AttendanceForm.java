package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.*;
import com.springthought.service.GenericManager;
import com.springthought.util.DateUtil;
import com.springthought.util.Queries;
import org.apache.commons.collections.CollectionUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.*;

@Component("attendanceForm")
@Scope("view")
public class AttendanceForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String from;
    private GenericManager<Attendance, Long> attendanceManager;
    private GenericManager<Person, Long> personManager;
    private GenericManager<Event, Long> eventManager;
    private GenericManager<EventType, Long> eventTypeManager;
    private Attendance attendance = new Attendance();
    private Long attendanceId;
    private ArrayList<Event> currentAttendanceEvents = new ArrayList<Event>(0);
    private ArrayList<Date> currentEventDates = new ArrayList<Date>(0);
    private EventType eventTypeFilter = new EventType();
    private Event selectedEvent = new Event();
    private Event selectedEventOccurrence = new Event();
    private Event priorSelectedEventOccurrence = new Event();

    private ArrayList<Person> persons = new ArrayList<Person>(0);
    private ArrayList<Person> filteredPersons = new ArrayList<Person>(0);
    private ArrayList<Person> selectedPersons = new ArrayList<Person>(0);
    private ArrayList<Person> sourcePersons = new ArrayList<Person>(0);

    private ArrayList<SelectItem> memberTypeFilterOptions = new ArrayList<SelectItem>(0);

    private String test = new String("");

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    private boolean newAttendance;

    public Event getPriorSelectedEventOccurrence() {
        return priorSelectedEventOccurrence;
    }

    public void setPriorSelectedEventOccurrence(Event priorSelectedEventOccurrence) {
        this.priorSelectedEventOccurrence = priorSelectedEventOccurrence;
    }

    @Autowired
    public void setAttendanceManager(
            @Qualifier("attendanceManager") GenericManager<Attendance, Long> attendanceManager) {
        this.attendanceManager = attendanceManager;
    }

    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    @Autowired
    public void setEventManager(@Qualifier("eventManager") GenericManager<Event, Long> eventManager) {
        this.eventManager = eventManager;
    }

    @Autowired
    public void setEventTypeManager(@Qualifier("eventTypeManager") GenericManager<EventType, Long> eventTypeManager) {
        this.eventTypeManager = eventTypeManager;
    }

    public Attendance getAttendance() {
        return attendance;
    }

    public void setAttendance(Attendance attendance) {
        this.attendance = attendance;
    }

    public void setAttendanceId(Long attendanceId) {
        this.attendanceId = attendanceId;
    }

    public boolean isNewAttendance() {
        return newAttendance;
    }

    public void setNewAttendance(boolean newAttendance) {
        this.newAttendance = newAttendance;
    }

    public Event getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(Event selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public String delete() {
        attendanceManager.remove(attendance.getAttendanceId());
        addMessage("attendance.deleted");

        return "attendances";
    }

    public void dummy(ActionEvent event) {
        System.out.print("dummy");
    }

    public String edit() {

        return "attendanceForm";
    }

    /**
     * Project: framework Method : locateEventOccurrence Return : Event Params
     * : @param eventId Params : @return Author : eairrick
     */
    private Event locateEventOccurrence(Long eventId) {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.EVENT_MIN_DATE_FIND_BY_PARENT_ID_PARM, eventId);

        ArrayList<Event> firstEvents = (ArrayList<Event>) eventManager
                .findByNamedQuery(Queries.EVENT_MIN_DATE_FIND_BY_PARENT_ID, queryParams);

        return (firstEvents.size() > 0) ? firstEvents.get(0) : new Event(selectedEvent.getAttendanceTrackType());

    }

    private void resetForm() {

        /*
         * I don't think I need this any longer.
         *
         * attendance = new Attendance(); eventTypeFilter = new EventType();
         *
         * currentAttendanceEvents = new ArrayList<Event>(0); currentEventDates = new
         * ArrayList<Date>(0);
         *
         * selectedEvent = new Event(); selectedEventOccurrence = new Event();
         * priorSelectedEventOccurrence = new Event(); selectedPersons = new
         * ArrayList<Person>(0); filteredPersons = new ArrayList<Person>(0);
         */

    }

    public String save() {

        boolean isNew = (attendance.getAttendanceId() == null || attendance.getAttendanceId() == 0);

        try {

            if (selectedPersons != null && selectedPersons.size() != 0) {
                // TODO: Add efficiencies should only do this if the initial
                // selectedPersons has changed.
                // need to find a Util that calcs a checksum from a collection
                //ArrayList<Person> people = (ArrayList<Person>) attendance.getPersons();
                List<Person> people = attendance.getPersons();

                attendance.getPersons().removeAll(people);
                attendance.getPersons().addAll(selectedPersons);

            } else {
                selectedPersons = new ArrayList<Person>(0);
            }


            attendance.setEvent(selectedEventOccurrence);
            attendance = attendanceManager.save(attendance);
            sourcePersons.addAll(selectedPersons);

            String key = (isNew) ? "attendance.added" : "attendance.updated";
            addMessage(key);

        } catch (Exception e) {

            UUID guid = UUID.randomUUID();

            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});

            log.error(guid.toString(), e);

            return "";
        }

        if (isNew) {
            return "attendances";
        } else {
            return "attendances";
        }
    }

    public void save(ActionEvent event) {
        this.save();
    }

    public List<Event> getAttendanceEvents() {
        return currentAttendanceEvents;
    }

    public ArrayList<Date> getEventDates() {
        return currentEventDates;
    }

    public void setEventDates(ArrayList<Date> currentEventDates) {
        this.currentEventDates = currentEventDates;
    }

    public EventType geteventTypeFilter() {
        return eventTypeFilter;
    }

    public void setEventTypeFilter(EventType eventTypeFilter) {
        this.eventTypeFilter = eventTypeFilter;
    }

    public Event getSelectedEventOccurrence() {
        return selectedEventOccurrence;
    }

    public void setSelectedEventOccurrence(Event selectedEventOccurrence) {
        this.selectedEventOccurrence = selectedEventOccurrence;
    }

    public ArrayList<Person> getPersons() {
        return persons;
    }

    public void setPersons(ArrayList<Person> persons) {
        this.persons = persons;
    }

    public ArrayList<Person> getSelectedPersons() {
        return selectedPersons;
    }

    public void setSelectedPersons(ArrayList<Person> selectedPersons) {
        this.selectedPersons = selectedPersons;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public ArrayList<Person> getFilteredPersons() {
        return filteredPersons;
    }

    public void setFilteredPersons(ArrayList<Person> filteredPersons) {
        this.filteredPersons = filteredPersons;
    }

    public ArrayList<SelectItem> getMemberTypeFilterOptions() {
        return memberTypeFilterOptions;
    }

    public void setMemberTypeFilterOptions(ArrayList<SelectItem> memberTypeFilterOptions) {
        this.memberTypeFilterOptions = memberTypeFilterOptions;
    }

    public void eventTypeChange(AjaxBehaviorEvent event) {

        currentAttendanceEvents = (ArrayList<Event>) dataService.getAttendanceEvents(eventTypeFilter);

        if (currentAttendanceEvents != null && currentAttendanceEvents.size() > 0) {
            selectedEvent = currentAttendanceEvents.get(0);
            selectedEventOccurrence = selectedEvent.getEventOccurrences().iterator().next();
            eventChange(null);

        } else {
            // reset Event and EventOccurence
            EventType save = eventTypeFilter;
            resetForm();
            eventTypeFilter = save;
            sourcePersons = selectedPersons;

        }
    }

    public void eventChange(AjaxBehaviorEvent event) {

        selectedEventOccurrence = selectedEvent.getEventOccurrences().iterator().next();
        eventOccurrenceChange(null);
        // TODO - Save previous and clear SelectedPersons
    }

    public void eventOccurrenceChange(AjaxBehaviorEvent event) {
        // Person grid can be empty intil; control sets selected to null
        // need to reinit to empty arraylistl
        if (selectedPersons == null) {
            selectedPersons = new ArrayList<Person>(0);
        }
        boolean isDirty = !CollectionUtils.isEqualCollection(sourcePersons, selectedPersons);

        if (isDirty) {
            PrimeFaces.current().executeScript("PF('saveAttendanceDlgWv').show()");
        } else {
            switchEventOccurrence("0");
        }
    }

    public void switchEventOccurrence(String save) {

        // TODO - Save...ClearSelected and Fetch selected person or created new

        if (save.equals("1")) {
            save();
        }

        selectedPersons = new ArrayList<Person>(0);

        priorSelectedEventOccurrence = selectedEventOccurrence;

        selectedEventOccurrence = eventManager.get(selectedEventOccurrence.getEventId());

        attendance = loadOrCreateAttendance();

        attendance.setEvent(selectedEventOccurrence);

        for (Person person : attendance.getPersons()) {
            selectedPersons.add(person);
        }

        sourcePersons = new ArrayList<Person>(selectedPersons);

        updateCount();

    }

    public void updateCount(SelectEvent event) {
        updateCount();
    }

    public void updateCount(UnselectEvent event) {
        updateCount();
    }

    public void toggleSelect(ToggleSelectEvent event) {

        if (event.isSelected()) {

        }

        updateCount();
    }

    private void updateCount() {

        if (attendance.getEvent() != null) {
            if (attendance.getEvent().getAttendanceTrackType().equals(Constants.AttendanceTrackingType.DETAILED)) {
                attendance.setGeneralCount(selectedPersons.size());
            }
        }

    }

    private Attendance loadOrCreateAttendance() {

        Attendance val = null;

        if (selectedEventOccurrence.getAttendance() != null
                && selectedEventOccurrence.getAttendance().getAttendanceId() != null) {
            // reload again a fetch Lazy loaded persons.
            val = attendanceManager.get(selectedEventOccurrence.getAttendance().getAttendanceId());

        } else {
            val = new Attendance(selectedEventOccurrence.getName() + " - " + DateUtil.getDateTime(
                    DateUtil.getDatePattern() + DateUtil.getTimePattern(), selectedEventOccurrence.getStartDate()));
            selectedEventOccurrence.setAttendance(val);
        }

        return val;

    }

    /**
     * Project: framework Method : getRecordable Return : boolean Params : @return
     * Author : eairrick
     */
    public boolean getRecordable() {

        boolean retval;

        if (selectedEvent == null) {
            retval = false;
        } else {

            switch (selectedEvent.getAttendanceTrackType()) {
                case DETAILED:
                    retval = selectedPersons.size() > 0;
                    break;
                case COUNT:
                    retval = attendance.getGeneralCount() > 0;
                    break;
                default:
                    retval = true;
                    break;
            }
        }
        return retval;
    }

    /**
     * Project: framework Method : isEqualCollection Return : boolean Params
     * : @param source Params : @param target Params : @return Author : eairrick
     * <p>
     * <p>
     * private boolean isEqualCollection(ArrayList<Person> source, ArrayList<Person>
     * target) {
     * <p>
     * boolean retval = true; if (source.size() == target.size()) { for (Person
     * person : target) { if (!source.contains(person)){ retval = false; break; } }
     * <p>
     * } else { retval = false; }
     * <p>
     * return retval;
     * <p>
     * }
     */


    @PostConstruct
    public void initialization() {

        this.persons = (ArrayList<Person>) personManager.getAllDistinct();

        ArrayList<LabelValue> lv = (ArrayList<LabelValue>) dataService.getMemberTypes();

        memberTypeFilterOptions.add(new SelectItem("", getText("button.selectOne")));

        for (LabelValue labelValue : lv) {
            memberTypeFilterOptions.add(new SelectItem(labelValue.getValue(), labelValue.getLabel()));
        }

        resetForm();

        // Load attendance

        from = getParameter("from") == null ? "" : new String(getParameter("from"));
        Long eventId = getParameter("eventId") == null ? 0L : new Long(getParameter("eventId"));

        if (attendanceId == null) {
            attendanceId = getParameter("attendanceId") == null ? 0L : new Long(getParameter("attendanceId"));
        }

        if (attendanceId != null && attendanceId != 0) {

            attendance = attendanceManager.get(attendanceId);

            setEventTypeFilter(attendance.getEvent().getEventType());
            setSelectedEvent(attendance.getEvent());
            setSelectedEventOccurrence(attendance.getEvent());

            getSelectedPersons().addAll(attendance.getPersons());
            sourcePersons.addAll(attendance.getPersons());

        } else {
            // new attendance from calendar
            // need set the event.
            if (from.equals("list") || from.equals("calendarform")) {
                setSelectedEvent(eventManager.get(eventId));
                selectedEventOccurrence = getSelectedEvent();
            } else {
                selectedEventOccurrence = locateEventOccurrence(selectedEvent.getEventId());
            }

            eventTypeFilter = eventTypeManager.findByNamedQuery(Queries.FIRST_EVENT_TYPE, null).get(0);

            attendance = loadOrCreateAttendance();
            attendance.setEvent(selectedEventOccurrence);

        }

    }
}
