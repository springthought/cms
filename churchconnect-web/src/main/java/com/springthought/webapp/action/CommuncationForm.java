package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.SayVerbLanguage;
import com.springthought.Constants.SayVerbVoiceOption;
import com.springthought.Constants.Status;
import com.springthought.model.Communication;
import com.springthought.model.Group;
import com.springthought.model.Message;
import com.springthought.model.User;
import com.springthought.service.GenericManager;
import com.springthought.util.ConvertUtil;
import com.twilio.sdk.verbs.Say;
import com.twilio.sdk.verbs.TwiMLResponse;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Component("communicationForm")
@Scope("view")
public class CommuncationForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String query;
    private GenericManager<Group, Long> groupManager;
    private GenericManager<Message, Long> messageManager;

    private List<Group> selectedGroups = new ArrayList<Group>(0);
    private Message selectedMessage = null;

    private boolean checked;
    private final String FIRST_STEP = "group";
    private final String LAST_STEP = "send";
    private static final String MESSAGE_TYPE_CREATE = "1";
    private static final String MESSAGE_TYPE_EXISTING = "2";
    private Message message = new Message();;

    private String currentStep = "group";
    private boolean skip;
    private String type = "1";
    private String send = "1";
    private Date startDate;
    private ArrayList<Date> dateList = new ArrayList<Date>(0);
    private Communication communication = new Communication();
    private GenericManager<Communication, Long> communicationManager;

    @Autowired
    public void setGroupManager(@Qualifier("groupManager") GenericManager<Group, Long> groupManager) {
	this.groupManager = groupManager;
    }

    @Autowired
    public void setMessageManager(@Qualifier("messageManager") GenericManager<Message, Long> messageManager) {
	this.messageManager = messageManager;
    }

    @Autowired
    public void setCommunicationManager(
	    @Qualifier("communicationManager") GenericManager<Communication, Long> communicationManager) {
	this.communicationManager = communicationManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public CommuncationForm() {
	setSortColumn("groupId"); // sets the default sort column
	SimpleDateFormat format = new SimpleDateFormat("MMM, d, yyyy hh:mm a z");
	message.setName(format.format(new Date()));

    }

    public List<Group> getGroups() {
	return groupManager.getAllDistinct();
    }

    public String search() {
	return "success";
    }

    public List<Group> getSelectedGroups() {
	return selectedGroups;
    }

    public void setSelectedGroups(List<Group> groups) {
	this.selectedGroups = groups;
    }

    public Message getSelectedMessage() {
	return selectedMessage;
    }

    public void setSelectedMessage(Message selectedMessage) {
	this.selectedMessage = selectedMessage;
    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public void onRowSelect(SelectEvent event) {
    }

    public void onRowUnSelect(UnselectEvent event) {
    }

    public void onToggleSelect(ToggleSelectEvent event) {
    }

    public void onMessageTypeChange() {
	SimpleDateFormat format = new SimpleDateFormat("MMM, d, yyyy hh:mm a z");
	message.setName(format.format(new Date()));
    }

    public void onSendTypeChange() {

	if (send.equals("1")) {
	    startDate = new Date();
	}

    }

    public void handleDateSelect(SelectEvent event) {

    }

    public boolean isChecked() {
	return checked;
    }

    public boolean isSkip() {
	return skip;
    }

    public void setSkip(boolean skip) {
	this.skip = skip;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }

    public String getCurrentStep() {
	return currentStep;
    }

    public void setCurrentStep(String currentStep) {
	this.currentStep = currentStep;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getSend() {
	return send;
    }

    public void setSend(String send) {
	this.send = send;
    }

    public Message getMessage() {
	return message;
    }

    public void setMessage(Message message) {
	this.message = message;
    }

    public String save() {

	try {

	    if (type.equals("1") && message.isPhone()) {

		TwiMLResponse twiml = new TwiMLResponse();

		Say say = new Say(ConvertUtil.html2text(message.getMessage()));
		say.setVoice(message.getVoice().toString().toLowerCase());
		say.setLanguage(message.getLang().toString().toLowerCase());

		twiml.append(say);

		message.setVerbal(twiml.toXML());

	    } else {
		message.setVerbal(null);
	    }

	    communication.setMessage((type.equals("1") ? messageManager.save(message) : selectedMessage));
	    communication.setGroups(new HashSet<Group>(getSelectedGroups()));
	    communication.setStatus(Status.SCHD);
	    communication.setStartDate(startDate);
	    communication.setName(communication.getMessage().getName());

	    boolean isNew = (communication.getCmmnctnId() == null || communication.getCmmnctnId() == 0);

	    communicationManager.save(communication);
	    String key = (isNew) ? "communication.added" : "communication.updated";
	    addMessage(key);

	    if (isNew) {
		return "communicationForm";
	    } else {
		return "communicationForm";
	    }
	}  catch (ConstraintViolationException e) {

	    DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
	 }

	catch (Exception e) {
	    System.out.println(e);
	    // UUID guid = UUID.randomUUID();
	    // addError("errors.generalApplicationError", new Object[] {
	    // e.getMessage(), guid.toString() });
	    // log.error(guid.toString(), e);
	}

	return "";

    }

    public String onFlowProcess(FlowEvent event) {

	if (isSkip()) {
	    setCurrentStep(LAST_STEP);
	    return LAST_STEP;
	} else {
	    setCurrentStep(event.getNewStep());
	    return event.getNewStep();
	}

    }

    @SuppressWarnings("unchecked")
    public List<Message> getMessages() {
	setSortColumn("messageId"); // sets the default sort column
	return sort(messageManager.getAll());
    }

    public void validateGroup(ComponentSystemEvent event) {
	addError("errors.selectOneGroup");
	getFacesContext().renderResponse();
    }

    public Date getStartDate() {
	return startDate;
    }

    public void setStartDate(Date startDate) {
	this.startDate = startDate;
    }

    public SayVerbVoiceOption[] getVoices() {
	return Constants.SayVerbVoiceOption.values();
    }

    public SayVerbLanguage[] getlanguages() {

	return Constants.SayVerbLanguage.values();

    }

    public void sendMail( ActionEvent event ) {
	User user = this.currentUser();
	this.setTemplateName("mailTemplate.vm");
	this.sendUserMessage(user,message.getMessage(),
		"http://197f2cf2.ngrok.io/ChurchClick/");
	addMessage("Test message was sucessfully sent");
    }

    @PostConstruct
    void initilization() {
	type = (hasAnyRole("ROLE_EDIT_MESSAGE")) ? "1" : "2";
	log.debug(type);
    }

}
