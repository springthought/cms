package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.Category;
import com.springthought.model.Group;
import com.springthought.service.GenericManager;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FlowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("groupForm")
@Scope("view")
public class GroupForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private GenericManager<Group, Long> groupManager;
    private Group group = new Group();
    private Long groupId;
    private String from;
    private String template = "/templates/mainMenu.xhtml";
    private GenericManager<Category, Long> categoryManager;
    private String currentStep = "groupDetail";

    @Autowired
    public void setGroupManager(@Qualifier("groupManager") GenericManager<Group, Long> groupManager) {
        this.groupManager = groupManager;
    }

    @Autowired
    public void setCategoryManager(@Qualifier("categoryManager") GenericManager<Category, Long> categoryManager) {
        this.categoryManager = categoryManager;
    }

    public Group getGroup() {
        return group;
    }

    public Group getSelectedGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    public List<Category> getCategories() {
        Criteria criteria = categoryManager.getSession().createCriteria(Category.class);
        Criterion groupCriterion = Restrictions.eq("categoryType", Constants.CategoryType.MINISTRY);
        Criterion tenantCriterion = Restrictions.eq("tenantId", currentUserTenantContext());
        List<Category> categories = criteria.add(Restrictions.and(groupCriterion, tenantCriterion)).list();
        return categories;
    }

    public synchronized String getCurrentStep() {
        return currentStep;
    }

    public synchronized void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }

    public String delete() {
        groupManager.remove(group.getGroupId());
        addMessage("group.deleted");

        return "groups";
    }

    public String edit() {
	
/*	groupId = getParameter("groupId") == null ? 0L : new Long(getParameter("groupId"));
	from = getParameter("from") == null ? "" : getParameter("from");

	if (groupId != null && groupId != 0) {
	    group = groupManager.get(groupId);
	} else {
	    group = new Group();
	}
	return from.equals("groupList") ? "groupMemberForm" : "groupForm";*/
        return "groupForm";
    }

    public String save() {

        boolean isNew = (group.getGroupId() == null || group.getGroupId() == 0);
        try {

            this.group = groupManager.save(group);

            String key = (isNew) ? "group.added" : "group.updated";
            addMessage(key);

            if (isNew) {
                return "groups";
            } else {
                return "groups";
            }

        } catch (DataIntegrityViolationException e) {
            DataIntegrityViolationExceptionHandler(e);
        } catch (ConstraintViolationException e) {
            DataIntegrityViolationExceptionHandler(new DataIntegrityViolationException(e.getMessage(), e));
        } catch (Exception e) {
            UUID guid = UUID.randomUUID();
            addError("errors.generalApplicationError", new Object[]{e.getMessage(), guid.toString()});
            log.error(guid.toString(), e);
        }
        return "";
    }

    public void save(ActionEvent event) {
        this.save();
    }

    public void saveAndClose(ActionEvent event) {

        if (!this.save().isEmpty()) {
            PrimeFaces.current().dialog().closeDynamic(this.group);
            //RequestContext.getCurrentInstance().closeDialog(this.group);
        }
    }

    public void close(ActionEvent event) {

        PrimeFaces.current().dialog().closeDynamic(new Group());

    }

    public String onFlowProcess(FlowEvent event) {

        setCurrentStep(event.getNewStep());
        return event.getNewStep();

    }

    @PostConstruct
    private void initScreen() {

        Category category;

        groupId = getParameter("groupId") == null ? 0L : new Long(getParameter("groupId"));

        from = getParameter("from") == null ? "" : getParameter("from");
        String view = getParameter("view") == null ? "" : getParameter("view");
        String mode = getParameter("mode") == null ? "" : getParameter("mode");

        Long categoryId = getParameter("categoryId") == null ? 0L : new Long(getParameter("categoryId"));

        if (groupId != null && groupId != 0) {
            group = groupManager.get(groupId);
            group.getGroupMembers().size();
        } else {
            group = new Group();
        }

        if (categoryId != null && categoryId != 0) {
            category = categoryManager.get(categoryId);
            group.setCategory(category);
        }

        if (from.equals("maintform")) {
            this.template = "/templates/mainNoMenu.xhtml";
        }

    }

}