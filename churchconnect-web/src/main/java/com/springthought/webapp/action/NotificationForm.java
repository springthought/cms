package com.springthought.webapp.action;

import com.springthought.model.Notification;
import com.springthought.service.GenericManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;

@Component("notificationForm")
@Scope("view")
public class NotificationForm extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5089598455904286538L;
    private GenericManager<Notification, Long> notificationManager;
    private Notification notification = new Notification();
    private Long notificationId;

    @Autowired
    public void setNotificationManager(
	    @Qualifier("notificationManager") GenericManager<Notification, Long> notificationManager) {
	this.notificationManager = notificationManager;
    }

    public Notification getNotification() {
	return notification;
    }

    public void setNotification(Notification notification) {
	this.notification = notification;
    }

    public void setNotificationId(Long notificationId) {
	this.notificationId = notificationId;
    }

    public String delete() {
	notificationManager.remove(notification.getNotificationId());
	addMessage("notification.deleted");

	return "notifications";
    }

    public String edit() {

	return "notificationForm";
    }

    public String save() {
	boolean isNew = (notification.getNotificationId() == null || notification.getNotificationId() == 0);
	notificationManager.save(notification);

	String key = (isNew) ? "notification.added" : "notification.updated";
	addMessage(key);

	if (isNew) {
	    return "notifications";
	} else {
	    return "notifications";
	}
    }

    public void save(ActionEvent event) {
	this.save();
    }

    @PostConstruct
    private void initScreen() {

	notificationId = getParameter("notificationId") == null ? 0L : new Long(getParameter("notificationId"));
	if (notificationId != null && notificationId != 0) {
	    notification = notificationManager.get(notificationId);
	} else {
	    notification = new Notification();
	}

    }

}
