
package com.springthought.webapp.action;

import com.springthought.Constants.CodeListType;
import com.springthought.model.LabelValue;
import com.springthought.model.Person;
import com.springthought.service.GenericManager;
import com.springthought.util.ConvertUtil;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.ByteArrayContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;





@Component("personList")
@Scope("view")
public class PersonList extends BasePage implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 5332333118165303360L;
    private String query;
    private GenericManager<Person, Long> personManager;
    private Person selectedPerson = new Person();
    private Long selectedPersonId = 0L;
    private boolean checked;
    private static final String IMAGE_FILE_NAME = "headoutline_1.png";
    private String defaultImageFile = null;
    private String filter = "";
    List<Person> people = new ArrayList<Person>(0);
    List<Person> peopleList = new ArrayList<Person>(0);

 
    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
	this.personManager = personManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public PersonList() {
	setSortColumn("lastName"); // sets the default sort column
    }

    @SuppressWarnings("unchecked")
    public List<Person> getPersons() {
	List<Person> suggestions = new ArrayList<Person>();
	if (filter.trim().length() > 2) {
	    for (Person p : people) {
		if (p.getName().toLowerCase().contains(filter.toLowerCase()))
		    suggestions.add(p);
	    }
	} else {
	    suggestions = people;
	}
	//return sort(suggestions);
	return suggestions;
    }

    public String search() {
	return "success";
    }

    public String getFilter() {
	return filter;
    }

    public void setFilter(String filter) {
	this.filter = filter;
    }

    public void filterList(ActionEvent event) {

	if (filter.trim().length() > 2) {
	    log.info(filter);
	}

    }

    public Person getSelectedPerson() {
	return selectedPerson;
    }

    public void setSelectedPerson(Person person) {
	log.debug("Person:" + person.getName() + " selected.");
	this.selectedPerson = person;
    }

    public Long getSelectedPersonId() {
	return selectedPersonId;
    }

    public void setSelectedPersonId(Long selectedPersonId) {
	this.selectedPersonId = selectedPersonId;
    }

    public void delete(ActionEvent event) {
	personManager.remove(selectedPerson.getPersonId());
	addMessage("person.deleted");
	checked = false;
    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }

    public StreamedContent pictures(Long personId) {

	ByteArrayContent pic = null;
	Person _person = null;

	if (personId != null) {

	    _person = personManager.get(personId);

	    if (_person != null && _person.getPersonBin() != null && _person.getPersonBin().getContents() != null) {

		pic = new ByteArrayContent(_person.getPersonBin().getContents(),
			_person.getPersonBin().getContentType(), _person.getPersonBin().getFileName());

	    } else {

		pic = generateDefaultPicture();

	    }
	}

	return pic;
    }

    private ByteArrayContent generateDefaultPicture() throws FacesException {

        ByteArrayContent pic = null;

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
                .getContext();

        defaultImageFile = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "images"
                + File.separator + IMAGE_FILE_NAME;

        try {

            pic = new ByteArrayContent(Files.readAllBytes(Paths.get(defaultImageFile)), "image/png", IMAGE_FILE_NAME);

        } catch (Exception e) {
            log.fatal(e);
            throw new FacesException("Error in writing file image.", e);
        }
        return pic;
    }


    @SuppressWarnings("unchecked")
    public List<LabelValue> getMemberTypes() {
	this.setAscending(true);
	return lookupManager.getSystemValueByCodeListType(CodeListType.STATUS);
    }


    @SuppressWarnings("unchecked")
    @PostConstruct
    private void initialization() {
	people = (List<Person>) ConvertUtil.deepClone(personManager.getAllDistinct());
    }


}
