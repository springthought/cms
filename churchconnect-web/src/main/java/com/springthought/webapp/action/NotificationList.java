
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Notification;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;


@Component("notificationList")
@Scope("view")
public class NotificationList extends BasePage implements Serializable {
    private String query;
    private GenericManager<Notification, Long> notificationManager;
	private Notification selectedNotification = new Notification();
    private boolean checked;

    @Autowired
    public void setNotificationManager(@Qualifier("notificationManager") GenericManager<Notification, Long> notificationManager) {
        this.notificationManager = notificationManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public NotificationList() {
        setSortColumn("notificationId"); // sets the default sort column
    }

    public List<Notification> getNotifications() {
        try {
            return notificationManager.search(query, Notification.class);
        } catch (SearchException se) {
            addError(se.getMessage());
            return sort(notificationManager.getAll());
        }
    }

    public String search() {
        return "success";
    }

    public Notification getSelectedNotification() {
        return selectedNotification;
    }

    public void setSelectedNotification(Notification notification) {
        this.selectedNotification = notification;
    }

	public void delete( ActionEvent event ){
    	notificationManager.remove( selectedNotification.getNotificationId());
	  	addMessage("notification.deleted");
	  	checked = false;
    }


    public void radioSelected(SelectEvent event) {checked = true;}

	public boolean isChecked() {return checked;}

	public void setChecked(boolean checked) {this.checked = checked;}
}
