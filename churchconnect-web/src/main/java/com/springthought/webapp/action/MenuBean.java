package com.springthought.webapp.action;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.springthought.Constants.MenuType;
import com.springthought.model.Notification;
import com.springthought.model.SystemMenu;
import com.springthought.model.SystemMenuGroup;
import com.springthought.model.SystemMenuItem;
import com.springthought.service.GenericManager;
import com.springthought.service.SystemMenuManager;
import com.springthought.util.FacesUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Scope("session")
@Component("menuBean")
public class MenuBean extends BasePage implements Serializable {

    private static final long serialVersionUID = -8445923884435946447L;
    private SystemMenuManager sysMenuManager;
    private List<SystemMenu> smMenuItem = null;
    private List<SystemMenu> smMenuPanelItem = null;
    private List<SystemMenu> smSplitItem = null;
    private MenuModel menuModel;
    private MenuModel splitMenuModel;
    private GenericManager<Notification, Long> notificationManager;
    private Notification sourceNotification;

    @Autowired
    public void setMenuManager(@Qualifier("systemMenuManager") SystemMenuManager sysMenuManager) {
	this.sysMenuManager = sysMenuManager;

    }

    @Autowired
    public void setNotificationManager(
	    @Qualifier("notificationManager") GenericManager<Notification, Long> notificationManager) {
	this.notificationManager = notificationManager;
    }

    /*
     *
     */
    @SuppressWarnings("unchecked")
    public List<SystemMenu> getSystemMenuAll() {
	List<SystemMenu> sm = this.sysMenuManager.getAll();
	setSortColumn("item_order");
	setAscending(true);
	return sort(sm);
    }

    public List<SystemMenu> getSystemMenu() {
	if (smMenuItem == null) {
	    smMenuItem = getMenu(MenuType.MENU);
	}
	return smMenuItem;
    }

    public List<SystemMenu> getSystemMenuButton() {

	if (smSplitItem == null) {
	    smSplitItem = getMenu(MenuType.SPLIT);
	}
	return smSplitItem;
    }

    public List<SystemMenu> getSystemPanelMenu() {
	if (smMenuPanelItem == null) {
	    smMenuPanelItem = getMenu(MenuType.PANEL);
	}
	return smMenuPanelItem;
    }

    private List<SystemMenu> getMenu(MenuType menuType) {
	List<SystemMenu> sm = this.sysMenuManager.findByMenuType(menuType);
	setSortColumn("itemOrder");
	setAscending(true);
	sort(sm);
	return sm;
    }

    public MenuModel getMenuModel() {
	return menuModel;
    }

    public void setMenuModel(MenuModel menuModel) {
	this.menuModel = menuModel;
    }

    public MenuModel getSplitMenuModel() {
	return splitMenuModel;
    }

    public void setSplitMenuModel(MenuModel splitMenuModel) {
	this.splitMenuModel = splitMenuModel;
    }

    @PostConstruct
    void initilization() {

	menuModel = new DefaultMenuModel();

	List<SystemMenu> systemMenu = getSystemMenu();

	for (SystemMenu menu : systemMenu) {
	    /**
	     * if Menu has a view specified add menu item v/s Sub Menu and don't
	     * add any children to it.
	     */

	    if (StringUtils.isNotBlank(menu.getView())) {

		DefaultMenuItem menuItem = new DefaultMenuItem();
		menuItem.setId("mnui_" + menu.getSystemMenuId());
		menuItem.setOutcome(menu.getView());
		menuItem.setAjax(false);
		menuItem.setValue(menu.getMenuItem());
		menuItem.setIcon(menu.getIconpath());
		menuItem.setRendered(hasAnyRole(menu.getRole()));
		menuModel.addElement(menuItem);
		continue;

	    }

	    DefaultSubMenu subMenu = new DefaultSubMenu();
	    subMenu.setLabel(menu.getMenuItem());
	    subMenu.setIcon(menu.getIconpath());
	    subMenu.setId("smb_" + menu.getSystemMenuId());
	    subMenu.setRendered(hasAnyRole(menu.getRole()));

	    menuModel.addElement(subMenu);

	    Set<SystemMenuGroup> systemMenuGroup = menu.getSystemMenuGroups();

	    for (SystemMenuGroup menugroup : systemMenuGroup) {


		if (StringUtils.isNotBlank(menugroup.getView())){

		    DefaultMenuItem menuItem = new DefaultMenuItem();
		    menuItem.setId("mnugi_" + menugroup.getSystemMenuGroupId());
		    menuItem.setOutcome(menugroup.getView());
		    menuItem.setAjax(false);
		    menuItem.setValue(menugroup.getName());
		    menuItem.setIcon(menugroup.getIconpath());
		    menuItem.setRendered(hasAnyRole(menugroup.getRole()));
		    subMenu.addElement(menuItem);
		    continue;
		}

		DefaultSubMenu subGroupMenu = new DefaultSubMenu();

		subGroupMenu.setLabel(menugroup.getName());
		subGroupMenu.setId("smbg_" + menugroup.getSystemMenuGroupId());
		subGroupMenu.setRendered(hasAnyRole(menugroup.getRole()));

		subMenu.addElement(subGroupMenu);

		Set<SystemMenuItem> items = menugroup.getSystemMenuItems();

		for (SystemMenuItem menuitem : items) {


		    DefaultMenuItem item = new DefaultMenuItem();

		    item.setId("mnui_" + menuitem.getSystemMenuItemId());
		    item.setAjax(false);
		    item.setValue(menuitem.getName());
		    item.setRendered(hasAnyRole(menuitem.getRole()));

		    if (StringUtils.isNotBlank(menuitem.getView())) {

			item.setOutcome(menuitem.getView());

		    } else if (StringUtils.isNotBlank(menuitem.getActionExpression())) {

			item.setCommand(FacesUtils.getJsfEl(menuitem.getActionExpression()));

			FacesUtils.createMethodExpression(FacesUtils.getJsfEl(menuitem.getActionExpression()),
				String.class, new Class[0]);

		    } else if (StringUtils.isNotBlank(menuitem.getActionListener())) {

			FacesUtils.createMethodActionListener(FacesUtils.getJsfEl(menuitem.getActionListener()),
				Void.class, new Class[] { ActionEvent.class });
		    }

		    subGroupMenu.addElement(item);

		}
	    }
	}

    }

    public Long getTotalNotifications() {

	String hql = "select count(*) from Notification n where n.user.id = :userId";
	Session session = sysMenuManager.getSession();
	Long count = 0L;

	if (currentUser() != null) {
	    count = (Long) session.createQuery(hql).setLong("userId", currentUser().getId()).uniqueResult();
	}
	return count;

    }

    @SuppressWarnings("unchecked")
    public ArrayList<Notification> getNotifications() {

	String hql = "from Notification n where n.user.id = :userId";
	Session session = notificationManager.getSession();
	ArrayList<Notification> notifications = new ArrayList<Notification>(0);

	if (currentUser() != null) {

	    notifications = (ArrayList<Notification>) session.createQuery(hql).setLong("userId", currentUser().getId())
		    .list();

	    for (Notification notification : notifications) {

		String params = StringUtils.defaultString(notification.getParm1()).trim() + ","
			+ StringUtils.defaultString(notification.getParm2()).trim() + ","
			+ StringUtils.defaultString(notification.getParm3()).trim();

		String[] args = Lists.newArrayList(Splitter.on(",").split(params)).stream().toArray(String[]::new);

		notification.setDescr(getText(notification.getKey(), args));
	    }
	}
	return notifications;

    }

    public void processNotificationAction(ActionEvent event) {

	Object object = (Object) event.getComponent().getAttributes().get("source");

	if (object != null) {

	    sourceNotification = (Notification) object;

	    switch (sourceNotification.getNotificationType()) {
	    case BIRTHDAY:
	    case ANNIVERSARY:
	    case JOINED:
		getFacesContext().getApplication().getNavigationHandler().handleNavigation(getFacesContext(), "null",
			gotoPerson());
		break;
	    default:
		break;
	    }
	}

    }

    private String gotoPerson() {
	return "/views/personForm.xhtml?faces-redirect=true&personId=" + sourceNotification.getPerson().getPersonId()
		+ "&from=list";
    }

 /*   private boolean hasAnyRole(String splitString) {

	boolean founded = false;

	String delims = ",";
	StringTokenizer token = new StringTokenizer(splitString, delims);
	while (token.hasMoreElements()) {

	    Permission permission = new Permission((String) token.nextElement());
	    founded = currentUser().getAuthorities().contains((GrantedAuthority) permission);

	    if (founded) {
		break;
	    }

	}

	return founded;

    }*/
}
