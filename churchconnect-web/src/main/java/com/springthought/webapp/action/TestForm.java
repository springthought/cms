/**
 *
 */
package com.springthought.webapp.action;

import org.primefaces.component.editor.Editor;
import org.primefaces.event.CloseEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author kinseye
 *
 */
@Component("testForm")
@Scope("session")
public class TestForm  extends BasePage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Date currentDateTime = new Date();
	private String text;
	private Integer count = 0;
	private Editor editor;


	/**
	 * @return the editor
	 */
	public Editor getEditor() {
		System.out.print("Get");
		log.debug("Get...");
		return editor;
	}


	/**
	 * @param editor the editor to set
	 */
	public void setEditor(Editor editor) {
		log.debug(editor.getClientId());
		this.editor = editor;
	}


	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}


	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		count++;
		this.text = text + " " + count;
		log.debug(this.text);
	}


	/*
	 *
	 */
	public String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date currentDateTime = new Date();
		return dateFormat.format(currentDateTime);
	}


	/**
	 * @return the currentDateTime
	 */
	public Date getCurrentDateTime() {
		return currentDateTime;
	}


	/**
	 * @param currentDateTime the currentDateTime to set
	 */
	public void setCurrentDateTime(Date currentDateTime) {
		this.currentDateTime = currentDateTime;
	}

	public void applyEdit(ActionEvent event){
		log.debug( event.toString());
	}


	public void handleClose(CloseEvent event) {

        facesContext = FacesContext.getCurrentInstance();

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
            event.getComponent().getId() + " closed", "So you don't like nature?");

        facesContext.addMessage(null, message);

        log.debug( event.toString());


    }

}
