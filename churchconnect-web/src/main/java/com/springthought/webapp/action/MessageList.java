
package com.springthought.webapp.action;

import com.springthought.model.Message;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Component("messageList")
@Scope("view")
public class MessageList extends BasePage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -4698837280702644875L;
    private String query;
    private GenericManager<Message, Long> messageManager;
    private Message selectedMessage = new Message();
    private boolean checked;

    @Autowired
    public void setMessageManager(@Qualifier("messageManager") GenericManager<Message, Long> messageManager) {
	this.messageManager = messageManager;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public String getQuery() {
	return query;
    }

    public MessageList() {
	setSortColumn("messageId"); // sets the default sort column
    }

    @SuppressWarnings("unchecked")
    public List<Message> getMessages() {
	return sort(messageManager.getAll());
    }

    public String search() {
	return "success";
    }

    public Message getSelectedMessage() {
	return selectedMessage;
    }

    public void setSelectedMessage(Message message) {
	this.selectedMessage = message;
    }

    public void delete(ActionEvent event) {
	try {
	    Message message = messageManager.get(selectedMessage.getMessageId());
	    messageManager.remove(message.getMessageId());
	    addMessage("message.deleted");
	} catch (DataIntegrityViolationException e) {

	    DataIntegrityViolationExceptionHandler(e);

	} catch (Exception e) {
	    UUID guid = UUID.randomUUID();
	    addError("errors.generalApplicationError", new String[] { e.getMessage(), guid.toString() });
	    log.error(guid.toString(), e);
	}

    }

    public void radioSelected(SelectEvent event) {
	checked = true;
    }

    public boolean isChecked() {
	return checked;
    }

    public void setChecked(boolean checked) {
	this.checked = checked;
    }
}
