
package com.springthought.webapp.action;

import com.springthought.dao.SearchException;
import com.springthought.model.Contribution;
import com.springthought.service.GenericManager;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.List;


@Component("contributionList")
@Scope("view")
public class ContributionList extends BasePage implements Serializable {
    /**
	 *
	 */
	private static final long serialVersionUID = 8571679418172087964L;
	private String query;
    private GenericManager<Contribution, Long> contributionManager;
	private Contribution selectedContribution = new Contribution();
    private boolean checked;

    @Autowired
    public void setContributionManager(@Qualifier("contributionManager") GenericManager<Contribution, Long> contributionManager) {
        this.contributionManager = contributionManager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public ContributionList() {
    	setAscending(false);
        setSortColumn("contributionId"); // sets the default sort column
    }

    public List<Contribution> getContributions() {
        try {
            return contributionManager.search(query, Contribution.class);
        } catch (SearchException se) {
            addError(se.getMessage());
            return sort(contributionManager.getAll());
        }
    }

    public String search() {
        return "success";
    }

    public Contribution getSelectedContribution() {
        return selectedContribution;
    }

    public void setSelectedContribution(Contribution contribution) {
        this.selectedContribution = contribution;
    }

	public void delete( ActionEvent event ){
    	contributionManager.remove( selectedContribution.getContributionId());
	  	addMessage("contribution.deleted");
	  	checked = false;
    }


    public void radioSelected(SelectEvent event) {checked = true;}

	public boolean isChecked() {return checked;}

	public void setChecked(boolean checked) {this.checked = checked;}
}
