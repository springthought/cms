/**
 * @author eairrick
 */
/**
 * @author eairrick
 *
 */
package com.springthought.webapp.listener;

import com.springthought.Constants;
import com.springthought.Constants.BasicProfileType;
import com.springthought.model.Notification;
import com.springthought.model.Permission;
import com.springthought.model.Role;
import com.springthought.model.User;
import com.springthought.service.GenericManager;
import com.springthought.service.UserExistsException;
import com.springthought.service.UserManager;
import com.springthought.util.DateUtil;
import com.springthought.util.Queries;
import com.springthought.webapp.action.NotificationUtilBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CustomAuthenticationSuccessListener implements AuthenticationSuccessHandler {

    private UserManager userManager;
    private GenericManager<Notification, Long> notificationManager;
    private NotificationUtilBean notificationUtilBean;
    private static String UNVERIFIED_TARGET_URI = "/ui/unVerifiedEmail.xhtml";
    private static String DEFAULT_PAGE = "/views/home.xhtml";
    protected final Log log = LogFactory.getLog(getClass());
    private GenericManager<Role, Long> roleManager;

    @Autowired
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    @Autowired
    public void setNotificationManager(
            @Qualifier("notificationManager") GenericManager<Notification, Long> notificationManager) {
        this.notificationManager = notificationManager;
    }

    @Autowired
    public void setRoleManager(@Qualifier("roleManager") GenericManager<Role, Long> roleManager) {
        this.roleManager = roleManager;
    }

    /*
     * @Autowired public void
     * setNotificationUtilBean(@Qualifier("notificationUtilBean")
     * NotificationUtilBean notificationUtilBean) { this.notificationUtilBean =
     * notificationUtilBean; }
     */

    @Autowired
    public void setNotificationUtilBean(@Qualifier("notificationUtilBean") NotificationUtilBean notificationUtilBean) {
        this.notificationUtilBean = notificationUtilBean;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {

        httpServletResponse.setStatus(HttpServletResponse.SC_OK);

        HttpSession session = httpServletRequest.getSession();
        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        authUser = userManager.get(authUser.getId());

        if (authUser.isVerified()) {

            authUser.setLastLogin(authUser.getCurrentLogin());
            authUser.setCurrentLogin(new Date());

            session.setAttribute("username", authUser.getUsername());
            session.setAttribute("authorities", authentication.getAuthorities());
            session.setAttribute("lastlogin", DateUtil.getDate(authUser.getLastLogin()));
            session.setAttribute("currentlogin", DateUtil.getDate(authUser.getCurrentLogin()));

            try {
                setupData(authUser);
                ArrayList<Notification> notifications = notificationUtilBean.processDates(authUser);
                notifications.forEach(n -> notificationManager.save(n));
                authUser = userManager.saveUser(authUser);
            } catch (UserExistsException e) {
                log.error(e.getMessage(), e);
            } catch (Exception e) {
                log.error(e);
            }

            // since we have created our custom success handler, its up to us to
            // where we will redirect the user after successfully login

            // DefaultSavedRequest dsr = new
            // DefaultSavedRequest(httpServletRequest,null);

            // DefaultSavedRequest targetRequest = (DefaultSavedRequest)
            // session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
            // httpServletResponse.sendRedirect(targetRequest.getRequestURI());

            DefaultSavedRequest targetRequest = (DefaultSavedRequest) session
                    .getAttribute("SPRING_SECURITY_SAVED_REQUEST");

            String targetURI = (targetRequest == null) ? httpServletRequest.getContextPath() + DEFAULT_PAGE
                    : targetRequest.getRequestURI();

            httpServletResponse.sendRedirect(targetURI);

        } else {
            // TODO:Keep user but remove all permission.
            httpServletResponse.sendRedirect(unVerifiedRequest(httpServletRequest));
        }

    }

    private String unVerifiedRequest(HttpServletRequest request) {
        String URI = request.getContextPath() + UNVERIFIED_TARGET_URI;
        return URI;

    }

    // TODO - Move this to new client route and initilize data.

    private void setupData(User user) {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        for (BasicProfileType p : Constants.BasicProfileType.values()) {

            queryParams.put(Queries.NAME_PARM, p.name());
            queryParams.put(Queries.TENANT_ID_PARM, user.getTenantId());

            List<Role> roles = roleManager.findByNamedQuery(Queries.ROLE_BY_TENANT, queryParams);

            if (roles.size() == 0) {

                queryParams.put(Queries.TENANT_ID_PARM, 0L);
                List<Role> copy = roleManager.findByNamedQuery(Queries.ROLE_BY_TENANT, queryParams);

                if (copy.size() > 0) {

                    Role role = copy.get(0);
                    Role newRole = new Role();
                    newRole.setLabel(role.getLabel());
                    newRole.setName(role.getName());
                    newRole.setTenantId(user.getTenantId());
                    newRole.setDescription(role.getDescription());
                    newRole.setBasic(false);
                    newRole.setCreateDate(new Date());
                    newRole.setCreatedByUser(user.getUsername());
                    newRole.setUpdatedByUser(user.getUsername());
                    newRole.setLastUpdate(new Date());
                    // copy permissions from source basic role.
                    for (Permission pm : role.getPermissions()) {

                        Permission permission = new Permission(pm.getName());
                        permission.setPermissionCategory(pm.getPermissionCategory());
                        permission.setDescr(pm.getDescr());
                        permission.setPermissionId(pm.getPermissionId());
                        newRole.getPermissions().add(permission);
                    }
                    //

                    roleManager.save(newRole);

                } else {
                    try {
                        throw new Exception("Unable able to locate system role:  " + p.name());
                    } catch (Exception e) {
                        log.error(e);
                    }
                }

            }
        }

    }
}
