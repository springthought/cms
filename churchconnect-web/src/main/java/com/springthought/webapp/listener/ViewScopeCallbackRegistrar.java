package com.springthought.webapp.listener;

import com.springthought.webapp.scope.ViewScope;

import javax.faces.component.UIViewRoot;
import javax.faces.event.*;
import java.util.HashMap;
import java.util.Map;

public class ViewScopeCallbackRegistrar implements ViewMapListener {

	@Override
	public void processEvent(SystemEvent event) throws AbortProcessingException {

		if (event instanceof PostConstructViewMapEvent) {

			PostConstructViewMapEvent viewMapEvent = (PostConstructViewMapEvent) event;
			UIViewRoot viewRoot = (UIViewRoot) viewMapEvent.getComponent();
			viewRoot.getViewMap().put(ViewScope.VIEW_SCOPE_CALLBACKS,
					new HashMap<String, Runnable>());

		} else if (event instanceof PreDestroyViewMapEvent) {

			PreDestroyViewMapEvent viewMapEvent = (PreDestroyViewMapEvent) event;
			UIViewRoot viewRoot = (UIViewRoot) viewMapEvent.getComponent();
			@SuppressWarnings("unchecked")
			Map<String, Runnable> callbacks = (Map<String, Runnable>) viewRoot
					.getViewMap().get(ViewScope.VIEW_SCOPE_CALLBACKS);

			if (callbacks != null) {
				for (Runnable c : callbacks.values()) {
					c.run();
				}
				callbacks.clear();
			}
		}
	}

	@Override
	public boolean isListenerForSource(Object source) {
		return source instanceof UIViewRoot;
	}
}
