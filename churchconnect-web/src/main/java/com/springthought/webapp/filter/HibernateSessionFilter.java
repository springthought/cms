/**
 *
 */
package com.springthought.webapp.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import javax.servlet.*;
import java.io.IOException;

public class HibernateSessionFilter implements Filter {

	protected final Log log = LogFactory.getLog(getClass());

	@Resource
	private SessionFactory sessionFactory;
	private Long test;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		log.debug("doFilter - Enabling Filter:  tenantFilter");
		log.debug("doFilter - Setting Tenant Id: " + "2");

		log.debug("Session Statistics: " + getSession().getStatistics());

		getSession().enableFilter("tenantFilter").setParameter("tenantId", 2L);

		chain.doFilter(request, response);

		log.debug("doFilter - Disabling Filter :tenantFilter");
		log.debug("doFilter - Setting Tenant Id: " + "0");
		log.debug("doFilter - End");
	}

	@Override
	public void destroy() {

	}

	/**
	 *
	 * Project: framework Method : getSessionFactory Return : SessionFactory
	 * Params : @return Author : eairrick
	 *
	 */

	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}

	public Session getSession() throws HibernateException {

		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	@Autowired
	@Required
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @return the test
	 */
	public Long getTest() {
		return test;
	}

	/**
	 * @param test
	 *            the test to set
	 */
	public void setTest(Long test) {
		this.test = test;
	}

}
