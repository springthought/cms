/**
 *
 */
package com.springthought.webapp.jsf.listener;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.ServletContext;

/**
 * @author eairrick
 *
 */
public class HibernateRenderResponsePhaseListener implements PhaseListener {

    /**
     *
     */
    private static final long serialVersionUID = -3514559095363702100L;

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    @Override
    public void afterPhase(PhaseEvent event) {

	SessionFactory sessionFactory = lookupSessionFactory();
	SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory);
	//SessionFactoryUtils.releaseSession(sessionHolder.getSession(), sessionFactory);
	SessionFactoryUtils.closeSession(sessionHolder.getSession());


    }

    @Override
    public void beforePhase(PhaseEvent event) {

    }

    @Override
    public PhaseId getPhaseId() {
	return PhaseId.RENDER_RESPONSE;
    }

    protected SessionFactory lookupSessionFactory() {
	FacesContext context = FacesContext.getCurrentInstance();
	ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
	WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(servletContext);
	return (SessionFactory) wac.getBean("sessionFactory", SessionFactory.class);
    }

}
