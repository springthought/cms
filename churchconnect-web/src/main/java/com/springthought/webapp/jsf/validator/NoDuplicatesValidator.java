/**
 *
 */
package com.springthought.webapp.jsf.validator;

import org.primefaces.component.celleditor.CellEditor;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;
import java.util.*;

/**
 * @author eairrick
 *
 */
@FacesValidator(value = "noDuplicates")
public class NoDuplicatesValidator extends MessageAttributeAwareValidator {
	public static final String REQUEST_SCOPE_KEY = "_NoDuplicatesValidatorValuesMap";
	public static final String DUPLICATES_MESSAGE = "duplicatesMessage";
	public static final String DEFAULT_MESSAGE = "Duplicate Value";

	public NoDuplicatesValidator() {
		super(DUPLICATES_MESSAGE, DEFAULT_MESSAGE);
	}

	@Override
	public void validate(final FacesContext context,
			final UIComponent component, final Object value)
			throws ValidatorException {

		UIComponent inputFacet = component.getParent();

		if (inputFacet instanceof UIPanel) {

			UIComponent inputCellEdit = inputFacet.getParent();

			if (inputCellEdit instanceof CellEditor) {

				UIComponent inputColumn = inputCellEdit.getParent();

				if (inputColumn instanceof Column) {

					UIComponent table = inputColumn.getParent();

					if (table instanceof DataTable) {

						DataTable data = (DataTable) table;

					}

				}

			}
		}

		List<Object> list = (List<Object>) component.getAttributes().get(
				"tableList");

		final UIInput input = (UIInput) component;
		final Set<Object> componentValues = getComponentValuesSet(context,
				input);

		if (componentValues.contains(value)) {
			failValidation(component);
		}

		componentValues.add(value);
	}

	private static Set<Object> getComponentValuesSet(
			final FacesContext context, final UIInput input) {

		final Map<String, Object> requestScope = getInitialisedRequestScope(context);

		final Map<UIInput, Set<Object>> valuesMap = (Map<UIInput, Set<Object>>) requestScope
				.get(REQUEST_SCOPE_KEY);

		if (null == valuesMap.get(input)) {
			valuesMap.put(input, new HashSet<Object>());
		}
		return valuesMap.get(input);
	}

	private static Map<String, Object> getInitialisedRequestScope(
			final FacesContext context) {
		final Map<String, Object> requestScope = context.getExternalContext()
				.getRequestMap();

		if (!requestScope.containsKey(REQUEST_SCOPE_KEY)) {
			requestScope.put(REQUEST_SCOPE_KEY,
					new HashMap<UIInput, Set<Object>>());
		}
		return requestScope;
	}

}
