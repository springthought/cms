package com.springthought.webapp.jsf.validator;

import com.springthought.webapp.action.BasePage;
import org.joda.time.DateTime;
import org.primefaces.validate.ClientValidator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@FacesValidator("PastDateValidator")
public class PastDateValidator extends BasePage implements Validator, ClientValidator  {

    private String message;

    @Override
    public void validate(FacesContext context, UIComponent uicomponent, Object value) throws ValidatorException {

	boolean valid = true;

	DateTime compareDate = new DateTime((Date) value);
	DateTime now = new DateTime(new Date());

	String key = (String) uicomponent.getAttributes().get("ErrorMessage");
	Boolean runValidation = (Boolean) uicomponent.getAttributes().get("Validate");
	message = getText(key);

	String uiId = uicomponent.getId();

	if (runValidation){
	    valid = compareDate.toLocalDate().compareTo(now.toLocalDate()) >= 0;
	}

	if (!valid) {
	    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, getText(key), getText(key));
	    addError(uiId, key, null);
	    throw new ValidatorException(message);
	}

    }

    @Override
    public Map<String, Object> getMetadata() {
	Map<String,Object> data = new HashMap<String,Object>();
	data.put("message", this.message);
	return data;
    }

    @Override
    public String getValidatorId() {
	return "PastDateValidator";
    }

}
