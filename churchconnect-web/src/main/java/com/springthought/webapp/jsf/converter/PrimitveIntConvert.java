package com.springthought.webapp.jsf.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;

@FacesConverter("com.springthought.jsf.converter.PrimitveIntConvert")
public class PrimitveIntConvert implements Converter, Serializable {
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return Integer.parseInt(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return (value==null) ? "" : value.toString() ;
    }
}
