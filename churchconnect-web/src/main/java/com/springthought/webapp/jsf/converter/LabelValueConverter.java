/**
 *
 */
package com.springthought.webapp.jsf.converter;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.springthought.model.LabelValue;
import com.springthought.webapp.action.BasePage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author eairrick
 *
 */
@Component("LabelValueConverter")
@Scope("session")
public class LabelValueConverter extends BasePage implements Converter, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4262730299422792060L;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	return value;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

	Object source = (Object) component.getAttributes().get("list");
	@SuppressWarnings("unchecked")
	ArrayList<LabelValue> values = new ArrayList<LabelValue>((ArrayList<LabelValue>) source);

	LabelValue labelValue = Iterables.find(values, new Predicate<LabelValue>() {
	    @Override
	    public boolean apply(LabelValue arg) {
		return arg.getValue().equals(((String) value));
	    }
	}, new LabelValue());

	return  StringUtils.defaultString(labelValue.getLabel(), "");
    }

}
