/**
 *
 */
package com.springthought.webapp.jsf.converter;

import com.springthought.webapp.action.BasePage;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author eairrick
 *
 */
@Component("timeConverter")
@Scope("session")
public class TimeConverter extends BasePage implements Converter, Serializable  {

	/**
	 *
	 */
	private static final long serialVersionUID = -4413768318064235823L;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {


		if (value.trim().equals("")) {
			return null;
		} else {
			try {
				DateFormat formatter = new SimpleDateFormat("hh:mm a");
				Date dt = formatter.parse(value);
				return dt;

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						getText("errors.conversionError"),
						getText("errors.InvalidObject")));
			} catch (ObjectRetrievalFailureException e) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						getText("errors.conversionError"),
						getText("errors.InvalidObject")));
			} catch (ParseException e) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						getText(e.getMessage()),
						getText(e.getMessage())));
			}
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		return value.toString();
	}


}
