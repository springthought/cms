/**
 *
 */
package com.springthought.webapp.jsf.chart;

import com.springthought.model.Aggregate;
import com.springthought.model.Campaign;
import com.springthought.model.Pledge;
import com.springthought.service.GenericManager;
import com.springthought.util.Queries;
import com.springthought.webapp.action.BasePage;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.optionconfig.title.Title;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



/**
 * @author eairrick
 *
 */
public class PledgeSummaryBarChart extends BasePage {

	private Campaign campaign;
	private BarChartModel barModel;
	private GenericManager<Aggregate, Long> aggregateManager;
	private BigDecimal pledgeAmount = null;
	private BigDecimal receivedAmount = null;

	HashMap<String, Object> queryParams = new HashMap<String, Object>();

	@PostConstruct
	public void initChart() {
		createModel();
	}

	@Autowired
	public void setAggregateManager(
			@Qualifier("aggregateManager") GenericManager<Aggregate, Long> aggregateManager) {
		this.aggregateManager = aggregateManager;
	}

	public PledgeSummaryBarChart(Campaign campaign,
			GenericManager<Aggregate, Long> aggregateManager) {
		this.campaign = campaign;
		this.aggregateManager = aggregateManager;
		createModel();
	}

	public BarChartModel getPledgeSummaryAnimatedModel() {
		return barModel;
	}

	public void setPledgeSummaryAnimatedModel(
			BarChartModel barModel) {
		this.barModel = barModel;
	}

	public Campaign setCampaign() {
		return campaign;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	public void setPledge(Campaign campaign) {
		this.campaign = campaign;
	}

	private void createModel() {

		barModel = new BarChartModel();
		ChartData data = new ChartData();
		BarChartDataSet barDataSet = new BarChartDataSet();
		barDataSet.setLabel(getText("pledgeDetail.goalAmount"));


		ArrayList<Number> values = new ArrayList<Number>(0);


		List<String> bgColor = new ArrayList<String>(0);
		List<String> borderColor = new ArrayList<String>(0);
		List<String> labels = new ArrayList<String>(0);

		/*
		pledgeSummaryAnimatedModel = initBarModel();
		pledgeSummaryAnimatedModel.setTitle(campaign.getName());
		pledgeSummaryAnimatedModel.setAnimate(true);
		pledgeSummaryAnimatedModel.setLegendPosition("ne");
		pledgeSummaryAnimatedModel.setSeriesColors("5e4fa2,3288bd,66c2a5");
		Axis yAxis = pledgeSummaryAnimatedModel.getAxis(AxisType.Y);
		yAxis.setMin(0);
		*/

		//set Bar Data
		values.add(campaign.getGoalAmt().intValue());
		values.add(getPledgeAmount().intValue());
		values.add(getReceivedAmount().intValue());
		values.add(campaign.getGoalAmt().subtract(getReceivedAmount()).intValue());
		barDataSet.setData(values);

		//set Bar Color
		bgColor.add("rgba(255, 205, 86, 0.2)");
		bgColor.add("rgba(75, 192, 192, 0.2)");
		bgColor.add("rgba(54, 162, 235, 0.2)");
		bgColor.add("rgba(153, 102, 255, 0.2)");
		barDataSet.setBackgroundColor(bgColor);


		//set Bar Border Color
		borderColor.add("rgb(255, 205, 86)");
		borderColor.add("rgb(75, 192, 192)");
		borderColor.add("rgb(54, 162, 235)");
		borderColor.add("rgb(153, 102, 255)");
		barDataSet.setBorderColor(borderColor);
		barDataSet.setBorderWidth(1);

		data.addChartDataSet(barDataSet);

		labels.add(getText("pledgeDetail.goalAmount"));
		labels.add(getText("pledgeDetail.pledgeTotal"));
		labels.add(getText("pledgeDetail.actualPledges"));
		labels.add(getText("pledgeDetail.difference"));
		data.setLabels(labels);
		barModel.setData(data);

		//Options
		BarChartOptions options = new BarChartOptions();
		CartesianScales cScales = new CartesianScales();
		CartesianLinearAxes linearAxes = new CartesianLinearAxes();
		CartesianLinearTicks ticks = new CartesianLinearTicks();
		ticks.setBeginAtZero(true);
		linearAxes.setTicks(ticks);
		cScales.addYAxesData(linearAxes);
		options.setScales(cScales);

		Title title = new Title();
		title.setDisplay(true);
		title.setText(this.getCampaign().getName());
		options.setTitle(title);

		barModel.setOptions(options);



	}

	/*private BarChartModel initBarModel() {

		HashMap<Object, Number> data = new HashMap<Object, Number>(0);

		BarChartModel model = new BarChartModel();

		ChartSeries campaign = new ChartSeries();
		campaign.setLabel(getText("campaign.campaignId"));
		campaign.set(getText("pledgeSummary.YTD"), this.campaign.getGoalAmt());

		ChartSeries pledges = new ChartSeries();
		pledges.setLabel(getText("pledgeList.pledges"));
		pledges.set(getText("pledgeSummary.YTD"), getPledgeAmount());

		ChartSeries received = new ChartSeries();
		received.setLabel(getText("pledgeSummary.Received"));
		received.set(getText("pledgeSummary.YTD"), getReceivedAmount());

		model.addSeries(campaign);
		model.addSeries(pledges);
		model.addSeries(received);
		// model.setDatatipFormat("<span style=\"display:none;\">%s</span><span>%s</span>");

		return model;
	}
*/
	public BigDecimal getPledgeAmount() {

		if (pledgeAmount == null) {

			float sum = 0;

			for (Pledge pledge : campaign.getPledges()) {
				sum += pledge.getPledgeAmt().floatValue();
			}
			pledgeAmount = new BigDecimal(sum);

		}

		return pledgeAmount;
	}

	public void setPledgeAmount(BigDecimal pledgeAmount) {
		this.pledgeAmount = pledgeAmount;
	}

	public BigDecimal getReceivedAmount() {

		if (receivedAmount == null) {

			queryParams.put(Queries.CONTRIBUTION_SUM_BY_CATEGORY_ID_PARM,
					campaign.getCategory());

			List<Aggregate> agg = aggregateManager.findByNamedQuery(
					Queries.CONTRIBUTION_SUM_BY_CATEGORY, queryParams);

			receivedAmount = (BigDecimal) ((agg.get(0) == null) ? BigDecimal.ZERO
					: agg.get(0));
		}

		return receivedAmount;
	}

	public void setReceivedAmount(BigDecimal receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

}
