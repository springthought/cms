package com.springthought.webapp.jsf.validator;

import com.springthought.Constants.RepeatOption;
import com.springthought.webapp.action.BasePage;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.Date;
import java.util.Locale;

@FacesValidator("validateMaxOccurrence")
public class validateMaxOccurrence extends BasePage implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent uicomponent,
			Object value) throws ValidatorException {

		@SuppressWarnings("unused")
		String message;

		if (value != null) {
			checkMaxOccurrence(value, uicomponent, getFacesContext()
					.getViewRoot().getLocale());
		}
	}

	private void checkMaxOccurrence(Object value, UIComponent uiComponent,
			Locale locale) {

		if (isOccurrenceInRange(uiComponent)) {

			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Test Error Message",
					"Test Error Message Details."));
		}
	}

	protected Boolean isOccurrenceInRange(UIComponent uiComponent) {

		Date startDate = (Date) uiComponent.getAttributes().get("startDate");

		Date endDate = (Date) uiComponent.getAttributes().get("endDate");

		Integer maxOccurrence = (Integer) uiComponent.getAttributes().get(
				"maximum");

		RepeatOption repeat = (RepeatOption) uiComponent.getAttributes().get(
				"frequence");

		return null;
	}

}
