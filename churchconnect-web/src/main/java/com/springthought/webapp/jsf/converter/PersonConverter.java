/**
 *
 */
package com.springthought.webapp.jsf.converter;

import com.springthought.model.Person;
import com.springthought.service.DataService;
import com.springthought.webapp.action.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import java.io.Serializable;


@Component("personConverter")
@Scope("session")
public class PersonConverter extends BasePage implements Converter, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8568340270233229346L;
	private DataService dataService;

	@Autowired
	public void setDataService(@Qualifier("dataService") DataService dataService) {
		this.dataService = dataService;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		if (value.trim().equals("")) {
			return null;
		} else {
			try {
				Long id = Long.parseLong(value);

				return dataService.getPersonManager().get(id);

			} catch (NumberFormatException exception) {

				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						getText("errors.conversionError"),
						getText("errors.InvalidObject")));
			} catch (ObjectRetrievalFailureException e) {

				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						getText("errors.conversionError"),
						getText("errors.InvalidObject")));
			}
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			Person person = (Person) value;
			if (person.getPersonId() == null) {
				return "";
			} else {
				return String.valueOf(person.getPersonId());
			}
		}
	}
}
