package com.springthought.webapp.jsf.validator;

import com.springthought.webapp.action.BasePage;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.List;

@FacesValidator("AtLeastOneSelectedValidator")
public class AtLeastOneSelectedValidator extends BasePage implements Validator {

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(FacesContext context, UIComponent uicomponent, Object value) throws ValidatorException {

	boolean checkval = false;
	String key = (String) uicomponent.getAttributes().get("ErrorMessage");

	Object selection = (Object)uicomponent.getAttributes().get("Selection");

	if ( selection instanceof List){
	    checkval = (selection == null || ((List)selection).size() < 1);

	}else{
	    checkval = (selection == null );
	}

	if (checkval) {

	    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, getText(key),
		    getText(key));

	    addError(key);

	    throw new ValidatorException(message);
	}

    }

}
