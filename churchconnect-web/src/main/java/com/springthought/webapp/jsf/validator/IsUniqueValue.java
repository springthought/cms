package com.springthought.webapp.jsf.validator;


import com.springthought.model.UserValue;
import com.springthought.webapp.action.BasePage;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.List;
import java.util.Locale;

@FacesValidator("IsUniqueValue")
public class IsUniqueValue extends BasePage implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent uicomponent,
			Object value) throws ValidatorException {

		@SuppressWarnings("unused")
		String message;

		if (value != null) {
			checkUniqueness(value, uicomponent, getFacesContext().getViewRoot()
					.getLocale());
		}
	}

	private void checkUniqueness(Object value, UIComponent uiComponent,
			Locale locale) {

		@SuppressWarnings("unchecked")

		List<UserValue> values = (List<UserValue>) uiComponent.getAttributes()
				.get("tablevalues");

		if (occurence((String) value, values) > 1) {

			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Test Error Message",
					"Test Error Message Details."));
		}
	}

	private Integer occurence(String sSearch, List<UserValue> uvSearched) {
		Integer count = 0;

		for (UserValue element : uvSearched) {

			if (element.getName().equals( sSearch)) {
				count++;
			}
		}
		return count;

	}

}
