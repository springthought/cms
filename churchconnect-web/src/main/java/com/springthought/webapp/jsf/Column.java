package com.springthought.webapp.jsf;

import javax.faces.component.FacesComponent;
import javax.faces.component.NamingContainer;


@FacesComponent("com.springthought.column")
public class Column extends org.primefaces.component.column.Column implements
		NamingContainer {

	@Override
	public String getFamily() {
		 return "javax.faces.NamingContainer";

	}
}
