/**
 * 
 */
package com.springthought.webapp.jsf.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * @author eairrick
 * 
 */
public abstract class MessageAttributeAwareValidator implements Validator{

	private final String messageAttributeName;
	private final String defaultMessage;

	protected MessageAttributeAwareValidator(final String messageAttributeName,
			final String defaultMessage) {
		
		this.messageAttributeName = messageAttributeName;
		this.defaultMessage = defaultMessage;
		
	}

	protected void failValidation(final UIComponent component) {
		
		String message = (String) component.getAttributes().get(
				messageAttributeName);
		
		message = message == null ? defaultMessage : message;
		
		throw new ValidatorException(new FacesMessage(
				FacesMessage.SEVERITY_ERROR, message, message));
	}

}
