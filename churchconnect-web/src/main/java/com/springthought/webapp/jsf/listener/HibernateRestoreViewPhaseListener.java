/**
 *
 */
package com.springthought.webapp.jsf.listener;

import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.ServletContext;

/**
 * @author eairrick
 *
 */
public class HibernateRestoreViewPhaseListener implements PhaseListener {

    /**
     *
     */
    private static final long serialVersionUID = -3514559095363702100L;

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    @Override
    public void afterPhase(PhaseEvent event) {
	// TODO Auto-generated method stub

    }

    @Override
    public void beforePhase(PhaseEvent event) {
	SessionFactory sessionFactory = lookupSessionFactory();
	if (!TransactionSynchronizationManager.hasResource(sessionFactory)) {
	    Session session = getSession(sessionFactory);
	    TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
	}
    }

    @Override
    public PhaseId getPhaseId() {
	return PhaseId.RESTORE_VIEW;
    }

    protected Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
	try {
	    Session session = sessionFactory.openSession();
	    session.setFlushMode(FlushMode.MANUAL);
	    return session;
	} catch (HibernateException ex) {
	    throw new DataAccessResourceFailureException("Could not open Hibernate Session", ex);
	}
    }

    protected SessionFactory lookupSessionFactory() {
	FacesContext context = FacesContext.getCurrentInstance();
	ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
	WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(servletContext);
	return (SessionFactory) wac.getBean("sessionFactory", SessionFactory.class);
    }

}
