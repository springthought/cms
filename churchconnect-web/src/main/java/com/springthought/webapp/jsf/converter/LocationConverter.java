/**
 *
 */
package com.springthought.webapp.jsf.converter;

import com.springthought.model.Location;
import com.springthought.service.DataService;
import com.springthought.webapp.action.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;

/**
 * @author eairrick
 *
 */

@Component("locationConverter")
@Scope("session")
@FacesConverter("locationConverter")
public class LocationConverter extends BasePage implements Converter, Serializable  {

	/**
	 *
	 */
	private static final long serialVersionUID = -4710217706624646780L;
	private DataService dataService;

	@Override
	@Autowired
	public void setDataService(DataService dataService) {
		this.dataService = dataService;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {


		if (value.trim().equals("")) {
			return null;
		} else {
			try {
				Long id = Long.parseLong(value);

				return dataService.getRoomManager().get(id);

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						getText("errors.conversionError"),
						getText("errors.InvalidObject")));
			} catch (ObjectRetrievalFailureException e) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						getText("errors.conversionError"),
						getText("errors.InvalidObject")));
			}
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			Location location  = (Location)value;
			if (location.getLocationId() == null) {
				return "";
			} else {
				return String.valueOf(location.getLocationId() );
			}
		}
	}
}
