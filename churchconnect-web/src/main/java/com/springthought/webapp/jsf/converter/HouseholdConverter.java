/**
 *
 */
package com.springthought.webapp.jsf.converter;

import com.springthought.model.Household;
import com.springthought.util.FacesUtils;
import com.springthought.webapp.action.PersonForm;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * @author eairrick
 *
 */
@FacesConverter("com.springthought.webapp.action.HouseholdConverter")
public class HouseholdConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		PersonForm personForm = (PersonForm) FacesUtils
				.getManagedBean("personForm");

		if (value.trim().equals("")) {
			return null;
		} else {
			try {
				Long key = Long.parseLong(value);

				for (Household h : personForm.getHouseholds()) {
					if (h.getHouseholdId() == key) {
						return h;
					}
				}

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid household"));
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((Household) value).getHouseholdId());
		}
	}

}
