/**
 *
 */
package com.springthought.webapp.jsf.converter;

import com.springthought.webapp.action.BasePage;
import org.springframework.orm.ObjectRetrievalFailureException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;


/**
 * @author eairrick
 *
 */

@FacesConverter("phoneConverter")
public class PhoneConverter extends BasePage implements Converter, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4413768318064235823L;


    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        throw new UnsupportedOperationException("Not implemented yet");

    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {


        if (value == null) {
            return null;
        } else {
            try {
                String phoneNumber = (String) value;
                StringBuilder formattedPhoneNumber = new StringBuilder();
                formattedPhoneNumber.append("(");
                formattedPhoneNumber.append(phoneNumber.substring(0, 3));
                formattedPhoneNumber.append(")");
                formattedPhoneNumber.append(" ");
                formattedPhoneNumber.append(phoneNumber.substring(3, 6));
                formattedPhoneNumber.append("-");
                formattedPhoneNumber.append(phoneNumber.substring(6));
                return formattedPhoneNumber.toString();

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        getText("errors.conversionError"), getText("errors.InvalidObject")));
            } catch (ObjectRetrievalFailureException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        getText("errors.conversionError"), getText("errors.InvalidObject")));
            }
        }

    }

}
