/**
 *
 */
package com.springthought.webapp.jsf.converter;

import com.springthought.model.LabelValue;
import com.springthought.service.DataService;
import com.springthought.webapp.action.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author eairrick
 *
 */
@Component("memberTypeConverter")
@Scope("session")
public class MemberTypeConverter extends BasePage implements Converter, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4262730379422792060L;

	private HashMap<String, String> memberTypeMap = new HashMap<String, String>();

	private DataService dataService;

	@Override
	@Autowired
	public void setDataService(DataService dataService) {
		this.dataService = dataService;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {

		return value;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		if (value == null || value.equals("")) {
			return "";
		} else {
			String label = memberTypeMap.get(value);
			return (label != null ? label : "");
		}
	}

	@PostConstruct
	public void load(){

		ArrayList<LabelValue> memberTypes = (ArrayList<LabelValue>) dataService
				.getMemberTypes();

		memberTypeMap = new HashMap<String, String>();

		for (LabelValue lv : memberTypes) {
			memberTypeMap.put(lv.getValue(), lv.getLabel());
		}
	}

}
