package com.springthought.webapp.servlet;

import com.twilio.sdk.verbs.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class TwilioServlet extends HttpServlet {

    /**
	 *
	 */
	private static final long serialVersionUID = -5879170854998802544L;

	public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Create a dict of people we know.
        HashMap<String, String> callers = new HashMap<String, String>();
        callers.put("+14158675309", "Curious George");
        callers.put("+14158675310", "Boots");
        callers.put("+14158675311", "Virgil");
        callers.put("+16788787621", "Eric");

        String fromNumber = request.getParameter("From");
        String knownCaller = callers.get(fromNumber);
        String message;

        if (knownCaller == null) {
            // Use a generic message
            message = "Hello Monkey";
        } else {
            // Use the caller's name
            message = "Hello " + knownCaller;
        }

        // Create a TwiML response and add our friendly message.
        TwiMLResponse twiml = new TwiMLResponse();

        Say say = new Say(message);

        // Play an MP3 for incoming callers.
        Play play = new Play("http://demo.twilio.com/hellomonkey/monkey.mp3");

        Gather gather = new Gather();
        gather.setAction("http://12276b5c.ngrok.io/ChurchClick/handle-key");
        gather.setNumDigits(1);
        gather.setMethod("POST");

        Say sayInGather = new Say("To speak to a real monkey, press 1. " +
                "Press 2 to record your own monkey howl. " +
                "Press any other key to start over.");
        try {
            gather.append(sayInGather);
            twiml.append(say);
            twiml.append(play);
            twiml.append(gather);

        } catch (TwiMLException e) {
            e.printStackTrace();
        }

        response.setContentType("application/xml");
        response.getWriter().print(twiml.toXML());
    }
}
