package com.springthought.webapp.servlet;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class TwilioStatusServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -5879170854998802544L;

    @Override
    public void init(ServletConfig config) throws ServletException {
	super.init(config);
	SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    // service() responds to both GET and POST requests.
    // You can also use doGet() or doPost()

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {

	Enumeration<String> headerNames = request.getHeaderNames();

	while (headerNames.hasMoreElements()) {
	    String headerName = headerNames.nextElement();
	    System.out.println(request.getHeader(headerName));
	}

	Enumeration<String> params = request.getParameterNames();

	while (params.hasMoreElements()) {
	    String paramName = params.nextElement();
	    System.out.println("Attribute Name - " + paramName + ", Value - " + request.getParameter(paramName));
	}

	response.setContentType("application/xml");
	response.getWriter().print("<?xml version='1.0' encoding='UTF-8'?>");

    }
}
