package com.springthought.webapp.servlet;

import com.springthought.model.Message;
import com.springthought.service.GenericManager;
import com.springthought.util.ConvertUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

public class TwilioMsgServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -5879170854998802544L;
    private GenericManager<Message, Long> messageManager;
    protected final Log log = LogFactory.getLog(getClass());

    @Autowired
    public void setMessageManager(@Qualifier("messageManager") GenericManager<Message, Long> messageManager) {
	this.messageManager = messageManager;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
	super.init(config);
	SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    // service() responds to both GET and POST requests.
    // You can also use doGet() or doPost()
    // Servlet returns the <say> verb from a message instance.
    // Looks for parameter ID from the request and streams the verbal XML back
    // on the response.

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {

	/**
	 *
	 *
	 * if (log.isDebugEnabled()) { Enumeration<String> headerNames =
	 * request.getHeaderNames();
	 *
	 * while (headerNames.hasMoreElements()) { String headerName = (String)
	 * headerNames.nextElement(); log.debug(request.getHeader(headerName));
	 * }
	 *
	 * Enumeration<String> params = request.getParameterNames();
	 *
	 * while (params.hasMoreElements()) { String paramName = (String)
	 * params.nextElement(); log.debug("Attribute Name : " + paramName +
	 * ", Value : " + request.getParameter(paramName)); } }
	 */

	HttpSession session = request.getSession(true);

	Integer counter = (Integer) session.getAttribute("counter");

	if (counter == null) {
	    counter = new Integer(0);
	}

	/* Increment the counter by one, and store the count in the session. */
	int count = counter.intValue();
	count++;
	session.setAttribute("counter", new Integer(count));

	String messageID = request.getParameter("id");
	Message message = null;

	if (StringUtils.isNotEmpty(messageID)) {

	    try {
		message = messageManager.get(Long.parseLong(messageID));
		log.info("Verbal = " + message.getVerbal());
		response.setContentType("application/xml");
		response.getWriter().print("<?xml version='1.0' encoding='UTF-8'?>"
			+ ConvertUtil.convertToUTF8(message.getVerbal().trim()));
	    } catch (Exception e) {
		UUID guid = UUID.randomUUID();
		log.error(guid.toString(), e);
	    }

	} else {
	    log.info("Empty messageID parameter");
	}

    }
}
