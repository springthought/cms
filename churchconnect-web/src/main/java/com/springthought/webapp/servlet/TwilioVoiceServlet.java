package com.springthought.webapp.servlet;

import com.twilio.sdk.verbs.Media;
import com.twilio.sdk.verbs.Message;
import com.twilio.sdk.verbs.TwiMLException;
import com.twilio.sdk.verbs.TwiMLResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;

public class TwilioVoiceServlet extends HttpServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -5879170854998802544L;

	// service() responds to both GET and POST requests.
	// You can also use doGet() or doPost()

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		Enumeration<String> headerNames = request.getHeaderNames();

		while (headerNames.hasMoreElements()) {
			String headerName = (String) headerNames.nextElement();
			System.out.println(request.getHeader(headerName));
		}

		Enumeration<String> params = request.getParameterNames();

		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			System.out.println("Attribute Name - " + paramName + ", Value - "
					+ request.getParameter(paramName));
		}

		HttpSession session = request.getSession(true);

		Integer counter = (Integer) session.getAttribute("counter");

		if (counter == null) {
			counter = new Integer(0);
		}

		/* Increment the counter by one, and store the count in the session. */
		int count = counter.intValue();
		count++;
		session.setAttribute("counter", new Integer(count));

		// Create a dict of people we know.
		HashMap<String, String> callers = new HashMap<String, String>();
		callers.put("+16788787621", "Eairrick");

		String fromNumber = request.getParameter("From");
		String toNumber = request.getParameter("To");
		String fromName = callers.get(fromNumber);

		if (fromName == null) {
			// Use the caller's name
			fromName = "SuperStar";
		}

		String replymessage = fromName + " has messaged " + toNumber + " "
				+ String.valueOf(count) + " times.";

		TwiMLResponse twiml = new TwiMLResponse();

		Message message = new Message(replymessage);


		Media media = new Media(
				"http://www.cinemablend.com/images/news_img/68373/X_Men_Apocalypse_68373.jpg");
		Media media2 = new Media(
				"http://1.media.dorkly.cvcdn.com/93/78/3265974ca309506a5f49ba0a5078e7fc-juggernaut-x-men.jpg");
		try {
			message.append(media);
			message.append(media2);

			System.out.println("Generated Message ID:" + message.getAttributes().get("MessageSid"));
			System.out.println("Generated SMS ID:" + message.getAttributes().get("SmsSid"));

			twiml.append(message);
		} catch (TwiMLException e) {
			e.printStackTrace();
		}

		response.setContentType("application/xml");
		response.getWriter().print(twiml.toXML());

	}
}
