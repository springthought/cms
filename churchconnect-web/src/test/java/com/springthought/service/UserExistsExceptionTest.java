package com.springthought.service;

import com.springthought.model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

//deprecated is Spring 3+
//import org.springframework.test.annotation.ExpectedException;

@ContextConfiguration(locations = {
        "classpath:/applicationContext-resources.xml", "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml", "classpath*:/**/applicationContext.xml", "classpath:/applicationContext-security.xml"
})
public class UserExistsExceptionTest extends AbstractTransactionalJUnit4SpringContextTests {
    @Autowired
    private UserManager manager;
    private Log log = LogFactory.getLog(UserExistsExceptionTest.class);

// @Test(expected = UserExistsException.class )
 @Test
 //public void testAddExistingUser() throws UserExistsException {
     public void testAddExistingUser()  {
        log.debug("entered 'testAddExistingUser' method");
        assertNotNull(manager);

        User user = manager.getUser("-1");

        // create new object with null id - Hibernate doesn't like setId(null)
        User user2 = new User();
        BeanUtils.copyProperties(user, user2);
        user2.setId(null);
        user2.setVersion(null);
        user2.setRoles(null);

        // try saving as new user, this should fail UserExistsException b/c of unique keys

        assertThrows(UserExistsException.class, () -> manager.saveUser(user2));

    }
}
