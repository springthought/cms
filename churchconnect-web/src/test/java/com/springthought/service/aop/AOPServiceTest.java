package com.springthought.service.aop;


import com.springthought.service.BaseManagerTestCase;
import com.springthought.util.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;

public class AOPServiceTest extends BaseManagerTestCase {
	// ~ Instance fields
	// ========================================================

	protected final Log log = LogFactory.getLog(getClass());

	@Autowired
	private Service simpleServiceBean;

	@Autowired
	private Service anotherServiceBean;

	@Test
	public void testAOP() throws Exception {

		System.out.println("---AOP Test Start---");

		simpleServiceBean.printNameId();

		System.out.println("--------------");

		System.out.println(simpleServiceBean.getName());

		System.out.println("--------------");

		try {
			simpleServiceBean.checkName();
		} catch (Exception e) {
			System.out
					.println("SimpleService: Method checkName() exception thrown..");
		}
		System.out.println("--------------");

		simpleServiceBean.sayHello("Javacodegeeks");

		System.out.println("--------------");

		String x = simpleServiceBean.getBye("Charle");
		System.out.println("Return from getBye" + x);

		assertTrue(true);

	}

	@Test
	public void testAnotherAOP() throws Exception {
	System.out.println("---Another AOP Test Start---");
		
	anotherServiceBean.printNameId();
		
		System.out.println("--------------");
	
		System.out.println( anotherServiceBean.getName() );
		
		System.out.println("--------------");
		
		
		try {
			anotherServiceBean.checkName();
		} catch (Exception e) {
			System.out
					.println("anotherServiceBean.: Method checkName() exception thrown..");
		}
		System.out.println("--------------");
		
		anotherServiceBean.sayHello("To the hand");
		
		System.out.println("--------------");
		
		String x = anotherServiceBean.getBye("Brown");
		System.out.println("Return from getBye" + x );
		
		assertTrue(true);
		
	}
}
