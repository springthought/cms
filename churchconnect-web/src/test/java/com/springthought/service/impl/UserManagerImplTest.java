package com.springthought.service.impl;

import com.springthought.Constants;
import com.springthought.model.Role;
import com.springthought.model.User;
import com.springthought.service.PasswordEncryptor;
import com.springthought.service.RoleManager;
import com.springthought.service.UserExistsException;
import com.springthought.service.UserManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectRetrievalFailureException;

import java.util.Date;

import static org.junit.Assert.*;


public class UserManagerImplTest extends BaseManagerMockTestCase {
    //~ Instance fields ========================================================

    @Autowired
    PasswordEncryptor passwordEncryptor;
    @Autowired
    private UserManager userManager;
    @Autowired
    private RoleManager roleManager;

    //~ Methods ================================================================
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testGetUser() throws Exception {

       /* final User testData = new User("1");
        testData.getRoles().add(new Role("user"));*/

        User user = userManager.get(-1L);

        assertTrue(user != null);

        assert user != null;

        assertTrue(user.getRoles().size() == 1);
    }

    @Test
    public void testSaveUser() throws Exception {
        User user = userManager.get(-1L);
        Date lastLogin = new Date();
        user.setLastLogin(lastLogin);
        User returned = userManager.saveUser(user);
        assertTrue( returned.getLastLogin().equals( lastLogin));
    }

    @Test
    public void testAddAndRemoveUser() throws Exception {

        User user = new User();

        user = (User) populate(user);

        Role role = roleManager.getRole(Constants.USER_ROLE);

        user.addRole(role);

        user = userManager.saveUser(user);

        assertTrue(user.getUsername().equals("john"));

        assertTrue(user.getRoles().size() == 1);

        final Long primaryId = user.getId();

        userManager.remove( primaryId ) ;

        assertThrows(ObjectRetrievalFailureException.class, () -> userManager.get( primaryId));


    }

    @Test
    public void testUserExistsException() {
        // set expectations
        final User user = new User("admin");
        user.setEmail("matt@raibledesigns.com");

        final Exception ex = new DataIntegrityViolationException("");

        // run test
        try {
            userManager.saveUser(user);
            fail("Expected UserExistsException not thrown");
        } catch (UserExistsException e) {
            log.debug("expected exception: " + e.getMessage());
            assertNotNull(e);
        }
    }

    @After
    public void onTearDown() {

    }
}
