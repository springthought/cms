package com.springthought.service.impl;

import com.springthought.Constants;
import com.springthought.dao.UserDao;
import com.springthought.model.Permission;
import com.springthought.model.Role;
import com.springthought.model.User;
import com.springthought.service.PasswordEncryptor;
import com.springthought.service.UserManager;
import com.springthought.service.UserSecurityAdvice;
import org.jetbrains.annotations.NotNull;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import java.util.LinkedHashSet;

import static org.junit.Assert.*;

@RunWith(JMock.class)
public class UserSecurityAdviceTest {


    Mockery context = new JUnit4Mockery();
    UserDao userDao = null;
    ApplicationContext ctx = null;
    SecurityContext initialSecurityContext = null;

    @Before
    public void setUp() throws Exception {
        // store initial security context for later restoration
        initialSecurityContext = SecurityContextHolder.getContext();

        SecurityContext context = new SecurityContextImpl();
        User user = new User("user");
        user.setId(1L);
        user.setTenantId(1L);
        user.setPassword("password");

        user.addRole(initiateRole(Constants.ADMIN_ROLE));

        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities());

        token.setDetails(user);
        context.setAuthentication(token);
        SecurityContextHolder.setContext(context);
    }


    @After
    public void tearDown() {
        SecurityContextHolder.setContext(initialSecurityContext);
    }

    @Test
    public void testAddUserWithoutAdminRole() throws Exception {

        resetCurrentUser(Constants.USER_ROLE);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        assertTrue(auth.isAuthenticated());


        UserManager userManager = makeInterceptedTarget();
        User user = new User("admin");
        user.setId(2L);
        user.setTenantId(1L);

        try {
            userManager.saveUser(user);
            fail("AccessDeniedException not thrown");
        } catch (AccessDeniedException expected) {
            assertNotNull(expected);
            Assert.assertEquals(expected.getMessage(), UserSecurityAdvice.ACCESS_DENIED);
        }
    }


    @Test
    public void testAddUserAsAdmin() throws Exception {

       /* SecurityContext securityContext = new SecurityContextImpl();

        User user = new User("admin");
        user.setId(2L);
        user.setTenantId(1L);
        user.setPassword("password");
        user.addRole(initiateRole((Constants.ADMIN_ROLE)));


        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities());
        token.setDetails(user);
        securityContext.setAuthentication(token);
        SecurityContextHolder.setContext(securityContext);*/

        resetCurrentUser(Constants.ADMIN_ROLE);

        UserManager userManager = makeInterceptedTarget();

        final User adminUser = new User("admin");
        adminUser.setId(2L);

        context.checking(new Expectations() {{
            one(userDao).saveUser(with(same(adminUser)));
        }});

        userManager.saveUser(adminUser);
    }

    @Test
    public void testUpdateUserProfile() throws Exception {

        UserManager userManager = makeInterceptedTarget();
        final User user = new User("user");
        user.setId(1L);
        user.setTenantId(1L);
        user.getRoles().add(initiateRole(Constants.USER_ROLE));


        context.checking(new Expectations() {{
            one(userDao).saveUser(with(same(user)));
        }});

        userManager.saveUser(user);
    }



    // Test fix to http://issues.appfuse.org/browse/APF-96
    @Test
    public void testAddAdminRoleWhenAlreadyHasUserRole() throws Exception {

        resetCurrentUser(Constants.USER_ROLE);

        UserManager userManager = makeInterceptedTarget();
        User user = new User("user");
        user.setId(1L);
        user.setTenantId(1L);
        user.getRoles().add(initiateRole(Constants.ADMIN_ROLE));
        user.getRoles().add(initiateRole(Constants.USER_ROLE));

        try {
            userManager.saveUser(user);
            fail("AccessDeniedException not thrown");
        } catch (AccessDeniedException expected) {
            assertNotNull(expected);
            assertEquals(expected.getMessage(), UserSecurityAdvice.ACCESS_DENIED);
        }
    }

    // Test fix to http://issues.appfuse.org/browse/APF-96
    @Test
     public void testAddUserRoleWhenHasAdminRole() throws Exception {
        SecurityContext securityContext = new SecurityContextImpl();
        User user1 = new User("user");
        user1.setId(1L);
        user1.setTenantId(1L);
        user1.setPassword("password");
        user1.addRole(initiateRole(Constants.ADMIN_ROLE));

        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(user1.getUsername(), user1.getPassword(), user1.getAuthorities());
        token.setDetails(user1);
        securityContext.setAuthentication(token);
        SecurityContextHolder.setContext(securityContext);

        UserManager userManager = makeInterceptedTarget();

        final User user = new User("user");
        user.setId(1L);
        user.setTenantId(1L);

        user.getRoles().add(initiateRole(Constants.ADMIN_ROLE));

        user.getRoles().add(initiateRole(Constants.USER_ROLE));

        context.checking(new Expectations() {{
            one(userDao).saveUser(with(same(user)));
        }});

        userManager.saveUser(user);
    }

    // Test fix to http://issues.appfuse.org/browse/APF-96
    @Test
    public void testUpdateUserWithUserRole() throws Exception {
        UserManager userManager = makeInterceptedTarget();
        final User user = new User("user");
        user.setId(1L);
        user.setTenantId(1L);


        user.getRoles().add(initiateRole(Constants.USER_ROLE));

        context.checking(new Expectations() {{
            one(userDao).saveUser(with(same(user)));
        }});

        userManager.saveUser(user);
    }

    private UserManager makeInterceptedTarget() {
        ctx = new ClassPathXmlApplicationContext("/applicationContext-test.xml");

        UserManager userManager = (UserManager) ctx.getBean("target");
        PasswordEncryptor passwordEncryptor = (PasswordEncryptor) ctx.getBean("passwordEncryptor");

        // Mock the userDao
        userDao = context.mock(UserDao.class);
        userManager.setUserDao(userDao);

        passwordEncryptor.setUserDao(userDao);
        userManager.setPasswordEncryptor(passwordEncryptor);

        return userManager;
    }

    @NotNull
    private Role initiateRole(String roleProfile) {
        Role createdRole = new Role(roleProfile);
        LinkedHashSet permission = new LinkedHashSet<Permission>();
        permission.add(new Permission(roleProfile));
        createdRole.setPermissions(permission);
        return createdRole;
    }

    @NotNull
    private void resetCurrentUser(String userRole) {

        SecurityContext context = new SecurityContextImpl();
        User currentUser = new User("user");
        currentUser.setId(1L);
        currentUser.setTenantId(1L);
        currentUser.setPassword("password");

        currentUser.addRole(initiateRole(userRole));

        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(currentUser.getUsername(), currentUser.getPassword(), currentUser.getAuthorities());

        token.setDetails(currentUser);
        context.setAuthentication(token);
        SecurityContextHolder.setContext(context);

    }
}
