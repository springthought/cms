package com.springthought.service.impl;


import com.springthought.dao.GenericDao;
import com.springthought.dao.hibernate.GenericDaoHibernate;
import com.springthought.model.Person;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-resources.xml", "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml", "classpath*:/**/applicationContext.xml", "classpath:/applicationContext-security.xml"
})
public class CSVFileImporterImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    GenericDao<Person, Long> genericPersonDao;
    File file;
    @Autowired
    SessionFactory sessionFactory;
    private String FILE_NAME = "people_import_template.csv";

    public static boolean isGetter(Method method) {

        if (!method.getName().startsWith("get")) return false;

        if (method.getParameterTypes().length != 0) return false;

        if (void.class.equals(method.getReturnType())) return false;

        return true;
    }

    public static boolean isSetter(Method method) {

        if (!method.getName().startsWith("set")) return false;

        if (method.getParameterTypes().length != 1) {
            return false;
        } else {
            final Class<?>[] parameterTypes = method.getParameterTypes();
            if (!parameterTypes[0].getName().equals("java.lang.String") && !parameterTypes[0].getName().equals("java.util.Date") && !parameterTypes[0].getName().equals("java.lang.Boolean") && !parameterTypes[0].getName().equals("java.lang.Short") && !parameterTypes[0].getName().equals("java.lang.Long"))
                return false;
        }


        return true;
    }

    @Before
    public void setUp() throws Exception {

        genericPersonDao = new GenericDaoHibernate<Person, Long>(Person.class,
                sessionFactory);

        file = new File(CSVFileImporterImplTest.class.getClassLoader().getResource(FILE_NAME).toURI());

    }

    @Test
    public void exportPersonCSVFileWithColums() throws IOException {

        Assert.assertEquals(Boolean.TRUE,Boolean.TRUE);

/*        ArrayList<String> objectFields = new ArrayList<>(0);

        Person person = new Person();

        Method[] allMethods = person.getClass().getDeclaredMethods();


        for (Method method : allMethods) {
            if (Modifier.isPublic(method.getModifiers())) {
                if (isSetter(method)) {
                    objectFields.add(method.getName().substring(3).toLowerCase());
                }
            }
        }


        List<Person> people = genericPersonDao.getAll();

        objectFields.sort(Comparator.naturalOrder());

        String[] columns = new String[objectFields.size()];

        columns = objectFields.toArray(columns);


        CsvWriter.CsvWriterDSL<Person> writerDsl =
                CsvWriter.from(Person.class).columns(columns).quote('"');


        try (FileWriter fileWriter = new FileWriter(file)) {

            CsvWriter<Person> writer =
                    writerDsl.to(fileWriter);

            if (people.size() > 0) {
                people.forEach(CheckedConsumer.toConsumer(writer::append));
            }

        }*/
    }

    @After
    public void tearDown() throws Exception {
        /*if (!sessionFactory.isClosed()) {
            sessionFactory.close();
        }
        genericPersonDao = null;*/
    }
}
