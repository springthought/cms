package com.springthought.service.impl;

import com.springthought.Constants.UserValueType;
import com.springthought.dao.UserValueDao;
import com.springthought.model.UserValue;
import org.jmock.Expectations;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class UserValueManagerImplTest extends BaseManagerMockTestCase {
	UserValueDao userValueDao;
	UserValueManagerImpl userValueManagerImpl;

	@Before
	public void setUp() throws Exception {
		userValueDao = context.mock(UserValueDao.class);
		userValueManagerImpl = new UserValueManagerImpl(userValueDao);
	}

	@Test
	public void getUserValueByType() throws Exception {

		final List<UserValue> testData = new ArrayList<UserValue>();

		testData.add(new UserValue());

		context.checking(new Expectations() {
			{
				one(userValueDao).getUserValueByType(
						with(same(UserValueType.CLASSIFICATION)));
				will(returnValue(testData));
			}
		}); 

		List<UserValue> uv = userValueManagerImpl
				.getUserValueByType(UserValueType.CLASSIFICATION);

		assertTrue(uv.size() > 0);
	}

}
