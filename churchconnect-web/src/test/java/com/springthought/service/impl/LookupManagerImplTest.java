package com.springthought.service.impl;

import com.springthought.Constants;
import com.springthought.model.ILabelValue;
import com.springthought.model.LabelValue;
import com.springthought.service.BaseManagerTestCase;
import com.springthought.service.LookupManager;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class LookupManagerImplTest extends BaseManagerTestCase {

    @Autowired
    private LookupManager mgr;

    //private LookupDao lookupDao;

    @Before
    public void setUp() throws Exception {
        //lookupDao = context.mock(LookupDao.class);
        //mgr.dao = lookupDao;
    }

    @Test
    public void testGetAllRoles() {

        List<LabelValue> roles = mgr.getAllRoles();
        assertTrue(roles.size() > 40);
    }


    @Test
    public void getSystemValueByValue() {

        ILabelValue labelValue = mgr.getSystemValueByValue(Constants.CodeListType.STAGEOFLIFE, "KG", true);
        assertTrue(labelValue.getLabel().equals("Kindergarten"));

        ILabelValue labelValue1 = mgr.getSystemValueByValue(Constants.CodeListType.STAGEOFLIFE, "123", true);
        assertNull(labelValue1);

    }


    @Test
    public void getSystemValueByLabel() {
        ILabelValue labelValue = mgr.getSystemValueByLabel(Constants.CodeListType.STAGEOFLIFE, "Kindergarten", true);
        assertTrue(labelValue.getLabel().equals("Kindergarten"));

        ILabelValue labelValue1 = mgr.getSystemValueByLabel(Constants.CodeListType.STAGEOFLIFE, "first grade", true);
        assertNull(labelValue1);
    }
}
