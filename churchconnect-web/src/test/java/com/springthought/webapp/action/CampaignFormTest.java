package com.springthought.webapp.action;

import com.springthought.model.Campaign;
import com.springthought.model.Category;
import com.springthought.model.Fund;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class CampaignFormTest extends BasePageMockitoTestCase {
    private CampaignForm bean;
    @Autowired
    private GenericManager<Campaign, Long> campaignManager;

    @Autowired
    private GenericManager<Fund, Long> fundManager;

    @Autowired
    private GenericManager<Category, Long> categoryManager;

    @Autowired
    DataService dataService;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean =  (CampaignForm) applicationContext.getBean("campaignForm");
        bean.setCampaignManager(campaignManager);
        bean.setDataService(dataService);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {

        List<Category> categoryFunds =dataService.getCategoryFunds();
        Optional<Category> firstCategoryFund = categoryFunds.stream().findFirst();
        assertEquals( firstCategoryFund.isPresent(), true);
        Category categoryFund = firstCategoryFund.get();

        Campaign campaign = new Campaign();
        campaign.setGoalAmt(new BigDecimal(3.30));
        campaign.setName("MuEzSbDnYwOhLaFxUrFcKmUePzMzChBjHzLsVnEfKyMhLhTpCe");
        campaign.setTenantId(-1L);
        campaign.setCategory(categoryFund);

        bean.setCampaign(campaign);
        assertEquals("campaigns", bean.save());
        assertFalse(bean.hasErrors());

    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setCampaignId(-1L);

        assertEquals("campaignForm", bean.edit());
        assertNotNull(bean.getCampaign());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setCampaignId(-1L);

        List<Category> categoryFunds =dataService.getCategoryFunds();
        Optional<Category> firstCategoryFund = categoryFunds.stream().findFirst();
        assertEquals( firstCategoryFund.isPresent(), true);
        Category categoryFund = firstCategoryFund.get();

        assertEquals("campaignForm", bean.edit());
        assertNotNull(bean.getCampaign());
        Campaign campaign = bean.getCampaign();

        // update required fields
        campaign.setGoalAmt(new BigDecimal("2.76"));
        campaign.setName("JeUhXuHcRjVxQeNzTaRvUfJlBzMeOaGlEzVvUwKqAtTuFqWoUw");
        campaign.setTenantId(-1L);
        campaign.setCategory(categoryFund);
        bean.setCampaign(campaign);

        assertEquals("campaigns", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Campaign campaign = new Campaign();
        campaign.setCampaignId(-2L);
        bean.setCampaign(campaign);

        assertEquals("campaigns", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
