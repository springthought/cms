package com.springthought.webapp.action;

import com.springthought.model.Batch;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BatchListTest extends BasePageMockitoTestCase {
    private BatchList bean;
    @Autowired
    private GenericManager<Batch, Long> batchManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new BatchList();
        bean.setBatchManager(batchManager);

        // add a test batch to the database
        Batch batch = new Batch();

        // enter all required fields
        batch.setBatchDate(new java.util.Date());
        batch.setDateReceived(new java.util.Date());
        batch.setName( UUID.randomUUID().toString());

        batchManager.save(batch);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllBatches() throws Exception {
        assertTrue(bean.getBatches().size() >= 1);
        assertFalse(bean.hasErrors());
    }


}
