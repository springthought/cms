package com.springthought.webapp.action;

import com.springthought.model.Campaign;
import com.springthought.model.Category;
import com.springthought.model.Fund;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CampaignListTest extends BasePageMockitoTestCase {
    private CampaignList bean;
    @Autowired
    private GenericManager<Campaign, Long> campaignManager;


    @Autowired
    private GenericManager<Fund, Long> fundManager;

    @Autowired
    private GenericManager<Category, Long> categoryManager;


    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new CampaignList();
        bean.setCampaignManager(campaignManager);

        Category category = categoryManager.get(-1L);
        // add a test campaign to the database
        Campaign campaign = new Campaign();

        // enter all required fields
        campaign.setGoalAmt(new BigDecimal("5.69"));
        campaign.setName("EzQeMiOaNjRoEqTwCdGdMsDzAyVqPvSfSpSdPlPoOoSrWsMhGx");
        campaign.setTenantId(1L);
        campaign.setCategory(category);

        campaignManager.save(campaign);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllCampaigns() throws Exception {
        assertTrue(bean.getCampaigns().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        campaignManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
