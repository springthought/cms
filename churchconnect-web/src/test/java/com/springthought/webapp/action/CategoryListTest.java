package com.springthought.webapp.action;

import com.springthought.Constants.CategoryType;
import com.springthought.model.Category;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class CategoryListTest extends BasePageMockitoTestCase {
    private CategoryList bean;
    @Autowired
    private GenericManager<Category, Long> categoryManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new CategoryList();
        bean.setCategoryManager(categoryManager);

        // add a test category to the database
        Category category = new Category();
        category.setName("Test2");
        category.setCategoryType(CategoryType.TALENT );
        // enter all required fields

        categoryManager.save(category);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllCategories() throws Exception {
        assertTrue(bean.getCategories().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        categoryManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
