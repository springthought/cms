package com.springthought.webapp.action;

import com.springthought.model.GroupRole;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class GroupRoleFormTest extends BasePageMockitoTestCase {
    private GroupRoleForm bean;
    @Autowired
    private GenericManager<GroupRole, Long> groupRoleManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new GroupRoleForm();
        bean.setGroupRoleManager(groupRoleManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        GroupRole groupRole = new GroupRole();

        // enter all required fields
        groupRole.setRoleName("RfJpNkAhGfZxOwCmLrGtYdJsE");
        bean.setGroupRole(groupRole);

        assertEquals("groupRoles", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setGrouproleId(-1L);

        assertEquals("groupRoleForm", bean.edit());
        assertNotNull(bean.getGroupRole());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setGrouproleId(-1L);

        assertEquals("groupRoleForm", bean.edit());
        assertNotNull(bean.getGroupRole());
        GroupRole groupRole = bean.getGroupRole();

        // update required fields
        groupRole.setRoleName("LqDpKtBrMuWfVwImAxEkIfOtQ");
        bean.setGroupRole(groupRole);

        assertEquals("groupRoles", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        GroupRole groupRole = new GroupRole();
        groupRole.setGrouproleId(-2L);
        bean.setGroupRole(groupRole);

        assertEquals("groupRoles", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
