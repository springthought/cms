package com.springthought.webapp.action;

import com.springthought.model.EventType;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class EventTypeListTest extends BasePageMockitoTestCase {
    private EventTypeList bean;
    @Autowired
    private GenericManager<EventType, Long> eventTypeManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new EventTypeList();
        bean.setEventTypeManager(eventTypeManager);

        // add a test eventType to the database
        EventType eventType = new EventType();

        // enter all required fields
        eventType.setName("BiXuYuPwUqXvFkLiUsVuFmQfGeIeLmJhJjCcAtOkQhEkOmJqTk");
        eventType.setTenantId(1L);

        eventTypeManager.save(eventType);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllEventTypes() throws Exception {
        assertTrue(bean.getEventTypes().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        eventTypeManager.reindex();

        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
