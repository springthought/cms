package com.springthought.webapp.action;

import com.springthought.model.Fund;
import com.springthought.model.Pledge;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class PledgeFormTest extends BasePageMockitoTestCase {
    private PledgeForm bean;
    @Autowired
    private GenericManager<Pledge, Long> pledgeManager;

    @Autowired
    private GenericManager<Fund, Long> fundManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        Mockito.when(requestMock.getParameter("campaignId")).thenReturn("-1");

        bean = (PledgeForm) applicationContext.getBean("pledgeForm");
        bean.setPledgeManager(pledgeManager);

    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Pledge pledge = new Pledge();
        // enter all required fields
        pledge.setPledgeAmt(new BigDecimal("50.00"));
        bean.setPledge(pledge);

        assertEquals("campaigns", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setPledgeId(-1L);
        assertEquals("pledgeForm", bean.edit());
        assertNotNull(bean.getPledge());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setPledgeId(-1L);

        assertEquals("pledgeForm", bean.edit());
        assertNotNull(bean.getPledge());
        Pledge pledge = bean.getPledge();
        // update required fields
        pledge.setPledgeAmt(new BigDecimal("150.00"));
        bean.setPledge(pledge);

        assertEquals("campaigns", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Pledge pledge = new Pledge();
        pledge.setPledgeId(-1L);
        bean.setSelectedPledge(pledge);
        assertEquals("pledges", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
