package com.springthought.webapp.action;

import com.springthought.model.EventType;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class EventTypeFormTest extends BasePageMockitoTestCase {
    private EventTypeForm bean;
    @Autowired
    private GenericManager<EventType, Long> eventTypeManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new EventTypeForm();
        bean.setEventTypeManager(eventTypeManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        EventType eventType = new EventType();

        // enter all required fields
        eventType.setName("BrJsNxSgQwGeYeOoFpCxFbClQoZkNbYhJtFwRvLcAbHsFfHaPy");
        eventType.setTenantId(1L);
        bean.setEventType(eventType);

        assertEquals("eventTypes", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setEventTypeId(-1L);

        assertEquals("eventTypeForm", bean.edit());
        assertNotNull(bean.getEventType());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setEventTypeId(-1L);

        assertEquals("eventTypeForm", bean.edit());
        assertNotNull(bean.getEventType());
        EventType eventType = bean.getEventType();

        // update required fields
        eventType.setName("ZvQwLuNnOxXdFwPgOsZaHmEkAiNxBmYdVmCiZpTvAwQcKxSbFn");
        eventType.setTenantId(1L);
        bean.setEventType(eventType);

        assertEquals("eventTypes", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        EventType eventType = new EventType();
        eventType.setEventTypeId(-2L);
        bean.setEventType(eventType);

        assertEquals("eventTypes", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
