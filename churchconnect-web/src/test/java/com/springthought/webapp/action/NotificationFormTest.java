package com.springthought.webapp.action;

import com.springthought.model.Notification;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class NotificationFormTest extends BasePageMockitoTestCase {
    private NotificationForm bean;
    @Autowired
    private GenericManager<Notification, Long> notificationManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        Mockito.when( requestMock.getParameter("notificationId")).thenReturn("-1");
        bean =  (NotificationForm) applicationContext.getBean("notificationForm");
        bean.setNotificationManager(notificationManager);

    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Notification notification = new Notification();

        // enter all required fields
        notification.setCompleteDate(new java.util.Date());
        notification.setName("GpNsXzVoAoLsLbPgZuVjXdRpUhXhLfXzTuYiXhRxDfTpWcTeHc");
        notification.setTenantId(1L);
        bean.setNotification(notification);

        assertEquals("notifications", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setNotificationId(-1L);
        assertEquals("notificationForm", bean.edit());
        assertNotNull(bean.getNotification());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setNotificationId(-1L);

        assertEquals("notificationForm", bean.edit());
        assertNotNull(bean.getNotification());
        Notification notification = bean.getNotification();

        // update required fields
        notification.setCompleteDate(new java.util.Date());
        notification.setName("BfXbSdCbDnVbDsKmTaMeXtHdZqDrWnMaDxTmSkCkHwZcSjBxCn");
        notification.setTenantId(9L);
        bean.setNotification(notification);

        assertEquals("notifications", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Notification notification = new Notification();
        notification.setNotificationId(-2L);
        bean.setNotification(notification);

        assertEquals("notifications", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
