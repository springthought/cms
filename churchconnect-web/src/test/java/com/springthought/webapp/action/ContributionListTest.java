package com.springthought.webapp.action;

import com.springthought.model.Batch;
import com.springthought.model.Category;
import com.springthought.model.Contribution;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class ContributionListTest extends BasePageMockitoTestCase {
    private ContributionList bean;
    @Autowired
    private GenericManager<Contribution, Long> contributionManager;


    @Autowired
    private GenericManager<Batch, Long> batchManager;

    @Autowired
    private GenericManager<Category, Long> categoryManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = (ContributionList) applicationContext.getBean("contributionList");
        bean.setContributionManager(contributionManager);

    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllContributions() throws Exception {
        assertTrue(bean.getContributions().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        contributionManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
