package com.springthought.webapp.action;

import com.springthought.model.Message;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class MessageListTest extends BasePageMockitoTestCase {
    private MessageList bean;
    @Autowired
    private GenericManager<Message, Long> messageManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = (MessageList) applicationContext.getBean("messageList");
        bean.setMessageManager(messageManager);

    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllMessages() throws Exception {
        assertTrue(bean.getMessages().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        messageManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
