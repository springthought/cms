package com.springthought.webapp.action;

import com.springthought.model.Address;
import com.springthought.model.Person;
import com.springthought.model.Tenant;
import com.springthought.model.User;
import com.springthought.service.MailEngine;
import com.springthought.service.RoleManager;
import com.springthought.service.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.mail.SimpleMailMessage;
import org.subethamail.wiser.Wiser;

import javax.faces.event.ActionEvent;

import static org.junit.Assert.assertFalse;

public class SignupFormTest extends BasePageMockitoTestCase {
    private SignupForm bean;


    @Mock
    private ActionEvent actionEventMock;

    @Override
    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = (SignupForm) applicationContext.getBean("signupForm");
        bean.setUserManager((UserManager) applicationContext.getBean("userManager"));
        bean.setRoleManager((RoleManager) applicationContext.getBean("roleManager"));
        bean.setMessage((SimpleMailMessage) applicationContext.getBean("mailMessage"));
        bean.setMailEngine((MailEngine) applicationContext.getBean("mailEngine"));
        bean.setTemplateName("accountCreated.vm");
        bean.setMailEngine(mailEngineMock);
    }

    @Test
    public void testExecute() throws Exception {

        Tenant tenant = new Tenant();

        tenant.setCity("KmEaZlJoLwZzFyPgWtNePxGqOjHhQnOxPwCwGfNuXoFxWqMlGe");
        tenant.setName("OtLxLcZgMtRjHkFwBgQbAdVhPfOeHtRrKeZnVzHdPmGgExBlMv");
        tenant.setPhoneNumber("DwRgOpYkPgFqYdWoBoDcWeKdNmNbIlRxVzRuBjLmPwBuZnJaFjBtKcFyGzJxUmDpPkUaStTkWdGqOnWkOqGtFeQpSpZyFxZaSqTuSqIjCiZaIaWeIxFhBvKjCqXjAzVsAqVlJgYuCwRcEyYlMbLgNdOnXxQrCsLkEvTnWvMgYdRsQoBjUzEsYfTaSuIoDgJaFtUcHyYvLaErRwSrVzWsJuDmDlCwSgAzKpGxSqSrRaNmVeWsNrMhBlCgErZbGyS");
        tenant.setPostalCode("LgFyHsEgStJlKiZ");

        Person person = new Person();

        person.setFirstName("MkSuMdPlLnFfKpKyJrAcTeEtQdCbLwZjDyXsHkJsTpOjUkXaRg");
        person.setLastName("StSiXvMlUeQfOpRtHoCuGwZbIdNkIeCbNwGpUjFqTpGhXySuEe");
        person.setRelationship("Rq");
        person.setEmail("QfOpRtHo@qfq.com");

        User user = new User("self-registered");
        user.setPassword("Password1");
        user.setConfirmPassword("Password1");
        user.setFirstName("First");
        user.setLastName("Last");
        user.setTenantId(1L);


        Address address = new Address();
        address.setCity("Denver");
        address.setProvince("CO");
        address.setCountry("USA");
        address.setPostalCode("80210");

        user.setEmail("self-registered@raibledesigns.com");
        user.setWebsite("http://raibledesigns.com");
        user.setPasswordHint("Password is one with you.");

        bean.setTenant(tenant);
        bean.setPerson(person);
        bean.setUser(user);

        // start SMTP Server
        Wiser wiser = new Wiser();
        wiser.setPort(getSmtpPort());
        wiser.start();

        bean.signUp( actionEventMock);

        assertFalse(bean.hasErrors());

        // verify an account information e-mail was sent
//        wiser.stop();
//        assertEquals("No email message found", 1, wiser.getMessages().size());

        // verify that success messages are in the session
     /*   assertNotNull(bean.getSession().getAttribute(Constants.REGISTERED));
        SecurityContextHolder.getContext().setAuthentication(null);*/
    }
}
