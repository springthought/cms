package com.springthought.webapp.action;

import com.springthought.model.Tenant;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class TenantFormTest extends BasePageMockitoTestCase {
	private TenantForm bean;
	@Autowired
	private GenericManager<Tenant, Long> tenantManager;

	@Before
	public void onSetUp() {
		super.onSetUp();
		Mockito.when(requestMock.getParameter("tenantId")).thenReturn("-1");
		bean = (TenantForm)applicationContext.getBean("tenantForm");
		bean.setTenantManager(tenantManager);
	}

	@After
	public void onTearDown() {
		super.onTearDown();
		bean = null;
	}

	@Test
	public void testAdd() throws Exception {
		Tenant tenant = new Tenant();

		// enter all required fields
		tenant.setCity("KmEaZlJoLwZzFyPgWtNePxGqOjHhQnOxPwCwGfNuXoFxWqMlGe");
		tenant.setName("OtLxLcZgMtRjHkFwBgQbAdVhPfOeHtRrKeZnVzHdPmGgExBlMv");
		tenant.setPhoneNumber("DwRgOpYkPgFqYdWoBoDcWeKdNmNbIlRxVzRuBjLmPwBuZnJaFjBtKcFyGzJxUmDpPkUaStTkWdGqOnWkOqGtFeQpSpZyFxZaSqTuSqIjCiZaIaWeIxFhBvKjCqXjAzVsAqVlJgYuCwRcEyYlMbLgNdOnXxQrCsLkEvTnWvMgYdRsQoBjUzEsYfTaSuIoDgJaFtUcHyYvLaErRwSrVzWsJuDmDlCwSgAzKpGxSqSrRaNmVeWsNrMhBlCgErZbGyS");
		tenant.setPostalCode("LgFyHsEgStJlKiZ");
		bean.setTenant(tenant);

		assertEquals("tenants", bean.save());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testEdit() throws Exception {
		log.debug("testing edit...");
		bean.setTenantId(-1L);
		assertEquals("tenantForm", bean.edit());
		assertNotNull(bean.getTenant());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testSave() {
		log.debug("testing save...");
		bean.setTenantId(-1L);
		assertEquals("tenantForm", bean.edit());
		assertNotNull(bean.getTenant());
		Tenant tenant = bean.getTenant();

		// update required fields
		tenant.setCity("IgSvMpXpLwLrXsTrKgTfDuEuPuEtYmSfHfGcNsKzVuGcEgXvBx");
		tenant.setName("AnVmBtRxZsDcNrJxAdCbOoMfJqUfRrBrCzVgTvLtKbLtKnLfKr");
		tenant.setPhoneNumber("RqMhZiGhLkTuAeZiRoLuNpWsCvIiGlBlShRkPpSrNrLnQbViIfXeUeYoTyDdTuQqMcFwNrMuNmBtXzRgJnLoRcUxNgHyFzFfNfHeObHnKfZyTyEnJrZvGjSqBhWgCbMdWuHeFlBjRgOmQoOrOmSpVfElCyWdOoQwCmPjRbLcFxWaNbCoReQkUaShDlVfZuCeCpDkEhMaTwYoDzOuBmQkOsJfGbIdUkVjPfTsWsRiKmEcMsYjBeQbYhKgLoJgRmU");
		tenant.setPostalCode("JfYgNpGfSlCtGkR");
		bean.setTenant(tenant);

		assertEquals("tenants", bean.save());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testRemove() throws Exception {
		log.debug("testing remove...");
		Tenant tenant = new Tenant();
		tenant.setTenantId(-1L);
		bean.setTenant(tenant);
		assertEquals("tenants", bean.delete());
		assertFalse(bean.hasErrors());
	}
}
