package com.springthought.webapp.action;

import com.springthought.model.Batch;
import com.springthought.model.Category;
import com.springthought.model.Contribution;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ContributionFormTest extends BasePageMockitoTestCase {
    private ContributionForm bean;
    @Autowired
    private GenericManager<Contribution, Long> contributionManager;

    @Autowired
    private GenericManager<Batch, Long> batchManager;


    @Autowired
    private GenericManager<Category, Long> categoryManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        Mockito.when(requestMock.getParameter("batchId")).thenReturn("-1");
        bean = (ContributionForm) applicationContext.getBean("contributionForm");
        bean.setContributionManager(contributionManager);
        bean.setBatchManager(batchManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Contribution contribution = new Contribution();

        Batch batch = batchManager.get(-1L);
        Category category = categoryManager.get(-1L);
        contribution.setBatch( batch );

        // enter all required fields
        contribution.setAmount(new BigDecimal(1500));
        contribution.setEnvelopeNumber(new Integer("17612"));
        contribution.setCategory(category);
        bean.setSelectedContribution(contribution);

        assertEquals("batches", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setContributionId(-1L);

       // assertEquals("contributionForm", bean.edit());
       // assertNotNull(bean.getContribution());
       // assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setContributionId(-1L);

       // assertEquals("contributionForm", bean.edit());
       // assertNotNull(bean.getContribution());
       Contribution contribution = bean.getEntryContribution();
       Category category = categoryManager.get(-2L);

        // update required fields

        Batch batch = batchManager.get(-1L);
        contribution.setBatch( batch );
        contribution.setAmount(new BigDecimal("50.20"));
        contribution.setEnvelopeNumber(new Integer("20498"));
        contribution.setCategory(category);

        bean.setSelectedContribution(contribution);

        assertEquals("batches", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Contribution contribution = new Contribution();
        contribution.setContributionId(-2L);
        bean.setSelectedContribution(contribution);

        assertEquals("contributions", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
