package com.springthought.webapp.action;

import com.springthought.model.Category;
import com.springthought.model.Group;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class GroupFormTest extends BasePageMockitoTestCase {
    private GroupForm bean;
    @Autowired
    private GenericManager<Group, Long> groupManager;
    @Autowired
    private GenericManager<Category, Long> categoryManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new GroupForm();
        bean.setGroupManager(groupManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
     public void testAdd() throws Exception {
        Group group = new Group();

        // enter all required fields
        group.setName("AsZeDiGqXtFdWiJrBlFuFtPiQxCoYoKrBdHpUfAtEhZbI");

        Category category = categoryManager.get(-1L);

        group.setCategory(category);

        bean.setGroup(group);

        assertEquals("groups", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setGroupId(-1L);

        assertEquals("groupForm", bean.edit());
        assertNotNull(bean.getGroup());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");

        Category category = categoryManager.get(-1L);

        bean.setGroupId(-1L);

        assertEquals("groupForm", bean.edit());
        assertNotNull(bean.getGroup());
        Group group = bean.getGroup();

        // update required fields
        group.setName("CyUwTfNjAfUlWxJwWgNtSdRxPvSnYrRxMfGwBoNvXnMqF");
        group.setCategory(category);
        bean.setGroup(group);

        assertEquals("groups", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Group group = new Group();
        group.setGroupId(-2L);
        bean.setGroup(group);

        assertEquals("groups", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
