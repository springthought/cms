package com.springthought.webapp.action;

import com.springthought.model.Role;
import com.springthought.service.GenericManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class RoleFormTest extends BasePageMockitoTestCase {
    private RoleForm bean;
    @Autowired
    private GenericManager<Role, Long> roleManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        Mockito.when(requestMock.getParameter("id")).thenReturn("-1");
        bean = (RoleForm) applicationContext.getBean("roleForm");
        bean.setRoleManager(roleManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Role role = new Role();

        // enter all required fields
        role.setTenantId(Long.parseLong("-1"));
        role.setLabel(RandomStringUtils.random(20,true,false));
        bean.setRole(role);

        assertEquals("roles", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setId(-1L);
        assertEquals("roleForm", bean.edit());
        assertNotNull(bean.getRole());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setId(-1L);
        assertEquals("roleForm", bean.edit());
        assertNotNull(bean.getRole());
        Role role = bean.getRole();

        // update required fields
        role.setTenantId(Long.parseLong("-1"));
        bean.setRole(role);

        assertEquals("roles", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Role role = new Role();
        role.setId(-2L);
        bean.setRole(role);

        assertEquals("roles", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
