package com.springthought.webapp.action;

import com.springthought.model.Resource;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class ResourceListTest extends BasePageMockitoTestCase {
    private ResourceList bean;
    @Autowired
    private GenericManager<Resource, Long> resourceManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = (ResourceList) applicationContext.getBean("resourceList");
        bean.setResourceManager(resourceManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllResources() throws Exception {
        assertTrue(bean.getResources().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        resourceManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
