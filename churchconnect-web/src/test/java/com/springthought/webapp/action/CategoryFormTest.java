package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.Constants.CategoryType;
import com.springthought.model.Category;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class CategoryFormTest extends BasePageMockitoTestCase {
    private CategoryForm bean;
    @Autowired
    private GenericManager<Category, Long> categoryManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new CategoryForm();
        bean.setCategoryManager(categoryManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Category category = new Category();

        category.setName("test");
        category.setCategoryType(Constants.CategoryType.GIFT);

        // enter all required fields
        bean.setCategory(category);

        assertEquals("categories", bean.save());
        assertFalse(bean.hasErrors());

        Category cat2 = bean.getCategory();

        assertEquals("test", cat2.getName());
    }

    @Test
    public void testEdit() throws Exception {

    	log.debug("testing edit...");
        bean.setCategoryId(-1L);

        assertEquals("categoryForm", bean.edit());
        assertNotNull(bean.getCategory());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setCategoryId(-1L);

        assertEquals("categoryForm", bean.edit());
        assertNotNull(bean.getCategory());
        Category category = bean.getCategory();
        category.setCategoryType(CategoryType.TALENT);

        // update required fields
        bean.setCategory(category);


        assertEquals("categories", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Category category = new Category();
        category.setCategoryId(-2L);
        category.setCategoryType(CategoryType.TALENT);
        bean.setCategory(category);

        assertEquals("categories", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
