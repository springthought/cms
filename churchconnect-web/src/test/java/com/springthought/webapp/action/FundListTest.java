package com.springthought.webapp.action;

import com.springthought.Constants.FundType;
import com.springthought.model.Fund;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class FundListTest extends BasePageMockitoTestCase {
    private FundList bean;
    @Autowired
    private GenericManager<Fund, Long> fundManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new FundList();
        bean.setFundManager(fundManager);

        // add a test fund to the database
        Fund fund = new Fund();

        // enter all required fields
        fund.setAcctnum("HgYpWgSbQbEfPpHcBzJqWgBzUdWjKcGpRxXuJmAbGvAyH");
        fund.setFundtype(FundType.TAXABLE);
        fund.setName("HsVmGtFnOqDeTfJcZoMzPtMaA");
        fund.setTenantId(7L);

        fundManager.save(fund);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllFunds() throws Exception {
        assertTrue(bean.getFunds().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        fundManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
