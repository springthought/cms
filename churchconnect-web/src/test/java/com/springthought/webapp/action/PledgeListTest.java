package com.springthought.webapp.action;

import com.springthought.model.Fund;
import com.springthought.model.Pledge;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class PledgeListTest extends BasePageMockitoTestCase {
    private PledgeList bean;
    @Autowired
    private GenericManager<Pledge, Long> pledgeManager;

    @Autowired
    private GenericManager<Fund, Long> fundManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = (PledgeList) applicationContext.getBean("pledgeList");
        bean.setPledgeManager(pledgeManager);


    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllPledges() throws Exception {
        assertTrue(bean.getPledges().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        pledgeManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
