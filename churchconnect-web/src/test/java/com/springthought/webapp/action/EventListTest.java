package com.springthought.webapp.action;

import com.springthought.model.Event;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;


public class EventListTest extends BasePageMockitoTestCase {


    private EventList bean;
    @Autowired
    private GenericManager<Event, Long> eventManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = (EventList) applicationContext.getBean("eventList");
        bean.setEventManager(eventManager);

        // add a test event to the database
        Event event = new Event();

        // enter all required fields
        event.setAllDay(Boolean.FALSE);
        event.setEditable(Boolean.FALSE);
        event.setName("RiUkEvMeBqEiLzGiOhGkRvVvMpSyRxYrWxWqNmXyDjWpXrPuHk");
        event.setPublicEvent(Boolean.FALSE);
        event.setReminder(2051888866);
        event.setTenantId(1L);


        eventManager.save(event);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllEvents() {
        assertTrue(bean.getEvents().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() {
        // regenerate indexes
        eventManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
