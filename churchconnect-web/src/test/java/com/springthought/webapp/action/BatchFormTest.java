package com.springthought.webapp.action;

import com.springthought.model.Batch;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.*;

public class BatchFormTest extends BasePageMockitoTestCase {
    private BatchForm bean;
    @Autowired
    private GenericManager<Batch, Long> batchManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new BatchForm();
        bean.setBatchManager(batchManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Batch batch = new Batch();

        // enter all required fields
        batch.setBatchDate(new java.util.Date());
        batch.setDateReceived(new java.util.Date());
        batch.setName(UUID.randomUUID().toString());
        bean.setBatch(batch);

        assertEquals("batches", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setBatchId(-1L);

        assertEquals("batchForm", bean.edit());
        assertNotNull(bean.getBatch());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setBatchId(-1L);

        assertEquals("batchForm", bean.edit());
        assertNotNull(bean.getBatch());
        Batch batch = bean.getBatch();

        // update required fields
        batch.setBatchDate(new java.util.Date());
        batch.setDateReceived(new java.util.Date());
        batch.setName(UUID.randomUUID().toString());
        bean.setBatch(batch);

        assertEquals("batches", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Batch batch = new Batch();
        batch.setBatchId(-2L);
        bean.setBatch(batch);

        assertEquals("batches", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
