package com.springthought.webapp.action;

import com.springthought.Constants.CustomFieldType;
import com.springthought.model.CustomMeta;
import com.springthought.service.GenericManager;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class CustomMetaFormTest extends BasePageMockitoTestCase {
    private CustomMetaForm bean;
    @Autowired
    private GenericManager<CustomMeta, Long> customMetaManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new CustomMetaForm();
        bean.setCustomMetaManager(customMetaManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        CustomMeta customMeta = new CustomMeta();

        // enter all required fields
        customMeta.setDataType("Pz");
        customMeta.setTenantId(1L);
        customMeta.setCustomFieldType(CustomFieldType.PERSON);
        customMeta.setDataType("TX");
        customMeta.setName("Add");
        bean.setCustomMeta(customMeta);

        assertEquals("customMetas", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setCustomMetaId(-1L);

        assertEquals("customMetaForm", bean.edit());
        assertNotNull(bean.getCustomMeta());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setCustomMetaId(-1L);

        assertEquals("customMetaForm", bean.edit());
        assertNotNull(bean.getCustomMeta());
        CustomMeta customMeta = bean.getCustomMeta();

        // update required fields
        customMeta.setDataType("Rh");
        customMeta.setTenantId(1L);
        customMeta.setCustomFieldType(CustomFieldType.PERSON);
        customMeta.setName(java.util.UUID.randomUUID().toString());
        bean.setCustomMeta(customMeta);

        assertEquals("customMetas", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        CustomMeta customMeta = new CustomMeta();
        customMeta.setCustomMetaId(-2L);
        bean.setCustomMeta(customMeta);

        assertEquals("customMetas", bean.delete());
        assertFalse(bean.hasErrors());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testDuplicateName() throws Exception {
        CustomMeta customMeta = new CustomMeta();

        // enter all required fields
        customMeta.setDataType("Pz");
        customMeta.setTenantId(1L);
        customMeta.setCustomFieldType(CustomFieldType.PERSON);
        customMeta.setName("Age");
        customMeta.setDataType("TX");
        bean.setCustomMeta(customMeta);
        bean.save();
        //assertEquals("customMetas", bean.save());
        //assertFalse(bean.hasErrors());

    }



}
