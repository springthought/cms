package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.Household;
import com.springthought.service.HouseholdManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class HouseholdFormTest extends BasePageMockitoTestCase {
    private HouseholdForm bean;
    @Autowired
    private HouseholdManager householdManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new HouseholdForm();
        bean.setHouseholdManager(householdManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Household household = new Household();
        // enter all required fields
        household.setEmail("QsYsShYiP@test.com");
        household.setName("DeYvBeYuPmSqUvXsIbBxNoMiQiJkTiEcCvZnCaFfAmDhVhNaEv");
        household.setStatusDate(new java.util.Date());
        household.setStatus(Constants.FamilyStatusType.ACTIVE);


        bean.setHousehold(household);

        assertEquals("households", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setHouseholdId(-1L);

        assertEquals("householdForm", bean.edit());
        assertNotNull(bean.getHousehold());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setHouseholdId(-1L);

        assertEquals("householdForm", bean.edit());
        assertNotNull(bean.getHousehold());

        Household household = bean.getHousehold();

        // update required fields
        household.setEmail("AxXqSjEpEcIcLvYvHdHvCeLqEcNrCaZqJcDkOpJfTtBcOtJtXrHzYvScEcTmYxFxPoKmLpOoPnDcJsFuAaUhPgYwPqUfRrHuDgXf");
        household.setName("DdChHyOvMxJwXdWyCqXcLqWiTjQhZwNcCcWzWjHuLjQcFbMvJr");
        household.setStatusDate(new java.util.Date());
        household.setStatus(Constants.FamilyStatusType.ACTIVE);
        bean.setHousehold(household);

        assertEquals("households", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Household household = new Household();
        household.setHouseholdId(-3L);
        household.setTenantId(1L);
        bean.setHousehold(household);

        assertEquals("households", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
