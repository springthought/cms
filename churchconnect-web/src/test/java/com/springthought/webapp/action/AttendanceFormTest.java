package com.springthought.webapp.action;


import com.springthought.model.Attendance;
import com.springthought.model.Event;
import com.springthought.model.EventType;
import com.springthought.model.Person;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class AttendanceFormTest extends BasePageMockitoTestCase {

    private AttendanceForm bean;

    @Autowired
    private GenericManager<Attendance, Long> attendanceManager;

    @Autowired
    private GenericManager<Event, Long> eventManager;

    @Autowired
    private GenericManager<Person, Long> personManager;

    @Autowired
    private GenericManager<EventType, Long> eventTypeManager;

    @Autowired
    private DataService dataService;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new AttendanceForm();
        bean.setAttendanceManager(attendanceManager);
        bean.setEventManager(eventManager);
        bean.setPersonManager(personManager);
        bean.setEventTypeManager(eventTypeManager);
        bean.setDataService(dataService);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {

        Attendance attendance = new Attendance();

        Event event = new Event();
        event.setReminder(636204731);
        event.setName("LkJsUjBkKoNvWzNdHwNzYdAkHtJqA");
        event = eventManager.save(event);
        event.setAttendance(attendance);


        attendance.setName("GhDuUmKkJsUjBkKoNvWzNdHwNzYdAkHtJqAcYzLnFxLoFxKsAh");
        attendance.setGeneralCount(100);
        attendance.setEvent(event);


        bean.setSelectedEventOccurrence(event);
        bean.setAttendance(attendance);

        assertEquals("attendances", bean.save());

        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setAttendanceId(-1L);

        assertEquals("attendanceForm", bean.edit());
        assertNotNull(bean.getAttendance());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setAttendanceId(-1L);
        bean.initialization();

        assertEquals("attendanceForm", bean.edit());

        assertNotNull(bean.getAttendance());

        Attendance attendance = bean.getAttendance();
        bean.setPriorSelectedEventOccurrence(eventManager.get(-3L));

        // update required fields
        attendance.setName("EbPzIqAiDeMgCbKoGoJkHcEdTgIbIhWdHlQtMqLmGkOmRrJnBh");
        attendance.setEvent(bean.getPriorSelectedEventOccurrence());
        bean.setAttendance(attendance);

        assertEquals("attendances", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Attendance attendance = new Attendance();
        attendance.setAttendanceId(-2L);
        bean.setAttendance(attendance);

        assertEquals("attendances", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
