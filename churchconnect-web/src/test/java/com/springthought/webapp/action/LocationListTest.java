package com.springthought.webapp.action;

import com.springthought.model.Location;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class LocationListTest extends BasePageMockitoTestCase {
    private LocationList bean;
    @Autowired
    private GenericManager<Location, Long> roomManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean =(LocationList) applicationContext.getBean("locationList");
        bean.setLocationManager(roomManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllRooms() throws Exception {
        assertTrue(bean.getLocations().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        roomManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
