package com.springthought.webapp.action;

import com.springthought.model.Person;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class PersonListTest extends BasePageMockitoTestCase {
    private PersonList bean;
    @Autowired
    private GenericManager<Person, Long> personManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = (PersonList) applicationContext.getBean("personList");
        bean.setPersonManager(personManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllPersons() throws Exception {
        assertTrue(bean.getPersons().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        personManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
