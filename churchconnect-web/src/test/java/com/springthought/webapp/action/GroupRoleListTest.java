package com.springthought.webapp.action;

import com.springthought.model.GroupRole;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class GroupRoleListTest extends BasePageMockitoTestCase {
    private GroupRoleList bean;
    @Autowired
    private GenericManager<GroupRole, Long> groupRoleManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = (GroupRoleList)applicationContext.getBean("groupRoleList");
        bean.setGroupRoleManager(groupRoleManager);
        // add a test groupRole to the database
        GroupRole groupRole = new GroupRole();
        // enter all required fields
        groupRole.setRoleName("LgZdHyXjYbBdLeEfXmHqQyRlS");
        groupRoleManager.save(groupRole);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllGroupRoles() throws Exception {
        assertTrue(bean.getGroupRoles().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
