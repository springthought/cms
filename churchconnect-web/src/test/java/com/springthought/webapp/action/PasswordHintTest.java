package com.springthought.webapp.action;

import com.springthought.service.MailEngine;
import com.springthought.service.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mail.SimpleMailMessage;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.event.ActionEvent;

import static org.junit.Assert.assertFalse;

public class PasswordHintTest extends BasePageMockitoTestCase {
    private PasswordHint bean;

    @Override
    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = (PasswordHint) applicationContext.getBean("passwordHint");

        bean.setUserManager((UserManager) applicationContext
                .getBean("userManager"));
        bean.setMessage((SimpleMailMessage) applicationContext
                .getBean("mailMessage"));
        bean.setMailEngine((MailEngine) applicationContext
                .getBean("mailEngine"));
        bean.setTemplateName("accountCreated.vm");
        bean.setMailEngine(mailEngineMock);

    }

    @Test
    public void testExecute() throws Exception {
        // start SMTP Server

        ActionEvent actionEventMock = Mockito.mock(ActionEvent.class);

        Mockito.when(actionEventMock.getSource()).thenReturn(Mockito.mock(HtmlCommandButton.class));

	/*	Wiser wiser = new Wiser();
		wiser.setHostname("localhost");
		wiser.setPort(getSmtpPort());
		wiser.start();
*/

        bean.setUsername(currentUser.getUsername());
        bean.execute(actionEventMock);

        assertFalse(bean.hasErrors());

        // verify an account information e-mail was sent
//		wiser.stop();
//		assertTrue(wiser.getMessages().size() == 1);
        // verify that success messages are in the request
        //assertNotNull(bean.getSession().getAttribute("messages"));
    }
}
