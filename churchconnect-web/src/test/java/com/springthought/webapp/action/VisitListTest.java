package com.springthought.webapp.action;

import com.springthought.model.Visit;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class VisitListTest extends BasePageMockitoTestCase {
    private VisitList bean;
    @Autowired
    private GenericManager<Visit, Long> visitManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = (VisitList) applicationContext.getBean("visitList");
        bean.setVisitManager(visitManager);

    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllVisits() throws Exception {
        assertTrue(bean.getVisits().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        visitManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
