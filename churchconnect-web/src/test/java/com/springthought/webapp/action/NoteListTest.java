package com.springthought.webapp.action;

import com.springthought.model.Note;
import com.springthought.model.Person;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.*;

public class NoteListTest extends BasePageMockitoTestCase {
    private NoteList bean;
    @Autowired
    private GenericManager<Note, Long> noteManager;

    @Autowired
    private GenericManager<Person, Long> personManager;

    @Before
    public void onSetUp() {
        super.onSetUp();


        bean = new NoteList();

        Person person = personManager.get(-1L);

        PersonForm personForm = new PersonForm();
        personForm.setPerson( person );

        bean.setNoteManager( noteManager );
        bean.setPersonForm( personForm );

        // add a test note to the database
        Note note = new Note();

        // enter all required fields
        note.setNotes("DwHgBvHeFgAlSyWgAqRdPyMoSjTrZxFaBdLnMyBkRcMjLnNoJrBtGeAlXzFwYqEkTvShUsWyEsYrHmTbXsQjJaOiQxPhKzKrPjKcZvRzVqVaIaDsTdNtHfUqLqNuAqDoBlZzUtWeOmLnJfXsLeZoPeJiIpLcHqUqRlVoOwDfJcRzLiZsFmUxTvUfKhVrMmVpJjIfMwFoAgTmAtEtZdAgSrMhIeRdMoKhYoFmYnPsQlMcFuWzPqZkGxCpLnMjRtVtUjCwFmInDyQzCzVdAkIbWeHeYtYhVeSjYdKtInNuAcNpNqBmDiRcRfKiGnNvQkYqHoJqJxPeNaZiCmJcMuHuAaSeSbSxFuHwDdKqQcFjWhObMpSgPqWlLaSfPiBbAcVfWyRcPsEhZbRnRvDdApYfCtTnGzZxWgIaDrXqNnPyZlPgYcRrJcHiJlZxPgEbJqUkUvNkWdPcRzBhOeVvXyObIiHmSbZmKfNcVgZjOlTnMpNzIsBcTnIwDsVfWmJpEjFjRbHhSoKgHoPyTmKxIgGtCsNeRoSfBnNmOhVlMfRkCxQgZtXxXbNrIwCqPeIgDnMdOpObIvZvDwQeTcNxOmRoJhYpNgTxTsSjGlGnHlViGwLvJkYkFsJiMxRnUeQfIuJpIzCiUpWaJnBePzGgCwShSaRjKoTlKkUwQaMzJkYaJxIkAgCqIuUpMfZvJeGfEnZmNlFcCaTgDgWiFgVdVxCgMfIuOdFrXpOtGlSaSiMmWrCjXuGjJhXeMfQdKhIpVgCdUoRmLuXwRhTiJoFlBdBiXoOqIcCkJeImUpVnZuNnJmPvKrVaZiSqElHkCiVxGdCoWsEtKvAiOpBwYkRnXiYpMxUdMoMjIjXaDgCxVhEbXrPkPdMnRkSlXeBcEjEbHfLfVfDhXmRvTnBqZqTiHyAlThVxDcHwVzWuNmSaWoLqYcHdLmVdUbMqYuIxXyUdXaMzFyGfOdCjWbYkAxLcBnYlWsNqFzSyNaQoVcNgYxTsTnWoTnZqWzMyLbIrErZjJtYzCsEqSmWqGgGnCxJiIfOdKfKkHqGfZvTkLzQbHrRdIyJhPuFvNsOoUtWlWmKxGxTpJpFjTwMxEsOhYyAvKxMdSnLdYcYqUvVnUnFgOrNnSaGiEcSuVwUeOxXfWdVvWmLkWlXvAxZwZvInEgPkCzTvBiOvWmKrOoYeDbYrEqCtCqYpHzErLnWrYfGvFoScCpFxNeUaEtTrGjHoKkRxYlNlSuJhAgEyIlOrBhVeJmFcZaSeEzMlRtBhQoGcHjZhUsCoLhAkHlBtWvGgLuWcCnTxCtVeVpOgOwZcHoSoHuMeHlDwYbGcRxQjCgKlZcNrVcDrEeWtZmTrKtTuKxImTwUrAkVsEaInIvDlFkDkHfYoPkVjPeShQfQfTpYzXtYbHgMiUoQfObXgZfSoYwQkCdZtWoAuVrCrMgBiRyUaAxPcFeDdDmDjXwNgEgShJdGrPjPeCaFbGqMeQhAgGuGtRjNnOyFuHqImVjVzLeGjNyGlLeSfVvQiDtTpAnOmShJbRpNnTeOpHfYdKhHmLcIyKiWsYwGlGmSnFdYfCmSxLqBgPgGbIdTnOqPeNxEyLwAaDpQbLhQeDtApNiKxMcFyRyQeSkPvNpAbTrDdKcOoIzDwIhBsUwFvWrOrHlRjHqDpKuKxYnWbMrHwMpIbNfIyRrBzWwYfHrPaZsAhJgYpLeJiOoVnBjJjSwHfTeHqJuRvWyZkNlZfQfTsHzOhYpNlMbTfOiZaGyFvYwQiKyQoItZiXzDzXhClPhBnFkApNhHrBlUdVkJmMtSpPcApNhBcDyHwJvWpUvLhLcGfMaNdOjCzOqHvYbIoEqLqXhEfGmUaVtIuDzCeTjElVeMpSuYpWyAtQwBiDdQpEgYuGuJcHvOwSiUvDdRcJqCwJdGeYhCqLwRxPgHeEbQjIzKlQmWrHyMnGxYkLoIeTfHxJiJeBuOwLpDsPnHnLjGvIvKdEuQuTnGdHkWtNkVyOrLsGyRvMkHjEpJrBcAnBpWhJxXyFyDbDpLcZgEzHtMdCfEgOlBlKvJs");
        note.setName(UUID.randomUUID().toString());
        noteManager.save(note);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllNotes() throws Exception {
        assertTrue(bean.getNotes().size() >1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        noteManager.reindex();

        bean.setQuery("*");
        assertEquals("success", bean.search());
        assertTrue(bean.getNotes().size() > 1 );
    }
}
