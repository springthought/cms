package com.springthought.webapp.action;

import com.springthought.model.Attendance;
import com.springthought.model.Event;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AttendanceListTest extends BasePageMockitoTestCase {
    private AttendanceList bean;
    @Autowired
    private GenericManager<Attendance, Long> attendanceManager;

    @Autowired
    private GenericManager<Event, Long> eventManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new AttendanceList();
        bean.setAttendanceManager(attendanceManager);

        // add a test attendance to the database
        Attendance attendance = new Attendance();

        Event event = eventManager.get(-2L);

        // enter all required fields
        attendance.setName("IhHkRxXjCqOgEsAdRuJvAoWkGqWkSgRyRaZzUsMwFpWtMqXnGd");
        attendance.setEvent(event);
        attendanceManager.save(attendance);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllAttendances() throws Exception {
        assertTrue(bean.getAttendances().size() >= 1);
        assertFalse(bean.hasErrors());
    }


}
