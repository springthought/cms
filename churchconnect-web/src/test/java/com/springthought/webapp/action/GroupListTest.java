package com.springthought.webapp.action;

import com.springthought.model.Category;
import com.springthought.model.Group;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class GroupListTest extends BasePageMockitoTestCase {
	private CommuncationForm bean;
	@Autowired
	private GenericManager<Group, Long> groupManager;

	@Autowired
	private GenericManager<Category, Long> categoryManager;

	@Before
	public void onSetUp() {
		super.onSetUp();

		bean = new CommuncationForm();
		bean.setGroupManager(groupManager);
		Category category = categoryManager.get(-1L);
		// add a test group to the database
		Group group = new Group();
		// enter all required fields
		group.setName("UyBtFiXnGoUfLgDkAiShKjQwNtTrAuClOeNpCbYnKqRzE");
		group.setCategory(category);

		groupManager.save(group);
	}

	@After
	public void onTearDown() {
		super.onTearDown();
		bean = null;
	}

	@Test
	public void testGetAllGroups() throws Exception {
		assertTrue(bean.getGroups().size() >= 1);
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testSearch() throws Exception {
		// regenerate indexes
		groupManager.reindex();
		bean.setQuery("*");
		assertEquals("success", bean.search());
		assertTrue( bean.getGroups().size() >=4 );
	}
}
