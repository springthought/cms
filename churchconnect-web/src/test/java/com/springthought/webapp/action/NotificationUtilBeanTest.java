/**
 *
 */
package com.springthought.webapp.action;

import com.springthought.model.Notification;
import com.springthought.model.Person;
import com.springthought.model.User;
import com.springthought.service.GenericManager;
import com.springthought.service.UserManager;
import org.hibernate.Query;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;


/**
 * @author eairrick
 *
 */
public class NotificationUtilBeanTest extends BasePageMockitoTestCase {

    private NotificationUtilBean bean;
    @Autowired
    private GenericManager<Person, Long> personManager;
    @Autowired
    private GenericManager<Notification, Long> notificationManager;
    @Autowired
    private UserManager userManager;

    @Mock
    private User userMock;

    //@Autowired
    //SecurityContextManager securityContextManager;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

        super.onSetUp();
        bean = (NotificationUtilBean)applicationContext.getBean("notificationUtilBean");
        bean.setPersonManager(personManager);
        bean.setUserManager(userManager);


    }

    @Test
    public void testNativeQuery() {


        Session session = personManager.getSessionFactory().openSession();

        String hql = "update Person set birthDate = :birthdate where personId = :personid";
        Query query = session.createQuery(hql);


        //Priority.MEDIUM
        query.setDate("birthdate", (new DateTime()).toDate());
        query.setLong("personid", -1L);

        assertEquals(1, query.executeUpdate());


        //Priority.HIGH

        query.setDate("birthdate", (((new DateTime()).minusDays(3)).toDate()));
        query.setLong("personid", -2);

        assertEquals(1, query.executeUpdate());


        //Priority.LOW
        query.setDate("birthdate", (((new DateTime()).plusDays(3)).toDate()));
        query.setLong("personid", -3);

        assertEquals(1, query.executeUpdate());


        if (session.isOpen()) {
            session.close();
        }


    }


    /**
     * Test method for
     * {@link com.springthought.webapp.action.NotificationUtilBean#processDates(java.lang.Long)}
     * .
     */
    @Test
    public void testProcessDates() throws Exception {

        ArrayList<Notification> notifications = bean.processDates(this.currentUser);
        assertEquals("Notification not sent, total # : " + notifications.size(), true, notifications.size() >= 1);



    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        super.onTearDown();
        bean = null;
    }

}
