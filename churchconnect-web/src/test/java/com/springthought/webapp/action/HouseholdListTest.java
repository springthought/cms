package com.springthought.webapp.action;

import com.springthought.model.Household;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class HouseholdListTest extends BasePageMockitoTestCase {
    private HouseholdList bean;
    @Autowired
    private GenericManager<Household, Long> householdManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = (HouseholdList) applicationContext.getBean("householdList");
        bean.setHouseholdManager(householdManager);

     /*   // add a test household to the database
        Household household = new Household();
        // enter all required fields
        household.setEmail("WcCyJyWlOfMpEnClEgIzPiOvJwStZdPiYoZsUaImPaSiQmZyLqKhAmWbTiHoEnCdHnTbNzIjEaBwOxWgLfHnTzWwKyTjCpHwVqWx");
        household.setName("KlWhWtXcKfIcNdEqGjPaFoXpWkNpWmIjNzAkQoCuPvPxVeYbPs");
        household.setStatusDate(new java.util.Date());

        householdManager.save(household);*/
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllHouseholds() throws Exception {
        assertTrue(bean.getHouseholds().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        householdManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
