package com.springthought.webapp.action;

import com.springthought.Constants.FundType;
import com.springthought.model.Fund;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class FundFormTest extends BasePageMockitoTestCase {

    private FundForm bean;

    @Autowired
    private GenericManager<Fund, Long> fundManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        Mockito.when(requestMock.getParameter("fundId")).thenReturn("-L");
        bean =  (FundForm) applicationContext.getBean("fundForm");
        bean.setFundManager(fundManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Fund fund = new Fund();
        // enter all required fields
        fund.setAcctnum("GyEiKsGhPuYmWaRzUnAtNtIaFoGoYpNsFfEeKfAqKaEhU");
        fund.setFundtype( FundType.TAXABLE);
        fund.setName("FbQoTtWxTfImQqDuGyGwYjWkX");
        fund.setTenantId(1L);
        bean.setFund(fund);
        assertEquals("funds", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setFundId(-1L);
        assertEquals("fundForm", bean.edit());
        assertNotNull(bean.getFund());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setFundId(-1L);
        assertEquals("fundForm", bean.edit());
        assertNotNull(bean.getFund());
        Fund fund = bean.getFund();

        // update required fields
        fund.setAcctnum("FzYyVtMeFwMdGaDqDyEqRzSlNfGhHiRuSvUzXpOuOeUbW");
        fund.setFundtype(FundType.IRA);
        fund.setName("BoNaUjBtDiZaUfWkUvZcQaJbX");
        fund.setTenantId(1L);
        bean.setFund(fund);
        assertEquals("funds", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Fund fund = new Fund();
        fund.setFundId(-1L);
        bean.setFund(fund);
        assertEquals("funds", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
