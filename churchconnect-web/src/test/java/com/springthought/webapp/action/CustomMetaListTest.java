package com.springthought.webapp.action;

import com.springthought.Constants.CustomFieldType;
import com.springthought.model.CustomMeta;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class CustomMetaListTest extends BasePageMockitoTestCase {
    private CustomMetaList bean;
    @Autowired
    private GenericManager<CustomMeta, Long> customMetaManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new CustomMetaList();
        bean.setCustomMetaManager(customMetaManager);

        // add a test customMeta to the database
        CustomMeta customMeta = new CustomMeta();

        // enter all required fields
        customMeta.setDataType("Vq");
        customMeta.setTenantId(1L);
        customMeta.setCustomFieldType(CustomFieldType.PERSON);
        customMeta.setName(java.util.UUID.randomUUID().toString());

        log.debug(CustomFieldType.PERSON);

        customMetaManager.save(customMeta);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllCustomMetas() throws Exception {
        assertTrue(bean.getCustomMetas().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        customMetaManager.reindex();

        bean.setQuery("*");
        assertEquals("success", bean.search());
        assertEquals(6, bean.getCustomMetas().size());
    }
}
