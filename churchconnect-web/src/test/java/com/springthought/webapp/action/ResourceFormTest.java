package com.springthought.webapp.action;

import com.springthought.Constants.ResourceType;
import com.springthought.model.Resource;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class ResourceFormTest extends BasePageMockitoTestCase {
	private ResourceForm bean;
	@Autowired
	private GenericManager<Resource, Long> resourceManager;

	@Before
	public void onSetUp() {
		super.onSetUp();
		bean = new ResourceForm();
		bean.setResourceManager(resourceManager);
	}

	@After
	public void onTearDown() {
		super.onTearDown();
		bean = null;
	}

	@Test
    public void testAdd() throws Exception {
        Resource resource = new Resource();

        // enter all required fields
        resource.setName("JeVkJfAiKmHxAnSvNmBvLuVpS");
        resource.setResourceType( ResourceType.TABLE);
        resource.setTenantId(1L);
        bean.setResource(resource);

        assertEquals("resources", bean.save());
        assertFalse(bean.hasErrors());
    }

	@Test
	public void testEdit() throws Exception {
		log.debug("testing edit...");
		bean.setResourceId(-1L);

		assertEquals("resourceForm", bean.edit());
		assertNotNull(bean.getResource());
		assertFalse(bean.hasErrors());
	}

	@Test
    public void testSave() {
        log.debug("testing save...");
        bean.setResourceId(-1L);

        assertEquals("resourceForm", bean.edit());
        assertNotNull(bean.getResource());
        Resource resource = bean.getResource();

        // update required fields
        resource.setName("GqMfThLaTlFyQcPhKkJwQnHsC");
        resource.setResourceType(ResourceType.TABLE);
        resource.setTenantId(1L);
        bean.setResource(resource);

        assertEquals("resources", bean.save());
        assertFalse(bean.hasErrors());
    }

	@Test
	public void testRemove() throws Exception {
		log.debug("testing remove...");
		Resource resource = new Resource();
		resource.setResourceId(-2L);
		bean.setResource(resource);

		assertEquals("resources", bean.delete());
		assertFalse(bean.hasErrors());
	}
}
