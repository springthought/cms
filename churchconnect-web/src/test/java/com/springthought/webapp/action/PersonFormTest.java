package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.*;
import com.springthought.service.GenericManager;
import com.springthought.service.LookupManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class PersonFormTest extends BasePageMockitoTestCase {
	private PersonForm bean;
	@Autowired
	private GenericManager<Person, Long> personManager;
	@Autowired
	private GenericManager<Household, Long> householdManager;
	@Autowired
	private LookupManager lookupManager;
	@Autowired
	private GenericManager<CustomMeta, Long> customMetaManager;

	@Before
	public void onSetUp() {
		super.onSetUp();
		Mockito.when(requestMock.getParameter("personId")).thenReturn("-1");
		bean = (PersonForm) applicationContext.getBean("personForm");
		bean.setPersonManager(personManager);
		bean.setHouseholdManager(householdManager);
		bean.setLookupManager(lookupManager);
		bean.setCustomMetaManager(customMetaManager);
	}

	@After
	public void onTearDown() {
		super.onTearDown();
		bean = null;
	}

	@Test
	public void testAdd() throws Exception {
		Person person = new Person();

		PersonBin bin = new PersonBin();
		person.setHousehold(new Household());

		//person.setPersonId(0L);

		CustomMeta customMeta = customMetaManager.get(-3L);

		CustomField customField = new CustomField();
		customField.setPerson( person );
		customField.setCustomFieldId(0L);
		customField.setCustomMeta(customMeta);
		customField.setTextValue(customMeta.getDefaultValue());
		customField.setUID( customMeta.getUID() );
		person.getCustomFields().add(customField);

		// enter all required fields
		person.setFirstName("MkSuMdPlLnFfKpKyJrAcTeEtQdCbLwZjDyXsHkJsTpOjUkXaRg");
		person.setLastName("StSiXvMlUeQfOpRtHoCuGwZbIdNkIeCbNwGpUjFqTpGhXySuEe");
		person.setRelationship("Rq");
		person.setEmail("QfOpRtHo@qfq.com");
		person.setPersonBin(bin);
		bin.setPerson(person);

		person.getHousehold().setEmail("QsYsShYiP@wGp.com");
		person.getHousehold().setName(
				"DeYvBeYsIbBxNoMiQiJkTiEcCvZnCaFfAmDhVhNaEv");
		person.getHousehold().setStatusDate(new java.util.Date());
		person.getHousehold().setStatus(Constants.FamilyStatusType.ACTIVE);


		bean.setPerson(person);

		assertEquals("persons", bean.save());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testEdit() throws Exception {
		log.debug("testing edit...");
		bean.setPersonId(-1L);
		assertEquals("personForm", bean.edit());
		assertNotNull(bean.getPerson());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testSave() {

		log.debug("testing save...");
		bean.setPersonId(-1L);

		assertEquals("personForm", bean.edit());
		assertNotNull(bean.getPerson());

		Person person = bean.getPerson();

		// update required fields
		person.setFirstName("BfArCjHkCxZdCgZqVqSkLeSuMhZoMyGzWpUdMnQqPqMmMwWwBd");
		person.setLastName("GmYzKoPeOiCnWiUgNcHuCnYvVhGnIcSePtGzBsGzOpGvEbZmQx");
		person.setEmail("kdueudj@uek.com");
		person.setRelationship("Gt");

		bean.setPerson(person);

		assertEquals("persons", bean.save());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testRemove() throws Exception {
		log.debug("testing remove...");
		Person person = new Person();
		person.setPersonId(-2L);
		bean.setPerson(person);

		assertEquals("persons", bean.delete());
		assertFalse(bean.hasErrors());
	}
}
