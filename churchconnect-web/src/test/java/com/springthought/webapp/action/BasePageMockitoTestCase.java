package com.springthought.webapp.action;

import com.springthought.Constants;
import com.springthought.model.User;
import com.springthought.service.MailEngine;
import com.springthought.service.UserManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.omnifaces.util.Faces;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.context.PrimeRequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.ApplicationFactory;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.render.RenderKit;
import javax.faces.render.RenderKitFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * <p>
 * Abstract JUnit test case base class, which sets up the JavaServer Faces mock
 * object environment for a particular simulated request. The following
 * protected variables are initialized in the <code>setUp()</code> method, and
 * cleaned up in the <code>tearDown()</code> method:
 * </p>
 * <ul>
 * <li><code>application</code> (<code>MockApplication</code>)</li>
 * <li><code>config</code> (<code>MockServletConfig</code>)</li>
 * <li><code>externalContext</code> (<code>MockExternalContext</code>)</li>
 * <li><code>facesContext</code> (<code>MockFacesContext</code>)</li>
 * <li><code>lifecycle</code> (<code>MockLifecycle</code>)</li>
 * <li><code>request</code> (<code>MockHttpServletRequest</code></li>
 * <li><code>response</code> (<code>MockHttpServletResponse</code>)</li>
 * <li><code>servletContext</code> (<code>MockServletContext</code>)</li>
 * <li><code>session</code> (<code>MockHttpSession</code>)</li>
 * </ul>
 * <p/>
 * <p>
 * In addition, appropriate factory classes will have been registered with
 * <code>javax.faces.FactoryFinder</code> for <code>Application</code> and
 * <code>RenderKit</code> instances. The created <code>FacesContext</code>
 * instance will also have been registered in the apppriate thread local
 * variable, to simulate what a servlet container would do.
 * </p>
 * <p/>
 * <p>
 * <strong>WARNING</strong> - If you choose to subclass this class, be sure your
 * <code>onSetUp()</code> and <code>onTearDown()</code> methods call
 * <code>super.setUp()</code> and <code>super.tearDown()</code> respectively,
 * and that you implement your own <code>suite()</code> method that exposes the
 * test methods for your test case.
 * </p>
 * <p/>
 * <p>
 * <strong>NOTE:</strong> This class is a copy of Shale's AbstractJsfTestCase,
 * except it extends Spring's AbstractTransactionalDataSourceSpringContextTests
 * instead of JUnit's TestCase.
 * </p>
 */


@ContextConfiguration(locations = {
        "classpath:/applicationContext-resources.xml",
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml",
        "classpath*:/applicationContext-security.xml",
        "classpath*:/applicationContext.xml",
        "classpath:**/applicationContext*.xml"})
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore({ "javax.net.ssl.*", "javax.security.*" })
@PrepareForTest(FactoryFinder.class)
public abstract class BasePageMockitoTestCase extends
        AbstractTransactionalJUnit4SpringContextTests {


    private static int smtpPort = 25250;
    protected final Log log = LogFactory.getLog(getClass());

    /* protected MockServletConfig config = null;
     protected MockExternalContext externalContext = null;
     protected MockFacesContext facesContext = null;
     protected MockFacesContextFactory facesContextFactory = null;
     protected MockLifecycle lifecycle = null;
     protected MockLifecycleFactory lifecycleFactory = null;
     protected MockRenderKit renderKit = null;
    */
    //protected Application application = null;

    protected User currentUser = null;
    @Mock
    protected HttpServletRequest requestMock;
    @Mock
    protected HttpServletResponse responseMock;
    @Mock
    protected ServletContext servletContextMock;
    @Mock
    protected FacesContext facesContextMock;
    @Mock
    protected ExternalContext externalContextMock;
    @Mock
    protected Flash flashMock;
    @Mock
    protected ApplicationFactory applicationFactoryMock;
    @Mock
    protected Application applicationMock;
    @Mock
    protected RenderKit renderKitMock;
    @Mock
    protected RenderKitFactory renderKitFactoryMock;
    @Mock
    protected PrimeRequestContext primeRequestContextMock;
    @Mock
    protected MailEngine mailEngineMock;
    @Mock
    ServletContext configMock;
    /*	protected MockHttpServletRequest request = null;
        protected MockHttpServletResponse response = null;
        protected MockServletContext servletContext = null;
        protected MockHttpSession session = null; */
    @Mock
    HttpSession sessionMock;
    @Autowired
    UserManager userManager;
    // Thread context class loader saved and restored after each test
    private ClassLoader threadContextClassLoader = null;

    /**
     * <p>
     * Set up instance variables required by this test case.
     * </p>
     */
    @Before
    public void onSetUp() {

        MockitoAnnotations.openMocks(this);
        PowerMockito.mockStatic(FactoryFinder.class);


        Faces.setContext(facesContextMock);

        Mockito.when(facesContextMock.getExternalContext()).thenReturn(externalContextMock);
        Mockito.when(externalContextMock.getFlash()).thenReturn(flashMock);
        Mockito.when(externalContextMock.getRequest()).thenReturn(requestMock);
        Mockito.when(externalContextMock.getSession(false)).thenReturn(sessionMock);
        Mockito.when(requestMock.getServletContext()).thenReturn(servletContextMock);
        Mockito.when(requestMock.getLocale()).thenReturn(new Locale("en"));
        Mockito.when(requestMock.getSession()).thenReturn(sessionMock);

        Mockito.when(facesContextMock.getApplication()).thenReturn(applicationMock);

        Mockito.doNothing().when(mailEngineMock).send(Mockito.isA(SimpleMailMessage.class));



        smtpPort = smtpPort + (int) (Math.random() * 100);

        // Set up a new thread context class loader
        threadContextClassLoader = Thread.currentThread()
                .getContextClassLoader();

        Thread.currentThread()
                .setContextClassLoader(
                        new URLClassLoader(new URL[0], this.getClass()
                                .getClassLoader()));

        setCurrentUser();


        Mockito.when(requestMock.getServletContext()).thenReturn(servletContextMock);
        Mockito.when(sessionMock.getServletContext()).thenReturn(servletContextMock);
        Mockito.when(requestMock.getLocale()).thenReturn(new Locale("en"));
        Mockito.when(requestMock.getServerPort()).thenReturn(8080);
        Mockito.when(requestMock.getScheme()).thenReturn("http");
        Mockito.when(requestMock.getServerName()).thenReturn("localhost");
        Mockito.when(requestMock.getContextPath()).thenReturn("/connectchms");

        //request = new MockHttpServletRequest(session);

        //request.setServletContext(servletContext);
        //request.setLocale(new Locale("en"));
        //response = new MockHttpServletResponse();


        // Set up JSF API Objects
       /* FactoryFinder.releaseFactories();

        FactoryFinder.setFactory(FactoryFinder.APPLICATION_FACTORY,
                "org.apache.shale.test.mock.MockApplicationFactory");
        FactoryFinder.setFactory(FactoryFinder.FACES_CONTEXT_FACTORY,
                "org.apache.shale.test.mock.MockFacesContextFactory");
        FactoryFinder.setFactory(FactoryFinder.LIFECYCLE_FACTORY,
                "org.apache.shale.test.mock.MockLifecycleFactory");
        FactoryFinder.setFactory(FactoryFinder.RENDER_KIT_FACTORY,
                "org.apache.shale.test.mock.MockRenderKitFactory");*/

        PowerMockito.when(FactoryFinder.getFactory(FactoryFinder.APPLICATION_FACTORY)).thenReturn(applicationFactoryMock);
        PowerMockito.when(applicationFactoryMock.getApplication()).thenReturn(applicationMock);
        Mockito.when(facesContextMock.getApplication()).thenReturn(applicationMock);


        UIViewRoot root = new UIViewRoot();
        root.setViewId("/viewId");
        root.setRenderKitId(RenderKitFactory.HTML_BASIC_RENDER_KIT);
        Mockito.when(facesContextMock.getViewRoot()).thenReturn(root);


        ApplicationFactory applicationFactory = (ApplicationFactory) FactoryFinder
                .getFactory(FactoryFinder.APPLICATION_FACTORY);

        Mockito.when(applicationMock.getMessageBundle()).thenReturn(Constants.BUNDLE_KEY);

        PowerMockito.when(FactoryFinder.getFactory(FactoryFinder.RENDER_KIT_FACTORY)).thenReturn(renderKitFactoryMock);

        RenderKitFactory renderKitFactory = (RenderKitFactory) FactoryFinder
                .getFactory(FactoryFinder.RENDER_KIT_FACTORY);

        PowerMockito.when(renderKitFactory.getRenderKit(facesContextMock, RenderKitFactory.HTML_BASIC_RENDER_KIT)).thenReturn(renderKitMock);

        renderKitFactory.addRenderKit(RenderKitFactory.HTML_BASIC_RENDER_KIT,
                renderKitMock);


        // set supported locales
        List<Locale> list = new ArrayList<Locale>();
        list.add(new Locale("en"));
        list.add(new Locale("fr"));
        list.add(new Locale("de"));
        list.add(new Locale("es"));

        //application.setSupportedLocales(list);
        Mockito.when(applicationMock.getSupportedLocales()).thenReturn(list.iterator());

        // change the port on the mailSender so it doesn't conflict with an
        // existing SMTP server on localhost

        JavaMailSenderImpl mailSender = (JavaMailSenderImpl) applicationContext
                .getBean("mailSender");
        mailSender.setPort(getSmtpPort());
        mailSender.setHost("localhost");
    }


    public int getSmtpPort() {
        return smtpPort;
    }

    @NotNull
    private void setCurrentUser() {


        currentUser = userManager.get(-2L);

        SecurityContext context = new SecurityContextImpl();

        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(currentUser.getUsername(), currentUser.getPassword(), currentUser.getAuthorities());

        token.setDetails(currentUser);
        context.setAuthentication(token);
        SecurityContextHolder.setContext(context);

    }


    /**
     * <p>
     * Tear down instance variables required by this test case.
     * </p>
     */
    @After
    public void onTearDown() {
        facesContextMock.release();
        FactoryFinder.releaseFactories();
        Thread.currentThread().setContextClassLoader(threadContextClassLoader);
        threadContextClassLoader = null;
        currentUser = null;
    }
}
