package com.springthought.webapp.action;

import com.springthought.model.Visit;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class VisitFormTest extends BasePageMockitoTestCase {
    private VisitForm bean;
    @Autowired
    private GenericManager<Visit, Long> visitManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new VisitForm();
        bean.setVisitManager(visitManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Visit visit = new Visit();

        // enter all required fields
        visit.setName("CsLoJoNpKxQpRpSlCaWtVtQvVoCkYtBuQdBlXiEtBaCrXaWnWe");
        visit.setVisitedById(1L);
        visit.setVisitType("Qm");
        visit.setVisitedId(5L);
        bean.setVisit(visit);

        assertEquals("visits", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setVisitId(-1L);

        assertEquals("visitForm", bean.edit());
        assertNotNull(bean.getVisit());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setVisitId(-1L);

        assertEquals("visitForm", bean.edit());
        assertNotNull(bean.getVisit());
        Visit visit = bean.getVisit();

        // update required fields
        visit.setName("QwOjNaIsMiTdGzEcLiNqNqDnXdKzHeWkUoGrRkJeUiPsPoPmTu");
        visit.setVisitedById(1L);
        visit.setVisitType("Sk");
        visit.setVisitedId(4L);
        bean.setVisit(visit);

        assertEquals("visits", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Visit visit = new Visit();
        visit.setVisitId(-2L);
        bean.setVisit(visit);

        assertEquals("visits", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
