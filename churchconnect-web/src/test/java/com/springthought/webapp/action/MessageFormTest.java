package com.springthought.webapp.action;

import com.springthought.model.Message;
import com.springthought.service.GenericManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class MessageFormTest extends BasePageMockitoTestCase {
    private MessageForm bean;
    @Autowired
    private GenericManager<Message, Long> messageManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        Mockito.when(requestMock.getParameter("messageId")).thenReturn("-1");
        bean = (MessageForm) applicationContext.getBean("messageForm");
        bean.setMessageManager(messageManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Message message = new Message();

        // enter all required fields
        message.setEmail(Boolean.FALSE);
        message.setPhone(Boolean.FALSE);
        message.setTenantId(1L);
        message.setText(Boolean.FALSE);
        message.setMessage(RandomStringUtils.random(100, true,true));
        message.setName(RandomStringUtils.random(50, true,false));
        bean.setMessage(message);

        assertEquals("messages", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setMessageId(-1L);

        assertEquals("messageForm", bean.edit());
        assertNotNull(bean.getMessage());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setMessageId(-1L);

        assertEquals("messageForm", bean.edit());
        assertNotNull(bean.getMessage());
        Message message = bean.getMessage();

        // update required fields
        message.setEmail(Boolean.FALSE);
        message.setPhone(Boolean.FALSE);
        message.setTenantId(1L);
        message.setText(Boolean.FALSE);
        bean.setMessage(message);

        assertEquals("messages", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Message message = new Message();
        message.setMessageId(-2L);
        bean.setMessage(message);

        assertEquals("messages", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
