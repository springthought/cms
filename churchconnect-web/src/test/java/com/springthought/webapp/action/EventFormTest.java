package com.springthought.webapp.action;

import com.springthought.Constants.RepeatOption;
import com.springthought.model.Event;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class EventFormTest extends BasePageMockitoTestCase {
	private EventForm bean;
	@Autowired
	private GenericManager<Event, Long> eventManager;


	@Before
	public void onSetUp() {
		super.onSetUp();
		Mockito.when(requestMock.getParameter("from")).thenReturn("list");
		Mockito.when(requestMock.getParameter("eventId")).thenReturn("-1");
		bean = (EventForm) applicationContext.getBean("eventForm");
		bean.setEventManager(eventManager);
	}

	@After
	public void onTearDown() {
		super.onTearDown();
		bean = null;
	}

	@Test
	public void testAdd() throws Exception {
		Event event = new Event();

		// enter all required fields
		event.setAllDay(Boolean.FALSE);
		event.setEditable(Boolean.FALSE);
		event.setName("ZxGwEyYoRxLhHwDiRlRbQeAgUvRcVrAnRaQrFuEcErGxZnYhHg");
		event.setPublicEvent(Boolean.FALSE);
		event.setReminder(2083513831);
		event.setTenantId(1L);
		event.setRepeat(RepeatOption.NO);


		bean.setEvent(event);

		assertEquals("eventForm", bean.save());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testEdit() throws Exception {
		log.debug("testing edit...");
		bean.setEventId(-1L);
		assertEquals("eventForm", bean.edit());
		assertNotNull(bean.getEvent());
		assertFalse(bean.hasErrors());
		assertEquals("Projector # 1", bean.getEvent().getResource().getName());
		assertEquals("Hallwhite Building", bean.getEvent().getLocation().getName());

	}

	@Test
	public void testSave() {
		log.debug("testing save...");
		bean.setEventId(-1L);
		//assertEquals("eventForm", bean.edit());
		assertNotNull(bean.getEvent());
		Event event = bean.getEvent();

		// update required fields
		event.setAllDay(Boolean.FALSE);
		event.setEditable(Boolean.FALSE);
		event.setName("QhWaRjMlRaKkBbHoXsPxJnMgXpWfQcYiWvWbDcQwImDeZwNgRo");
		event.setPublicEvent(Boolean.FALSE);
		event.setReminder(423830042);
		event.setTenantId(1L);
		bean.setEvent(event);

		assertEquals("eventForm", bean.save());
		assertFalse(bean.hasErrors());
	}

	@Test
	public void testRemove() throws Exception {

		log.debug("testing remove...");
		Event event = new Event();
		event.setEventId(-2L);
		bean.setEvent(event);

		assertEquals("events", bean.delete());
		assertFalse(bean.hasErrors());
	}
}
