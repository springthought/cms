package com.springthought.webapp.action;

import com.springthought.model.Role;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

public class RoleListTest extends BasePageMockitoTestCase {
    private RoleList bean;
    @Autowired
    private GenericManager<Role, Long> roleManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean =  (RoleList) applicationContext.getBean("roleList");
        bean.setRoleManager(roleManager);

    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllRoles() throws Exception {
        List<Role> roleListList = bean.getRoles();
        assertTrue("List of roles is empty",roleListList.size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        roleManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
