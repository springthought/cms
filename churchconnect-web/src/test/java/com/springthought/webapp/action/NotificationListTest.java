package com.springthought.webapp.action;

import com.springthought.model.Notification;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class NotificationListTest extends BasePageMockitoTestCase {
    private NotificationList bean;
    @Autowired
    private GenericManager<Notification, Long> notificationManager;

    @Before
    public void onSetUp() {
        super.onSetUp();

        bean = new NotificationList();
        bean.setNotificationManager(notificationManager);

        // add a test notification to the database
        Notification notification = new Notification();

        // enter all required fields
        notification.setCompleteDate(new java.util.Date());
        notification.setName("YbXkBxEoUlNiHnKoQwFgTnCxRhQuDbXoQaKcXxIzYhLzFeRwMg");
        notification.setTenantId(1L);

        notificationManager.save(notification);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testGetAllNotifications() throws Exception {
        assertTrue(bean.getNotifications().size() >= 1);
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSearch() throws Exception {
        // regenerate indexes
        notificationManager.reindex();
        bean.setQuery("*");
        assertEquals("success", bean.search());
    }
}
