package com.springthought.webapp.action;

import com.springthought.model.User;
import com.springthought.service.UserManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class UserFormTest extends BasePageMockitoTestCase {
    private UserForm bean;
    @Autowired
    private UserManager userManager;

    @Override
    @Before
    public void onSetUp() {
        super.onSetUp();
        Mockito.when(requestMock.getParameter("id")).thenReturn("-1");
        bean =  (UserForm) applicationContext.getBean("userForm");
        bean.setUserManager(userManager);
        assertNotNull(bean);
    }

    @Override
    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testEdit() throws Exception {
        bean.setId("-1");
        assertEquals("editProfile", bean.edit());
        assertNotNull("User not found",bean.getUser().getUsername());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() throws Exception {
        User user = userManager.getUser("-1");
        user.setPassword("user");
        user.setConfirmPassword("user");
        bean.setUser(user);

        assertEquals("/admin/users", bean.save());
        assertNotNull(bean.getUser());
        assertFalse(bean.hasErrors());
    }

    /*@Test
    *remove test; no longer allow a user to delete an user from the userForm only List.
    public void testRemove() throws Exception {
        User user2Delete = new User();
        user2Delete.setId(-2L);
        bean.setUser(user2Delete);
        assertEquals("/admin/users", bean.delete());
        assertFalse(bean.hasErrors());
    }*/
}
