package com.springthought.webapp.action;

import com.springthought.model.Location;
import com.springthought.service.GenericManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class LocationFormTest extends BasePageMockitoTestCase {
    private LocationForm bean;
    @Autowired
    private GenericManager<Location, Long> locationManager;

    @Before
    public void onSetUp() {
        super.onSetUp();
        bean = new LocationForm();
        bean.setlocationManager(locationManager);
    }

    @After
    public void onTearDown() {
        super.onTearDown();
        bean = null;
    }

    @Test
    public void testAdd() throws Exception {
        Location location = new Location();

        // enter all required fields
        location.setName("KfJhMpKgNcDzEsHyReMmDeSgUqRpOpSfIkWtZnSsSmKdTqLjCa");
        location.setTenantId(1L);
        bean.setLocation(location);

        assertEquals("locations", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        bean.setlocationId(-1L);

        assertEquals("locationForm", bean.edit());
        assertNotNull(bean.getLocation());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testSave() {
        log.debug("testing save...");
        bean.setlocationId(-1L);

        assertEquals("locationForm", bean.edit());
        assertNotNull(bean.getLocation());
        Location location = bean.getLocation();

        // update required fields
        location.setName("LnTzDtXfCiJyOfKeIuCoRoMfJyPgVqYfEeEgWnZvBzOcEuFoSu");
        location.setTenantId(1L);
        bean.setLocation(location);

        assertEquals("locations", bean.save());
        assertFalse(bean.hasErrors());
    }

    @Test
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        Location location = new Location();
        location.setLocationId(-2L);
        bean.setLocation(location);

        assertEquals("locations", bean.delete());
        assertFalse(bean.hasErrors());
    }
}
