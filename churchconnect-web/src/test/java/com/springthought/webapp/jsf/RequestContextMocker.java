/**
 * 
 */
package com.springthought.webapp.jsf;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.primefaces.context.PrimeRequestContext;

import javax.faces.context.FacesContext;
 
public abstract class RequestContextMocker extends PrimeRequestContext {
 

    private static final Release RELEASE = new Release();

    public RequestContextMocker(FacesContext context) {
        super(context);
    }

    private static class Release implements Answer<Void> {
        @Override
        public Void answer(InvocationOnMock invocation) throws Throwable {
            setCurrentInstance(null, null);
            return null;
        }
    }
 
    public static PrimeRequestContext mockRequestContext(final FacesContext facesContext) {
        PrimeRequestContext context = Mockito.mock(PrimeRequestContext.class);
        setCurrentInstance(context,facesContext);
        Mockito.doAnswer(RELEASE).when(context).release();
        return context;
    }
}
