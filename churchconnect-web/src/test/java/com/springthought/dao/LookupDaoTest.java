package com.springthought.dao;

import com.springthought.Constants.CodeListType;
import com.springthought.model.SystemValue;
import com.springthought.service.LookupManager;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * This class tests the current LookupDao implementation class
 * @author mraible
 */
public class LookupDaoTest extends BaseDaoTestCase {
    @Autowired
    LookupDao lookupDao;

    @Autowired
    LookupManager lookupManager;


    @Test
    public void testGetRoles() {
        List roles = lookupDao.getRoles();
        log.debug(roles);
        assertTrue(roles.size() > 0);
    }

    @Test
	public void testGetSystemValueByCodeListType() {

		List<SystemValue> systemValues = lookupDao
				.getSystemValueByCodeListType( CodeListType.STAGEOFLIFE );
		log.debug(systemValues);
		assertTrue(systemValues.size() > 0);

	}


}
