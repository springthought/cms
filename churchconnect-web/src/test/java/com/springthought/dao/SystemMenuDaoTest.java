package com.springthought.dao;

import com.springthought.Constants.MenuType;
import com.springthought.model.SystemMenu;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SystemMenuDaoTest extends BaseDaoTestCase {

	@Autowired
	private SystemMenuDao sysMenuDao;

	@Test
	public void testFindByMenuType() {

		List<SystemMenu> systemMenu = sysMenuDao.findByMenuType(MenuType.MENU);
		assertTrue(systemMenu.size() > 0);

	}

	@Test
	public void testAddandRemoveMenu() {

		SystemMenu sysmenu = new SystemMenu();
		sysmenu.setSystemMenuId(-99L);
		sysmenu.setLocale("en");
		sysmenu.setRole("M");
		sysmenu.setMenuItem("Other Actions");
		sysmenu.setItemOrder(100);
		sysmenu.setTooltip("Other Actions Text...");
		sysmenu.setIconpath("ui-icon-help");
		sysmenu.setView("Other");
		sysmenu.setMenu_type(MenuType.SPLIT);

		SystemMenu savedMenu = sysMenuDao.save(sysmenu);

		flush();

		SystemMenu sysmenuSaved = sysMenuDao.get(savedMenu.getSystemMenuId());
		assertNotNull(sysmenuSaved.getSystemMenuId());

	}

}
