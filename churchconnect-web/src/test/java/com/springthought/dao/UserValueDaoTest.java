package com.springthought.dao;

import com.springthought.Constants.UserValueType;
import com.springthought.model.UserValue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class UserValueDaoTest extends BaseDaoTestCase {

	@Autowired
	private UserValueDao userValueDao;

	@Test
	public void testGetUserValueByType() {

		List<UserValue> userValues = userValueDao
				.getUserValueByType(UserValueType.CLASSIFICATION);
		
		assertTrue(userValues.size() > 0);

	}

}
