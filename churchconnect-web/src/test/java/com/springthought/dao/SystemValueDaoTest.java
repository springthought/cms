package com.springthought.dao;

import com.springthought.Constants.CodeListType;
import com.springthought.model.SystemValue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class SystemValueDaoTest extends BaseDaoTestCase {

	@Autowired
	private SystemValueDao systemValueDao;

	@Test
	public void testGetUserValueByType() {

		List<SystemValue> systemValues = systemValueDao
				.getSystemValueByCodeListType( CodeListType.STAGEOFLIFE );

		assertTrue(systemValues.size() > 0);

	}

}
