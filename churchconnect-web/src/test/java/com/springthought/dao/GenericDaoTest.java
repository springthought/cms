package com.springthought.dao;

import com.springthought.Constants;
import com.springthought.Constants.CategoryType;
import com.springthought.dao.hibernate.GenericDaoHibernate;
import com.springthought.model.*;
import com.springthought.util.Queries;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class GenericDaoTest extends BaseDaoTestCase {

    Log log = LogFactory.getLog(GenericDaoTest.class);
    GenericDao<User, Long> genericDao;
    GenericDao<Note, Long> genericNoteDao;
    GenericDao<Person, Long> genericPersonDao;
    GenericDao<CustomMetaValue, Long> genericCustomMetaValueDao;
    GenericDao<CustomMeta, Long> genericCustomMetaDao;
    GenericDao<Group, Long> genericGroupDao;
    GenericDao<Category, Long> genericCategoryDao;
    GenericDao<Aggregate, Long> genericAggregateDao;

    @Autowired
    private Validator validator;

    @Autowired
    SessionFactory sessionFactory;


    @Before
    public void setUp() {

        genericDao = new GenericDaoHibernate<User, Long>(User.class,
                sessionFactory);

        genericNoteDao = new GenericDaoHibernate<Note, Long>(Note.class,
                sessionFactory);

        genericPersonDao = new GenericDaoHibernate<Person, Long>(Person.class,
                sessionFactory);

        genericCustomMetaValueDao = new GenericDaoHibernate<CustomMetaValue, Long>(
                CustomMetaValue.class, sessionFactory);

        genericCustomMetaDao = new GenericDaoHibernate<CustomMeta, Long>(
                CustomMeta.class, sessionFactory);

        genericGroupDao = new GenericDaoHibernate<Group, Long>(Group.class,
                sessionFactory);

        genericCategoryDao = new GenericDaoHibernate<Category, Long>(
                Category.class, sessionFactory);

        genericAggregateDao = new GenericDaoHibernate<Aggregate, Long>(
                Aggregate.class, sessionFactory);
    }


    @Test
    public void getUser() {
        User user = genericDao.get(-1L);
        assertNotNull(user);
        assertEquals("user", user.getUsername());

        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.NOTE_FIND_BY_PERSON_ID_PARM, -1L);
        List<Note> note = genericNoteDao.findByNamedQuery(
                Queries.NOTE_FIND_BY_PERSON_ID, queryParams);

        assertNotNull(note);

    }


    @Test
    public void getPerson() {

        Person person = genericPersonDao.get(-3L);
        assertEquals("kinsey", person.getLastName().toLowerCase().trim());

    }


    @Test(expected = javax.validation.ValidationException.class)
    public void saveDeletePerson() {

        Person person = new Person();
        person.setFirstName("Minny");
        person.setLastName("Moe");
        person.setFirstName("Bilbo");
        person.setLastName("Baggins");
        person.setRelationship("OT");
        person.setGender("MA");

        PersonBin bin = new PersonBin();

        person.setPersonBin(bin);

        Household household = new Household();

        household.setEmail("moe@acme.com");
        household.setName("Moe Famlly");
        household.setStatusDate(new Date());

        Set<ConstraintViolation<Person>> constraintViolations =
                validator.validate( person);


        assertEquals( 0, constraintViolations.size() );

        Person savedPerson = genericPersonDao.save( person );

        flush();

        assertNotNull(savedPerson);

        Long id = savedPerson.getPersonId();

        genericPersonDao.remove(savedPerson);

        Person deletedObject = genericPersonDao.get(id);

        assertNull(deletedObject);

    }

    @Test
    public void getCustomMetaValue() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID_PARM, -1L);

        List<CustomMetaValue> values = genericCustomMetaValueDao
                .findByNamedQuery(Queries.CUSTOMVALUES_FIND_BY_FIELD_ID,
                        queryParams);

        assertNotNull(values);

    }

    @Test
    public void checkCustomFieldExists() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CUSTOMMETA_CHECK_EXISTS_PERSONID_PARM, -1L);
        queryParams.put(Queries.CUSTOMMETA_CHECK_EXISTS_USAGE_PARM,
                Constants.CustomFieldType.PERSON.getValue());

        List<CustomMeta> values = genericCustomMetaDao.findByNamedQuery(
                Queries.CUSTOMMETA_CHECK_EXISTS, queryParams);

        assertEquals(3, values.size());

    }

    @Test
    public void getGroupsByCategory() {
        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.CATEGORY_TYPE_PARM, CategoryType.CLASS.name());
        queryParams.put(Queries.TENANT_ID_PARM, 1L);
        List<Group> groups = genericGroupDao.findByNamedQuery(Queries.GROUPS_FIND_BY_CATEGORY, queryParams);
        assertEquals(4, groups.size());
    }

    @Test
    public void getCategoryGroupByType() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CATEGORY_TYPE_PARM,
                CategoryType.GIFT.toString());

        List<Category> categories = genericCategoryDao.findByNamedQuery(
                Queries.CATEGORY_FIND_BY_TYPE, queryParams);

        assertEquals(1, categories.size());

        List<Group> groups = categories.get(0).getGroups();

        assertEquals(1, groups.size());

        for (Group group : groups) {

            List<GroupMember> members = group.getGroupMembers();
            System.out.println(group.getName());

            assertEquals(5, members.size());

            for (GroupMember member : members) {
                assertNotNull(member.getPerson());
                assertNotNull(member.getGroup());
            }
        }

    }

    @Test
    public void getCategoryClass() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CATEGORY_TYPE_PARM,
                CategoryType.CLASS.toString());

        List<Category> categories = genericCategoryDao.findByNamedQuery(
                Queries.CATEGORY_FIND_BY_TYPE, queryParams);

        assertEquals(2, categories.size());

    }

    @Test
    public void getAllNonClassCategory() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CATEGORY_TYPE_PARM,
                CategoryType.CLASS.toString());

        List<Category> categories = genericCategoryDao.findByNamedQuery(
                Queries.CATEGORY_EXLUCDE_BY_TYPE, queryParams);

        assertEquals(10, categories.size());

    }


    @Test
    public void AddAndRemoveCategoryGroupByType() {

        Person person1 = genericPersonDao.get(-5L);
        Person person2 = genericPersonDao.get(-6L);

        GroupMember member1 = new GroupMember();
        GroupMember member2 = new GroupMember();

        Category category = new Category();

        Group group = new Group();
        group.setName("Test Category 1 Group");
        group.setDescr("Descr Category 1 Group");
        group.setCategory(category);

        category.setName("Test Category 1");
        category.setActive(true);

        category.setCategoryType(CategoryType.TALENT);

        category.getGroups().add(group);

        member1.setPerson(person1);
        member1.setGroup(group);
        member2.setPerson(person2);
        member2.setGroup(group);

        group.getGroupMembers().add(member1);
        group.getGroupMembers().add(member2);

        Category cat = genericCategoryDao.save(category);
        flush();

        assertNotNull(cat.getCategoryId());

        Category saved = genericCategoryDao.get(cat.getCategoryId());

        assertEquals("Test Category 1", saved.getName());
        assertEquals(1, saved.getGroups().size()); // check group was save
        assertEquals(2, saved.getGroups().get(0).getGroupMembers().size()); // check
        // member
        // save

    }

    @Test
    public void sumContributionByCategory() {

        Category category = genericCategoryDao.get(-7L);
        HashMap<String, Object> queryParams = new HashMap<String, Object>();


        queryParams.put(Queries.CONTRIBUTION_SUM_BY_CATEGORY_ID_PARM,
                category);

        List<Aggregate> agg = genericAggregateDao.findByNamedQuery(
                Queries.CONTRIBUTION_SUM_BY_CATEGORY, queryParams);


        assertTrue(agg.size() > 0);

    }

}
