package com.springthought.dao;

import com.springthought.Constants;
import com.springthought.model.Household;
import com.springthought.model.Person;
import com.springthought.model.PersonBin;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class HouseholdDaoTest extends BaseDaoTestCase {

	Log log = LogFactory.getLog(GenericDaoTest.class);

	@Autowired
	private HouseholdDao householdDao;

	// @Autowired
	// SessionFactory sessionFactory;

	@Test
	public void getHousehold() {
		Household household = householdDao.get(-3L);
		assertNotNull(household);
		assertEquals("The Eight Family", household.getName());
	}

	@Test
	public void getAllHouseholds() {

		List<Household> households = householdDao.getAll();
		assertNotNull(households);
		assertTrue(households.size() > 1);

	}

	@Test
	public void getAllDistinctHouseholds() {

		List<Household> households = householdDao.getAllDistinct();
		assertNotNull(households);
		assertTrue(households.size() > 1);

	}

	@Test
	public void saveHousehold() {

		Household household = new Household();

		Person person1 = new Person();
		Person person2 = new Person();

		Set<Person> persons = new HashSet<Person>(0);

		household.setEmail("moe@acme.com");
		household.setName("Moe Famlly");
		household.setStatusDate(new Date());
		household.setTenantId(-1L);




		person1.setFirstName("Minny");
		person1.setLastName("Moe");
		person1.setRelationship("DT");
		person1.setEmail("email@123.com");
		person1.setPersonBin(new PersonBin());
		person1.setTenantId(-1L);

		person2.setFirstName("Bilbo");
		person2.setLastName("Baggins");
		person2.setRelationship("DT");
		person2.setEmail("kd983@aid.com");
		person2.setPersonBin(new PersonBin());
		person2.setTenantId(-1L);


		persons.add(person1);
		persons.add(person2);

		household.setPersons(persons);
		household.setStatus(Constants.FamilyStatusType.ACTIVE);

		Household savedHousehold = householdDao.save(household);
		flush();
		assertNotNull(savedHousehold);

	}

}
