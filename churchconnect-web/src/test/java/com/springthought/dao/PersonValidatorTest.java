package com.springthought.dao;


import com.springthought.model.Person;
import com.springthought.service.LookupManager;
import com.springthought.service.validation.impl.CodeListValidator;
import com.springthought.util.ApplicationContextHolder;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PersonValidatorTest extends BaseDaoTestCase   {

//    private static Validator validator;

    @Autowired
    private Validator validator;

    @Autowired
    private LookupManager lookupManager;

    @Autowired
    ApplicationContextHolder contextHolder;

    @Autowired
    CodeListValidator codeListValidator;

    @BeforeClass
    public static void setUpValidator() {

        //ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        //validator = factory.getValidator();

    }

    @Test
    public void validation(){

        Person person = new Person();
        person.setGender("M1");
        person.setFirstName(StringUtils.repeat("X", 100));
        person.setLastName(StringUtils.repeat("X", 100));
        person.setPhoneNumber("45516");

        Set<ConstraintViolation<Person>> constraintViolations =
                validator.validate( person );

        assertEquals( 4, constraintViolations.size() );
        assertTrue(constraintViolations.toString().contains("size must be between 1 and 50"));

    }

}
