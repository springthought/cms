package com.springthought.process;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BatchMain implements CommandLineRunner {

    protected final Log log = LogFactory.getLog(getClass());


    @Autowired
    JdbcTemplate jdbcTemplate;


    public static void main(String[] args) {
        SpringApplication.run(BatchMain.class, args);
    }


    @Override
    public void run(String... strings) throws Exception {

        log.info("running...");

        /*
         * //different implementations for different scenarios are
         * neededList<CustomPojo> pojos =
         * jdbcTemplate.query("SELECT * FROM pojo", new RowMapper<CustomPojo>()
         * {
         *
         * @Override public CustomPojo mapRow(ResultSet rs, int row) throws
         * SQLException { return new CustomPojo(rs.getString(1),
         * rs.getString(2)); } });
         *
         *
         *
         * for (CustomPojo pojo : pojos) { log.info(pojo); }
         */

    }

}
