package com.springthought.process;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

    protected final Log log = LogFactory.getLog(getClass());
    
    @Override
    public void afterJob(JobExecution jobExecution) {

	super.afterJob(jobExecution);

	if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
	    log.info("Job Config Name:" + jobExecution.getJobId() + ". Status:" + jobExecution.getStatus());
	}

    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
	super.beforeJob(jobExecution);
	if (jobExecution.getStatus() == BatchStatus.STARTING || jobExecution.getStatus() == BatchStatus.STARTED) {
	    log.info("Job Config Name:" + jobExecution.getJobId() + ". Status:" + jobExecution.getStatus());
	}
    }

}