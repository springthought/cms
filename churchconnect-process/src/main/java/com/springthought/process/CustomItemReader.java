package com.springthought.process;

import com.springthought.model.Communication;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import java.util.Iterator;
import java.util.List;

public class CustomItemReader implements ItemReader<Communication> {

    private List<Communication> communications;

    private Iterator<Communication> iterator;

    @Override
    public Communication read()
	    throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

	if (getIterator().hasNext()) {
	    return getIterator().next();
	}
	return null;

    }

    public List<Communication> getCommunications() {
	return communications;
    }

    public void setCommunications(List<Communication> communications) {
	this.communications = communications;
    }

    public Iterator<Communication> getIterator() {
	return iterator;
    }

    public void setIterator(Iterator<Communication> iterator) {
	this.iterator = iterator;
    }

}
