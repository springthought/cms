/**
 *
 */
package com.springthought.process;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author eairrick
 *
 */
public class LongRowMapper implements RowMapper<Long> {

    @Override
    public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
	Long id = new Long(rs.getLong("cmmnctn_id"));
	return id;
    }

}
