package com.springthought.process;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduledTasks {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("sendCommunicationJob")
    private Job job;

    protected final Log log = LogFactory.getLog(getClass());

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedDelay = 10000)
    public void runSendCommunicationJob() {
	try {

	    log.info("..........START Scheduled Task.........");
	    String dateParam = new Date().toString();
	    JobParameters param = new JobParametersBuilder().addString("date", dateParam).toJobParameters();
	    log.info(dateParam);
	    JobExecution execution = jobLauncher.run(job, param);
	    log.info("The time is now " + dateFormat.format(new Date()));
	    log.info("Exit Status : " + execution.getStatus());
	    log.info("..........END Scheduled Task.........");

	} catch (Exception e) {

	    e.printStackTrace();

	}
    }

}
