package com.springthought.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

@Component
public class InterceptingJobExecution implements JobExecutionListener {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void beforeJob(JobExecution jobExecution) {

        log.info("Intercepting Job Execution - Before Job!");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("Intercepting Job Execution - After Job!");
    }

}
