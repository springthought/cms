package com.springthought.process;

import com.springthought.model.Communication;
import com.springthought.process.processor.EmailItemProcessor;
import com.springthought.process.processor.MmsItemProcessor;
import com.springthought.process.processor.Text2VoiceItemProcessor;
import com.springthought.service.MailEngine;
import com.springthought.service.MmsSender;
import com.springthought.service.Text2VoiceDailer;
import com.springthought.service.impl.TwilioMmsSender;
import com.springthought.service.impl.TwilioText2VoiceDailer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.HibernateItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration {

    protected final Log log = LogFactory.getLog(getClass());

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Autowired
    private InterceptingJobExecution listener;

    @Bean(name = {"compositeWriter"})
    // TODO may need compositeItem writer to send Mime email,via
    // MimeMessageItemWriter and second HibernateItemWriter
    // to save Communication Details with email sent status
    public CompositeItemWriter<?> compositeItemWriter() {
        return null;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})

    @Bean(name = {"interceptingJobExecution"})
    public InterceptingJobExecution interceptingJobExecution() {
        return new InterceptingJobExecution();
    }

    @Bean(name = {"compositeProcessor"})

    public CompositeItemProcessor<Long, Communication> compositeItemProcessor() {

        CompositeItemProcessor<Long, Communication> processor = new CompositeItemProcessor<Long, Communication>();

        List delegates = new ArrayList(0);

        delegates.add(eMailItemProcessor());
        delegates.add(mMsItemProcessor());
        delegates.add(text2VoiceItemProcessor());

        processor.setDelegates(delegates);
        return processor;

    }

    @Bean(name = {"Text2VoiceDailer"})
    public Text2VoiceDailer dailer() {
        Text2VoiceDailer dailer = new TwilioText2VoiceDailer();
        dailer.connect();
        return dailer;
    }

    @Bean
    public DataSource dataSource() {

        final DriverManagerDataSource dataSource = new DriverManagerDataSource();

        try (InputStream input = this.getClass().getClassLoader().getResourceAsStream("jdbc.properties")) {

            Properties prop = new Properties();
            prop.load(input);

            Properties cxnprop = new Properties();
            cxnprop.setProperty("defaultAutoCommit", "true");
            cxnprop.setProperty("maxActive", "100");
            cxnprop.setProperty("maxWait", "1000");
            cxnprop.setProperty("poolPreparedStatements", "true");

            dataSource.setDriverClassName(prop.getProperty("jdbc.driverClassName"));
            dataSource.setUrl(prop.getProperty("jdbc.url"));
            dataSource.setUsername(prop.getProperty("jdbc.username"));
            dataSource.setPassword(prop.getProperty("jdbc.password"));
            dataSource.setConnectionProperties(cxnprop);

        } catch (Exception e) {
            log.error("Unable to connect to database", e);
        }

        return dataSource;
    }

    @Bean(name = {"eMailItemProcessor"})
    public ItemProcessor<Long, Communication> eMailItemProcessor() {
        return new EmailItemProcessor();
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public JobLauncherTestUtils jobLauncherTestUtils() {
        return new JobLauncherTestUtils();
    }

    @Bean(name = {"mailEngine"})
    public MailEngine mailEngine(JavaMailSenderImpl mailSender, VelocityEngineFactoryBean velocityEngine) {

        MailEngine mailEngine = new MailEngine();

        try (InputStream input = this.getClass().getClassLoader().getResourceAsStream("mail.properties")) {

            Properties props = new Properties();
            props.load(input);
            mailEngine.setMailSender(mailSender);
            mailEngine.setVelocityEngine(velocityEngine.getObject());
            mailEngine.setFrom(props.getProperty("mail.default.from"));

        } catch (IOException e) {
            log.error("Unable read mail.properties file", e);
        }

        return mailEngine;

    }

    @Bean(name = {"mailSender"})
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();

        try (InputStream input = this.getClass().getClassLoader().getResourceAsStream("mail.properties")) {

            Properties props = new Properties();
            props.load(input);

            javaMailSenderImpl.setHost(props.getProperty("mail.host"));
            javaMailSenderImpl.setDefaultEncoding("UTF-8");
            javaMailSenderImpl.setPort(Integer.parseInt(props.getProperty("mail.port")));
            javaMailSenderImpl.setProtocol(props.getProperty("mail.protocol"));
            javaMailSenderImpl.setUsername(props.getProperty("mail.username"));
            javaMailSenderImpl.setPassword(props.getProperty("mail.password"));

            Properties javaMailProperties = new Properties();

            javaMailProperties.setProperty("mail.smtp.auth", "true");
            javaMailProperties.setProperty("mail.smtp.starttls.enable", "false");
            javaMailProperties.setProperty("mail.smtp.quitwait", "false");
            javaMailProperties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            javaMailProperties.setProperty("mail.smtp.socketFactory.fallback", "false");
            javaMailProperties.setProperty("mail.debug", "true");

            javaMailSenderImpl.setJavaMailProperties(javaMailProperties);

        } catch (IOException e) {
            log.error("Unable read mail.properties file", e);
        }

        return javaMailSenderImpl;
    }

    @Bean(name = {"mMsItemProcessor"})
    public ItemProcessor<Communication, Communication> mMsItemProcessor() {
        return new MmsItemProcessor();
    }

    @Bean(name = {"MmsSender"})
    public MmsSender mMsSender() {
        return new TwilioMmsSender();
    }

    /**
     * @param localSessionFactory
     * @return
     * @Bean public ItemReader<Communication> reader(LocalSessionFactoryBean
     * localSessionFactory) {
     * <p>
     * HibernateCursorItemReader<Communication> reader = new
     * HibernateCursorItemReader<Communication>();
     * <p>
     * reader.setSaveState(true); // set to false if in a concurrent state
     * reader.setSessionFactory(localSessionFactory.getObject());
     * reader.setQueryString("from Communication c order by c.tenantId");
     * reader.setFetchSize(100); reader.setUseStatelessSession(false);
     * <p>
     * return reader;
     * <p>
     * }
     */

    @Bean(name = {"emailReader"})
    public ItemReader<Long> reader(JdbcTemplate jdbcTemplate) {

        JdbcCursorItemReader<Long> reader = new JdbcCursorItemReader<Long>();
        reader.setDataSource(jdbcTemplate.getDataSource());
        reader.setSql(
                "SELECT c.cmmnctn_id FROM communication as c where  c.status = ?  and c.startDate <= ? order by c.cmmnctn_id");

        reader.setPreparedStatementSetter(new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setString(1, "SCHD");
                ps.setDate(2, new Date(new java.util.Date().getTime()));
            }
        });
        reader.setSaveState(true);
        reader.setFetchSize(100);
        reader.setRowMapper(new LongRowMapper());
        return reader;

    }

    @Bean(name = {"personNotificationReader"})
    public ItemReader<Long> personNotificationReader(JdbcTemplate jdbcTemplate) {

        JdbcCursorItemReader<Long> reader = new JdbcCursorItemReader<Long>();
        reader.setDataSource(jdbcTemplate.getDataSource());
        reader.setSql("SELECT p.* FROM person as p order by p.person_id");
        reader.setSaveState(true);
        reader.setFetchSize(100);
        reader.setRowMapper(new LongRowMapper());
        return reader;

    }

    @Bean(name = {"sendCommunicationJob"})
    public Job sendCommunicationJob(@Qualifier("stepSendCommunications") Step stepSendCommunications) throws SQLException {

        // return jobs.get("sendCommunicationJob").incrementer(new
        // RunIdIncrementer()).listener(listener).flow(step1)
        // .next(step2).end().build();

        return jobs.get("sendCommunicationJob").incrementer(new RunIdIncrementer()).listener(listener)
                .flow(stepSendCommunications).end().build();
    }

    @Bean(name = {"sessionFactory"})
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {

        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
        hibernateProperties.setProperty("hibernate.query.substitutions", "true 'Y', false 'N'");
        hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", "true");
        hibernateProperties.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.EhCacheProvider");

        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setConfigLocations(new ClassPathResource("hibernate.cfg.xml"));

        sessionFactory.setHibernateProperties(hibernateProperties);

        return sessionFactory;
    }

 /*   @Bean(name = {"step1"})
    public Step step1(@Qualifier("emailReader") ItemReader<Long> reader, @Qualifier("mailWriter") ItemWriter<Communication> writer, @Qualifier("eMailItemProcessor") ItemProcessor<Long, Communication> processor) throws SQLException
    {
        return steps.get("step1").<Long, Communication>
                chunk(10).reader(reader).processor(processor).writer(writer)
                .build();
    }*/


    /**
     *
     *
     *
     * @Bean(name = { "step2" }) public Step step2(ItemReader
     *            <Long> reader, @Qualifier("mMsItemProcessor")
     *            ItemProcessor<Long, Communication> processor, ItemWriter
     *            <Communication> writer) throws SQLException { return
     *            steps.get("step2").<Long, Communication>
     *            chunk(10).reader(reader).processor(processor).writer(writer)
     *            .build(); }
     */


    /**
     * @Bean(name = { "step3" }) public Step step3(ItemReader
     * <Long> reader, @Qualifier("text2VoiceItemProcessor")
     * ItemProcessor<Long, Communication> processor, ItemWriter
     * <Communication> writer) throws SQLException { return
     * steps.get("step3").<Long, Communication>
     * chunk(10).reader(reader).processor(processor).writer(writer)
     * .build(); }
     */

    @Bean(name = {"stepSendCommunications"})
    public Step stepSendCommunications(@Qualifier("emailReader") ItemReader<Long> reader,
                                       @Qualifier("compositeProcessor") ItemProcessor<Long, Communication> processor,
                                       ItemWriter<Communication> writer) throws SQLException {
        return steps.get("stepSendCommunications").<Long, Communication>chunk(10).reader(reader).processor(processor)
                .writer(writer).build();
    }

    @Bean(name = {"templateEngine"})
    public VelocityEngineFactoryBean templateEngine() {

        VelocityEngineFactoryBean velocityEngineFactoryBean = new VelocityEngineFactoryBean();

        Properties velocityProperties = new Properties();

        velocityProperties.setProperty("resource.loader", "class");
        velocityProperties.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        velocityProperties.setProperty("velocimacro.library", "");

        velocityEngineFactoryBean.setVelocityProperties(velocityProperties);

        return velocityEngineFactoryBean;

    }

    @Bean(name = {"text2VoiceItemProcessor"})
    public ItemProcessor<Communication, Communication> text2VoiceItemProcessor() {
        return new Text2VoiceItemProcessor();
    }

    @Bean(name = {"hibernateTxnManager"})
    public HibernateTransactionManager transactionManager(LocalSessionFactoryBean localSessionFactory) {
        HibernateTransactionManager txnManager = new HibernateTransactionManager(localSessionFactory.getObject());
        return txnManager;
    }

    @Bean(name = {"mailWriter"})
    public ItemWriter<Communication> writer(LocalSessionFactoryBean localSessionFactory) {

        HibernateItemWriter<Communication> writer = new HibernateItemWriter<Communication>();
        writer.setClearSession(true);
        writer.setSessionFactory(localSessionFactory.getObject());
        return writer;

    }
}
