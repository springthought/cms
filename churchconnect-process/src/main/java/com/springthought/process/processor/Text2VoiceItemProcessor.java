package com.springthought.process.processor;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.springthought.Constants.Status;
import com.springthought.model.*;
import com.springthought.process.BaseProcessor;
import com.springthought.service.Text2VoiceDailer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Text2VoiceItemProcessor extends BaseProcessor implements ItemProcessor<Communication, Communication> {


    // @Autowired
    // private LocalSessionFactoryBean sessionFactory;

    @Autowired
    private Text2VoiceDailer dailer;

    @Override
    public Communication process(final Communication communication) throws Exception {



	if (communication.getMessage().isPhone()) {
	    return processCommunication(communication);
	} else {
	    communication.setStatus(Status.SUC);
	    return communication;
	}

    }

    /**
     *
     * @param communication
     * @return
     */

    private Communication processCommunication(Communication communication) {
	communication.setStatus(Status.RUN);

	List<Group> groups = new ArrayList<Group>(communication.getGroups());

	for (Group group : groups) {

	    List<GroupMember> members = new ArrayList<GroupMember>(group.getGroupMembers());
	    log.info("==== " + group.getName().toUpperCase() + "====");

	    List<GroupMember> membersWithVoice = members.stream()
		    .filter(m -> StringUtils.isNotEmpty(m.getPerson().getMobileNumber())
			    || StringUtils.isNotEmpty(m.getPerson().getPhoneNumber()))
		    .collect(Collectors.toList());

	    for (GroupMember member : membersWithVoice) {

		log.info("Create & Send Voice to:" + member.getPerson().getName());

		JobLog joblog = null;

		JobLog theJobLog = Iterables.find(communication.getJobLogs(), new Predicate<JobLog>() {
		    @Override
		    public boolean apply(JobLog arg) {
			return arg.getPerson() == member.getPerson();
		    }
		}, null);

		if (theJobLog != null) {
		    joblog = theJobLog;
		    log.info("Remove Job Log Entry : " + joblog.toString() + " from collection");
		    communication.getJobLogs().remove(theJobLog);
		} else {
		    joblog = new JobLog();
		}

		joblog.setCommunication(communication);
		joblog.setPerson(member.getPerson());
		joblog.setDeliveryDate(new Date());
		joblog.setPhoneResultStatus(Status.SCHD);
		joblog.setVersion(0);
		joblog.setCommunication(communication);

		StatusResult result = sendVoiceMessage(member.getPerson(), communication.getMessage());

		joblog.setPhoneResultStatus(result.getStatus());
		joblog.setPhoneResult(result.getResultMessage());
		joblog.setsId(result.getMeta().trim());

		log.info("Recording Job Log Entry : " + joblog.toString());
		communication.getJobLogs().add(joblog);
	    }
	}
	communication.setStatus(Status.SUC);
	return communication;
    }

    /**
     *
     * @param person
     * @param msg
     * @return
     */
    private StatusResult sendVoiceMessage(Person person, Message msg) {

	MessageTemplateSupplier supplier = new MessageTemplateSupplier();
	supplier.setPerson(person);
	supplier.setMessage(msg);
	supplier.setSender(null);
	String phoneNumber = StringUtils.isNotEmpty(person.getPhoneNumber()) ? person.getPhoneNumber()
		: person.getMobileNumber();

	log.debug("sending voice message  to user [" + phoneNumber + "]...");

	return dailer.makeCall(phoneNumber, supplier);

    }

}
