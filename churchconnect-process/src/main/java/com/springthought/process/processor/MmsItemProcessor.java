package com.springthought.process.processor;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.springthought.Constants.Status;
import com.springthought.model.*;
import com.springthought.process.BaseProcessor;
import com.springthought.service.MmsSender;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class MmsItemProcessor extends BaseProcessor implements ItemProcessor<Communication, Communication>  {


    // @Autowired
    // private LocalSessionFactoryBean sessionFactory;

    @Autowired
    private MmsSender mMsSender;

    @Override
    // public Communication process(final Long communication_id) throws
    // Exception {
    public Communication process(final Communication communication) throws Exception {

	// Communication communication = (Communication)
	// sessionFactory.getObject().getCurrentSession()
	// .get(Communication.class, communication_id);

	if (communication.getMessage().isText()) {
	    return processCommunication(communication);
	} else {
	    communication.setStatus(Status.SUC);
	    return communication;
	}

    }

    /**
     * @author eairrick
     * @param communication
     * @return
     */

    private Communication processCommunication(Communication communication) {

	communication.setStatus(Status.RUN);

	List<Group> groups = new ArrayList<Group>(communication.getGroups());

	for (Group group : groups) {

	    List<GroupMember> members = new ArrayList<GroupMember>(group.getGroupMembers());

	    log.info("==== " + group.getName().toUpperCase() + "====");

	    List<GroupMember> membersWithMobile = members.stream()
		    .filter(m -> StringUtils.isNotEmpty(m.getPerson().getMobileNumber())).collect(Collectors.toList());

	    for (GroupMember member : membersWithMobile) {

		log.info("Create & Send MMS to:" + member.getPerson().getName() + " mobile: "
			+ member.getPerson().getMobileNumber().trim());

		JobLog joblog = null;

		JobLog theJobLog = Iterables.find(communication.getJobLogs(), new Predicate<JobLog>() {
		    @Override
		    public boolean apply(JobLog arg) {
			return arg.getPerson() == member.getPerson();
		    }
		}, null);

		if (theJobLog != null) {
		    joblog = theJobLog;
		    log.info("Remove Job Log Entry : " + joblog.toString() + " from collection");
		    communication.getJobLogs().remove(theJobLog);
		} else {
		    joblog = new JobLog();
		}

		joblog.setCommunication(communication);
		joblog.setPerson(member.getPerson());
		joblog.setDeliveryDate(new Date());
		joblog.setTextResultStatus(Status.SCHD);
		// joblog.setVersion(0);
		joblog.setCommunication(communication);

		StatusResult result = sendPersonMessage(member.getPerson(), communication.getMessage());

		joblog.setTextResultStatus(result.getStatus());
		joblog.setTextResult(result.getResultMessage());

		log.info("Recording Job Log Entry : " + joblog.toString());
		communication.getJobLogs().add(joblog);
	    }
	}
	communication.setStatus(Status.SUC);
	return communication;

    }

    /**
     *
     * @param person
     * @param msg
     * @return
     */

    private StatusResult sendPersonMessage(Person person, Message msg) {

	log.debug("sending MMS to user [" + person.getMobileNumber() + "]...");
	MessageTemplateSupplier supplier = new MessageTemplateSupplier();
	supplier.setPerson(person);
	supplier.setMessage(msg);
	supplier.setSender(null);
	return mMsSender.sendMms(person.getMobileNumber(), supplier);

    }

}
