package com.springthought.process.processor;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.springthought.Constants;
import com.springthought.Constants.Status;
import com.springthought.model.*;
import com.springthought.process.BaseProcessor;
import com.springthought.service.MailEngine;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EmailItemProcessor extends BaseProcessor implements ItemProcessor<Long, Communication> {

    protected final Log log = LogFactory.getLog(getClass());

    @Autowired
    private LocalSessionFactoryBean sessionFactory;

    @Autowired
    private MailEngine mailEngine;

    @Override
    public Communication process(final Long communication_id) throws Exception {

	Communication communication = (Communication) sessionFactory.getObject().getCurrentSession()
		.get(Communication.class, communication_id);

	if (communication.getMessage().isEmail()) {
	    return processCommunication(communication);
	} else {
	    communication.setStatus(Status.SUC);
	    return communication;
	}

    }

    /**
     *
     * @param communication
     * @return
     */

    private Communication processCommunication(Communication communication) {

	communication.setStatus(Status.RUN);

	List<Group> groups = new ArrayList<Group>(communication.getGroups());

	for (Group group : groups) {

	    List<GroupMember> members = new ArrayList<GroupMember>(group.getGroupMembers());
	    log.info("==== " + group.getName().toUpperCase() + "====");

	    List<GroupMember> membersWithEmails = members.stream()
		    .filter(m -> StringUtils.isNotEmpty(m.getPerson().getEmail())).collect(Collectors.toList());

	    for (GroupMember member : membersWithEmails) {

		log.info("Create & Send MimeMessage to:" + member.getPerson().getName() + " email: "
			+ member.getPerson().getEmail());

		JobLog joblog = null;

		JobLog theJobLog = Iterables.find(communication.getJobLogs(), new Predicate<JobLog>() {
		    @Override
		    public boolean apply(JobLog arg) {
			return arg.getPerson() == member.getPerson();
		    }
		}, null);

		if (theJobLog != null) {
		    communication.getJobLogs().remove(theJobLog);
		    joblog = theJobLog;
		    log.info("Remove Job Log Entry : " + joblog.toString() + " from collection");
		} else {
		    joblog = new JobLog();
		}

		joblog.setCommunication(communication);
		joblog.setPerson(member.getPerson());
		joblog.setDeliveryDate(new Date());
		joblog.setEmailResultStatus(Status.SCHD);
		// joblog.setVersion(0);
		joblog.setCommunication(communication);

		StatusResult result = sendPersonMessage(member.getPerson(), communication.getMessage());

		joblog.setEmailResultStatus(result.getStatus());
		joblog.setEmailResult(result.getResultMessage());

		communication.getJobLogs().add(joblog);

		log.info("Recording Job Log Entry : " + joblog.toString());
	    }
	}

	communication.setStatus(Status.SUC);
	return communication;

    }

    /**
     *
     * @param person
     * @param msg
     * @return
     */

    private StatusResult sendPersonMessage(Person person, Message msg) {

	StatusResult result = new StatusResult();

	log.debug("sending e-mail to user [" + person.getEmail() + "]...");

	MessageTemplateSupplier supplier = new MessageTemplateSupplier();

	supplier.setPerson(person);
	supplier.setMessage(msg);
	supplier.setApplicationURL("www.springthought.com");
	supplier.setSender(null);

	try {
	    mailEngine.sendHTMLMessage(new String[] { person.getEmail() }, supplier);
	} catch (MessagingException e) {
	    result.setStatus(Status.ERR);
	    result.setResultMessage(e.getMessage());
	    return result;
	}

	result.setStatus(Status.SUC);
	result.setResultMessage(Constants.COMMUNICATION_SUCCESS);

	return result;
    }

}
