package com.springthought.process;

import org.junit.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.assertEquals;

/**
 * very simple example of how to use the class jobLauncherTestUtils
 *
 * @author dgutierrez-diez
 */
@ContextConfiguration(classes = BatchConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class SpringBatchUnitTest {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void testLaunchJob() throws Exception {
// @todo add ample springbatch test cases
// /**/JobExecution jobExecution = jobLauncherTestUtils.launchJob();
// assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
        assertEquals(BatchStatus.COMPLETED, BatchStatus.COMPLETED);
    }

    @Test
    public void testLaunchStep() {
 //@todo add ample springbatch test cases
//		JobExecution jobExecution = jobLauncherTestUtils.launchStep("step1");
//		assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
        assertEquals(BatchStatus.COMPLETED, BatchStatus.COMPLETED);
    }
}
