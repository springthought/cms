/**
 *
 */
package com.springthought.service;

import biweekly.ICalendar;
import com.springthought.model.Event;

import java.util.ArrayList;
import java.util.Locale;

/**
 * @author eairrick
 *
 */
public interface ICalBuilderManager {

	public ICalendar generateICalendar(Locale locale,
			ArrayList<Event> events);

}
