package com.springthought.service;

import com.springthought.Constants.CodeListType;
import com.springthought.model.SystemValue;

import java.util.List;

public interface SystemValueManager extends GenericManager<SystemValue, Long> {

	public List<SystemValue> getSystemValueByCodeListType(CodeListType codelist);

}
