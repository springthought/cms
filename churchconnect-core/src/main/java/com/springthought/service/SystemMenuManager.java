
package com.springthought.service;

import com.springthought.Constants.MenuType;
import com.springthought.model.SystemMenu;

import java.util.List;
/**
 * @author kinseye
 *
 */
public interface SystemMenuManager extends GenericManager<SystemMenu, Long> {
    List<SystemMenu> findByMenuType(MenuType menuType);
}
