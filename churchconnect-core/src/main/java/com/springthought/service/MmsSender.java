package com.springthought.service;


import com.springthought.model.MessageTemplateSupplier;
import com.springthought.model.StatusResult;

public interface MmsSender {
    public StatusResult sendMms(String recipient, MessageTemplateSupplier supplier);
}
