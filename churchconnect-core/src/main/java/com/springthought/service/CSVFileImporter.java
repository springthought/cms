package com.springthought.service;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public interface CSVFileImporter<T, PK extends Serializable> {
    List<T> loadCSVFileIntoObject(Object object, File file) throws IOException;
    List<T> loadCSVFileIntoObject(Object object, Reader in) throws IOException;
    ArrayList<String> retrieveCSVFileHeaders(File file) throws IOException;
    ArrayList<String> retrieveCSVFileHeaders(Reader in) throws IOException;
    ArrayList<Field>  retrieveObjectFields(Object object);
    ArrayList<String> validateHeadersInObjectField(Reader in, Object object ) throws IOException;
    ArrayList<String> validateHeadersInObjectField(File file, Object object ) throws IOException;
}