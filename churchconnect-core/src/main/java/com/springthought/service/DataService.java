/**
 * 
 */
package com.springthought.service;

import com.springthought.Constants;
import com.springthought.Constants.CategoryType;
import com.springthought.Constants.DataServiceType;
import com.springthought.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eairrick
 *
 */
public interface DataService {

    public abstract List<LabelValue> fetchDataList(DataServiceType type);

    public abstract List<Event> getAttendanceEvents(EventType type);

    public abstract Constants.AttendanceTrackingType[] getAttendanceTrackingTypes();

    public abstract Constants.DataImportType[] getDataImportTypes();

    /**
     * @return the campaignManager
     */
    public abstract GenericManager<Campaign, Long> getCampaignManager();

    public abstract List<Campaign> getCampaigns();

    public abstract List<Category> getCategories();


    public abstract List<Category> getCategoryByType(CategoryType type);

    public abstract List<Category> getCategoryClasses();

    public abstract List<Category> getCategoryFunds();

    public abstract List<Category> getCategoryMinistry();

    public abstract List<Group> getAllGroupClasses(Long tenant_id);

    /**
     * @return the categoryManager
     */
    public abstract GenericManager<Category, Long> getCategoryManager();

    /**
     * @return the customMetaManager
     */
    public abstract GenericManager<CustomMeta, Long> getCustomMetaManager();

    public abstract GenericManager<Event, Long> getEventManager();

    public abstract GenericManager<EventType, Long> getEventTypeManager();

    public abstract List<EventType> getEventTypes();

    /**
     * @return the groupManager
     */
    public abstract GenericManager<Group, Long> getGroupManager();

    /**
     * @return the groupRoleManager
     */
    public abstract GenericManager<GroupRole, Long> getGroupRoleManager();

    public abstract List<GroupRole> getGroupRoles();

    /**
     * @return the householdManager
     */
    public abstract GenericManager<Household, Long> getHouseholdManager();

    /**
     * @return the lookupManager
     */
    public abstract LookupManager getLookupManager();

    public abstract List<LabelValue> getMemberTypes();

    /**
     * @return the personManager
     */
    public abstract GenericManager<Person, Long> getPersonManager();

    public abstract List<Person> getPersons();

    public abstract GenericManager<Resource, Long> getResourceManager();

    public abstract ArrayList<LabelValue> getStates();

    public abstract List<Resource> getResources();

    public abstract GenericManager<Location, Long> getRoomManager();

    public abstract List<Location> getLocations();

    public abstract void setCampaignManager(GenericManager<Campaign, Long> campaignManager);

    public abstract void setCategoryManager(GenericManager<Category, Long> categoryManager);

    public abstract void setCustomMetaManager(GenericManager<CustomMeta, Long> customMetaManager);

    public abstract void setEventManager(GenericManager<Event, Long> eventManager);

    public abstract void setEventTypeManager(GenericManager<EventType, Long> eventTypeManager);

    public abstract void setGroupManager(GenericManager<Group, Long> groupManager);

    public abstract void setGroupRoleManager(GenericManager<GroupRole, Long> groupRoleManager);

    public abstract void setHouseholdManager(GenericManager<Household, Long> householdManager);

    public abstract void setLookupManager(LookupManager lookupManager);

    public abstract void setPersonManager(GenericManager<Person, Long> personManager);

    public abstract void setResourceManager(GenericManager<Resource, Long> resourceManager);

    public abstract void setRoomManager(GenericManager<Location, Long> roomManager);

}
