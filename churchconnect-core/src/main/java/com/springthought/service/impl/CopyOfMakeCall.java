package com.springthought.service.impl;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;

import java.util.HashMap;
import java.util.Map;

public class CopyOfMakeCall {

	public static final String ACCOUNT_SID = "ACa64d9d2d2a1e81e420af26059536268b";
	public static final String AUTH_TOKEN = "bbf5685359ea24636227f6d2947b07b3";

    public static void main(String[] args) throws TwilioRestException {

        TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
        Account mainAccount = client.getAccount();
        CallFactory callFactory = mainAccount.getCallFactory();
        Map<String, String> callParams = new HashMap<String, String>();

        callParams.put("To", "+16788787621"); // Replace with your phone number
        callParams.put("From", "+16784432132"); // Replace with a Twilio number
        callParams.put("Url", "http://demo.twilio.com/welcome/voice/");
        callParams.put("StatusCallback", "http://12276b5c.ngrok.io/ChurchClick/status");
        // Make the call
        Call call = callFactory.create(callParams);
        // Print the call SID (a 32 digit hex like CA123..)"
        System.out.println(call.getSid());
    }
}
