/**
 *
 */
package com.springthought.service.impl;

import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.property.DateEnd;
import biweekly.property.DateStart;
import biweekly.property.Summary;
import biweekly.util.Duration;
import biweekly.util.Recurrence;
import biweekly.util.Recurrence.DayOfWeek;
import biweekly.util.Recurrence.Frequency;
import com.springthought.Constants.WeekOption;
import com.springthought.model.Event;
import com.springthought.service.ICalBuilderManager;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * @author eairrick
 *
 */
@Service("iCalBuilder")
public class ICalBuilderManagerImpl implements ICalBuilderManager {

	@Override
	public ICalendar generateICalendar(Locale locale, ArrayList<Event> events) {

		ICalendar ical = new ICalendar();

		for (Event event : events) {

			VEvent vevent = new VEvent();

			Summary summary = vevent.setSummary(event.getTitle());
			summary.setLanguage(locale.toString());

			vevent.addComment(event.getDescription());
			vevent.addCategories(event.getEventType().getName());

			if (event.isAllDay()) {

				DateTime dt = new DateTime(event.getEndDate()).plus(Period
						.days(1));
				vevent.setDateStart(new DateStart(event.getStartDate(), false));
				vevent.setDateEnd(new DateEnd(dt.toDate(), false));

			} else {

				vevent.setDateStart(new DateStart(event.getStartDate(), true));
				vevent.setDateEnd(new DateEnd(event.getEndDate(), true));
			}

			vevent.setDuration(new Duration.Builder().hours(2).build());

			// Recurrence recur = new
			// Recurrence.Builder(Frequency.YEARLY).byDay(DayOfWeek.MONDAY).count(5).build();

			// Recurrence rrule = new Recurrence.Builder(Frequency.WEEKLY)
			// .interval(2).until(event.getRepeatUntilDate()).build();

			// Recurrence recur = new Recurrence.Builder(event.frequency())
			// .build();

			vevent.setRecurrenceRule(calculateRecurrence(event));

			vevent.setLocation(event.getLocation() != null ? event.getLocation()
					.getName() : "");

			vevent.setCreated(new Date());
			vevent.setUid(java.util.UUID.randomUUID().toString());

			ical.addEvent(vevent);

		}

		return ical;

	}

	private Recurrence calculateRecurrence(Event event) {

		Recurrence recurr = null;

		switch (event.getRepeat()) {
		case DAILY:
			recurr = daily(event);
			break;
		case WEEKLY:
			recurr = weekly(event);
			break;
		/*case BIWEEKLY:
			recurr = biWeekly(event);
			break;*/
		case MONTHLY:
			recurr = yearMonthly(event, true);
			break;
		case YEARLY:
			recurr = yearMonthly(event, false);
			break;
		default:
			break;
		}

		return recurr;
	}

	private Recurrence daily(Event event) {

		Recurrence rrule = null;

		if (event.isWeekDaysOnly()) {

			ArrayList<DayOfWeek> days = new ArrayList<DayOfWeek>(Arrays.asList(
					DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY,
					DayOfWeek.THURSDAY, DayOfWeek.FRIDAY));

			rrule = new Recurrence.Builder(Frequency.WEEKLY).byDay(days)
					.until(event.getRepeatUntilDate()).build();
		} else {

			rrule = new Recurrence.Builder(Frequency.DAILY).until(
					event.getRepeatUntilDate()).build();
		}

		return rrule;

	}

	private Recurrence weekly(Event event) {

		Recurrence rrule = null;

		rrule = new Recurrence.Builder(Frequency.WEEKLY).until(
				event.getRepeatUntilDate()).build();

		return rrule;

	}

	/*private Recurrence biWeekly(Event event) {

		Recurrence rrule = null;


		rrule = new Recurrence.Builder(Frequency.WEEKLY).interval(2)
				.until(event.getRepeatUntilDate()).build();

		return rrule;

	}*/

	private Recurrence yearMonthly(Event event, boolean isMonthly) {

		Frequency freq = isMonthly ? Frequency.MONTHLY : Frequency.YEARLY;

		Recurrence rrule = null;
		// MONTHLY;UNTIL=20180619T140000Z;BYMONTHDAY=19

		if (event.getMonthYearOption() == 0) {
			rrule = new Recurrence.Builder(freq).byMonthDay(event.getDayNth())
					.until(event.getRepeatUntilDate()).build();
		} else {

			WeekOption weekOption = event.getWeekOccurrence();
			Integer dayNum = event.getWeekDayNum();

			switch (weekOption) {
			case FIRST:
				// RRULE:FREQ=MONTHLY;UNTIL=20180801T110000Z;BYDAY=1WE
				rrule = new Recurrence.Builder(freq)
						.byDay(1, retrieveDayOfWeek(dayNum))
						.until(event.getRepeatUntilDate()).build();
				break;
			case LAST:
				rrule = new Recurrence.Builder(freq)
						.byDay(-1, retrieveDayOfWeek(dayNum))
						.until(event.getRepeatUntilDate()).build();
				break;
			default:
				rrule = new Recurrence.Builder(freq)
						.byDay(weekOption.n(), retrieveDayOfWeek(dayNum))
						.until(event.getRepeatUntilDate()).build();
				break;

			}

		}
		return rrule;

	}

	public DayOfWeek retrieveDayOfWeek(Integer nDay) {
		DayOfWeek day = null;

		switch (nDay) {

		case 1:
			day = DayOfWeek.MONDAY;
			break;
		case 2:
			day = DayOfWeek.TUESDAY;
			break;
		case 3:
			day = DayOfWeek.WEDNESDAY;
			break;
		case 4:
			day = DayOfWeek.THURSDAY;
			break;
		case 5:
			day = DayOfWeek.FRIDAY;
			break;
		case 6:
			day = DayOfWeek.SATURDAY;
			break;
		default:
			day = DayOfWeek.SUNDAY;
			break;
		}

		return day;
	}

}
