package com.springthought.service.impl;

import com.springthought.dao.UserDao;
import com.springthought.model.User;
import com.springthought.service.PasswordEncryptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service("passwordEncryptor")
public class PasswordEncryptorImpl implements PasswordEncryptor {

    protected final Log log = LogFactory.getLog(getClass());
    private UserDao userDao;
    private PasswordEncoder passwordEncoder;

    @Autowired(required = false)
    private SaltSource saltSource;

    /* (non-Javadoc)
     * @see com.springthought.service.impl.PasswordEncryptor#setPasswordEncoder(org.springframework.security.authentication.encoding.PasswordEncoder)
     */
    @Override
    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
	this.passwordEncoder = passwordEncoder;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.impl.PasswordEncryptor#setUserDao(com.springthought.dao.UserDao)
     */
    @Override
    @Autowired
    public void setUserDao(UserDao userDao) {
	this.userDao = userDao;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.impl.PasswordEncryptor#encrypt(com.springthought.model.User)
     */
    @Override
    public User encrypt(User user) {

	boolean isNew = (user.getId() == null || user.getId() == 0);

	if (isNew) {
	    user.setId(null);
	}

	// Get and prepare password management-related artifacts
	boolean passwordChanged = false;
	if (passwordEncoder != null) {
	    // Check whether we have to encrypt (or re-encrypt) the password
	    if (isNew) {
		// New user, always encrypt
		passwordChanged = true;
	    } else {
		// Existing user, check password in DB
		String currentPassword = userDao.getUserPassword(user.getId());
		if (currentPassword == null) {
		    passwordChanged = true;
		} else {
		    if (!currentPassword.equals(user.getPassword())) {
			passwordChanged = true;
		    }
		}
	    }

	    // If password was changed (or new user), encrypt it
	    if (passwordChanged) {
		if (saltSource == null) {
		    // backwards compatibility
		    user.setPassword(passwordEncoder.encodePassword(user.getPassword(), null));
		    log.warn("SaltSource not set, encrypting password w/o salt");
		} else {
		    user.setPassword(passwordEncoder.encodePassword(user.getPassword(), saltSource.getSalt(user)));
		}
	    }
	} else {
	    log.warn("PasswordEncoder not set, skipping password encryption...");
	}

	return user;

    }

}
