package com.springthought.service.impl;

import com.springthought.dao.GenericDao;
import com.springthought.model.ITenant;
import com.springthought.service.GenericManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*import javax.validation.Validation;

import javax.validation.ValidatorFactory;*/
//import org.springframework.validation.Validator;

/**
 * This class serves as the Base class for all other Managers - namely to hold
 * common CRUD methods that they might all use. You should only need to extend
 * this class when your require custom CRUD logic.
 * <p/>
 * <p>
 * To register this class in your Spring context file, use the following XML.
 *
 * <pre>
 *     &lt;bean id="userManager" class="com.springthought.service.impl.GenericManagerImpl"&gt;
 *         &lt;constructor-arg&gt;
 *             &lt;bean class="com.springthought.dao.hibernate.GenericDaoHibernate"&gt;
 *                 &lt;constructor-arg value="com.springthought.model.User"/&gt;
 *                 &lt;property name="sessionFactory" ref="sessionFactory"/&gt;
 *             &lt;/bean&gt;
 *         &lt;/constructor-arg&gt;
 *     &lt;/bean&gt;
 * </pre>
 * <p/>
 * <p>
 * If you're using iBATIS instead of Hibernate, use:
 *
 * <pre>
 *     &lt;bean id="userManager" class="com.springthought.service.impl.GenericManagerImpl"&gt;
 *         &lt;constructor-arg&gt;
 *             &lt;bean class="com.springthought.dao.ibatis.GenericDaoiBatis"&gt;
 *                 &lt;constructor-arg value="com.springthought.model.User"/&gt;
 *                 &lt;property name="dataSource" ref="dataSource"/&gt;
 *                 &lt;property name="sqlMapClient" ref="sqlMapClient"/&gt;
 *             &lt;/bean&gt;
 *         &lt;/constructor-arg&gt;
 *     &lt;/bean&gt;
 * </pre>
 *
 * @param <T>  a type variable
 * @param <PK> the primary key for that type
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Updated by
 * jgarcia: added full text search + reindexing
 */

public class GenericManagerImpl<T, PK extends Serializable> implements GenericManager<T, PK>, Serializable {
    /**
     * Log variable for all child classes. Uses LogFactory.getLog(getClass())
     * from Commons Logging
     */
    protected final Log log = LogFactory.getLog(getClass());
    private static final long serialVersionUID = 1L;
/*
    @Autowired
    private Validator validator;*/


    /**
     * GenericDao instance, set by constructor of child classes
     */
    protected GenericDao<T, PK> dao;

    public GenericManagerImpl() {
        //ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        //validator = factory.getValidator();
    }

    public GenericManagerImpl(GenericDao<T, PK> genericDao) {
        this();
        this.dao = genericDao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getAll() {
        return dao.getAll();
    }

    @Override
    public List<T> getAllDistinct() {
        return dao.getAllDistinct();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(PK id) {
        return dao.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists(PK id) {
        return dao.exists(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T save(T object) {

        if (object instanceof ITenant) {
            ((ITenant) object).setTenantId(1L);
        }

        return dao.save(object);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(T object) {
        dao.remove(object);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(PK id) {
        dao.remove(id);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Search implementation using Hibernate Search.
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> search(String q, Class clazz) {

        if (q == null || "".equals(q.trim())) {
            return getAll();
        }

        return dao.search(q);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reindex() {
        dao.reindex();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reindexAll(boolean async) {
        dao.reindexAll(async);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.service.GenericManager#doWork()
     */
    @Override
    public void doWork() throws SQLException {
        dao.doWork();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.service.GenericManager#findByNamedQuery(java.lang.
     * String, java.util.Map)
     */
    @Override
    public List<T> findByNamedQuery(String queryName, Map<String, Object> queryParams) {

        return dao.findByNamedQuery(queryName, queryParams);
    }

    @Override
    public Session getSession() throws HibernateException {
        return dao.getSession();
    }

    @Override
    public Set<ConstraintViolation<T>> validate(T object) {
        return dao.validate( object);

    }

    @Override
    public SessionFactory getSessionFactory() {
        return dao.getSessionFactory();
    }

    @Override
    public Validator getValidator() {
        return dao.getValidator();
    }


}
