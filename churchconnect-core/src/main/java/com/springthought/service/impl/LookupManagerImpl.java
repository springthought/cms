package com.springthought.service.impl;

import com.springthought.Constants.CodeListType;
import com.springthought.dao.LookupDao;
import com.springthought.model.*;
import com.springthought.service.LookupManager;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of LookupManager interface to talk to the persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("lookupManager")
public class LookupManagerImpl implements LookupManager {

    @Autowired
    LookupDao dao;

    /**
     * {@inheritDoc}
     */
    public List<LabelValue> getAllRoles() {
        List<Role> roles = dao.getRoles();
        List<LabelValue> list = new ArrayList<LabelValue>();

        for (Role role1 : roles) {
            list.add(new LabelValue(role1.getName(), role1.getName()));
        }

        return list;
    }

    public List<LabelValue> getAllHouseHoldStatues() {

        List<UserValue> userValues = dao.getHouseHoldStatus();
        List<LabelValue> list = new ArrayList<LabelValue>();

        for (UserValue uv : userValues) {
            list.add(new LabelValue(uv.getName(), uv.getUservalueId()
                    .toString()));
        }

        return list;

    }


    /* (non-Javadoc)
     * @see com.springthought.service.LookupManager#getSystemValueByCodeListType(com.springthought.Constants.CodeListType)
     */
    @Override
    public List<LabelValue> getSystemValueByCodeListType(CodeListType codeListType) {

        List<SystemValue> systemValues = dao.getSystemValueByCodeListType(codeListType);

        List<LabelValue> list = new ArrayList<LabelValue>();

        for (SystemValue sv : systemValues) {
            list.add(new LabelValue(sv.getDescr(), sv.getValue()));
        }

        return list;
    }

    /**
     *
     * @param codeListType
     * @param value
     * @param ignoreCase
     * @return
     */
    @Override
    public ILabelValue getSystemValueByValue(CodeListType codeListType, String value, boolean ignoreCase) {
        return fetchLabelValue(codeListType, value, ignoreCase, "value");
    }

    /***
     *
     * @param codeListType
     * @param description
     * @param ignoreCase
     * @return
     */
    @Override
    public ILabelValue getSystemValueByLabel(CodeListType codeListType, String description, boolean ignoreCase) {
        return fetchLabelValue(codeListType, description, ignoreCase, "descr");
    }

    /**
     *
     * @param codeListType
     * @param value
     * @param ignoreCase
     * @param propertyName
     * @return
     */
    private ILabelValue fetchLabelValue(CodeListType codeListType, String value, boolean ignoreCase, String propertyName) {

        SystemValue systemValue = null;
        ILabelValue iLabelValue = null;

        Session session = dao.getSession();


        Criteria criteria = session.createCriteria(SystemValue.class);
        Criterion valueCriterion;
        Criterion localeCriterion;
        Criterion codeListTypeCriterion;

        if (ignoreCase) {
            valueCriterion = Restrictions.eq(propertyName, value).ignoreCase();
        } else {
            valueCriterion = Restrictions.eq(propertyName, value);
        }

        localeCriterion = Restrictions.eq("locale", "en");
        codeListTypeCriterion = Restrictions.eq("code_list", codeListType );

        systemValue = (SystemValue) criteria.add(Restrictions.and(valueCriterion, localeCriterion, codeListTypeCriterion)).uniqueResult();

        return systemValue;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dao == null) ? 0 : dao.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LookupManagerImpl other = (LookupManagerImpl) obj;
        if (dao == null) {
            return other.dao == null;
        } else return dao.equals(other.dao);
    }

}
