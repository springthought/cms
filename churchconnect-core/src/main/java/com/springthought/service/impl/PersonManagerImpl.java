/**
 *
 */
package com.springthought.service.impl;

import com.springthought.model.Person;
import com.springthought.service.PersonManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author eairrick
 *
 */
public class PersonManagerImpl extends GenericManagerImpl<Person, Long> implements PersonManager {

    /**
     *
     */
    private static final long serialVersionUID = -3774763514345519849L;



    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#getAllDistinct()
     */
    @Override
    public List<Person> getAllDistinct() {
	// TODO Auto-generated method stub
	return null;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#getAll()
     */
    @Override
    public List<Person> getAll() {
	// TODO Auto-generated method stub
	return null;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#get(java.io.Serializable)
     */
    @Override
    public Person get(Long id) {
	// TODO Auto-generated method stub
	return null;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#exists(java.io.Serializable)
     */
    @Override
    public boolean exists(Long id) {
	// TODO Auto-generated method stub
	return false;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#save(java.lang.Object)
     */
    @Override
    public Person save(Person object) {
	// TODO Auto-generated method stub
	return null;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#remove(java.lang.Object)
     */
    @Override
    public void remove(Person object) {
	// TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#remove(java.io.Serializable)
     */
    @Override
    public void remove(Long id) {
	// TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#search(java.lang.String, java.lang.Class)
     */
    @Override
    public List<Person> search(String searchTerm, Class clazz) {
	// TODO Auto-generated method stub
	return null;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#reindex()
     */
    @Override
    public void reindex() {
	// TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#reindexAll(boolean)
     */
    @Override
    public void reindexAll(boolean async) {
	// TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#doWork()
     */
    @Override
    public void doWork() throws SQLException {
	// TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#findByNamedQuery(java.lang.String, java.util.Map)
     */
    @Override
    public List<Person> findByNamedQuery(String queryName, Map<String, Object> queryParams) {
	// TODO Auto-generated method stub
	return null;
    }

    /* (non-Javadoc)
     * @see com.springthought.service.GenericManager#getSession()
     */
    @Override
    public Session getSession() throws HibernateException {
	// TODO Auto-generated method stub
	return null;
    }

}
