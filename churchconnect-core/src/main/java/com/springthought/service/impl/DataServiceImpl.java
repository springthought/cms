/**
 *
 */
package com.springthought.service.impl;

import com.springthought.Constants;
import com.springthought.Constants.CategoryType;
import com.springthought.Constants.CodeListType;
import com.springthought.Constants.DataServiceType;
import com.springthought.model.*;
import com.springthought.service.DataService;
import com.springthought.service.GenericManager;
import com.springthought.service.LookupManager;
import com.springthought.util.Queries;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.*;

/**
 * @author eairrick
 */

@Service("dataService")
public class DataServiceImpl implements Serializable, DataService {

    public static final int NUMBER_OF_DAYS_AGED_EVENT = 360;
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private GenericManager<Group, Long> groupManager;
    private GenericManager<Category, Long> categoryManager;
    private GenericManager<Campaign, Long> campaignManager;
    private GenericManager<Person, Long> personManager;
    private GenericManager<Household, Long> householdManager;
    private LookupManager lookupManager;
    private GenericManager<CustomMeta, Long> customMetaManager;

    private GenericManager<GroupRole, Long> groupRoleManager;
    HashMap<String, Object> queryParams = new HashMap<String, Object>();
    private GenericManager<Location, Long> locationManager;
    private GenericManager<Resource, Long> resourceManager;
    private GenericManager<EventType, Long> eventTypeManager;
    private GenericManager<Event, Long> eventManager;

    private ArrayList<LabelValue> states = new ArrayList<>(0);

    public DataServiceImpl() {
        super();
        for (String code : Constants.listOfUSStatesCode) {
            states.add(new LabelValue(Constants.mapOfUSStates.get(code), code));
        }

        Collections.sort(states, (s1, s2) -> s2.getValue().compareTo(s1.getValue()));
        Collections.reverse(states);

    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.service.DataService#getDataList(java.lang.String)
     */
    @Override
    public List<LabelValue> fetchDataList(DataServiceType type) {

        List<LabelValue> values = new ArrayList<LabelValue>(0);

        switch (type) {

            case MEMBERTYPES:
                values = getMemberTypes();
                break;

            default:
                break;
        }
        return values;

    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getAttendanceEvents(com.
     * springthought.model.EventType)
     */
    @Override
    public List<Event> getAttendanceEvents(EventType type) {

        DateTime dateTime = new DateTime(new Date());
        dateTime = dateTime.plusDays(NUMBER_OF_DAYS_AGED_EVENT * -1);

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.EVENT_INSTANCE_IN_RANGE_BY_TYPE_START_PARM, dateTime.toDate());
        queryParams.put(Queries.EVENT_INSTANCE_IN_RANGE_BY_TYPE_EVENT_TYPE_PARM, type);

        ArrayList<Event> events = (ArrayList<Event>) eventManager
                .findByNamedQuery(Queries.EVENT_INSTANCE_IN_RANGE_BY_TYPE, queryParams);

        return events;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.springthought.webapp.action.DataService#getAttendanceTrackingTypes()
     */
    @Override
    public Constants.AttendanceTrackingType[] getAttendanceTrackingTypes() {
        return Constants.AttendanceTrackingType.values();
    }

    @Override
    public Constants.DataImportType[] getDataImportTypes() {
        return Constants.DataImportType.values();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getCampaignManager()
     */
    @Override
    public GenericManager<Campaign, Long> getCampaignManager() {
        return campaignManager;
    }

    @Override
    public List<Campaign> getCampaigns() {
        return this.campaignManager.getAllDistinct();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getCategories()
     */
    @Override
    public List<Category> getCategories() {

        List<Category> categories = categoryManager.findByNamedQuery(Queries.CATEGORY_EXLUCDE_BY_TYPE, queryParams);

        return categories;
    }

    @Override
    public List<Category> getCategoryByType(CategoryType type) {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CATEGORY_TYPE_PARM, type.toString());

        List<Category> categories = categoryManager.findByNamedQuery(Queries.CATEGORY_FIND_BY_TYPE, queryParams);

        return categories;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getCategoryClasses()
     */
    @Override
    public List<Category> getCategoryClasses() {
        List<Category> categories = getCategoryByType(CategoryType.CLASS);
        return categories;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.service.DataService#getCategoryFunds()
     */
    @Override
    public List<Category> getCategoryFunds() {

        HashMap<String, Object> queryParams = new HashMap<String, Object>();

        queryParams.put(Queries.CATEGORY_TYPE_PARM, CategoryType.FUND.toString());

        List<Category> categories = categoryManager.findByNamedQuery(Queries.CATEGORY_FIND_BY_TYPE, queryParams);

        return categories;
    }

    @Override
    public List<Category> getCategoryMinistry() {
        return getCategoryByType(CategoryType.MINISTRY);
    }

    @Override
    public List<Group> getAllGroupClasses(Long tenant_id) {
        HashMap<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put(Queries.CATEGORY_TYPE_PARM , CategoryType.CLASS.name());
        queryParams.put(Queries.TENANT_ID_PARM, tenant_id);
        List<Group> groups =  groupManager.findByNamedQuery(Queries.GROUPS_FIND_BY_CATEGORY, queryParams);
        return groups;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getCategoryManager()
     */
    @Override
    public GenericManager<Category, Long> getCategoryManager() {
        return categoryManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getCustomMetaManager()
     */
    @Override
    public GenericManager<CustomMeta, Long> getCustomMetaManager() {
        return customMetaManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getEventManager()
     */
    @Override
    public GenericManager<Event, Long> getEventManager() {
        return eventManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getEventTypeManager()
     */
    @Override
    public GenericManager<EventType, Long> getEventTypeManager() {
        return eventTypeManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getEventTypes()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<EventType> getEventTypes() {
        List<EventType> types = eventTypeManager.getAllDistinct();
        return types;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getGroupManager()
     */
    @Override
    public GenericManager<Group, Long> getGroupManager() {
        return groupManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getGroupRoleManager()
     */
    @Override
    public GenericManager<GroupRole, Long> getGroupRoleManager() {
        return groupRoleManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getGroupRoles()
     */
    @Override
    public List<GroupRole> getGroupRoles() {
        List<GroupRole> gr = this.groupRoleManager.getAllDistinct();
        return gr;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getHouseholdManager()
     */
    @Override
    public GenericManager<Household, Long> getHouseholdManager() {
        return householdManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getRooms()
     */
    @Override
    public List<Location> getLocations() {
        List<Location> locations = locationManager.getAllDistinct();
        return locations;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getLookupManager()
     */
    @Override
    public LookupManager getLookupManager() {
        return lookupManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getMemberTypes()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<LabelValue> getMemberTypes() {
        // this.setAscending(true);
        return lookupManager.getSystemValueByCodeListType(CodeListType.STATUS);
    }


    /*public List<LabelValue> getContributionMethods(){
        return lookupManager.getSystemValueByCodeListType(CodeListType.METHOD);

    }*/

    public Constants.ContributionMethodType[] getContributionMethods() {
        return Constants.ContributionMethodType.values();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getPersonManager()
     */
    @Override
    public GenericManager<Person, Long> getPersonManager() {
        return personManager;
    }

    @Override
    public List<Person> getPersons() {
        List<Person> people = personManager.getAllDistinct();
        return people;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getResourceManager()
     */
    @Override
    public GenericManager<Resource, Long> getResourceManager() {
        return resourceManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getResources()
     */
    @Override
    public List<Resource> getResources() {
        List<Resource> resources = resourceManager.getAllDistinct();
        return resources;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#getRoomManager()
     */
    @Override
    public GenericManager<Location, Long> getRoomManager() {
        return locationManager;
    }

    @Override
    public ArrayList<LabelValue> getStates() {
        return states;
    }

    @PostConstruct
    private void initScreen() {

        queryParams.put(Queries.CATEGORY_TYPE_PARM, CategoryType.CLASS.toString());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setCampaignManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setCampaignManager(@Qualifier("campaignManager") GenericManager<Campaign, Long> campaignManager) {
        this.campaignManager = campaignManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setCategoryManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setCategoryManager(@Qualifier("categoryManager") GenericManager<Category, Long> categoryManager) {
        this.categoryManager = categoryManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.springthought.webapp.action.DataService#setCustomMetaManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setCustomMetaManager(
            @Qualifier("customMetaManager") GenericManager<CustomMeta, Long> customMetaManager) {
        this.customMetaManager = customMetaManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setEventManager(com.
     * springthought .service.GenericManager)
     */
    @Override
    @Autowired
    public void setEventManager(@Qualifier("eventManager") GenericManager<Event, Long> eventManager) {
        this.eventManager = eventManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setEventTypeManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setEventTypeManager(@Qualifier("eventTypeManager") GenericManager<EventType, Long> eventTypeManager) {
        this.eventTypeManager = eventTypeManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setGroupManager(com.
     * springthought .service.GenericManager)
     */
    @Override
    @Autowired
    public void setGroupManager(@Qualifier("groupManager") GenericManager<Group, Long> groupManager) {
        this.groupManager = groupManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setGroupRoleManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setGroupRoleManager(@Qualifier("groupRoleManager") GenericManager<GroupRole, Long> groupRoleManager) {
        this.groupRoleManager = groupRoleManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setHouseholdManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setHouseholdManager(@Qualifier("householdManager") GenericManager<Household, Long> householdManager) {
        this.householdManager = householdManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setLookupManager(com.
     * springthought.service.LookupManager)
     */
    @Override
    @Autowired
    public void setLookupManager(@Qualifier("lookupManager") LookupManager lookupManager) {
        this.lookupManager = lookupManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setPersonManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setPersonManager(@Qualifier("personManager") GenericManager<Person, Long> personManager) {
        this.personManager = personManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setResourceManager(com.
     * springthought.service.GenericManager)
     */
    @Override
    @Autowired
    public void setResourceManager(@Qualifier("resourceManager") GenericManager<Resource, Long> resourceManager) {
        this.resourceManager = resourceManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.webapp.action.DataService#setRoomManager(com.
     * springthought .service.GenericManager)
     */
    @Override
    @Autowired
    public void setRoomManager(@Qualifier("locationManager") GenericManager<Location, Long> roomManager) {
        this.locationManager = roomManager;
    }



}
