package com.springthought.service.impl;

import com.springthought.Constants.MenuType;
import com.springthought.dao.SystemMenuDao;
import com.springthought.model.SystemMenu;
import com.springthought.service.SystemMenuManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service("systemMenuManager")
public class SystemMenuManagerImpl extends GenericManagerImpl<SystemMenu, Long> implements SystemMenuManager, Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private SystemMenuDao systemMenuDao;

    @Autowired
    public SystemMenuManagerImpl( SystemMenuDao systemMenuDao ) {
        super(systemMenuDao);
        this.systemMenuDao = systemMenuDao;
    }

	/* (non-Javadoc)
	 * @see com.springthought.service.SystemMenuManager#findByMenuType(com.springthought.Constants.MenuType)
	 */

	public List<SystemMenu> findByMenuType(MenuType menuType) {
			return systemMenuDao.findByMenuType(menuType);
	}


}
