package com.springthought.service.impl;

import com.springthought.dao.UserDao;
import com.springthought.model.User;
import com.springthought.service.PasswordEncryptor;
import com.springthought.service.UserExistsException;
import com.springthought.service.UserManager;
import com.springthought.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import java.util.List;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("userManager")
@WebService(serviceName = "UserService", endpointInterface = "com.springthought.service.UserService")
public class UserManagerImpl extends GenericManagerImpl<User, Long> implements UserManager, UserService {
    private PasswordEncoder passwordEncoder;

    private UserDao userDao;




    /*
     * @Autowired(required = false) private SaltSource saltSource;
     *
     * @Autowired public void setPasswordEncoder(PasswordEncoder
     * passwordEncoder) { this.passwordEncoder = passwordEncoder; }
     */
    @Autowired
    private PasswordEncryptor passwordEncryptor;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.dao = userDao;
        this.userDao = userDao;
    }

    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public PasswordEncryptor getPasswordEncryptor() {
        return passwordEncryptor;
    }

    public void setPasswordEncryptor(PasswordEncryptor passwordEncryptor) {
        this.passwordEncryptor = passwordEncryptor;
    }

    /**
     * {@inheritDoc}
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User getUser(String userId) {
        return userDao.get(new Long(userId));
    }

    /**
     * {@inheritDoc}
     */

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> getUsers() {
        return userDao.getAllDistinct();
    }

    /**
     * {@inheritDoc}
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User saveUser(User user) throws UserExistsException {

        user = passwordEncryptor.encrypt(user);
        /*
         * boolean isNew = (user.getId() == null || user.getId() == 0);
         *
         * if (isNew) { // if new user, lowercase userId
         * user.setUsername(user.getUsername().toLowerCase()); user.setId( null
         * ); }
         *
         * // Get and prepare password management-related artifacts boolean
         * passwordChanged = false; if (passwordEncoder != null) { // Check
         * whether we have to encrypt (or re-encrypt) the password if (isNew) {
         * // New user, always encrypt passwordChanged = true; } else { //
         * Existing user, check password in DB String currentPassword =
         * userDao.getUserPassword(user.getId()); if (currentPassword == null) {
         * passwordChanged = true; } else { if
         * (!currentPassword.equals(user.getPassword())) { passwordChanged =
         * true; } } }
         *
         * // If password was changed (or new user), encrypt it if
         * (passwordChanged) { if (saltSource == null) { // backwards
         * compatibility
         * user.setPassword(passwordEncoder.encodePassword(user.getPassword(),
         * null)); log.warn("SaltSource not set, encrypting password w/o salt");
         * } else {
         * user.setPassword(passwordEncoder.encodePassword(user.getPassword(),
         * saltSource.getSalt(user))); } } } else { log.warn(
         * "PasswordEncoder not set, skipping password encryption..."); }
         */

        try {
            return userDao.saveUser(user);
        } catch (Exception e) {
            log.fatal(e);
            throw new UserExistsException("User '" + user.getUsername() + "' already exists!");
        }
    }

    /**
     * {@inheritDoc}
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeUser(User user) {
        log.debug("removing user: " + user);
        userDao.remove(user);
    }

    /**
     * {@inheritDoc}
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeUser(String userId) {
        log.debug("removing user: " + userId);
        userDao.remove(new Long(userId));
    }

    /**
     * {@inheritDoc}
     *
     * @param username the login name of the human
     * @return User the populated user object
     * @throws UsernameNotFoundException thrown when username not found
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User getUserByUsername(String username) throws UsernameNotFoundException {
        return (User) userDao.loadUserByUsername(username);
    }

    public User getUserByEmail(String email) throws UsernameNotFoundException {
        return (User) userDao.loadUserByEmail(email);
    }

    /**
     * {@inheritDoc}
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> search(String searchTerm) {
        return super.search(searchTerm, User.class);
    }
}
