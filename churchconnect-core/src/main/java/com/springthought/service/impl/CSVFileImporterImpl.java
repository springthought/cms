package com.springthought.service.impl;

import com.springthought.Constants;
import com.springthought.model.Category;
import com.springthought.model.Contribution;
import com.springthought.model.Person;
import com.springthought.service.CSVFileImporter;
import org.apache.commons.collections.CollectionUtils;
import org.simpleflatmapper.csv.CsvMapperFactory;
import org.simpleflatmapper.csv.CsvParser;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class CSVFileImporterImpl implements CSVFileImporter {

    //static String FILE_NAME = "people_import_template.csv";
    //static String FILE_NAME = "people_import_template_bad_header.csv";
    //static String FILE_NAME = "category_import_template.csv";
    //static String FILE_NAME = "groups_import_template.csv";
    static String FILE_NAME = "contributions_import_template.csv";
    private ArrayList<String> csvHeader = new ArrayList<>(0);
    private ArrayList<String> objectFields = new ArrayList<>(0);


    public static void main(String[] args) {

        File file = new File(CSVFileImporterImpl.class.getClassLoader().getResource(FILE_NAME).getFile());
        Category category = new Category();
        Person person = new Person();
        Object object = new Contribution();


        CSVFileImporterImpl loader = new CSVFileImporterImpl();
        try {

            ArrayList<String> invalidFields = loader.validateHeadersInObjectField(file, object);
            invalidFields.stream().forEach(System.out::println);

            ArrayList<Object> datarow = (ArrayList<Object>) loader.loadCSVFileIntoObject(object, file);
            datarow.stream().forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * @param object
     * @param in
     * @return List of object mapped from Reader
     * @throws IOException
     */
    @Override
    public List loadCSVFileIntoObject(Object object, Reader in) throws IOException {

        ArrayList objects = new ArrayList(0);
        CsvParser
                .dsl().trimSpaces()
                .mapWith(
                        CsvMapperFactory
                                .newInstance()
                                .defaultDateFormat(Constants.DATE_FORMAT_MM_DD_YYYY)
                                .newMapper(object.getClass()))
                .forEach(in, objects::add);
        return objects;
    }


    @Override
    public List loadCSVFileIntoObject(Object object, File file) throws IOException {
        try (BufferedReader in
                     = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            return loadCSVFileIntoObject(object, in);
        }
    }

    @Override
    public ArrayList<String> retrieveObjectFields(Object object) {

        Class<?> concreteClass = object.getClass();
        Field[] fields = concreteClass.getDeclaredFields();
        objectFields = new ArrayList<>(0);

        Arrays.stream(fields).forEach(field -> {
            objectFields.add(field.getName());
        });

        return objectFields;
    }

    @Override
    public ArrayList<String> retrieveCSVFileHeaders(File file) throws IOException {
        try (BufferedReader in
                     = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            return retrieveCSVFileHeaders(in);
        }

    }

    @Override
    public ArrayList<String> retrieveCSVFileHeaders(Reader in) throws IOException {
        Iterator<String[]> itr = CsvParser.iterator(in);
        csvHeader = new ArrayList<>(Arrays.asList(itr.next()));
        return csvHeader;
    }

    public ArrayList<String> validateHeadersInObjectField(File file, Object object) throws IOException {

        try (BufferedReader in
                     = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            return validateHeadersInObjectField(in, object);
        }

    }

    public ArrayList<String> validateHeadersInObjectField(Reader in, Object object) throws IOException {

        ArrayList<String> minus= new ArrayList<>();

        try {

            ArrayList<String> headers = this.retrieveCSVFileHeaders(in);
            ArrayList<String> fields = this.retrieveObjectFields(object);

            ArrayList<String> results_headers = new ArrayList<>(0);
            ArrayList<String> results_fields = new ArrayList<>(0);

            headers.stream().map((header) -> header.toLowerCase().trim())
                    .collect(Collectors.toCollection(() -> results_headers));

            fields.stream().map((field) -> field.toLowerCase().trim())
                    .collect(Collectors.toCollection(() -> results_fields));

            minus = new ArrayList<>(CollectionUtils.subtract(results_headers, results_fields));

        } catch (IOException e) {
            throw e;
        }


        return minus;
    }


}