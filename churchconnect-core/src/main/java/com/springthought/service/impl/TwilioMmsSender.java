package com.springthought.service.impl;

import com.springthought.Constants;
import com.springthought.Constants.Status;
import com.springthought.model.MessageTemplateSupplier;
import com.springthought.model.StatusResult;
import com.springthought.service.MmsSender;
import com.springthought.util.ConvertUtil;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class TwilioMmsSender implements MmsSender {

    public static final String ACCOUNT_SID = "ACa64d9d2d2a1e81e420af26059536268b";
    public static final String AUTH_TOKEN = "bbf5685359ea24636227f6d2947b07b3";
    protected static final Log log = LogFactory.getLog(TwilioMmsSender.class);

    public static void main(String[] args) throws TwilioRestException {

	MessageTemplateSupplier sup = new MessageTemplateSupplier();
	com.springthought.model.Message msg = new com.springthought.model.Message();
	msg.setMessage("Church.View Rock!");
	sup.setMessage(msg);
	TwilioMmsSender sender = new TwilioMmsSender();
	StatusResult status = sender.sendMms("6788787621", sup);
	log.info(status.toString());

    }

    @Override
    public StatusResult sendMms(String recipient, MessageTemplateSupplier supplier) {

	TwilioRestClient client = new TwilioRestClient(Constants.ACCOUNT_SID, Constants.AUTH_TOKEN);
	Message mms = null;
	StatusResult result = new StatusResult();

	Account account = client.getAccount();

	MessageFactory messageFactory = account.getMessageFactory();

	List<NameValuePair> params = new ArrayList<NameValuePair>();

	params.add(new BasicNameValuePair("To", "+1" + recipient.trim()));
	params.add(new BasicNameValuePair("From", Constants.TWILIO_PHONE_NUMBER));
	params.add(new BasicNameValuePair("Body", ConvertUtil.html2text(supplier.getMessage().getMessage())));

	// TODO: Add ChurchView Logo
	params.add(new BasicNameValuePair("MediaUrl",
		"https://cdn.psychologytoday.com/sites/default/files/blogs/38/2008/12/2598-75772.jpg"));

	try {
	    mms = messageFactory.create(params);
	    log.info("MMS SID = " + mms.getSid());
	    result.setStatus(Status.SUC);
	    result.setResultMessage(Constants.COMMUNICATION_SUCCESS);
	} catch (TwilioRestException e) {
	    log.error("Unable to see MMS to: " + "+1" + recipient.trim(), e);
	    result.setStatus(Status.ERR);
	    result.setResultMessage(e.getErrorMessage());
	}

	return result;
    }
}
