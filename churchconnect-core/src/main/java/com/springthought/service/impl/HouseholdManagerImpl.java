package com.springthought.service.impl;

import com.springthought.dao.HouseholdDao;
import com.springthought.model.Household;
import com.springthought.service.HouseholdManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("householdManager")
public class HouseholdManagerImpl extends GenericManagerImpl<Household, Long>
		implements HouseholdManager {

	private HouseholdDao householdDao = null;

	@Autowired
	public void setHouseholdDao(HouseholdDao householdDao) {
		this.householdDao = householdDao;
		this.dao = householdDao;
	}

	@Override
	public Household saveHousehold(Household household) {
		return householdDao.saveHousehold( household );
	}

}
