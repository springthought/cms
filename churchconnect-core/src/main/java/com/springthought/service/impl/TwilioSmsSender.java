package com.springthought.service.impl;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class TwilioSmsSender {


    public static final String ACCOUNT_SID = "ACa64d9d2d2a1e81e420af26059536268b";
    public static final String AUTH_TOKEN = "bbf5685359ea24636227f6d2947b07b3";
    protected static final Log log = LogFactory.getLog(TwilioSmsSender.class);

    public static void main(String[] args) throws TwilioRestException {

        TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

        Account account = client.getAccount();

        MessageFactory messageFactory = account.getMessageFactory();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("To", "+16788787621"));
        params.add(new BasicNameValuePair("From", "+16784432132"));
        params.add(new BasicNameValuePair("Body", "SpringThought Software,Inc. Rocks!!"));
        Message sms = messageFactory.create(params);
        System.out.println(sms.getSid());
    }

}
