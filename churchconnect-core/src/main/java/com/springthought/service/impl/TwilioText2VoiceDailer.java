package com.springthought.service.impl;

import com.springthought.Constants;
import com.springthought.Constants.Status;
import com.springthought.model.MessageTemplateSupplier;
import com.springthought.model.StatusResult;
import com.springthought.service.Text2VoiceDailer;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Call;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

public class TwilioText2VoiceDailer implements Text2VoiceDailer {

    public static final String ACCOUNT_SID = "ACa64d9d2d2a1e81e420af26059536268b";
    public static final String AUTH_TOKEN = "bbf5685359ea24636227f6d2947b07b3";
    protected static final Log log = LogFactory.getLog(TwilioMmsSender.class);

    private TwilioRestClient client = null;
    private Account mainAccount = null;

    public synchronized TwilioRestClient getClient() {
	return client;
    }

    public synchronized void setClient(TwilioRestClient client) {
	this.client = client;
    }

    public synchronized Account getMainAccount() {
	return mainAccount;
    }

    public synchronized void setMainAccount(Account mainAccount) {
	this.mainAccount = mainAccount;
    }

    public static void main(String[] args) throws TwilioRestException {

	TwilioText2VoiceDailer dailer = new TwilioText2VoiceDailer();
	//dailer.connect();
	MessageTemplateSupplier sup = new MessageTemplateSupplier();
	com.springthought.model.Message msg = new com.springthought.model.Message();
	msg.setMessageId(-1L);
	msg.setMessage("Church.View Rock!");
	sup.setMessage(msg);
	dailer.makeCall("6788787621", sup);
    }

    @Override
    public StatusResult makeCall(String phoneNumber, MessageTemplateSupplier supplier) {

	StatusResult result = new StatusResult();
	CallFactory callFactory = getMainAccount().getCallFactory();
	Call call = null;
	Map<String, String> callParams = new HashMap<String, String>();

	phoneNumber = "+1" + phoneNumber;


	callParams.put("To", phoneNumber);
	callParams.put("From", Constants.TWILIO_PHONE_NUMBER);
	callParams.put("Url", "http://a154846b.ngrok.io/ChurchClick/msgs/" + supplier.getMessage().getMessageId());
	callParams.put("StatusCallback", "http://a154846b.ngrok.io/ChurchClick/status");

	try {
	    call = callFactory.create(callParams);
	    log.info("Dailed:" + phoneNumber + " SID:" + call.getSid());
	    result.setStatus(Status.SUC);
	    result.setResultMessage(Constants.COMMUNICATION_SUCCESS);
	    result.setMeta(call.getSid());
	} catch (TwilioRestException e) {
	    log.error("Unable to call phone number:" + phoneNumber, e);
	    result.setStatus(Status.ERR);
	    result.setResultMessage(e.getErrorMessage());
	}
	return result;
    }

    @Override
    public void connect() {
	client = new TwilioRestClient(Constants.ACCOUNT_SID, Constants.AUTH_TOKEN);
	mainAccount = client.getAccount();
    }

}
