package com.springthought.service.impl;


import com.springthought.Constants.UserValueType;
import com.springthought.dao.UserValueDao;
import com.springthought.model.UserValue;
import com.springthought.service.UserValueManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service("userValueManager")
public class UserValueManagerImpl extends GenericManagerImpl<UserValue, Long> implements  UserValueManager, Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private UserValueDao userValueDao;

	@Autowired
	public UserValueManagerImpl(UserValueDao uvdao) {
		super(uvdao);
		this.userValueDao = uvdao;
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<UserValue> getUserValueByType(UserValueType userValuetype) {
		return this.userValueDao.getUserValueByType( userValuetype );
	}


}
