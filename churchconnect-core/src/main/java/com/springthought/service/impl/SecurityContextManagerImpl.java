/**
 *
 */
package com.springthought.service.impl;

import com.springthought.model.User;
import com.springthought.service.SecurityContextManager;
import com.springthought.util.FacesUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;


/**
 * @author eairrick
 *
 */

@Service("securityContext")
public class SecurityContextManagerImpl implements SecurityContextManager {

	private String springSecurityContextKey = HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;
	protected final Log log = LogFactory.getLog(getClass());

	/*
	 * (non-Javadoc)
	 *
	 * @see com.springthought.service.impl.SecurityContextManager#getContext()
	 */
	@Override
	public SecurityContext getContext() {

		SecurityContext context = null;

		try {
			context = readSecurityContextFromSession(FacesUtils
					.getHttpSession(false));
		} catch (NullPointerException e) {
			// FacesContext.getCurrentInstance().getExternalContext() is null
			// must be running this as a junit test
			context = null;
		} catch (Exception e) {
			throw e;
		}

		if (context != null) {
			User user = (User) context.getAuthentication().getPrincipal();
			log.debug(user.toString());
		}

		return context;
	}

	/**
	 *
	 * Project: framework Method : readSecurityContextFromSession Return :
	 * SecurityContext Params : @param httpSession Params : @return Author :
	 * eairrick
	 *
	 */
	private SecurityContext readSecurityContextFromSession(
			HttpSession httpSession) {

		if (httpSession == null) {
			log.debug("No HttpSession currently exists");
			return null;
		}

		// Session exists, so try to obtain a context from it.

		Object contextFromSession = httpSession
				.getAttribute(springSecurityContextKey);

		if (contextFromSession == null) {
			log.debug("HttpSession returned null object for SPRING_SECURITY_CONTEXT");
			return null;
		}

		// We now have the security context object from the session.
		if (!(contextFromSession instanceof SecurityContext)) {
			log.warn(springSecurityContextKey
					+ " did not contain a SecurityContext but contained: '"
					+ contextFromSession
					+ "'; are you improperly modifying the HttpSession directly "
					+ "(you should always use SecurityContextHolder) or using the HttpSession attribute "
					+ "reserved for this class?");
			return null;
		}

		log.debug("Obtained a valid SecurityContext from "
				+ springSecurityContextKey + ": '" + contextFromSession + "'");

		// Everything OK. The only non-null return from this method.

		return (SecurityContext) contextFromSession;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.springthought.service.impl.SecurityContextManager#retriveTenantContext
	 * ()
	 */
	@Override
	public Long retriveTenantContext() {

		SecurityContext context = getContext();
		Long tenantId = null;

		if (context != null) {
			User user = (User) context.getAuthentication().getPrincipal();
			tenantId = user.getTenantId();
			log.debug("Setting Tenant ID: " + tenantId + " for User :"
					+ user.getUsername());
		} else {
			// context not found; set TeantId to Default test client id
			// this should only happen in JUNIT testing.
			tenantId = DEFAULT_TEST_CLIENT;
			log.debug("Security context not found. Defaulting ID to test client...");
		}
		return tenantId;
	}

	public User retrivePrincipal() {

		SecurityContext context = getContext();

		User user;

		if (context != null) {
			user = (User) context.getAuthentication().getPrincipal();
			log.debug("Retrive User:" + user.getUsername());
		} else {
			log.debug("Security context not found.");
			user = null;
		}
		return user;
	}

}
