package com.springthought.service.impl;


import com.springthought.Constants.CodeListType;
import com.springthought.dao.SystemValueDao;
import com.springthought.model.SystemValue;
import com.springthought.service.SystemValueManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("systemValueManager")
public class SystemValueManagerImpl extends GenericManagerImpl<SystemValue, Long> implements  SystemValueManager {


	private SystemValueDao systemValueDao;

	@Autowired
	public SystemValueManagerImpl(SystemValueDao svdao) {
		super(svdao);
		this.systemValueDao = svdao;
	}


	/* (non-Javadoc)
	 * @see com.springthought.service.SystemValueManager#getSystemValueByCodeListType(com.springthought.Constants.CodeListType)
	 */
	@Override
	public List<SystemValue> getSystemValueByCodeListType(CodeListType codelist) {
		return this.systemValueDao.getSystemValueByCodeListType(codelist);
	}


}
