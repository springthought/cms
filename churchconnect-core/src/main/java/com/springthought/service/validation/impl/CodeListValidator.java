package com.springthought.service.validation.impl;

import com.springthought.model.ILabelValue;
import com.springthought.service.LookupManager;
import com.springthought.service.validation.CodeList;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Service
public class CodeListValidator implements ConstraintValidator<CodeList, String> {

    private CodeList annotation;

    @Autowired
    private LookupManager lookupManager;


    @Override
    public void initialize(CodeList annotation) {
        this.annotation = annotation;
    }

    @Override
    public boolean isValid(String typeToLookup, ConstraintValidatorContext constraintValidatorContext) {

        boolean result = true;

        if (StringUtils.isNotBlank(typeToLookup)) {
            ILabelValue iLabelValue = lookupManager.getSystemValueByValue(annotation.codeListType(), typeToLookup, true);
            result = (iLabelValue != null);
        }
        return result;
    }



}
