package com.springthought.service.validation;

import com.springthought.Constants.CodeListType;
import com.springthought.service.validation.impl.CodeListValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Constraint(validatedBy = {CodeListValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface CodeList {

    String message() default "{com.springthought.service.validation.CodeList.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    CodeListType codeListType() default CodeListType.STAGEOFLIFE ;
    boolean ignoreCase() default false;
}
