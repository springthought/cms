package com.springthought.service;

import com.springthought.dao.UserDao;
import com.springthought.model.User;
import org.springframework.security.authentication.encoding.PasswordEncoder;

public interface PasswordEncryptor {

    void setPasswordEncoder(PasswordEncoder passwordEncoder);

    void setUserDao(UserDao userDao);

    User encrypt(User user);

}
