/**
 * 
 */
package com.springthought.service;

import com.springthought.model.Person;

/**
 * @author eairrick
 *
 */
public interface PersonManager extends GenericManager<Person, Long> {

}
