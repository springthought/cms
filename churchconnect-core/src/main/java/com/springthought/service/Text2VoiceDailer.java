package com.springthought.service;


import com.springthought.model.MessageTemplateSupplier;
import com.springthought.model.StatusResult;

public interface Text2VoiceDailer {
    public StatusResult makeCall(String phoneNumber, MessageTemplateSupplier supplier);
    public void connect();
}
