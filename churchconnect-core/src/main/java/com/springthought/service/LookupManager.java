package com.springthought.service;

import com.springthought.Constants.CodeListType;
import com.springthought.model.ILabelValue;
import com.springthought.model.LabelValue;

import java.util.List;

/**
 * Business Service Interface to talk to persistence layer and
 * retrieve values for drop-down choice lists.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public interface LookupManager {
    /**
     * Retrieves all possible roles from persistence layer
     * @return List of LabelValue objects
     */
    List<LabelValue> getAllRoles();
    List<LabelValue> getAllHouseHoldStatues();
    List<LabelValue> getSystemValueByCodeListType(CodeListType codeListType);
    ILabelValue getSystemValueByValue(CodeListType codeListType, String value, boolean ignoreCase);
    ILabelValue getSystemValueByLabel(CodeListType codeListType, String description, boolean ignoreCase);
}

