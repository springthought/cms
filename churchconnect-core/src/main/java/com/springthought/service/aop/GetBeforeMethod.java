/**
 *
 */
package com.springthought.service.aop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @author eairrick
 *
 */
public class GetBeforeMethod implements MethodBeforeAdvice {

	protected final Log log = LogFactory.getLog(getClass());

	/* (non-Javadoc)
	 * @see org.springframework.aop.MethodBeforeAdvice#before(java.lang.reflect.Method, java.lang.Object[], java.lang.Object)
	 */


	@Override
	public void before(Method method, Object[] args, Object target)
			throws Throwable {

		log.debug("GetBeforeMethod : Before method hijacked!");

	}

}
