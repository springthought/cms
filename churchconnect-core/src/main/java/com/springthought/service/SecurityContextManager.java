/**
 *
 */
package com.springthought.service;

import com.springthought.model.User;
import org.springframework.security.core.context.SecurityContext;

/**
 * @author eairrick
 *
 */
public interface SecurityContextManager {

	public static final Long DEFAULT_TEST_CLIENT = 1L;

	public SecurityContext getContext();

	public User retrivePrincipal();

	public Long retriveTenantContext();

}
