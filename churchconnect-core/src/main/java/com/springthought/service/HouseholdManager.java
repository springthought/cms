package com.springthought.service;

import com.springthought.model.Household;

public interface HouseholdManager extends GenericManager<Household,  Long> {
	
	public Household saveHousehold( Household household );

}
