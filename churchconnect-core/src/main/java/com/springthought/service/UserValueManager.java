package com.springthought.service;

import com.springthought.Constants.UserValueType;
import com.springthought.model.UserValue;

import java.util.List;

public interface UserValueManager extends GenericManager<UserValue, Long> {

	public List<UserValue> getUserValueByType(UserValueType userValuetype);

}
