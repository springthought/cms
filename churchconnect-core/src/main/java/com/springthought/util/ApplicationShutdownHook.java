/**
 * 
 */
package com.springthought.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author eairrick
 * 
 */
public class ApplicationShutdownHook {
	
    protected final Log log = LogFactory.getLog(getClass());
	
	public void attachShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				log.info("Inside Add Shutdown Hook");
			}
		});
		log.info("Shut Down Hook Attached.");
	}
}