/**
 *
 */
package com.springthought.util;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.util.Map;


public class ReportConfigUtil {

	/**
	 * PRIVATE METHODS
	 */
	private static void setCompileTempDir(ServletContext context, String uri) {
		System.setProperty("jasper.reports.compile.temp",
				context.getRealPath(uri));
	}

	/**
	 * PUBLIC METHODS
	 */
	public static boolean compileReport(ServletContext context,
			String compileDir, String filename) throws JRException {

		String jasperFileName = context.getRealPath(compileDir + filename
				+ ".jasper");

		File jasperFile = new File(jasperFileName);

		if (jasperFile.exists()) {
			return true; // jasper file already exists, do not compile again
		}
		try {
			// jasper file has not been constructed yet, so compile the xml file
			setCompileTempDir(context, compileDir);

			String xmlFileName = jasperFileName.substring(0,
					jasperFileName.indexOf(".jasper"))
					+ ".jrxml";
			JasperCompileManager.compileReportToFile(xmlFileName);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static JasperPrint fillReport(File reportFile,
			Map<String, Object> parameters, Connection conn) throws JRException {

		parameters.put("BaseDir", reportFile.getParentFile());

		JasperPrint jasperPrint = JasperFillManager.fillReport(
				reportFile.getPath(), parameters, conn);

		return jasperPrint;
	}

	public static String getJasperFilePath(ServletContext context,
			String compileDir, String jasperFile) {
		return context.getRealPath(compileDir + jasperFile);
	}



	public static void exportReportAsRTF(JasperPrint jasperPrint,
			HttpServletResponse response) throws JRException, FileNotFoundException,
			IOException {

		response.setContentType("application/rtf");
		response.setHeader("Content-disposition","attachment; filename=directoryList.rtf");

		JRRtfExporter exporterRTF = new JRRtfExporter();
		exporterRTF.setExporterInput( new SimpleExporterInput(jasperPrint));
		exporterRTF.setExporterOutput(new SimpleWriterExporterOutput( response.getOutputStream()));
		SimpleRtfReportConfiguration configuration = new SimpleRtfReportConfiguration();
		configuration.setOverrideHints(Boolean.TRUE);

		exporterRTF.setConfiguration(configuration);

		exporterRTF.exportReport();

		response.getOutputStream().close();

	}

	public static void exportReportAsExcel(JasperPrint jasperPrint,
			HttpServletResponse response) throws JRException, FileNotFoundException,
			IOException {

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition","attachment; filename=directoryList.xls");

		JRXlsExporter exporterXLS = new JRXlsExporter();


		exporterXLS.setExporterInput( new SimpleExporterInput(jasperPrint));
		exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput( response.getOutputStream()) );
		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
		configuration.setOnePagePerSheet(Boolean.FALSE);
		configuration.setDetectCellType(Boolean.TRUE);
		configuration.setWhitePageBackground(Boolean.FALSE);
		configuration.setRemoveEmptySpaceBetweenRows(Boolean.TRUE);
		exporterXLS.setConfiguration(configuration);
		exporterXLS.exportReport();

		response.getOutputStream().close();
	}

	public static void exportReportAsPDF(JasperPrint jasperPrint,
			HttpServletResponse response) throws IOException, JRException {

		response.setContentType("application/x-pdf");
		response.setHeader("Content-disposition","attachment; filename=directoryList.pdf");
		JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
		response.getOutputStream().close();

	}
}
