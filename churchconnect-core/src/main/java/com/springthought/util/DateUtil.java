package com.springthought.util;

import com.springthought.Constants;
import io.lamma.DayOfWeek;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Date Utility Class used to convert Strings to Dates and Timestamps
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a> to correct time
 *         pattern. Minutes should be mm not MM (MM is month).
 */
public final class DateUtil {
	private static Log log = LogFactory.getLog(DateUtil.class);
	private static final String TIME_PATTERN = "HH:mm a";

	/**
	 * Checkstyle rule: utility classes should not have public constructor
	 */
	private DateUtil() {
	}

	/**
	 * Return default datePattern (MM/dd/yyyy)
	 *
	 * @return a string representing the date pattern on the UI
	 */
	public static String getDatePattern() {
		Locale locale = LocaleContextHolder.getLocale();
		String defaultDatePattern;
		try {
			defaultDatePattern = ResourceBundle.getBundle(Constants.BUNDLE_KEY,
					locale).getString("date.format");
		} catch (MissingResourceException mse) {
			defaultDatePattern = "MM/dd/yyyy";
		}

		return defaultDatePattern;
	}

	public static String getDateTimePattern() {
		return DateUtil.getDatePattern() + " hh:mm:ss a";
	}

	public static String getTimePattern(){
		return " "+ TIME_PATTERN;
	}

	/**
	 * This method attempts to convert an Oracle-formatted date in the form
	 * dd-MMM-yyyy to mm/dd/yyyy.
	 *
	 * @param aDate
	 *            date from database as a string
	 * @return formatted string for the ui
	 */
	public static String getDate(Date aDate) {
		SimpleDateFormat df;
		String returnValue = "";

		if (aDate != null) {
			df = new SimpleDateFormat(getDatePattern());
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * This method generates a string representation of a date/time in the
	 * format you specify on input
	 *
	 * @param aMask
	 *            the date pattern the string is in
	 * @param strDate
	 *            a string representation of a date
	 * @return a converted Date object
	 * @throws ParseException
	 *             when String doesn't match the expected format
	 * @see java.text.SimpleDateFormat
	 */
	public static Date convertStringToDate(String aMask, String strDate)
			throws ParseException {
		SimpleDateFormat df;
		Date date;
		df = new SimpleDateFormat(aMask);

		if (log.isDebugEnabled()) {
			log.debug("converting '" + strDate + "' to date with mask '"
					+ aMask + "'");
		}

		try {
			date = df.parse(strDate);
		} catch (ParseException pe) {
			// log.error("ParseException: " + pe);
			throw new ParseException(pe.getMessage(), pe.getErrorOffset());
		}

		return (date);
	}

	/**
	 * This method returns the current date time in the format: MM/dd/yyyy HH:MM
	 * a
	 *
	 * @param theTime
	 *            the current time
	 * @return the current date/time
	 */
	public static String getTimeNow(Date theTime) {
		return getDateTime(TIME_PATTERN, theTime);
	}

	/**
	 * This method returns the current date in the format: MM/dd/yyyy
	 *
	 * @return the current date
	 * @throws ParseException
	 *             when String doesn't match the expected format
	 */
	public static Calendar getToday() throws ParseException {
		Date today = new Date();
		SimpleDateFormat df = new SimpleDateFormat(getDatePattern());

		// This seems like quite a hack (date -> string -> date),
		// but it works ;-)
		String todayAsString = df.format(today);
		Calendar cal = new GregorianCalendar();
		cal.setTime(convertStringToDate(todayAsString));

		return cal;
	}

	/**
	 * This method generates a string representation of a date's date/time in
	 * the format you specify on input
	 *
	 * @param aMask
	 *            the date pattern the string is in
	 * @param aDate
	 *            a date object
	 * @return a formatted string representation of the date
	 * @see java.text.SimpleDateFormat
	 */
	public static String getDateTime(String aMask, Date aDate) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (aDate == null) {
			log.warn("aDate is null!");
		} else {
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * This method generates a string representation of a date based on the
	 * System Property 'dateFormat' in the format you specify on input
	 *
	 * @param aDate
	 *            A date to convert
	 * @return a string representation of the date
	 */
	public static String convertDateToString(Date aDate) {
		return getDateTime(getDatePattern(), aDate);
	}

	/**
	 * This method set the hour of a date field
	 *
	 *
	 *
	 * @param date
	 *            to be add set hour
	 * @param hour
	 *            24 hour
	 * @return
	 *
	 *
	 */

	public static Date setHour(Date date, Integer hour) {

		Calendar time = Calendar.getInstance();
		time.setTime(date);
		time.set(Calendar.HOUR_OF_DAY, hour);
		return time.getTime();
	}

	/**
	 *
	 * @param date
	 * @return
	 */
	public static Integer getHour(Date date) {
		Calendar time = Calendar.getInstance();
		time.setTime(date);
		return time.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 *
	 * @param date
	 * @return
	 */

	public static Integer getMinute(Date date) {
		Calendar time = Calendar.getInstance();
		time.setTime(date);
		time.get(Calendar.MINUTE);
		return time.get(Calendar.MINUTE);
	}

	/**
	 * This method set the minute on a date
	 *
	 * @param date
	 * @param minute
	 * @return
	 *
	 */

	public static Date setMinute(Date date, Integer minute) {
		Calendar time = Calendar.getInstance();
		time.setTime(date);
		time.set(Calendar.MINUTE, minute);
		return time.getTime();
	}

	/**
	 * This method converts a String to a date using the datePattern
	 *
	 * @param strDate
	 *            the date to convert (in format MM/dd/yyyy)
	 * @return a date object
	 * @throws ParseException
	 *             when String doesn't match the expected format
	 */
	public static Date convertStringToDate(final String strDate)
			throws ParseException {
		return convertStringToDate(getDatePattern(), strDate);
	}

	/**
	 *
	 * @ testDate @ start Date @ end Date @ return boolean
	 */

	public static boolean isDateWithinRange(Date testDate, Date startDate,
			Date endDate) {
		return testDate.getTime() >= startDate.getTime()
				&& testDate.getTime() <= endDate.getTime();
	}

	public static DayOfWeek retrieveDayOfWeek(Integer nDay) {
		DayOfWeek day = null;

		switch (nDay) {

		case 1:
			day = DayOfWeek.SUNDAY;
			break;
		case 2:
			day = DayOfWeek.MONDAY;
			break;
		case 3:
			day = DayOfWeek.TUESDAY;
			break;
		case 4:
			day = DayOfWeek.WEDNESDAY;
			break;
		case 5:
			day = DayOfWeek.THURSDAY;
			break;
		case 6:
			day = DayOfWeek.FRIDAY;
			break;
		case 7:
			day = DayOfWeek.SATURDAY;
			break;
		default:
			day = DayOfWeek.SUNDAY;
			break;
		}

		return day;
	}

}
