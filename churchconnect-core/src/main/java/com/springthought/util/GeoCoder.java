/**
 * 
 */
package com.springthought.util;

import com.springthought.Constants.GeocodingStatusCode;
import com.springthought.model.Address;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.primefaces.model.map.LatLng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author eairrick
 * 
 */
public class GeoCoder {

	protected final Log log = LogFactory.getLog(getClass());
	private static String SERVICE_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=";
	private static String SERVICE_OPTION = "&sensor=true";
	private static String API_KEY="AIzaSyAUMDmceOQu15BwieTACvTvShq0jI2z2vE";
	private Address address = null;
	public GeocodingStatusCode status = GeocodingStatusCode.ZERO_RESULTS;
	LatLng latlng = null;


	public static void main(String[] args) {

		GeoCoder geo = new GeoCoder();
		Address address = new Address();

		address.setAddress("3342 Perservation Circle");
		address.setCity("Lilburn");
		address.setProvince("GA");
		address.setPostalCode("30042");

		geo.retrieveAddressLatitudeAndLongitude(address);
	}

	public GeoCoder() {

		log.debug("GeoCoder()");

	}

	/**
	 * 
	 * Project: framework
	 * Method : retrieveAddressLatitudeAndLongitude
	 * Return : Address
	 * Params : @param address
	 * Params : @return
	 * Author : eairrick
	 *
	 */
	public LatLng retrieveAddressLatitudeAndLongitude(Address address) {

		URL url;
		this.address = address;
		BufferedReader reader = null;
		

		try {

			String parms = (address.getAddress() + ",+" + address.getCity()
					+ ",+" + address.getProvince()).replace(" ", "+");

			url = new URL(SERVICE_URL + parms + SERVICE_OPTION+"&key="+API_KEY);

			log.debug(url);
			log.debug(parms);

			InputStream is = url.openStream();
			JSONParser parser = new JSONParser();

			reader = new BufferedReader(new InputStreamReader(is));

			Object obj = parser.parse(reader);

			JSONObject jsonObject = (JSONObject) obj;
			setStatus(GeocodingStatusCode.valueOf((String) jsonObject.get("status")));
			log.info("RESTFul Service call status:" + this.getStatus());

			processJSON(jsonObject);

		} catch (MalformedURLException e) {
			setStatus(GeocodingStatusCode.UNKNOWN_ERROR);
			log.fatal(e);
		} catch (IOException e) {
			setStatus(GeocodingStatusCode.UNKNOWN_ERROR);
			log.fatal(e);
		} catch (ParseException e) {
			setStatus(GeocodingStatusCode.UNKNOWN_ERROR);
			log.fatal(e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				log.debug(e);
			}

		}

		return latlng;

	}
	/**
	 * 
	 * Project: framework
	 * Method : processJSON
	 * Return : void
	 * Params : @param jsonObject
	 * Author : eairrick
	 *
	 */
	private void processJSON(JSONObject jsonObject) {

		JSONArray results = (JSONArray) jsonObject.get("results");
     
		for (int i = 0; i < results.size(); i++) {

			JSONObject json = (JSONObject) results.get(i);
			JSONObject geometry = (JSONObject) json.get("geometry");
			JSONObject location = (JSONObject) geometry.get("location");

			Double lat = (Double) location.get("lat");
			Double lng = (Double) location.get("lng");
			
			
			this.latlng = new LatLng(lat, lng);
		

			log.debug("Geometry Object:" + geometry);
			log.debug("Location Object:" + location);
			log.debug("Latitude: " + lat);
			log.debug("Longitude: " + lng);
			
		   
			

			/**
			 * JSONArray componet = (JSONArray)json.get("address_components");
			 * 
			 * for (Object object : componet) { log.info(object.toString()); }
			 * 
			 */
		}
		
		
	}

	/**
	 * @return the status
	 */
	public GeocodingStatusCode getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(GeocodingStatusCode status) {
		this.status = status;
	}

}
