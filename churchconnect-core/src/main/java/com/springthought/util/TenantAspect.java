/**
 *
 */
package com.springthought.util;

import com.springthought.dao.GenericDao;
import com.springthought.service.SecurityContextManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author eairrick
 *
 */
@Aspect
public class TenantAspect {

    protected final Log log = LogFactory.getLog(getClass());
    @SuppressWarnings("rawtypes")
    protected GenericDao dao;

    @Autowired
    SecurityContextManager securityContext;

    @SuppressWarnings("rawtypes")
    @Around("execution(* com.springthought.dao.GenericDao.get*(..))")
    public Object tenantBeforeinvoke(ProceedingJoinPoint joinPoint) throws Throwable {

        long startTime = System.currentTimeMillis();
        log.info("Before joinpoint = " + joinPoint.getSignature().getName());

        if (joinPoint.getTarget() instanceof GenericDao) {

            ((GenericDao) joinPoint.getTarget()).getSession().enableFilter("tenantFilter")
                    .setParameter("tenantFilterParam", securityContext.retriveTenantContext());
        }

        Object result = joinPoint.proceed();

        log.info("After joinpoint = " + joinPoint.getSignature().getName());

        if (joinPoint.getTarget() instanceof GenericDao) {

            ((GenericDao) joinPoint.getTarget()).getSession().disableFilter("tenantFilter");
        }

        long estimatedTime = System.currentTimeMillis() - startTime;

        log.info(joinPoint.getSignature().getName() + "() Total time: " + estimatedTime + "ms");

        return result;

    }


}
