
package com.springthought.util;


/**
 * Constant interface for named Hibernate queries.
 * 

 * 
 */
public interface Queries {

	public static final String NOTE_FIND_BY_PERSON_ID = "Note.findByPersonId";
	public static final String NOTE_FIND_BY_PERSON_ID_PARM = "personId";
	
	public static final String HOUSEHOLD_FIND_BY_NAME = "Household.findByName";
	public static final String HOUSEHOLD_NAME = "name";
	
	public static final String CUSTOMVALUES_FIND_BY_FIELD_ID = "CustomValues.ByFieldID";
	public static final String CUSTOMVALUES_FIND_BY_FIELD_ID_PARM = "custommeta_id";
	
	public static final String CUSTOMMETA_FIND_BY_NAME = "CustomMeta.findByName";
	public static final String CUSTOMMETA_NAME = "name";
	
	public static final String CUSTOMMETA_CHECK_EXISTS = "CustomMeta.CheckExists";
	public static final String CUSTOMMETA_CHECK_EXISTS_USAGE_PARM ="type";
	public static final String CUSTOMMETA_CHECK_EXISTS_PERSONID_PARM ="id";
	
	public static final String CATEGORY_FIND_BY_TYPE = "Category.findByType";
	public static final String CATEGORY_EXLUCDE_BY_TYPE = "Category.excludeByType";
	public static final String CATEGORY_TYPE_PARM = "type";
	
	
	public static final String GROUP_FIND_CATEGORY_ID = "Group.findByCategoryID";
	public static final String CATEGORY_ID_PARM = "id";
	
	public static final String EVENT_IS_PARENT = "Event.parentIsNull";
	public static final String EVENT_IS_PARENT_IN_RANGE = "Event.parentIsNull.dates";
	public static final String EVENT_START_PARM = "startDte";
	
	public static final String EVENT_INSTANCE_IN_RANGE_BY_TYPE = "event.findByType";
	public static final String EVENT_INSTANCE_IN_RANGE_BY_TYPE_START_PARM = "startDte";
	public static final String EVENT_INSTANCE_IN_RANGE_BY_TYPE_EVENT_TYPE_PARM = "type";
	
	public static final String FIRST_EVENT_TYPE = "eventType.findFirst";
	
	public static final String EVENT_MIN_DATE_FIND_BY_PARENT_ID ="Event.MinDateFindByParentID";
	public static final String EVENT_MIN_DATE_FIND_BY_PARENT_ID_PARM = "eventid";
	
	
	public static final String CONTRIBUTION_FIND_BY_BATCH ="Contribution.findbatchid";
	public static final String CONTRIBUTION_BY_BATCH_ID_PARM = "batchid";
	
	public static final String CONTRIBUTION_SUM_BY_CATEGORY ="Contribution.sumCategory";
	public static final String CONTRIBUTION_SUM_BY_CATEGORY_ID_PARM = "category";
	
	public static final String CONNECT_BY_UUID ="connect.findByUUID";
	public static final String CONNECT_ID_PARM = "uuid";
	public static final String USER_BY_CONFIRM_ID ="user.findByconfirmationID";
	public static final String USER_CONFIRM_ID_PARM = "confirmationId";
	
	public static final String ATTENDANCE_BY_PERSONID ="attendance.findByPersonID";
	public static final String ATTENDANCE_PERSON_ID_PARM = "personID";
	
	public static final String ROLE_BY_TENANT = "role.findByNameAndTenantID";
	public static final String NAME_PARM ="name";
	public static final String TENANT_ID_PARM ="tenantId";

	public static final String GROUPS_FIND_BY_CATEGORY = "groups.findByCategory";
	public static final String CATEGORY_PARM = "category";

}
