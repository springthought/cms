package com.springthought.util;

import io.lamma.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Utility class to generate calender event dates
 *
 */
public final class EventDateRepeaterUtil {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(EventDateRepeaterUtil.class);

    private EventDateRepeaterUtil() {
    }

    public static List<Date> daily(Date startDate, Date endDate, boolean isWeekDaysOnly, Integer repeatEvery) {

	if (isWeekDaysOnly) {
	    return Dates.from(startDate).to(endDate).by(repeatEvery).except(HolidayRules.weekends()).build();
	} else {
	    return Dates.from(startDate).to(endDate).by(repeatEvery).build();
	}
    }

    public static List<Date> weekly(Date startDate, Date endDate, Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byWeeks(repeatEvery).build();
    }


    public static List<Date> monthly(Date startDate, Date endDate, Integer nthDay, Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byMonths(repeatEvery).on(Locators.nthDay(nthDay)).build();
    }

    public static List<Date> firstDayMonthly(Date startDate, Date endDate, DayOfWeek dayOfWeek, Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byMonths(repeatEvery).on(Locators.first(dayOfWeek)).build();

    }

    public static List<Date> lastDayMonthly(Date startDate, Date endDate, DayOfWeek dayOfWeek, Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byMonths(repeatEvery).on(Locators.last(dayOfWeek)).build();

    }

    public static List<Date> nthMonthly(Date startDate, Date endDate, Integer nthDay, DayOfWeek dayOfWeek,
	    Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byMonths(repeatEvery).on(Locators.nth(nthDay, dayOfWeek)).build();
    }

    public static List<Date> nthDayMonthly(Date startDate, Date endDate, Integer nthDay, Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byMonths(repeatEvery).on(Locators.nthDay(nthDay)).build();
    }

    /**
     *
     * Project: framework Method : yearly operations
     *
     */
    public static List<Date> yearly(Date startDate, Date endDate, Integer repeatEvery) {
	return Dates.from(startDate).to(endDate).byYear().build();
    }

    /**
     *
     * Project: framework Method : firstDayYearly Return : List<Date>
     *
     * @param startDate
     * @param endDate
     * @param dayOfWeek
     * @return Author : eairrick
     *
     */
    public static List<Date> firstDayYearly(Date startDate, Date endDate, DayOfWeek dayOfWeek, Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byYears(repeatEvery)
		.on(Locators.first(dayOfWeek).of(startDate.month())).build();

    }

    public static List<Date> lastDayYearly(Date startDate, Date endDate, DayOfWeek dayOfWeek, Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byYears(repeatEvery).on(Locators.last(dayOfWeek).of(startDate.month()))
		.build();

    }

    public static List<Date> nthYearly(Date startDate, Date endDate, Integer nthDay, DayOfWeek dayOfWeek,
	    Integer repeatEvery) {

	return Dates.from(startDate).to(endDate).byYears(repeatEvery)
		.on(Locators.nth(nthDay, dayOfWeek).of(startDate.month())).build();

    }

    public static java.util.Date convertTz(java.util.Date date, TimeZone tz) {

	GregorianCalendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
	c.setTime(date);
	c.setTimeZone(TimeZone.getTimeZone(tz.getID()));
	c.get(Calendar.DATE);

	return c.getTime();
    }

}
