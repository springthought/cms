/**
 * 
 */
package com.springthought.util;

import org.springframework.stereotype.Service;




/**
 * @author eairrick
 * 
 */

@Service("simpleServiceBean")
public class SimpleService implements com.springthought.util.Service {

	private String name = "Auto Wired Serive use AOP";

	private int id = 567890;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#getId()
	 */
	@Override
	public int getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#setId(int)
	 */
	@Override
	public void setId(int id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#printNameId()
	 */
	@Override
	public void printNameId() {
		System.out.println("SimpleService : Method printNameId() : My name is "
				+ name + " and my id is " + id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#checkName()
	 */
	@Override
	public void checkName() {
		if (name.length() < 20) {
			throw new IllegalArgumentException();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#sayHello(java.lang.String)
	 */
	@Override
	 

	public void sayHello(String message) {
		System.out.println("SimpleService : Method sayHello() : Hello! "
				+ message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.springthought.util.Service#getBye(java.lang.String)
	 */
	@Override
	public String getBye(String message) {
		System.out.println("SimpleService : Method sayBye() : Bye! " + message);
		return message;
	}

}
