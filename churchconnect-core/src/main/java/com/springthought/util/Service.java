/**
 * 
 */
package com.springthought.util;




/**
 * @author eairrick
 *
 */
public interface Service {

	public abstract String getName();

	public abstract void setName(String name);

	public abstract int getId();

	public abstract void setId(int id);

	public abstract void printNameId();

	public abstract void checkName();
	
	public abstract void sayHello(String message);
	
	public abstract String getBye(String message);

}