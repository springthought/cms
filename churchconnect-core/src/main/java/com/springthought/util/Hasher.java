package com.springthought.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hasher {

    protected final Log log = LogFactory.getLog(getClass());

    public final String  ALGORITHM_MD5 = "MD5";
    public final String  ALGORITHM_SHA_1 = "SHA-1";
    public final String  ALGORITHM_SHA_256 = "SHA-256";

    /*
     * <li>{@code MD5}</li>
     * <li>{@code SHA-1}</li>
     * <li>{@code SHA-256}</li>
     */

    public String hashMessage(String message, String algorithm) {

	MessageDigest md;
	String myHash = null;

	try {
	    md = MessageDigest.getInstance("MD5");
	    md.update(message.getBytes());
	    byte[] digest = md.digest();
	    myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
	} catch (NoSuchAlgorithmException e) {
	    log.error("Unable to hash message", e);
	}
	return myHash;
    }

}
