package com.springthought;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Constant values used throughout the application.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public final class Constants {



    private Constants() {
        // hide me
    }

    // ~ Static fields/initializers
    // =============================================

    /**
     * The name of the ResourceBundle used in this application
     */
    protected static Log log = LogFactory.getLog(Constants.class);
    public static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
    public static final String COOKIE_TIME_ZONE = "TIME_ZONE";
    public static final String COOKIE_TIME_ZONE_OFFSET = "TIME_OFFSET";
    public static final String BUNDLE_KEY = "ApplicationResources";
    public static final Integer MAX_EVENT_OCCURRENCE = 30;

    public static final String EDIT_MODE_ONE = "ONE";
    public static final String EDIT_MODE_SERIES = "SERIES";
    public static final String EDIT_MODE_NEW = "NEW";

    public static final String KEY_EVENT_PARENT = "PARENT";
    public static final String KEY_EVENT_OCCUREENCE = "OCCURRENCE";

    public static final String ICALENDAR_FILE_NAME = "calendar.ics";
    public static final String ICALENDAR_NAME = "Chruch.View";
    public static final String SESSION_ATTRIBUTE_USER_NAME = "username";
    public static final String SESSION_ATTRIBUTE_AUTHORITIES = "authorities";
    public static final String SESSION_ATTRIBUTE_LAST_LOGIN = "lastlogin";
    public static final String SESSION_ATTRIBUTE_CURRENT_LOGIN = "currentlogin";
    public static final String JDBC_PROPERTIES = "jdbc.properties";
    public static final String TWILIO_PHONE_NUMBER = "+16784432132";
    public static final String COMMUNICATION_SUCCESS = "Sent Sucessfully";
    public static final String ROLE_PREFIX = "ROLE";
    public static final String TREE_NODE_TYPE_PSEUDO = "pseudo";

    public static final String CALENDAR_DETAILS = "DETAILS";
    public static final String CALENDAR_CHECK_IN = "CHECK-IN";


    public static final int EVENT_MAX_REPEATS_EVERY = 5;
    /**
     * File separator from System properties
     */
    public static final String FILE_SEP = System.getProperty("file.separator");

    /**
     * User home from System properties
     */
    public static final String USER_HOME = System.getProperty("user.home") + FILE_SEP;

    /**
     * The name of the configuration hashmap stored in application scope.
     */
    public static final String CONFIG = "appConfig";

    /**
     * Session scope attribute that holds the locale set by the user. By setting
     * this key to the same one that Struts uses, we get synchronization in Struts
     * w/o having to do extra work or have two session-level variables.
     */
    public static final String PREFERRED_LOCALE_KEY = "org.apache.struts2.action.LOCALE";

    /**
     * The request scope attribute under which an editable user form is stored
     */
    public static final String USER_KEY = "userForm";

    /**
     * The request scope attribute that holds the user list
     */
    public static final String USER_LIST = "userList";

    /**
     * The request scope attribute for indicating a newly-registered user
     */
    public static final String REGISTERED = "registered";

    /**
     * The name of the Administrator role, as specified in web.xml
     */
    public static final String ADMIN_ROLE = "ROLE_ADMIN";
    public static final String ADMIN_CATEGORY = "Administration";

    /**
     * The name of the User role, as specified in web.xml
     */
    public static final String USER_ROLE = "ROLE_USER";
    public static final String MEMBER_ROLE = "ROLE_MEMBER";

    /**
     * The name of the user's role list, a request-scoped attribute when
     * adding/editing a user.
     */
    public static final String USER_ROLES = "userRoles";

    /**
     * The name of the available roles list, a request-scoped attribute when
     * adding/editing a user.
     */
    public static final String AVAILABLE_ROLES = "availableRoles";

    /**
     * The name of the CSS Theme setting.
     */
    public static final String CSS_THEME = "csstheme";

    /**
     * List of System Constants
     */
    public static final String SYSTEM_TYPES_FUND_TYPE = "FT";
    public static final String SYSTEM_TYPES_USER_VALUE_TYPE = "UV";
    public static final String SYSTEM_TYPES_RESOURCE_TYPES = "RS";

    /**
     * CUSTOM FIELD DATA TYPE
     */
    public static final String DATA_TYPE_TEXT = "TX";
    public static final String DATA_TYPE_NUMBER = "NM";
    public static final String DATA_TYPE_DATE = "DT";
    public static final String DATA_TYPE_BOOLEAN = "BL";
    public static final String DATA_TYPE_LIST = "LT";

    /**
     * The name of system values stored in the application scope.
     */
    public static final String SYSTEM_VALUES = "SystemValues";
    /**
     * List of Group Types;
     */
    public static final String SYSTEM_VALUES_GROUP_TYPE_PERSON = "I";
    public static final String SYSTEM_VALUES_GROUP_TYPE_HOUSEHOLDS = "H";
    public static final String SYSTEM_VALUES_PERSON_STATUS = "ST";

    /**
     * List of user value type.
     */
    public static final String USER_VALUE_TYPE_CATEGORY = "C";
    public static final String USER_VALUE_TYPE_RELATIONSHIP = "R";
    public static final String USER_VALUE_TYPE_CONTACT = "L";
    public static final String USER_VALUE_TYPE_VIST = "V";

    /**
     * List of SQL Error Codes
     */
    public static final Integer SQL_ERROR_DUPLICATE = 1062;
    public static final Integer SQL_ERROR_NULL_COLUMN = 1048;

    public static enum CommonDBFields {
        organization_id, type
    }

    ;

    /**
     * Resource Types
     */
    public static final String RESOURCE_TYPE_ROOM = "R";
    public static final String RESOURCE_TYPE_MATERIAL = "M";

    /**
     * System Menu Type
     */
    public static final String SYSMENU_TYPE_MENU_PAD = "M";
    public static final String SYSMENU_TYPE_SPLIT_BUTTON = "S";

    // Twilio Informaiton

    public static final String ACCOUNT_SID = "ACa64d9d2d2a1e81e420af26059536268b";
    public static final String AUTH_TOKEN = "bbf5685359ea24636227f6d2947b07b3";

    public static final String[] EVENT_PARENT_EXCLUSION_PROPS = new String[]{"eventId", "parentId", "version",
            "editable", "eventOccurrences", "parent", "created_by_user", "updated_by_user", "create_date",
            "last_update"};

    public enum MenuType {

        MENU("M"), SPLIT("S"), PANEL("P");
        String value;

        MenuType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum DataImportType {

        PEOPLE("People"), CATEGORIES("Categories"), GROUPS("Groups"), CONTRIBUTIONS("Contributions"), FUNDS("Funds");

        String value;

        DataImportType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public String getFileTemplate() {

            String templateName = "";

            switch (getValue()) {
                case "People":
                    templateName = "people_import_template.csv";
                    break;
                case "Categories":
                    templateName = "categories_import_template.csv";
                    break;
                case "Groups":
                    templateName = "groups_import_template.csv";
                    break;
                case "Contributions":
                    templateName = "contributions_import_template.csv";
                    break;
                case "Funds":
                    templateName = "funds_import_template.csv";
                    break;

            }
            return templateName;
        }

        public Object getModelObject() {


            String objclass = null;
            Object object = null;

            switch (getValue()) {
                case "People":
                    objclass = "com.springthought.model.Person";
                    break;
                case "Categories":
                    objclass = "com.springthought.model.Category";
                    break;
                case "Groups":
                    objclass = "com.springthought.model.Group";
                    break;
                case "Contributions":
                    objclass = "com.springthought.model.Contribution";
                    break;
                case "Funds":
                    objclass = "com.springthought.model.Category";
                    break;
            }

            try {
                object = Class.forName(objclass).getConstructor().newInstance();
            } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                log.fatal(e);
            }

            return object;

        }

    }


    public enum ContributionMethodType {

        CASH("Cash"), CHECK("Check"), ACH("ACH Online"), CREDIT("Credit/Debit Online"), DONATED("Donated Goods"), SECURITIES("Securities");

        String value;

        ContributionMethodType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    /**
     * Enum to classify user value list.
     *
     * @author kinseye
     */
    public enum UserValueType {

        GIFT("G"), VISIT("V"), CONTACT("L"), RELATIONSHIP("R"), CLASSIFICATION("C"), EVENT("E"), MILESTONE("M"),
        STATUS("S"), STAGEOFLIFE("L"), GENDER("G"), ENTHIC("EG"), FUND("FD"), CATEGORY("CT"), MARITAL("MS");

        String value;

        private UserValueType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum CodeListType {

        GIFT("G"), VISIT("V"), CONTACT("L"), RELATIONSHIP("R"), CLASSIFICATION("C"), EVENT("E"), MILESTONE("M"),
        STATUS("S"), STAGEOFLIFE("L"), GENDER("G"), ENTHIC("EG"), FUND("FD"), CATEGORY("CT"), DATATYPE("DT"),
        MARITAL("MS"), METHOD("CM");

        String value;

        private CodeListType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    /**
     * Enum Fund Type
     */

    public enum FundType {

        TAXABLE("T"), EXEMPT("E"), IRA("IRA");

        String value;

        private FundType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    /*
     * HouseHold
     */
    public enum FamilyStatusType {

        ACTIVE("ACTIVE"), INACTIVE("INACTIVE"), VISITOR("VISITOR");

        String value;

        private FamilyStatusType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum BinType {

        PIC("P"), DOC("D");

        public enum StatusType {

            PIC("P"), DOC("D");

            String value;

            private StatusType(String value) {
                this.value = value;
            }

            public String getValue() {
                return this.value;
            }

        }

        ;

        String value;

        private BinType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum GeocodingStatusCode {

        OK("OK"), ZERO_RESULTS("ZERO_RESULTS"), OVER_QUERY_LIMIT("OVER_QUERY_LIMIT"), REQUEST_DENIED("REQUEST_DENIED"),
        INVALID_REQUEST("INVALID_REQUEST"), UNKNOWN_ERROR("UNKNOWN_ERROR");

        private String statusCode;

        private GeocodingStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getGeocodingStatusCode() {
            return statusCode;
        }
    }

    public enum CustomFieldType {

        // @todo: Add GetKey translation to support other lange
        HOUSEHOLD("HOUSEHOLD"), PERSON("PERSON"), REPORT("REPORT");

        String value;

        private CustomFieldType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    public enum CategoryType {

        CLASS("Class"), TALENT("Talent"), MINISTRY("Ministry"), GIFT("Gift"), FUND("Fund"), EVENT("Event");

        String value;

        private CategoryType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum ResourceType {

        AUDIO("Audio"), VISUAL("Visual"), PROJECTOR("Projector"), TABLE("Table");

        String value;

        private ResourceType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum EventStatus {

        SCHEDULED("Scheduled"), TENTATIVE("Tentative"), APPROVED("Approved"), CANCELED("Canceled");

        String value;

        private EventStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum AttendanceTrackingType {

        NONE("No Attendance Tracking"), DETAILED("Who Attended"), COUNT("How Many Were In Attendance");

        String value;

        private AttendanceTrackingType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public static enum Frequency {
        SECONDLY, MINUTELY, HOURLY, DAILY, WEEKLY, MONTHLY, YEARLY
    }

    public enum RepeatOption {

        NO("No"), DAILY("Daily"), WEEKLY("Weekly"), MONTHLY("Monthly"), YEARLY("Yearly");

        String value;

        private RepeatOption(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum ExportOption {

        PDF, EXCEL, RTF
    }

    public enum WeekOption {

        FIRST("First"), SECOND("Second"), THRID("Thrid"), FOURTH("Fourth"), LAST("Last");

        String value;

        private WeekOption(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public int n() {
            return this.ordinal() + 1;
        }

        public static WeekOption of(int i) {
            return values()[i - 1];
        }

        @Override
        public String toString() {
            String str = StringUtils.capitalize(this.value.toLowerCase());
            return str;
        }

    }

    ;

    public static enum DataServiceType {
        MEMBERTYPES
    }

    ;

    public static enum MetaDataType {

        DATE("DT"), CHECKBOXMENU("CM"), INPUTTEXT("TX"), SPINNER("NM"), CHECKBOX("BL"), SELECTMENU("LT");

        String value;

        private MetaDataType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    ;

    public enum SayVerbVoiceOption {
        MAN("Man"), WOMAN("Woman");

        String value;

        private SayVerbVoiceOption(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    public enum DeliveryOption {
        PHONE, TEXT, EMAIL
    }

    public enum SayVerbLanguage {

        en("English"), es("Spainsh"), fr("French");
        String value;

        private SayVerbLanguage(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    public enum Status {

        SCHD("Scheduled"), RUN("Running"), SUC("Success"), ERR("Error");
        String value;

        private Status(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    public enum ConnectStatus {

        OPEN("Open"), ASGN("Assigned"), INP("In Progress"), HLD("Hold"), COMP("Complete");

        String label;

        private ConnectStatus(String value) {
            this.label = value;
        }

        public String getLabel() {
            return this.label;
        }

        public static String[] labels() {
            return Stream.of(ConnectAction.values()).map(ConnectAction::getLabel).toArray(String[]::new);
        }
    }

    public enum ConnectAction {

        VIST("Visit"), PHONE("Phone"), EMAIL("Email"), TXT("Text");

        String label;

        private ConnectAction(String value) {
            this.label = value;
        }

        public String getLabel() {
            return this.label;
        }

        public static String[] labels() {
            return Stream.of(ConnectAction.values()).map(ConnectAction::getLabel).toArray(String[]::new);
        }
    }

    public enum ConnectTabs {

        tbAssign("Assign"), tbMyConnection("My Connections"), tbConnecitons("All Connections"),
        tbConnected("Connected");

        String label;

        private ConnectTabs(String value) {
            this.label = value;
        }

        public String getLabel() {
            return WordUtils.capitalize(this.label);
        }

        public static String[] labels() {
            return Stream.of(ConnectAction.values()).map(ConnectAction::getLabel).toArray(String[]::new);
        }
    }

    @SuppressWarnings("serial")
    public final static Map<String, String> mapOfUSStates = new HashMap<String, String>() {
        {
            put("AL", "Alabama");
            put("AK", "Alaska");
            put("AZ", "Arizona");
            put("AR", "Arkansas");
            put("CA", "California");
            put("CO", "Colorado");
            put("CT", "Connecticut");
            put("DE", "Delaware");
            put("DC", "Dist of Columbia");
            put("FL", "Florida");
            put("GA", "Georgia");
            put("HI", "Hawaii");
            put("ID", "Idaho");
            put("IL", "Illinois");
            put("IN", "Indiana");
            put("IA", "Iowa");
            put("KS", "Kansas");
            put("KY", "Kentucky");
            put("LA", "Louisiana");
            put("ME", "Maine");
            put("MD", "Maryland");
            put("MA", "Massachusetts");
            put("MI", "Michigan");
            put("MN", "Minnesota");
            put("MS", "Mississippi");
            put("MO", "Missouri");
            put("MT", "Montana");
            put("NE", "Nebraska");
            put("NV", "Nevada");
            put("NH", "New Hampshire");
            put("NJ", "New Jersey");
            put("NM", "New Mexico");
            put("NY", "New York");
            put("NC", "North Carolina");
            put("ND", "North Dakota");
            put("OH", "Ohio");
            put("OK", "Oklahoma");
            put("OR", "Oregon");
            put("PA", "Pennsylvania");
            put("RI", "Rhode Island");
            put("SC", "South Carolina");
            put("SD", "South Dakota");
            put("TN", "Tennessee");
            put("TX", "Texas");
            put("UT", "Utah");
            put("VT", "Vermont");
            put("VA", "Virginia");
            put("WA", "Washington");
            put("WV", "West Virginia");
            put("WI", "Wisconsin");
            put("WY", "Wyoming");
        }
    };

    public final static List<String> listOfUSStatesCode = new ArrayList<String>(mapOfUSStates.keySet());
    public final static List<String> listOfUSStatesName = new ArrayList<String>(mapOfUSStates.values());

    public enum Priority {

        NONE("None"), LOW("Low"), MEDIUM("Medium"), HIGH("High");

        String label;

        private Priority(String value) {
            this.label = value;
        }

        public String getLabel() {
            return this.label;
        }

        public static String[] labels() {
            return Stream.of(Priority.values()).map(Priority::getLabel).toArray(String[]::new);
        }
    }

    public enum NotificationType {

        NONE("None"), BIRTHDAY("Birthday"), ANNIVERSARY("Anniversary"), TASK("Task"), EVENT("Event"),
        BCKGRD("Background Check Needed"), JOINED("Membership");

        String label;

        private NotificationType(String value) {
            this.label = value;
        }

        public String getLabel() {
            return this.label;
        }

        public static String[] labels() {
            return Stream.of(Priority.values()).map(Priority::getLabel).toArray(String[]::new);
        }
    }

    public enum BasicProfileType {
        ROLE_ADMIN, ROLE_LEADER, ROLE_USER, ROLE_MEMBER;

    }

    public enum FileLinkType {
        FILE("FILE"), URL("URL");

        String label;

        private FileLinkType(String value) {
            this.label = value;
        }

        public String getLabel() {
            return this.label;
        }

        public static String[] labels() {
            return Stream.of(FileLinkType.values()).map(FileLinkType::getLabel).toArray(String[]::new);
        }

    }

    public enum MimeType {
        $323("text/h323"), $3gp("video/3gpp"), $7z("application/x-7z-compressed"), abw("application/x-abiword"),
        ai("application/postscript"), aif("audio/x-aiff"), aifc("audio/x-aiff"), aiff("audio/x-aiff"),
        alc("chemical/x-alchemy"), art("image/x-jg"), asc("text/plain"), asf("video/x-ms-asf"),
        $asn("chemical/x-ncbi-asn1"), asn("chemical/x-ncbi-asn1-spec"), aso("chemical/x-ncbi-asn1-binary"),
        asx("video/x-ms-asf"), atom("application/atom"), atomcat("application/atomcat+xml"),
        atomsrv("application/atomserv+xml"), au("audio/basic"), avi("video/x-msvideo"), bak("application/x-trash"),
        bat("application/x-msdos-program"), b("chemical/x-molconn-Z"), bcpio("application/x-bcpio"),
        bib("text/x-bibtex"), bin("application/octet-stream"), bmp("image/x-ms-bmp"), book("application/x-maker"),
        boo("text/x-boo"), bsd("chemical/x-crossfire"), c3d("chemical/x-chem3d"), cab("application/x-cab"),
        cac("chemical/x-cache"), cache("chemical/x-cache"), cap("application/cap"), cascii("chemical/x-cactvs-binary"),
        cat("application/vnd.ms-pki.seccat"), cbin("chemical/x-cactvs-binary"), cbr("application/x-cbr"),
        cbz("application/x-cbz"), cc("text/x-c++src"), cdf("application/x-cdf"), cdr("image/x-coreldraw"),
        cdt("image/x-coreldrawtemplate"), cdx("chemical/x-cdx"), cdy("application/vnd.cinderella"),
        cef("chemical/x-cxf"), cer("chemical/x-cerius"), chm("chemical/x-chemdraw"), chrt("application/x-kchart"),
        cif("chemical/x-cif"), $class("application/java-vm"), cls("text/x-tex"), cmdf("chemical/x-cmdf"),
        cml("chemical/x-cml"), cod("application/vnd.rim.cod"), com("application/x-msdos-program"),
        cpa("chemical/x-compass"), cpio("application/x-cpio"), cpp("text/x-c++src"), $cpt("application/mac-compactpro"),
        cpt("image/x-corelphotopaint"), crl("application/x-pkcs7-crl"), crt("application/x-x509-ca-cert"),
        csf("chemical/x-cache-csf"), $csh("application/x-csh"), csh("text/x-csh"), csm("chemical/x-csml"),
        csml("chemical/x-csml"), css("text/css"), csv("text/csv"), ctab("chemical/x-cactvs-binary"), c("text/x-csrc"),
        ctx("chemical/x-ctx"), cu("application/cu-seeme"), cub("chemical/x-gaussian-cube"), cxf("chemical/x-cxf"),
        cxx("text/x-c++src"), dat("chemical/x-mopac-input"), dcr("application/x-director"),
        deb("application/x-debian-package"), diff("text/x-diff"), dif("video/dv"), dir("application/x-director"),
        djv("image/vnd.djvu"), djvu("image/vnd.djvu"), dll("application/x-msdos-program"), dl("video/dl"),
        dmg("application/x-apple-diskimage"), dms("application/x-dms"), doc("application/msword"),
        docx("application/msword"), dot("application/msword"), d("text/x-dsrc"), dvi("application/x-dvi"),
        dv("video/dv"), dx("chemical/x-jcamp-dx"), dxr("application/x-director"), emb("chemical/x-embl-dl-nucleotide"),
        embl("chemical/x-embl-dl-nucleotide"), eml("message/rfc822"), $ent("chemical/x-ncbi-asn1-ascii"),
        ent("chemical/x-pdb"), eps("application/postscript"), etx("text/x-setext"), exe("application/x-msdos-program"),
        ez("application/andrew-inset"), fb("application/x-maker"), fbdoc("application/x-maker"),
        fch("chemical/x-gaussian-checkpoint"), fchk("chemical/x-gaussian-checkpoint"), fig("application/x-xfig"),
        flac("application/x-flac"), fli("video/fli"), fm("application/x-maker"), frame("application/x-maker"),
        frm("application/x-maker"), gal("chemical/x-gaussian-log"), gam("chemical/x-gamess-input"),
        gamin("chemical/x-gamess-input"), gau("chemical/x-gaussian-input"), gcd("text/x-pcs-gcd"),
        gcf("application/x-graphing-calculator"), gcg("chemical/x-gcg8-sequence"), gen("chemical/x-genbank"),
        gf("application/x-tex-gf"), gif("image/gif"), gjc("chemical/x-gaussian-input"),
        gjf("chemical/x-gaussian-input"), gl("video/gl"), gnumeric("application/x-gnumeric"),
        gpt("chemical/x-mopac-graph"), gsf("application/x-font"), gsm("audio/x-gsm"), gtar("application/x-gtar"),
        hdf("application/x-hdf"), hh("text/x-c++hdr"), hin("chemical/x-hin"), hpp("text/x-c++hdr"),
        hqx("application/mac-binhex40"), hs("text/x-haskell"), hta("application/hta"), htc("text/x-component"),
        $h("text/x-chdr"), html("text/html"), htm("text/html"), hxx("text/x-c++hdr"), ica("application/x-ica"),
        ice("x-conference/x-cooltalk"), ico("image/x-icon"), ics("text/calendar"), icz("text/calendar"),
        ief("image/ief"), iges("model/iges"), igs("model/iges"), iii("application/x-iphone"),
        inp("chemical/x-gamess-input"), ins("application/x-internet-signup"), iso("application/x-iso9660-image"),
        isp("application/x-internet-signup"), ist("chemical/x-isostar"), istr("chemical/x-isostar"),
        jad("text/vnd.sun.j2me.app-descriptor"), jar("application/java-archive"), java("text/x-java"),
        jdx("chemical/x-jcamp-dx"), jmz("application/x-jmol"), jng("image/x-jng"), jnlp("application/x-java-jnlp-file"),
        jpeg("image/jpeg"), jpe("image/jpeg"), jpg("image/jpeg"), js("application/x-javascript"), kar("audio/midi"),
        key("application/pgp-keys"), kil("application/x-killustrator"), kin("chemical/x-kinemage"),
        kml("application/vnd.google-earth.kml+xml"), kmz("application/vnd.google-earth.kmz"),
        kpr("application/x-kpresenter"), kpt("application/x-kpresenter"), ksp("application/x-kspread"),
        kwd("application/x-kword"), kwt("application/x-kword"), latex("application/x-latex"), lha("application/x-lha"),
        lhs("text/x-literate-haskell"), lsf("video/x-la-asf"), lsx("video/x-la-asf"), ltx("text/x-tex"),
        lyx("application/x-lyx"), lzh("application/x-lzh"), lzx("application/x-lzx"), $m3u("audio/mpegurl"),
        m3u("audio/x-mpegurl"), $m4a("audio/mpeg"), m4a("video/mp4"), m4b("video/mp4"), m4v("video/mp4"),
        maker("application/x-maker"), man("application/x-troff-man"), mcif("chemical/x-mmcif"),
        mcm("chemical/x-macmolecule"), mdb("application/msaccess"), me("application/x-troff-me"), mesh("model/mesh"),
        mid("audio/midi"), midi("audio/midi"), mif("application/x-mif"), mm("application/x-freemind"),
        mmd("chemical/x-macromodel-input"), mmf("application/vnd.smaf"), mml("text/mathml"),
        mmod("chemical/x-macromodel-input"), mng("video/x-mng"), moc("text/x-moc"), mol2("chemical/x-mol2"),
        mol("chemical/x-mdl-molfile"), moo("chemical/x-mopac-out"), mop("chemical/x-mopac-input"),
        mopcrt("chemical/x-mopac-input"), movie("video/x-sgi-movie"), mov("video/quicktime"), mp2("audio/mpeg"),
        mp3("audio/mpeg"), mp4("video/mp4"), mpc("chemical/x-mopac-input"), mpega("audio/mpeg"), mpeg("video/mpeg"),
        mpe("video/mpeg"), mpga("audio/mpeg"), mpg("video/mpeg"), ms("application/x-troff-ms"), msh("model/mesh"),
        msi("application/x-msi"), mvb("chemical/x-mopac-vib"), mxu("video/vnd.mpegurl"), nb("application/mathematica"),
        nc("application/x-netcdf"), nwc("application/x-nwc"), o("application/x-object"), oda("application/oda"),
        odb("application/vnd.oasis.opendocument.database"), odc("application/vnd.oasis.opendocument.chart"),
        odf("application/vnd.oasis.opendocument.formula"), odg("application/vnd.oasis.opendocument.graphics"),
        odi("application/vnd.oasis.opendocument.image"), odm("application/vnd.oasis.opendocument.text-master"),
        odp("application/vnd.oasis.opendocument.presentation"), ods("application/vnd.oasis.opendocument.spreadsheet"),
        odt("application/vnd.oasis.opendocument.text"), oga("audio/ogg"), ogg("application/ogg"), ogv("video/ogg"),
        ogx("application/ogg"), old("application/x-trash"), otg("application/vnd.oasis.opendocument.graphics-template"),
        oth("application/vnd.oasis.opendocument.text-web"),
        otp("application/vnd.oasis.opendocument.presentation-template"),
        ots("application/vnd.oasis.opendocument.spreadsheet-template"),
        ott("application/vnd.oasis.opendocument.text-template"), oza("application/x-oz-application"),
        p7r("application/x-pkcs7-certreqresp"), pac("application/x-ns-proxy-autoconfig"), pas("text/x-pascal"),
        patch("text/x-diff"), pat("image/x-coreldrawpattern"), pbm("image/x-portable-bitmap"), pcap("application/cap"),
        pcf("application/x-font"),
        // pcf.Z ("application/x-font"),
        pcx("image/pcx"), pdb("chemical/x-pdb"), pdf("application/pdf"), pfa("application/x-font"),
        pfb("application/x-font"), pgm("image/x-portable-graymap"), pgn("application/x-chess-pgn"),
        pgp("application/pgp-signature"), php3("application/x-httpd-php3"),
        php3p("application/x-httpd-php3-preprocessed"), php4("application/x-httpd-php4"),
        php("application/x-httpd-php"), phps("application/x-httpd-php-source"), pht("application/x-httpd-php"),
        phtml("application/x-httpd-php"), pk("application/x-tex-pk"), pls("audio/x-scpls"), pl("text/x-perl"),
        pm("text/x-perl"), png("image/png"), pnm("image/x-portable-anymap"), pot("text/plain"),
        ppm("image/x-portable-pixmap"), pps("application/vnd.ms-powerpoint"), ppt("application/vnd.ms-powerpoint"),
        pptx("application/vnd.ms-powerpoint"), prf("application/pics-rules"), prt("chemical/x-ncbi-asn1-ascii"),
        ps("application/postscript"), psd("image/x-photoshop"), p("text/x-pascal"), pyc("application/x-python-code"),
        pyo("application/x-python-code"), py("text/x-python"), qtl("application/x-quicktimeplayer"),
        qt("video/quicktime"), $ra("audio/x-pn-realaudio"), ra("audio/x-realaudio"), ram("audio/x-pn-realaudio"),
        rar("application/rar"), ras("image/x-cmu-raster"), rd("chemical/x-mdl-rdfile"), rdf("application/rdf+xml"),
        rgb("image/x-rgb"), rhtml("application/x-httpd-eruby"), rm("audio/x-pn-realaudio"), roff("application/x-troff"),
        ros("chemical/x-rosdal"), rpm("application/x-redhat-package-manager"), rss("application/rss+xml"),
        rtf("application/rtf"), rtx("text/richtext"), rxn("chemical/x-mdl-rxnfile"), sct("text/scriptlet"),
        sd2("audio/x-sd2"), sda("application/vnd.stardivision.draw"), sdc("application/vnd.stardivision.calc"),
        sd("chemical/x-mdl-sdfile"), sdd("application/vnd.stardivision.impress"),
        $sdf("application/vnd.stardivision.math"), sdf("chemical/x-mdl-sdfile"),
        sds("application/vnd.stardivision.chart"), sdw("application/vnd.stardivision.writer"),
        ser("application/java-serialized-object"), sgf("application/x-go-sgf"),
        sgl("application/vnd.stardivision.writer-global"), $sh("application/x-sh"), shar("application/x-shar"),
        sh("text/x-sh"), shtml("text/html"), sid("audio/prs.sid"), sik("application/x-trash"), silo("model/mesh"),
        sis("application/vnd.symbian.install"), sisx("x-epoc/x-sisx-app"), sit("application/x-stuffit"),
        sitx("application/x-stuffit"), skd("application/x-koan"), skm("application/x-koan"), skp("application/x-koan"),
        skt("application/x-koan"), smi("application/smil"), smil("application/smil"), snd("audio/basic"),
        spc("chemical/x-galactic-spc"), $spl("application/futuresplash"), spl("application/x-futuresplash"),
        spx("audio/ogg"), src("application/x-wais-source"), stc("application/vnd.sun.xml.calc.template"),
        std("application/vnd.sun.xml.draw.template"), sti("application/vnd.sun.xml.impress.template"),
        stl("application/vnd.ms-pki.stl"), stw("application/vnd.sun.xml.writer.template"), sty("text/x-tex"),
        sv4cpio("application/x-sv4cpio"), sv4crc("application/x-sv4crc"), svg("image/svg+xml"), svgz("image/svg+xml"),
        sw("chemical/x-swissprot"), swf("application/x-shockwave-flash"), swfl("application/x-shockwave-flash"),
        sxc("application/vnd.sun.xml.calc"), sxd("application/vnd.sun.xml.draw"),
        sxg("application/vnd.sun.xml.writer.global"), sxi("application/vnd.sun.xml.impress"),
        sxm("application/vnd.sun.xml.math"), sxw("application/vnd.sun.xml.writer"), t("application/x-troff"),
        tar("application/x-tar"), taz("application/x-gtar"), $tcl("application/x-tcl"), tcl("text/x-tcl"),
        texi("application/x-texinfo"), texinfo("application/x-texinfo"), tex("text/x-tex"), text("text/plain"),
        tgf("chemical/x-mdl-tgf"), tgz("application/x-gtar"), tiff("image/tiff"), tif("image/tiff"), tk("text/x-tcl"),
        tm("text/texmacs"), torrent("application/x-bittorrent"), tr("application/x-troff"), tsp("application/dsptype"),
        ts("text/texmacs"), tsv("text/tab-separated-values"), txt("text/plain"), udeb("application/x-debian-package"),
        uls("text/iuls"), ustar("application/x-ustar"), val("chemical/x-ncbi-asn1-binary"), vcd("application/x-cdlink"),
        vcf("text/x-vcard"), vcs("text/x-vcalendar"), vmd("chemical/x-vmd"), vms("chemical/x-vamas-iso14976"),
        $vrml("model/vrml"), vrml("x-world/x-vrml"), vrm("x-world/x-vrml"), vsd("application/vnd.visio"),
        wad("application/x-doom"), wav("audio/x-wav"), wax("audio/x-ms-wax"), wbmp("image/vnd.wap.wbmp"),
        wbxml("application/vnd.wap.wbxml"), wk("application/x-123"), wma("audio/x-ms-wma"), wmd("application/x-ms-wmd"),
        wmlc("application/vnd.wap.wmlc"), wmlsc("application/vnd.wap.wmlscriptc"), wmls("text/vnd.wap.wmlscript"),
        wml("text/vnd.wap.wml"), wm("video/x-ms-wm"), wmv("video/x-ms-wmv"), wmx("video/x-ms-wmx"),
        wmz("application/x-ms-wmz"), wp5("application/wordperfect5.1"), wpd("application/wordperfect"),
        $wrl("model/vrml"), wrl("x-world/x-vrml"), wsc("text/scriptlet"), wvx("video/x-ms-wvx"),
        wz("application/x-wingz"), xbm("image/x-xbitmap"), xcf("application/x-xcf"), xht("application/xhtml+xml"),
        xhtml("application/xhtml+xml"), xlb("application/vnd.ms-excel"), xls("application/vnd.ms-excel"),
        xlsx("application/vnd.ms-excel"), xlt("application/vnd.ms-excel"), xml("application/xml"),
        xpi("application/x-xpinstall"), xpm("image/x-xpixmap"), xsl("application/xml"), xtel("chemical/x-xtel"),
        xul("application/vnd.mozilla.xul+xml"), xwd("image/x-xwindowdump"), xyz("chemical/x-xyz"),
        zip("application/zip"), zmt("chemical/x-mopac-input"),
        ;
        public String contentType;

        MimeType(String contentType) {
            this.contentType = contentType;
        }

    }
}
