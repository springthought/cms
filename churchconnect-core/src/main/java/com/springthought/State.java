/**
 * 
 */
package com.springthought;



public interface State {

	public void doAction();

	public void loadAction();

	public void saveAction();
}
