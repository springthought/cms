package com.springthought.model;

import com.google.common.base.Objects;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "permission")
public class Permission  implements GrantedAuthority,Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 4843115126027730060L;
    private Long permissionId;
    private String name;
    private String descr;
    private PermissionCategory permissionCategory;

    //private Role role;


    public Permission(){
	super();

    }
    public Permission(String name) {
 	super();
 	this.name = name;
     }


    @ManyToOne( fetch = FetchType.EAGER )
    @JoinColumn(name = "permcategory_id", nullable = false)
    public PermissionCategory getPermissionCategory() {
        return permissionCategory;
    }
    public void setPermissionCategory(PermissionCategory permissionCategory) {
        this.permissionCategory = permissionCategory;
    }
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
	return name;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "permission_id", unique = true, nullable = false)
    public Long getPermissionId() {
	return this.permissionId;
    }

    @Transient
    public String getPrivilege() {
	return name;
    }

 /*   @ManyToOne(fetch = FetchType.EAGER, optional = true )
    public Role getRole() {
	return role;
    }*/

    public void setName(String name) {
	this.name = name;
    }

    public void setPermissionId(Long permissionId) {
	this.permissionId = permissionId;
    }

 /*   public void setRole(Role role) {
	this.role = role;
    }
*/
    @Column(name = "descr", nullable = true, length = 100)
    public String getDescr() {
	return descr;
    }

    public void setDescr(String descr) {
	this.descr = descr;
    }

    @Override
    @Transient
    public String getAuthority() {
	return this.name;
    }




    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Permission) {
            Permission that = (Permission) object;
            return Objects.equal(this.name, that.name);
        }
        return false;
    }



}
