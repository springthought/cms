/**
 *
 */
/**
 * @author eairrick
 *
 */

@FilterDef(name="tenantFilter", parameters=@ParamDef(name="tenantId", type="long"))

package com.springthought.model;

import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

