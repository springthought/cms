package com.springthought.model;

import com.springthought.Constants.CategoryType;
import com.springthought.util.Queries;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entity implementation class for Entity: Category
 */
@Entity
@Table(name = "category", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"tenant_id", "name"}, name = "UQ_CAT_NAME")})

@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({@Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam")})
@NamedNativeQueries({
        @NamedNativeQuery(
                name = Queries.GROUPS_FIND_BY_CATEGORY,
                query = "select * from group_parent g where g.category_id in (select c.category_id from category c where cat_type = :type and c.tenant_id = :tenantId);", resultClass = Group.class)})
@NamedQueries({
        @NamedQuery(name = Queries.CATEGORY_EXLUCDE_BY_TYPE, query = "from Category c where trim(lower(c.categoryType)) != :type order by c.name"),
        @NamedQuery(name = Queries.CATEGORY_FIND_BY_TYPE, query = "from Category c where trim(lower(c.categoryType)) = :type order by c.name")
})
public class Category extends BaseObject implements Serializable, ITenant {

    /**
     *
     */
    private static final long serialVersionUID = 2474327708039947156L;
    private Long categoryId;
    private String name;
    private String descr;
    private CategoryType categoryType = CategoryType.MINISTRY;
    private Boolean active = true;
    private Long tenantId;
    private List<Group> groups = new ArrayList<Group>(0);

    private String color = "00ffc4";

    private String acctnum;
    private String majoracctnum;
    private String minoracctnum;
    private Date startdate;
    private Date enddate;


    public Category() {
        super();
    }

    public void removeGroups() {
        getGroups().removeIf(g -> g.getCategory().equals(this));
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "category_id", unique = true, nullable = false)
    public Long getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Long category_id) {
        this.categoryId = category_id;
    }

    @Column(name = "", length = 25)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "descr", length = 100)
    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     * @return the cat_type
     */

    @Column(name = "cat_type", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    public CategoryType getCategoryType() {
        return categoryType;
    }

    /**
     * @param cat_type the cat_type to set
     */
    public void setCategoryType(CategoryType cat_type) {
        this.categoryType = cat_type;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;

    }

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {

        this.color = color;
    }

    @Column(length = 20)
    @Size(max = 20)
    public String getAcctnum() {
        return acctnum;
    }

    public void setAcctnum(String acctnum) {
        this.acctnum = acctnum;
    }

    @Column(length = 20)
    public String getMajoracctnum() {
        return majoracctnum;
    }

    public void setMajoracctnum(String majoracctnum) {
        this.majoracctnum = majoracctnum;
    }

    @Column(length = 20)
    public String getMinoracctnum() {
        return minoracctnum;
    }

    public void setMinoracctnum(String minoracctnum) {
        this.minoracctnum = minoracctnum;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }


    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Category [name=" + name + ", descr=" + descr + ", categoryType=" + categoryType + ", active=" + active
                + ", tenantId=" + tenantId + "]";
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((categoryType == null) ? 0 : categoryType.hashCode());
        result = prime * result + ((descr == null) ? 0 : descr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((tenantId == null) ? 0 : tenantId.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Category other = (Category) obj;
        if (active == null) {
            if (other.active != null) {
                return false;
            }
        } else if (!active.equals(other.active)) {
            return false;
        }
        if (categoryType != other.categoryType) {
            return false;
        }
        if (descr == null) {
            if (other.descr != null) {
                return false;
            }
        } else if (!descr.equals(other.descr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (tenantId == null) {
            if (other.tenantId != null) {
                return false;
            }
        } else if (!tenantId.equals(other.tenantId)) {
            return false;
        }
        return true;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }


}
