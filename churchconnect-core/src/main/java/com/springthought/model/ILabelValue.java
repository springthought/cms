/**
 * 
 */
package com.springthought.model;

import java.util.Comparator;

/**
 * @author eairrick
 *
 */
public interface ILabelValue {

	/**
	 * Comparator that can be used for a case insensitive sort of
	 * <code>LabelValue</code> objects.
	 */
	public static final Comparator CASE_INSENSITIVE_ORDER = new Comparator() {
		public int compare(Object o1, Object o2) {
			String label1 = ((ILabelValue) o1).getLabel();
			String label2 = ((ILabelValue) o2).getLabel();
			return label1.compareToIgnoreCase(label2);
		}
	};

	public abstract String getLabel();

	public abstract void setLabel(String label);

	public abstract String getValue();

	public abstract void setValue(String value);

	/**
	 * Compare LabelValueBeans based on the label, because that's the human
	 * viewable part of the object.
	 *
	 * @see Comparable
	 * @param o LabelValue object to compare to
	 * @return 0 if labels match for compared objects
	 */
	public abstract int compareTo(Object o);

	/**
	 * Return a string representation of this object.
	 * @return object as a string
	 */
	public abstract String toString();

	/**
	 * LabelValueBeans are equal if their values are both null or equal.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param obj object to compare to
	 * @return true/false based on whether values match or not
	 */
	public abstract boolean equals(Object obj);

	/**
	 * The hash code is based on the object's value.
	 *
	 * @see java.lang.Object#hashCode()
	 * @return hashCode
	 */
	public abstract int hashCode();

}