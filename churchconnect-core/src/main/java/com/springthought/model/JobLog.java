/**
 *
 */
package com.springthought.model;

import com.springthought.Constants.Status;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author eairrick
 *
 */
@Entity
@Table(name = "job_log")
public class JobLog  {

    private Long jobLogId;
    private Person person;
    private Communication communication;
    private Status phoneResultStatus;
    private Status textResultStatus;
    private Status emailResultStatus;
    private String phoneResult;
    private String textResult;
    private String emailResult;
    private Date deliveryDate;
    private Integer version;
    private String sId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deliveryDate", length = 19)
    public Date getDeliveryDate() {
	return deliveryDate;
    }

    @Column(name = "email_status", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public Status getEmailResultStatus() {
	return emailResultStatus;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "joblog_id", unique = true, nullable = false)
    public Long getJobLogId() {
	return jobLogId;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "person_joblog",
    joinColumns = @JoinColumn(name = "joblog_id"),
    inverseJoinColumns = @JoinColumn(name = "person_id"))
    public Person getPerson() {
	return person;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "communication_joblog",
    joinColumns = @JoinColumn(name = "joblog_id"),
    inverseJoinColumns = @JoinColumn(name = "cmmnctnId"))
    public Communication getCommunication() {
        return communication;
    }

    public void setCommunication(Communication communication) {
        this.communication = communication;
    }

    @Column(name = "phone_status", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public Status getPhoneResultStatus() {
	return phoneResultStatus;
    }

    @Column(name = "text_status", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)

    public Status getTextResultStatus() {
	return textResultStatus;
    }

    public void setDeliveryDate(Date date) {
	this.deliveryDate = date;
    }

    public void setEmailResultStatus(Status emailResultStatus) {
	this.emailResultStatus = emailResultStatus;
    }

    public void setJobLogId(Long jobLogId) {
	this.jobLogId = jobLogId;
    }



    @Column( nullable = true, length = 100)
    public  String getPhoneResult() {
        return phoneResult;
    }

    public  void setPhoneResult(String phoneResult) {
        this.phoneResult = phoneResult;
    }

    @Column( nullable = true, length = 50)
    public  String getTextResult() {
        return textResult;
    }

    public  void setTextResult(String textResult) {
        this.textResult = textResult;
    }

    @Column( nullable = true, length = 100)
    public  String getEmailResult() {
        return emailResult;
    }

    public  void setEmailResult(String emailResult) {
        this.emailResult = emailResult;
    }

    public void setPerson(Person person) {
	this.person = person;
    }

    public void setPhoneResultStatus(Status phoneResultStatus) {
	this.phoneResultStatus = phoneResultStatus;
    }

    public void setTextResultStatus(Status textResultStatus) {
	this.textResultStatus = textResultStatus;
    }

    @Column(name = "sid", length = 36, nullable = true)
    public  String getsId() {
        return sId;
    }

    public  void setsId(String sId) {
        this.sId = sId;
    }

    @Version
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
    @Override
    public String toString() {
	return "JobLog [jobLogId=" + jobLogId + ", phoneResultStatus=" + phoneResultStatus + ", textResultStatus="
		+ textResultStatus + ", emailResultStatus=" + emailResultStatus + ", phoneResult=" + phoneResult
		+ ", textResult=" + textResult + ", emailResult=" + emailResult + ", deliveryDate=" + deliveryDate
		+ ", sId=" + sId + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((deliveryDate == null) ? 0 : deliveryDate.hashCode());
	result = prime * result + ((emailResult == null) ? 0 : emailResult.hashCode());
	result = prime * result + ((emailResultStatus == null) ? 0 : emailResultStatus.hashCode());
	result = prime * result + ((jobLogId == null) ? 0 : jobLogId.hashCode());
	result = prime * result + ((phoneResult == null) ? 0 : phoneResult.hashCode());
	result = prime * result + ((phoneResultStatus == null) ? 0 : phoneResultStatus.hashCode());
	result = prime * result + ((sId == null) ? 0 : sId.hashCode());
	result = prime * result + ((textResult == null) ? 0 : textResult.hashCode());
	result = prime * result + ((textResultStatus == null) ? 0 : textResultStatus.hashCode());
	result = prime * result + ((version == null) ? 0 : version.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	JobLog other = (JobLog) obj;
	if (deliveryDate == null) {
	    if (other.deliveryDate != null)
		return false;
	} else if (!deliveryDate.equals(other.deliveryDate))
	    return false;
	if (emailResult == null) {
	    if (other.emailResult != null)
		return false;
	} else if (!emailResult.equals(other.emailResult))
	    return false;
	if (emailResultStatus != other.emailResultStatus)
	    return false;
	if (jobLogId == null) {
	    if (other.jobLogId != null)
		return false;
	} else if (!jobLogId.equals(other.jobLogId))
	    return false;
	if (phoneResult == null) {
	    if (other.phoneResult != null)
		return false;
	} else if (!phoneResult.equals(other.phoneResult))
	    return false;
	if (phoneResultStatus != other.phoneResultStatus)
	    return false;
	if (sId == null) {
	    if (other.sId != null)
		return false;
	} else if (!sId.equals(other.sId))
	    return false;
	if (textResult == null) {
	    if (other.textResult != null)
		return false;
	} else if (!textResult.equals(other.textResult))
	    return false;
	if (textResultStatus != other.textResultStatus)
	    return false;
	if (version == null) {
	    if (other.version != null)
		return false;
	} else if (!version.equals(other.version))
	    return false;
	return true;
    }
}
