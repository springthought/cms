package com.springthought.model;

import com.springthought.Constants.ConnectAction;
import com.springthought.Constants.ConnectStatus;
import com.springthought.util.Queries;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "connect")
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({@Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam")})
@NamedQueries({
        @NamedQuery(name = Queries.CONNECT_BY_UUID, query = "from Connect c where c.uUID = :uuid")
})
public class Connect extends BaseObject implements java.io.Serializable, ITenant {

    /**
     *
     */
    private static final long serialVersionUID = 914238867168476770L;
    private Long connectID;
    private Long tenantId;
    private ConnectStatus status = ConnectStatus.OPEN;
    private ConnectAction action;
    private Person assignee;
    private Person individual;
    private boolean notifyOnComplete = true;
    private Date completeDate;
    private Date completeOnDate;
    private String note;
    private String instructions;
    private Long uUID;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "connect_id", unique = true, nullable = false)
    public Long getConnectId() {
        return this.connectID;
    }

    public void setConnectId(Long connectId) {
        this.connectID = connectId;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;

    }

    @Column(name = "status", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public ConnectStatus getStatus() {
        return status;
    }

    public void setStatus(ConnectStatus status) {
        this.status = status;
    }

    @Column(name = "action", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public ConnectAction getAction() {
        return action;
    }

    public void setAction(ConnectAction action) {
        this.action = action;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinTable(name = "connect_assignee", joinColumns = @JoinColumn(name = "connect_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "person_id", nullable = false))
    public Person getAssignee() {
        return assignee;
    }

    public void setAssignee(Person assignee) {
        this.assignee = assignee;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinTable(name = "connect_individual", joinColumns = @JoinColumn(name = "connect_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "person_id", nullable = false))
    public Person getIndividual() {
        return individual;
    }

    public void setIndividual(Person individual) {
        this.individual = individual;
    }

    public boolean isNotifyOnComplete() {
        return notifyOnComplete;
    }

    public void setNotifyOnComplete(boolean notifyOnComplete) {
        this.notifyOnComplete = notifyOnComplete;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public Date getCompleteOnDate() {
        return completeOnDate;
    }

    public void setCompleteOnDate(Date completeOnDate) {
        this.completeOnDate = completeOnDate;
    }

    @Column(name = "complete_note", length = 5200, columnDefinition = "Text", nullable = true)
    public String getCompleteNote() {
        return note;
    }

    public void setCompleteNote(String note) {
        this.note = note;
    }

    @Column(name = "instruction", length = 5200, columnDefinition = "Text", nullable = false)
    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }


    @Column(name = "uuid", nullable = false)
    public Long getuUID() {
        return uUID;
    }

    public void setuUID(Long uUID) {
        this.uUID = uUID;
    }

    @Override
    public String toString() {
        return "Connect [connectID=" + connectID + ", tenantId=" + tenantId + ", notifyOnComplete=" + notifyOnComplete
                + ", completeDate=" + completeDate + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Connect other = (Connect) obj;
        if (completeDate == null) {
            if (other.completeDate != null) {
                return false;
            }
        } else if (!completeDate.equals(other.completeDate)) {
            return false;
        }
        if (connectID == null) {
            if (other.connectID != null) {
                return false;
            }
        } else if (!connectID.equals(other.connectID)) {
            return false;
        }
        if (notifyOnComplete != other.notifyOnComplete) {
            return false;
        }
        if (tenantId == null) {
            if (other.tenantId != null) {
                return false;
            }
        } else if (!tenantId.equals(other.tenantId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((completeDate == null) ? 0 : completeDate.hashCode());
        result = prime * result + ((connectID == null) ? 0 : connectID.hashCode());
        result = prime * result + (notifyOnComplete ? 1231 : 1237);
        result = prime * result + ((tenantId == null) ? 0 : tenantId.hashCode());
        return result;
    }

}
