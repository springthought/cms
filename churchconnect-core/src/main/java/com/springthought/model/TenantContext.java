/**
 * 
 */
package com.springthought.model;

/**
 * @author eairrick
 *
 */
public class TenantContext extends BaseObject implements java.io.Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long tenant_id;

	/**
	 * @return the tenant_id
	 */
	public Long getTenant_id() {
		return tenant_id;
	}

	/**
	 * @param tenant_id the tenant_id to set
	 */
	public void setTenant_id(Long tenant_id) {
		this.tenant_id = tenant_id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((tenant_id == null) ? 0 : tenant_id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TenantContext other = (TenantContext) obj;
		if (tenant_id == null) {
			if (other.tenant_id != null)
				return false;
		} else if (!tenant_id.equals(other.tenant_id))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TenantContext [tenant_id=" + tenant_id + "]";
	}
	


}
