package com.springthought.model;

import org.hibernate.Session;

public interface NameResolvable {
    public boolean accept(NameResolverVisitor visitor, Session session);
}
