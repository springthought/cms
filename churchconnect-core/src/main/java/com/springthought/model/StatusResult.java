package com.springthought.model;

import com.springthought.Constants.Status;

public class StatusResult {

    private Status status;
    private String resultMessage;
    private String meta;


    public synchronized String getMeta() {
        return meta;
    }

    public synchronized void setMeta(String meta) {
        this.meta = meta;
    }

    public Status getStatus() {
	return status;
    }

    public void setStatus(Status status) {
	this.status = status;
    }

    public String getResultMessage() {
	return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
	this.resultMessage = resultMessage;
    }

    @Override
    public String toString() {
	return "StatusResult [status=" + status + ", resultMessage=" + resultMessage + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((resultMessage == null) ? 0 : resultMessage.hashCode());
	result = prime * result + ((status == null) ? 0 : status.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	StatusResult other = (StatusResult) obj;
	if (resultMessage == null) {
	    if (other.resultMessage != null)
		return false;
	} else if (!resultMessage.equals(other.resultMessage))
	    return false;
	if (status != other.status)
	    return false;
	return true;
    }

}
