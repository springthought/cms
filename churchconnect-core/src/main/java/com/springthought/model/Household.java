package com.springthought.model;

import com.springthought.Constants.FamilyStatusType;
import com.springthought.util.Queries;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;

// Generated Oct 7, 2012 10:37:22 AM by Hibernate Tools 3.4.0.CR1

@Entity
@Table(name = "household")
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({@Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam")})
@NamedQueries({
        @NamedQuery(name = Queries.HOUSEHOLD_FIND_BY_NAME, query = "from Household h where trim(lower(h.name)) = :name")
})
public class Household extends BaseObject implements java.io.Serializable, ITenant {

    /**
     *
     */
    private static final long serialVersionUID = 1128718888692418109L;
    private Long householdId;
    private Long tenantId;
    private String name;
    private FamilyStatusType status;
    private Date statusDate;
    private Integer envelopeNum;
    private String email;
    private String phoneNumber;
    private Boolean phoneNumberListed;

    private Set<Note> notes = new HashSet<Note>(0);

    private Set<Person> persons = new LinkedHashSet<Person>();

    private Address address = new Address();

    public Household() {
    }

    /**
     * @return the address
     */
    @Embedded
    //@IndexedEmbedded
    public Address getAddress() {

        if (address == null) {
            address = new Address();
        }
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    @Column(name = "email", nullable = true, length = 100)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "envelope_num")
    public Integer getEnvelopeNum() {
        return this.envelopeNum;
    }

    public void setEnvelopeNum(Integer envelopeNum) {
        this.envelopeNum = envelopeNum;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "household_id", unique = true, nullable = false)
    public Long getHouseholdId() {
        return this.householdId;
    }

    public void setHouseholdId(Long householdId) {
        this.householdId = householdId;
    }

    /**
     * @return the name
     */
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    /**
     * @param name the formal to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "household", orphanRemoval = true, cascade = CascadeType.PERSIST)
    public Set<Note> getNotes() {
        return this.notes;
    }

    public void setNotes(Set<Note> notes) {
        this.notes = notes;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "household", cascade = {CascadeType.ALL}, orphanRemoval = false)
    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    @Column(name = "phone_number")
    //@Pattern(regexp = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$|^(\\d{3})[\\.](\\d{3})[\\.](\\d{4})$", message = "{user.phone.invalid}")
    @Pattern(regexp = "((\\(\\d{3}\\) ?)|(\\d{3}-))?\\d{3}-\\d{4}|^$", message = "{user.phone.invalid}")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "status", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    public FamilyStatusType getStatus() {
        return this.status;
    }

    public void setStatus(FamilyStatusType status) {
        this.status = status;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "status_date", nullable = false, length = 10)
    public Date getStatusDate() {
        return this.statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    public Long getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;

    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     *
     *
     * /**
     *
     * @return the phoneNumberListed
     */
    public Boolean isPhoneNumberListed() {
        return phoneNumberListed;
    }

    /**
     * @param phoneNumberListed the phoneNumberListed to set
     */
    public void setPhoneNumberListed(Boolean phoneNumberListed) {
        this.phoneNumberListed = phoneNumberListed;
    }

    @Override
    public String toString() {
        return String.format("%s [id=%d]", getClass().getSimpleName(), householdId);
    }

    @Transient
    public Boolean hasFamilyMember() {
        return getPersons().size() > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Household household = (Household) o;
        return Objects.equals(householdId, household.householdId) &&
                tenantId.equals(household.tenantId) &&
                name.equals(household.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(householdId, tenantId, name);
    }
}
