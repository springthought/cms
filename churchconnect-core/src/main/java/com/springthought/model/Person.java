package com.springthought.model;


import com.springthought.Constants.CodeListType;
import com.springthought.service.validation.CodeList;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Person generated by hbm2java
 */

@Entity
/*@Table(name = "person", uniqueConstraints = @UniqueConstraint(columnNames = "email", name = "PERSON_EMAIL_CONSTRAINT"))*/
@Table(name = "person")
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({@Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam")})
//@Repository
public class Person extends BaseObject implements java.io.Serializable, ITenant {

    private static final long serialVersionUID = -4560302518112013794L;


    private Long personId;
    private Long tenantId;
    private String relationship;
    private String status;
    private String email;
    private String title;
    private String perferredName;
    private String firstName;
    private String middleName;
    private String lastName;
    private String suffix;
    private Boolean headofHouse = false;
    private Boolean sameAsHousehold;
    private Short envelopeNum;
    private String occupation;
    private Date birthDate;
    private Date dateJoined;
    private Date AnniversaryDate;
    private String gender;
    private String maritalStatus;
    private String lifeStage;
    private String ethnicGroup;
    private String memberType;
    private String phoneNumber;
    private Boolean phoneNumberListed = true;
    private String mobileNumber;
    private Boolean mobileNumberListed = true;
    private String businessNumber;
    private Boolean businessNumberListed = true;
    private Address address = new Address();


    // Attached Objects
    private List<GroupMember> groupMembers = new ArrayList<GroupMember>(0);
    private Set<Note> notes = new LinkedHashSet<Note>(0);
    private Set<Contribution> contributions = new LinkedHashSet<Contribution>();
    private Set<Pledge> pledges = new LinkedHashSet<Pledge>();
    private PersonBin personBin = new PersonBin();
    private List<CustomField> customfields = new ArrayList<CustomField>(0);
    private List<Attendance> attendances = new ArrayList<Attendance>(0);
    private Set<JobLog> jobLogs = new HashSet<JobLog>(0);
    private Set<Connect> individualConnections = new LinkedHashSet<Connect>(0);
    private Set<Connect> assigneeConnections = new LinkedHashSet<Connect>(0);
    private Set<FileLink> fileLinks = new LinkedHashSet<FileLink>(0);
    private User user = null;
    private Household household = null;


    private String street = new String();
    private String city = new String();
    private String province = new String();
    private String country = new String();
    private String postalCode = new String();


    private String UID = UUID.randomUUID().toString();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "assignee", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Connect> getAssigneeConnections() {
        return assigneeConnections;
    }


    @ManyToMany(mappedBy = "persons")
    @ForeignKey(name = "FK_PRNS_HAS_ATTNDN")
    public List<Attendance> getAttendances() {
        return attendances;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Contribution> getContributions() {
        return contributions;
    }

    @OneToMany(cascade = {javax.persistence.CascadeType.ALL})
    @JoinColumn(name = "person_id")
    @IndexColumn(name = "seqno", base = 1)
    public List<CustomField> getCustomFields() {
        return customfields;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "person_filelink", joinColumns = {@JoinColumn(name = "person_id")}, inverseJoinColumns = {
            @JoinColumn(name = "file_link_id")})
    public Set<FileLink> getFileLinks() {
        return fileLinks;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person", orphanRemoval = true, cascade = CascadeType.ALL)
    public List<GroupMember> getGroupMembers() {
        return this.groupMembers;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.PERSIST)
    @JoinTable(name = "person_household", joinColumns = @JoinColumn(name = "person_id", nullable = false, updatable = true), inverseJoinColumns = @JoinColumn(name = "household_id", nullable = false, updatable = true))
    public Household getHousehold() {
        return household;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return UID.equals(person.UID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(UID);
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "individual", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Connect> getIndiviualConnections() {
        return individualConnections;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<JobLog> getJobLogs() {
        return jobLogs;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person", cascade = CascadeType.ALL)
    public Set<Note> getNotes() {
        return this.notes;
    }

    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "person_picture", joinColumns = {
            @JoinColumn(name = "person_id", unique = true)}, inverseJoinColumns = {@JoinColumn(name = "person_bin_id")})
    public PersonBin getPersonBin() {
        return this.personBin;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Pledge> getPledges() {
        return pledges;
    }

    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "person_user", joinColumns = {@JoinColumn(name = "person_id", unique = true)}, inverseJoinColumns = {@JoinColumn(name = "id")})
    public User getUser() {
        return user;
    }

    public void setAssigneeConnections(Set<Connect> connections) {
        this.assigneeConnections = connections;
    }

    public void setAttendances(List<Attendance> attendances) {
        this.attendances = attendances;
    }

    public void setContributions(Set<Contribution> contributions) {
        this.contributions = contributions;
    }

    /**
     * @param customfields the customfields to set public void setCustomFields(Set
     *                     <CustomField> customfields) { this.customfields =
     *                     customfields; }
     */

    public void setCustomFields(List<CustomField> customfields) {
        this.customfields = customfields;
    }

    public void setFileLinks(Set<FileLink> fileLinks) {
        this.fileLinks = fileLinks;
    }

    /**
     * @param groupMembers the groupMembers to set
     */
    public void setGroupMembers(List<GroupMember> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public void setHousehold(Household household) {
        this.household = household;
    }

    public void setIndiviualConnections(Set<Connect> connections) {
        this.individualConnections = connections;
    }

    public void setJobLogs(Set<JobLog> jobLogs) {
        this.jobLogs = jobLogs;
    }

    public void setNotes(Set<Note> notes) {
        this.notes = notes;
    }

    public void setPersonBin(PersonBin personBin) {
        this.personBin = personBin;
    }

    public void setPledges(Set<Pledge> pledges) {
        this.pledges = pledges;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Transient
    public boolean isNew() {
        return (getPersonId() == null || getPersonId() == 0);
    }

    /**
     * @return the Address
     */
    @Embedded
    public Address getAddress() {

        if (address == null) {
            address = new Address();
        }

        return address;
    }

    /**
     * @return the anniversaryDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "anniv_date", length = 19)
    public Date getAnniversaryDate() {
        return AnniversaryDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "birthDate", length = 19)
    public Date getBirthDate() {
        return this.birthDate;
    }

    /**
     * @return the businessNumber
     */
    @Column(name = "business_number")
    //@Pattern(regexp ="^(\\d{10})$",message = "{user.phone.invalid}")
    //@PhoneNumber
    @Pattern(regexp = "((\\(\\d{3}\\) ?)|(\\d{3}-))?\\d{3}-\\d{4}|^$", message = "{user.phone.invalid}")
    public String getBusinessNumber() {
        return businessNumber;
    }

    /**
     * @param businessNumberListed the businessNumberListed to set
     */
    @Column(name = "business_listed")
    public Boolean getBusinessNumberListed() {
        return businessNumberListed;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateJoined", length = 19)
    public Date getDateJoined() {
        return this.dateJoined;
    }

    @Column(name = "email", length = 100, nullable = true)
    //@Pattern(regexp = "^[\\w-]+(\\.[\\w-]+)*@([a-z0-9-]+(\\.[a-z0-9-]+)*?\\.[a-z]{2,6}|(\\d{1,3}\\.){3}\\d{1,3})(:\\d{4})?$", message = "{user.email.invalid}")
    public String getEmail() {
        return this.email;
    }

    @Column(name = "envelopeNum")
    public Short getEnvelopeNum() {
        return this.envelopeNum;
    }

    /**
     * * @return the ethnicGroup
     */
    @Column(name = "ethnic_grop", length = 2)
    public String getEthnicGroup() {
        return ethnicGroup;
    }

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "first_name", nullable = false, length = 50)
    public String getFirstName() {
        return this.firstName;
    }

    @Column(name = "gender", length = 2)
    @Size(max = 2)
    @CodeList(codeListType = CodeListType.GENDER, ignoreCase = true)
    public String getGender() {
        return this.gender;
    }

    @Transient
    public Boolean getHeadofHouse() {
        return headofHouse;
    }

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "last_name", nullable = false, length = 50)
    public String getLastName() {
        return this.lastName;
    }

    @Column(name = "lifeStage")
    public String getLifeStage() {
        return this.lifeStage;
    }

    @Column(name = "maritalStatus")
    public String getMaritalStatus() {
        return this.maritalStatus;
    }

    /**
     * @return the memberType
     */
    @Column(name = "member_type", length = 2)
    public String getMemberType() {
        return memberType;
    }

    @Column(name = "middleName")
    public String getMiddleName() {
        return this.middleName;
    }

    /**
     * @return the mobileNumber
     */
    @Column(name = "moblie_number")
    //@Pattern(regexp ="^(\\d{10})$",message = "{user.phone.invalid}")
    //@PhoneNumber
    @Pattern(regexp = "((\\(\\d{3}\\) ?)|(\\d{3}-))?\\d{3}-\\d{4}|^$", message = "{user.phone.invalid}")
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @return the mobileNumberListed
     */
    @Transient
    public Boolean getMobileNumberListed() {
        return mobileNumberListed;
    }

    @Transient
    public String getName() {
        return firstName + " " + lastName;
    }

    /**
     * @param groups the groups to set
     */
    // public void setGroups(Set<Group> groups) {
    // this.groups = groups;
    // }
    @Column(name = "occupation")
    public String getOccupation() {
        return this.occupation;
    }

    @Column(name = "perferredName")
    public String getPerferredName() {
        return this.perferredName;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "person_id", unique = true, nullable = false)
    public Long getPersonId() {
        return this.personId;
    }

    /**
     * @return the phoneNumber
     */
    @Column(name = "phone_number")
    //@Pattern(regexp ="^(\\d{10})$",message = "{user.phone.invalid}")
    //@PhoneNumber
    @Pattern(regexp = "((\\(\\d{3}\\) ?)|(\\d{3}-))?\\d{3}-\\d{4}|^$", message = "{user.phone.invalid}")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @return the phoneNumberListed
     */
    @Transient
    public Boolean getPhoneNumberListed() {
        return phoneNumberListed;
    }

    @Column(name = "relationship", nullable = true, length = 2)
    public String getRelationship() {
        return this.relationship;
    }

    /**
     * @return the sameAsHousehold
     */
    @Transient
    public Boolean getSameAsHousehold() {
        return sameAsHousehold;
    }

    @Column(name = "status", length = 1)
    public String getStatus() {
        return this.status;
    }

    @Column(name = "suffix")
    public String getSuffix() {
        return this.suffix;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
        return tenantId;
    }

    @Column(name = "title")
    public String getTitle() {
        return this.title;
    }

    @Transient
    public int getHashCode() {
        return this.hashCode();
    }

    /**
     * @return the headofHouse
     */
    @Column(name = "head_of_house")
    public Boolean isHeadofHouse() {
        return headofHouse;
    }

    /**
     * @return the mobileNumberListed
     */
    @Column(name = "moblie_listed")
    public Boolean isMobileNumberListed() {
        return mobileNumberListed;
    }

    /**
     * @return the phoneNumberListed
     */
    @Column(name = "phone_listed")
    public Boolean isPhoneNumberListed() {
        return phoneNumberListed;
    }

    /**
     * @return the sameAsHousehold
     */
    @Column(name = "same_as_household")
    public Boolean isSameAsHousehold() {
        return sameAsHousehold;
    }

    /**
     * @param Address the Address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @param anniversaryDate the anniversaryDate to set
     */
    public void setAnniversaryDate(Date anniversaryDate) {
        AnniversaryDate = anniversaryDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @param businessNumber the businessNumber to set
     */
    public void setBusinessNumber(String businessNumber) {
        this.businessNumber = businessNumber;
    }

    /**
     * @param businessNumberListed the businessNumberListed to set
     */
    public void setBusinessNumberListed(Boolean businessNumberListed) {
        this.businessNumberListed = businessNumberListed;
    }

    /**
     * @param customfields the customfields to set public void setCustomFields(Set
     *                     <CustomField> customfields) { this.customfields =
     *                     customfields; }
     */

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEnvelopeNum(Short envelopeNum) {
        this.envelopeNum = envelopeNum;
    }

    /**
     * @param ethnicGroup the ethnicGroup to set
     */
    public void setEthnicGroup(String ethnicGroup) {
        this.ethnicGroup = ethnicGroup;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @param groupMembers
     *            the groupMembers to set
     */
    /**
     * @param headofHouse the headofHouse to set
     */
    public void setHeadofHouse(Boolean headofHouse) {
        this.headofHouse = headofHouse;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setLifeStage(String lifeStage) {
        this.lifeStage = lifeStage;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @param memberType the memberType to set
     */
    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @param mobileNumberListed the mobileNumberListed to set
     */
    public void setMobileNumberListed(Boolean mobileNumberListed) {
        this.mobileNumberListed = mobileNumberListed;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setPerferredName(String perferredName) {
        this.perferredName = perferredName;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @param phoneNumberListed the phoneNumberListed to set
     */
    public void setPhoneNumberListed(Boolean phoneNumberListed) {
        this.phoneNumberListed = phoneNumberListed;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    /**
     * @param sameAsHousehold the sameAsHousehold to set
     */
    public void setSameAsHousehold(Boolean sameAsHousehold) {
        this.sameAsHousehold = sameAsHousehold;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;

    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return Optional.ofNullable(firstName).orElse("") + " " + Optional.ofNullable(lastName).orElse("");
    }


    @Transient
    public String getStreet() {
        return getAddress().getAddress();
    }

    public void setStreet(String street) {
        address.setAddress(street);
    }

    @Transient
    public String getCity() {
        return getAddress().getCity();
    }

    public void setCity(String city) {
        address.setCity(city);
    }

    @Transient
    public String getProvince() {
        return getAddress().getProvince();
    }

    public void setProvince(String province) {
        address.setProvince(province);
    }

    @Transient
    public String getCountry() {
        return getAddress().getCountry();
    }

    public void setCountry(String country) {
        address.setCountry(country);
    }

    @Transient
    public String getPostalCode() {
        return getAddress().getPostalCode();
    }

    public void setPostalCode(String postalCode) {
        address.setPostalCode(postalCode);
    }

    @Column(length = 36, name = "uid", nullable = false, unique = true)
    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

}
