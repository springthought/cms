/**
 *
 */
package com.springthought.model;

import javax.persistence.Column;
import java.util.Date;

/**
 * @author eairrick Table is to hold a user report paramter selection and recall
 *         them when the report is run again. Also plan is to submit report via
 *         secheule Will defer this until milestone 2.
 *
 */

public class ReportParamHolder implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long rptParamSelID;

	private SystemParamDef systemParamDef;

	private String textValue = "";
	private Integer numberValue = 0;
	private Boolean booleanValue = false;
	private Date dateValue = new Date();
	private Integer seqno;
	private String UID = "";
	private String[] selectedItems;

	/**
	 * @param param
	 */
	public ReportParamHolder(SystemParamDef param) {

		setSystemParamDef(param);
	}

	public SystemParamDef getSystemParamDef() {
		return systemParamDef;
	}

	public void setSystemParamDef(SystemParamDef systemParamDef) {
		this.systemParamDef = systemParamDef;
	}

	@Column(name = "txt_val", nullable = true, length = 50)
	public String getTextValue() {
		return textValue;
	}

	/**
	 * @param textValue
	 *            the textValue to set
	 */
	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}

	/**
	 * @return the numberValue
	 */
	@Column(name = "num_val", nullable = true)
	public Integer getNumberValue() {
		return numberValue;
	}

	/**
	 * @param numberValue
	 *            the numberValue to set
	 */

	public void setNumberValue(Integer numberValue) {
		this.numberValue = numberValue;
	}

	/**
	 * @return the booleanValue
	 */
	@Column(name = "bln_val", nullable = true)
	public Boolean getBooleanValue() {
		return booleanValue;
	}

	/**
	 * @param booleanValue
	 *            the booleanValue to set
	 */
	public void setBooleanValue(Boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	/**
	 * @return the dateValue
	 */
	@Column(name = "dte_val", nullable = true)
	public Date getDateValue() {
		return dateValue;
	}

	/**
	 * @param dateValue
	 *            the dateValue to set
	 */
	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}

	/**
	 * @return the uID
	 */
	@Column(length = 36, name = "uid", nullable = false, unique = false)
	public String getUID() {
		return UID;
	}

	/**
	 * @param uID
	 *            the uID to set
	 */
	public void setUID(String uID) {
		UID = uID;
	}

	public String[] getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}

	/**
	 * @return the systemParamDef
	 * @OneToOne(fetch = FetchType.EAGER)
	 * @JoinColumn(name = "paramdef_id", nullable = true)
	 * @ForeignKey(name = "fk_parm_def") public SystemParamDef
	 *                  getSystemParamDef() { return systemParamDef; } public
	 *                  void setSystemParamDef(SystemParamDef systemParamDef) {
	 *                  this.systemParamDef = systemParamDef; }
	 */

	@Override
	public String toString() {
		return "CustomField [rptParamSelID=" + rptParamSelID + ", textValue="
				+ textValue + ", numberValue=" + numberValue
				+ ", booleanValue=" + booleanValue + ", dateValue=" + dateValue
				+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ReportParamHolder)) {
			return false;
		}
		ReportParamHolder other = (ReportParamHolder) obj;
		if (UID == null) {
			if (other.UID != null) {
				return false;
			}
		} else if (!UID.equals(other.UID)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((UID == null) ? 0 : UID.hashCode());
		return result;
	}

}
