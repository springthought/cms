package com.springthought.model;

import com.springthought.util.Queries;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This class is used to represent available roles in the database.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Version by
 * Dan Kibler dan@getrolling.com Extended to implement Acegi
 * GrantedAuthority interface by David Carter david@carter.net
 */
@Entity
@Table(name = "role", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"tenant_id", "label"}, name = "UQ_RLE_LABEL")})
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({@Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam")})
@NamedQueries({
        @NamedQuery(name = Queries.ROLE_BY_TENANT, query = "from Role r where r.name = :name and r.tenantId = :tenantId")
})

public class Role extends BaseObject implements Serializable, GrantedAuthority, ITenant {
    private static final long serialVersionUID = 3690197650654049848L;
    private Long id;
    private String name;
    private String label;
    private String description;
    private Set<Permission> permissions = new LinkedHashSet<Permission>(0);
    private boolean basic = false;
    private Long tenantId;
//    private List<User> users = new ArrayList<User>(0);

    /**
     * Default constructor - creates a new instance with no values set.
     */
    public Role() {
    }

    /**
     * Create a new instance and set the name.
     *
     * @param name name of the role.
     */
    public Role(final String name) {
        this.name = name;
    }

    @Column(nullable = true)
    public boolean isBasic() {
        return basic;
    }

    public void setBasic(boolean basic) {
        this.basic = basic;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name property (getAuthority required by Acegi's
     * GrantedAuthority interface)
     * @see org.springframework.security.core.GrantedAuthority#getAuthority()
     */
    @Transient
    public String getAuthority() {
        return getName();
    }

    @Column(length = 50)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(length = 64)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(length = 20, nullable = false)
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


   /* @ManyToMany( fetch = FetchType.LAZY, mappedBy = "User", cascade = CascadeType.ALL )
    public List<User> getUsers() {
        return users;
    }
*/

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Role)) {
            return false;
        }

        final Role role = (Role) o;

        return !(name != null ? !name.equals(role.name) : role.name != null);

    }

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return String.format("%s[id=%d]", getClass().getSimpleName(), getId());
    }

    /**
     * {@inheritDoc}
     */
    public int compareTo(Object o) {
        return (equals(o) ? 0 : -1);
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_permission", joinColumns = {
            @JoinColumn(name = "id", nullable = false, unique = false)}, inverseJoinColumns = @JoinColumn(name = "permission_id", nullable = false, unique = false), uniqueConstraints = @UniqueConstraint(columnNames = {
            "id", "permission_id"}, name = "uq_id_perm"))
    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public void removeAllPermissions() {
        getPermissions().removeIf(p -> getPermissions().contains(p));
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
        return this.tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
