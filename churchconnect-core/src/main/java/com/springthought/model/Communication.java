package com.springthought.model;

import com.springthought.Constants.Status;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Filter(name = "tenantFilter", condition = "tenant_id=:tenantId")
@Table(name = "communication", uniqueConstraints = {
	@UniqueConstraint(columnNames = { "tenant_id", "name" }, name = "UQ_COMM_NAME") })
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({ @Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam") })
public class Communication extends BaseObject implements java.io.Serializable, ITenant {

    /**
     *
     */
    private static final long serialVersionUID = 8199501005344776294L;
    private String name;
    private Long cmmnctnId;
    private Long tenantId;
    private Status status = Status.SCHD;
    private Date startDate;
    private Message message;
    private Set<Group> groups = new HashSet<Group>(0);
    private Set<JobLog> jobLogs = new HashSet<JobLog>(0);;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "cmmnctn_id", unique = true, nullable = false)
    public Long getCmmnctnId() {
	return cmmnctnId;
    }

    public void setCmmnctnId(Long cmmnctnId) {
	this.cmmnctnId = cmmnctnId;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    public Long getTenantId() {
	return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
	this.tenantId = tenantId;

    }

    @Column(name = "status", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public Status getStatus() {
	return status;
    }

    public void setStatus(Status status) {
	this.status = status;
    }

    public Date getStartDate() {
	return startDate;
    }

    public void setStartDate(Date startDate) {
	this.startDate = startDate;
    }

    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "msg_id")
    @ForeignKey(name = "FK_CMMNCTN_HAS_MSG")
    public Message getMessage() {
	return message;
    }

    public void setMessage(Message message) {
	this.message = message;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "communication_group",
    		joinColumns = @JoinColumn(name = "cmmnctn_id"),
    		inverseJoinColumns = @JoinColumn(name = "group_id"),
    		uniqueConstraints = @UniqueConstraint(columnNames = {"cmmnctn_id","group_id" }, name = "uq_cmmn_grp"))
    @ForeignKey(name = "FK_HAS_GRP", inverseName = "FK_HAS_CMMNCTN")
    public Set<Group> getGroups() {
	return groups;
    }

    public void setGroups(Set<Group> groups) {
	this.groups = groups;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "communication", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<JobLog> getJobLogs() {
	return jobLogs;
    }

    public void setJobLogs(Set<JobLog> jobLogs) {
	this.jobLogs = jobLogs;
    }

    @Override
    public String toString() {
	return "Communication [name=" + name + ", cmmnctnId=" + cmmnctnId + ", tenantId=" + tenantId + ", status="
		+ status + ", startDate=" + startDate + "]";
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Communication other = (Communication) obj;
	if (cmmnctnId == null) {
	    if (other.cmmnctnId != null)
		return false;
	} else if (!cmmnctnId.equals(other.cmmnctnId))
	    return false;
	if (startDate == null) {
	    if (other.startDate != null)
		return false;
	} else if (!startDate.equals(other.startDate))
	    return false;
	if (status != other.status)
	    return false;
	if (tenantId == null) {
	    if (other.tenantId != null)
		return false;
	} else if (!tenantId.equals(other.tenantId))
	    return false;
	return true;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cmmnctnId == null) ? 0 : cmmnctnId.hashCode());
	result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
	result = prime * result + ((status == null) ? 0 : status.hashCode());
	result = prime * result + ((tenantId == null) ? 0 : tenantId.hashCode());
	return result;
    }

}
