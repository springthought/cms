package com.springthought.model;

import com.springthought.Constants.SayVerbLanguage;
import com.springthought.Constants.SayVerbVoiceOption;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "message", uniqueConstraints = {@UniqueConstraint(columnNames = {
        "tenant_id", "name"}, name = "UQ_MSG_NAME")})
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({@Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam")})
public class Message extends BaseObject implements java.io.Serializable,
        ITenant {

    /**
     *
     */
    private static final long serialVersionUID = -7062071740336459490L;

    private Long messageId;

    private Long tenantId;

    private String name;
    private String descr;
    private String sender;
    private SayVerbVoiceOption voice = SayVerbVoiceOption.WOMAN;
    private SayVerbLanguage lang = SayVerbLanguage.en;
    private String message;

    public String getSecondMessage() {
        return secondMessage;
    }

    public void setSecondMessage(String secondMessage) {
        this.secondMessage = secondMessage;
    }

    private String secondMessage;
    private String verbal;



    private String title;
    private boolean text;
    private boolean email;
    private boolean phone;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "msg_id", unique = true, nullable = false)
    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    @Transient
    public String getSubject() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubject(String subject) {
        this.name = subject;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Column(name = "voice", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public SayVerbVoiceOption getVoice() {
        return voice;
    }

    public void setVoice(SayVerbVoiceOption voice) {
        this.voice = voice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "lang", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public SayVerbLanguage getLang() {
        return lang;
    }

    public void setLang(SayVerbLanguage lang) {
        this.lang = lang;
    }

    @Column(name = "msg", length = 65535, columnDefinition = "Text", nullable = false)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;

    }

    @Column(name = "verbal", length = 65535, columnDefinition = "Text", nullable = true)
    public String getVerbal() {
        return verbal;
    }

    public void setVerbal(String verbal) {
        this.verbal = verbal;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;

    }

    public boolean isText() {
        return text;
    }

    public void setText(boolean text) {
        this.text = text;
    }

    public boolean isEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isPhone() {
        return phone;
    }

    public void setPhone(boolean phone) {
        this.phone = phone;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((descr == null) ? 0 : descr.hashCode());
        result = prime * result + (email ? 1231 : 1237);
        result = prime * result + ((lang == null) ? 0 : lang.hashCode());
        result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + (phone ? 1231 : 1237);
        result = prime * result + ((sender == null) ? 0 : sender.hashCode());
        result = prime * result + ((tenantId == null) ? 0 : tenantId.hashCode());
        result = prime * result + (text ? 1231 : 1237);
        result = prime * result + ((voice == null) ? 0 : voice.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Message other = (Message) obj;
        if (descr == null) {
            if (other.descr != null)
                return false;
        } else if (!descr.equals(other.descr))
            return false;
        if (email != other.email)
            return false;
        if (lang != other.lang)
            return false;
        if (messageId == null) {
            if (other.messageId != null)
                return false;
        } else if (!messageId.equals(other.messageId))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (phone != other.phone)
            return false;
        if (sender == null) {
            if (other.sender != null)
                return false;
        } else if (!sender.equals(other.sender))
            return false;
        if (tenantId == null) {
            if (other.tenantId != null)
                return false;
        } else if (!tenantId.equals(other.tenantId))
            return false;
        if (text != other.text)
            return false;
        if (voice != other.voice)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Message [name=" + name + ", voice=" + voice + ", lang=" + lang + "]";
    }

}
