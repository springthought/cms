/**
 *
 */
package com.springthought.model;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author eairrick reassign Location to hold locations.
 *
 */
@Entity
@Table(name = "location", uniqueConstraints = {
	@UniqueConstraint(columnNames = { "tenant_id", "name" }, name = "UQ_DUPLICATE_ENTRY") })
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({ @Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam") })
public class Location extends BaseObject implements ITenant, ILabelValue {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */

    private Long tenantId;
    private Long locationId;
    private String name;
    private String descr;
    private Set<Event> events = new HashSet<Event>();
    private Address address = new Address();
    private String phoneNumber;

    @OneToMany(targetEntity = Event.class, mappedBy = "location")
    @OrderBy("name ASC")
    public Set<Event> getEvents() {
	return events;
    }

    public void setEvents(Set<Event> events) {
	this.events = events;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "location_id", unique = true, nullable = false)
    public Long getLocationId() {
	return locationId;
    }

    /**
     * @param classRoomId
     *            the classRoomId to set
     */
    public void setLocationId(Long id) {
	this.locationId = id;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
	return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
	this.tenantId = tenantId;

    }

    /*
     *
     * /**
     *
     * @return the topic
     */
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
	return name;
    }

    /**
     * @param topic
     *            the topic to set
     */
    public void setName(String name) {
	this.name = name;
    }

    @Column(name = "descr", length = 100)
    public String getDescr() {
	return this.descr;
    }

    public void setDescr(String descr) {
	this.descr = descr;
    }

    /**
     * @param Address
     *            the Address to set
     */
    public void setAddress(Address address) {
	this.address = address;
    }

    @Embedded
    public Address getAddress() {

	if (address == null) {
	    address = new Address();
	}

	return address;
    }

    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.model.ILabelValue#getLabel()
     */
    @Override
    @Transient
    public String getLabel() {
	return this.descr;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.model.ILabelValue#setLabel(java.lang.String)
     */
    @Override
    public void setLabel(String label) {
	this.descr = label;

    }

    /*
     * (non-Javadoc)
     *
     * @see com.springthought.model.ILabelValue#getValue()
     */
    @Override
    @Transient
    public String getValue() {
	return this.name;
    }

    @Override
    public void setValue(String value) {
	this.name = value;

    }

    @Override
    public int compareTo(Object o) {
	// Implicitly tests for the correct type, throwing
	// ClassCastException as required by interface
	String otherLabel = ((ILabelValue) o).getLabel();
	return this.getLabel().compareTo(otherLabel);
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (!(obj instanceof Location)) {
	    return false;
	}
	Location other = (Location) obj;
	if (name == null) {
	    if (other.name != null) {
		return false;
	    }
	} else if (!name.equals(other.name)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Location [name=" + name + "]";
    }

}
