package com.springthought.model;

import com.springthought.Constants.AttendanceTrackingType;
import com.springthought.Constants.EventStatus;
import com.springthought.Constants.RepeatOption;
import com.springthought.Constants.WeekOption;
import com.springthought.util.DateUtil;
import com.springthought.util.Queries;
import io.lamma.DayOfWeek;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.*;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleRenderingMode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
/*
 * @Table(name = "event" ,uniqueConstraints = { @UniqueConstraint(columnNames =
 * { "tenant_id", "name" }, name="UQ_EVENT_NAME") })
 */
@Table(name = "event")
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({@Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam")})
@NamedNativeQueries({
        @NamedNativeQuery(name = Queries.EVENT_IS_PARENT, query = "select DISTINCT e.*  from event e where e.parent_id is null", resultClass = Event.class),
        @NamedNativeQuery(
                name = Queries.EVENT_MIN_DATE_FIND_BY_PARENT_ID,
                query = "select *  from event e  where startDate in (select min(startDate) from event where parent_id = :eventid) and parent_id = :eventid", resultClass = Event.class),
})
@NamedQueries({
        @NamedQuery(name = Queries.EVENT_IS_PARENT_IN_RANGE, query = "from Event e where e.parentId  is null and e.repeatUntilDate >= :startDte ORDER BY e.startDate"),
        @NamedQuery(name = Queries.EVENT_INSTANCE_IN_RANGE_BY_TYPE, query = "from Event e where e.parentId  is null and e.repeatUntilDate >= :startDte and e.eventType = :type ORDER BY e.startDate")
})
public class Event extends BaseObject implements java.io.Serializable, ITenant, ScheduleEvent {

    /**
     *
     */
    private static final long serialVersionUID = -7062071740336459490L;
    private Long eventId;
    private Long tenantId;
    private Long parentId;

    private String name;
    private String title;
    private String descr;
    private String url;
    private Date startDate;
    private Date endDate;
    private Date repeatUntilDate;
    private Integer attendanceCnt;
    private Integer mthYrOption = 0;
    private Integer onDayNth = 0;
    private Integer weekDayNum = 1; // week day number the event happens.
    private Integer occurrence = 1;
    private int reminder = 0;
    private AttendanceTrackingType attendanceTrackType = AttendanceTrackingType.DETAILED;
    private RepeatOption repeat = RepeatOption.NO;
    private EventStatus status = EventStatus.APPROVED;
    private WeekOption weekOccurrence = WeekOption.FIRST;
    private boolean publicEvent = true;
    private boolean allDay = true;
    private boolean weekDaysOnly = true;
    // Member Objects
    private List<Group> groups = new ArrayList<Group>(0); // groups to notify of
    private Set<Event> eventOccurrences = new LinkedHashSet<Event>(0);
    private Attendance attendance;
    private Location location;
    private Resource resource;
    private Event event;
    private EventType eventType;
    private Date setStartDate;
    // end
    private Date setEndDate;
    private String id;
    private boolean editable = false;
    private String styleClass;
    private Object data;
    private Long duration;
    private Integer repeatEvery;
    /**
     * @TODO: remove parent field
     */
    private Boolean parent = true;

    public Event() {
        super();
        this.startDate = new Date();
        this.endDate = new Date();
    }

    public Event(AttendanceTrackingType type) {
        super();
        this.setAttendanceTrackType(type);

    }

    public Event(Date startDate, Date endDate) {
        super();
        this.startDate = startDate;
        this.endDate = endDate;

    }

    private void removeEventOccurrence(Event e) {

        boolean isDeleted = this.getEventOccurrences().remove(e);

    }

    public void removeAllEventOccurrences() {
        ArrayList<Event> event = new ArrayList<Event>(eventOccurrences);
        event.forEach(e -> removeEventOccurrence(e));

    }

    public void removeAllGroups() {
        ArrayList<Group> grp = new ArrayList<Group>(getGroups());
        grp.forEach(g -> removeGroup(g));
    }

    private void removeGroup(Group g) {
        boolean isDeleted = this.getGroups().remove(g);

    }

    public void removeAttendance() {

        if (attendance != null) {
            attendance.setEvent(null);
            this.setAttendance(null);
        }
    }

    public void removeLocation() {

        if (location != null) {
            this.setLocation(null);
        }
    }

    public void removeResource() {

        if (resource != null) {
            resource.setEvents(null);
            this.setResource(null);
        }
    }

    /**
     *
     */
    public void removeAllAssociations() {

        removeAllEventOccurrences();
        removeAllGroups();
        removeAttendance();
        removeResource();
        removeLocation();
        removeEventParent();


    }

    public void removeEventParent() {
        setEventParent(null);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Event other = (Event) obj;
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (parent == null) {
            if (other.parent != null)
                return false;
        } else if (!parent.equals(other.parent))
            return false;
        if (startDate == null) {
            if (other.startDate != null)
                return false;
        } else if (!startDate.equals(other.startDate))
            return false;
        if (title == null) {
            return other.title == null;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @OneToOne(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @ForeignKey(name = "FK_EVNT_HAS_ATTNDN")
    public Attendance getAttendance() {
        return this.attendance;
    }

    public void setAttendance(Attendance attendance) {
        this.attendance = attendance;
    }

    @Column(name = "attendance_cnt")
    public Integer getAttendanceCnt() {
        return this.attendanceCnt;
    }

    public void setAttendanceCnt(Integer attendanceCnt) {
        this.attendanceCnt = attendanceCnt;
    }

    @Column(name = "attendance_track_type", length = 10)
    @Enumerated(EnumType.STRING)
    public AttendanceTrackingType getAttendanceTrackType() {
        return this.attendanceTrackType;
    }

    public void setAttendanceTrackType(AttendanceTrackingType attendanceTrackType) {
        this.attendanceTrackType = attendanceTrackType;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.primefaces.model.ScheduleEvent#getData()
     */
    @Override
    @Transient
    public Object getData() {
        return this.data;
    }

    @Column(name = "daynth", nullable = true)
    public Integer getDayNth() {
        return onDayNth;
    }

    public void setDayNth(Integer onDay) {
        this.onDayNth = onDay;
    }

    @Column(name = "descr", length = 100)
    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.primefaces.model.ScheduleEvent#getDescription()
     */
    @Override
    @Transient
    public String getDescription() {
        return this.descr;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    // remove group notificaiton in milestone 1

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate", length = 19)
    @Override
    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date enddate) {
        this.endDate = enddate;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "event_id", unique = true, nullable = false)
    public Long getEventId() {
        return this.eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "parent_id", nullable = true)
    @ForeignKey(name = "EK_EVNT_HAS_PRNT")
    public Event getEventParent() {
        return event;
    }

    public void setEventParent(Event event) {
        this.event = event;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "eventParent", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("startDate ASC")
    @ForeignKey(name = "FK_EVENT_HAS_OCCRRNCS")
    public Set<Event> getEventOccurrences() {
        return eventOccurrences;
    }

    public void setEventOccurrences(Set<Event> eventOccurrences) {
        this.eventOccurrences = eventOccurrences;
    }

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "eventType_id", nullable = true)
    @ForeignKey(name = "FK_EVENT_HAS_EVNT_TYP")
    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType type) {
        this.eventType = type;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "event_group", joinColumns = {@JoinColumn(name = "event_id")}, inverseJoinColumns = {
            @JoinColumn(name = "group_id")}, uniqueConstraints = @UniqueConstraint(columnNames = {"event_id",
            "group_id"}, name = "uq_evnt_grp"))
    @ForeignKey(name = "FK_EVENT_HAS_GRPS")
    public List<Group> getGroups() {
        return this.groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    @Transient
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;

    }

    @Column(name = "month_option", nullable = true)
    public Integer getMonthYearOption() {
        return (mthYrOption == null) ? 0 : mthYrOption;
    }

    public void setMonthYearOption(Integer monthOption) {
        this.mthYrOption = monthOption;
    }

    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
        this.title = name;
    }

    @Column(name = "parent_id", nullable = true, insertable = false, updatable = false)
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Column(name = "reminder", nullable = false)
    public int getReminder() {
        return this.reminder;
    }

    public void setReminder(int reminder) {
        this.reminder = reminder;
    }

    /**
     * @return the recurrence
     */
    @Column(name = "reoccurs", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public RepeatOption getRepeat() {
        return repeat;
    }

    /**
     * @param repeat the recurrence to set
     */
    public void setRepeat(RepeatOption repeat) {
        this.repeat = repeat;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "repeatUntil", length = 19, nullable = true)
    public Date getRepeatUntilDate() {
        return repeatUntilDate;
    }

    public void setRepeatUntilDate(Date repeatUntilDate) {
        this.repeatUntilDate = repeatUntilDate != null ? DateUtil.setMinute(DateUtil.setHour(repeatUntilDate, 23), 0)
                : repeatUntilDate;
    }

    /**
     * @return the resource
     */
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "resource_id", nullable = true)
    @ForeignKey(name = "FK_EVNT_HAS_RSRC")
    public Resource getResource() {
        return resource;
    }

    /**
     * @param resource the resource to set
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "setEndDate", length = 19)
    public Date getSetEndDate() {
        return setEndDate;
    }

    public void setSetEndDate(Date setEndDate) {
        this.setEndDate = setEndDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "setStartDate", length = 19)
    public Date getSetStartDate() {
        return setStartDate;
    }

    public void setSetStartDate(Date setStartDate) {
        this.setStartDate = setStartDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startDate", length = 19)
    @Override
    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startdate) {
        this.startDate = startdate;
    }

    @Transient
    public String getStartDateText() {
        DateFormat dateFormat = new SimpleDateFormat("MMMM d");
        return dateFormat.format(this.startDate);
    }

    /**
     * @return the status
     */
    @Column(name = "status", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    public EventStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EventStatus status) {
        this.status = status;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.primefaces.model.ScheduleEvent#getStyleClass()
     */
    @Override
    @Transient
    public String getStyleClass() {

        return this.styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;

    }

    @Override
    @Transient
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
        this.name = title;
    }

    @Transient
    public String getWeekDayName() {
        return DayOfWeek.of(weekDayNum).toString();
    }

    @Column(name = "weekdaynum", nullable = true)
    public Integer getWeekDayNum() {
        return (weekDayNum == null) ? 1 : weekDayNum;
    }

    public void setWeekDayNum(Integer weekDayNum) {
        this.weekDayNum = weekDayNum;
    }

    @Column(name = "week_occur", length = 10, nullable = true)
    @Enumerated(EnumType.STRING)
    public WeekOption getWeekOccurrence() {
        return (weekOccurrence != null ? weekOccurrence : WeekOption.FIRST);
    }

    public void setWeekOccurrence(WeekOption weekOccurrence) {
        this.weekOccurrence = weekOccurrence;
    }

    @Transient
    public String getWeekOccurrenceName() {
        return StringUtils.capitalize(getWeekOccurrence().toString().toLowerCase());
    }

    @Column(nullable = true)
    public Integer getRepeatEvery() {
        return repeatEvery;
    }

    public void setRepeatEvery(Integer count) {
        this.repeatEvery = count;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parent == null) ? 0 : parent.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    /**
     * @return the allDay
     */
    @Override
    public boolean isAllDay() {
        return allDay;
    }

    /**
     * @param allDay the allDay to set
     */
    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    @Override
    public boolean isEditable() {
        return this.editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Column(name = "parent", nullable = true)
    public Boolean isParent() {
        return parent;
    }

    /**
     * @return the public_event
     */
    public boolean isPublicEvent() {
        return publicEvent;
    }

    /**
     * @param publicevent the public_event to set
     */
    public void setPublicEvent(boolean publicevent) {
        this.publicEvent = publicevent;
    }

    public boolean isWeekDaysOnly() {
        return weekDaysOnly;
    }

    public void setWeekDaysOnly(boolean weekDaysOnly) {
        this.weekDaysOnly = weekDaysOnly;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    /*
     * Maps Event Recurrence to RFC 2445 frequency.
     */

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "location_id", nullable = true)
    @ForeignKey(name = "FK_EVNT_HAS_LOCATION")

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Event [name=");
        builder.append(name);
        builder.append(", title=");
        builder.append(title);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", parent=");
        builder.append(parent);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    @Transient
    public ScheduleRenderingMode getRenderingMode() {
        return null;
    }

    @Override
    @Transient
    public Map<String, Object> getDynamicProperties() {
        return null;
    }

}
