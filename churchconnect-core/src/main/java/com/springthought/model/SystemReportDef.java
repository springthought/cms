/**
 * 
 */
package com.springthought.model;

import com.google.common.base.Objects;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author eairrick
 * 
 */
@Entity
@Table(name = "system_reportdef", uniqueConstraints = { @UniqueConstraint(columnNames = { "name" }, name = "UQ_NAME") })
//@Indexed
public class SystemReportDef extends BaseObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2684336808040085945L;
	private Long reportDefID;
	private String name;
	private String category;
	private String reportFileName;
	private Set<SystemParamDef> systemParamDefs = new HashSet<SystemParamDef>(0);
	private String descr;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "reportdef_id", unique = true, nullable = false)
	public Long getReportDefID() {
		return reportDefID;
	}

	public void setReportDefID(Long reportDefID) {
		this.reportDefID = reportDefID;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "category", nullable = false, length = 50)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "reportfile", nullable = false, length = 50)
	public String getReportFileName() {
		return reportFileName;
	}

	public void setReportFileName(String reportFileName) {
		this.reportFileName = reportFileName;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "systemReportDef", orphanRemoval = true)
	@Cascade({ CascadeType.ALL, CascadeType.DELETE })
	@OrderBy("name ASC")
	public Set<SystemParamDef> getSystemParamDefs() {
		return systemParamDefs;
	}

	public void setSystemParamDefs(Set<SystemParamDef> systemParamDefs) {
		this.systemParamDefs = systemParamDefs;
	}

	@Column(name = "descr", length = 100)
	public String getDescr() {
		return this.descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getReportDefID(), getName(), getCategory(),
				getReportFileName());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("reportDefID", reportDefID)
				.append("name", name)
				.append("category", category)
				.append("reportFileName", reportFileName)
				.toString();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof SystemReportDef) {
			SystemReportDef that = (SystemReportDef) object;
			return Objects.equal(this.getReportDefID(), that.getReportDefID())
					&& Objects.equal(this.getName(), that.getName())
					&& Objects.equal(this.getCategory(), that.getCategory())
					&& Objects.equal(this.getReportFileName(),
							that.getReportFileName());
		}
		return false;
	}

}
