/**
 * 
 */
package com.springthought.model;


import org.primefaces.model.UploadedFile;



/**
 * @author eairrick
 *
 */
public interface ILobValue extends UploadedFile {
	public long getId();


}
