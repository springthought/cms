package com.springthought.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Cabinet generated by hbm2java
 */
@Entity
@Table(name = "filelinkbin")
public class FileLinkBin implements java.io.Serializable {


    /**
     *
     */
    private static final long serialVersionUID = -2712638594298732814L;


    private Long fileLinkBinId;
    private byte[] contents;


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "filelinkbin_id", unique = true, nullable = false)
    public Long getFileLinkBinId() {
        return fileLinkBinId;
    }

    public void setFileLinkBinId(Long fileLinkBinId) {
        this.fileLinkBinId = fileLinkBinId;
    }

    @Lob
    @Basic(fetch = FetchType.LAZY)
    public byte[] getContents() {
        return contents;
    }

    public void setContents(byte[] contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
	return "FileLinkBin [fileLinkBinId=" + fileLinkBinId + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((fileLinkBinId == null) ? 0 : fileLinkBinId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (!(obj instanceof FileLinkBin)) {
	    return false;
	}
	FileLinkBin other = (FileLinkBin) obj;
	if (fileLinkBinId == null) {
	    if (other.fileLinkBinId != null) {
		return false;
	    }
	} else if (!fileLinkBinId.equals(other.fileLinkBinId)) {
	    return false;
	}
	return true;
    }



}
