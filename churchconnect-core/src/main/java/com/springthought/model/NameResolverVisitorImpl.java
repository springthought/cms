package com.springthought.model;

import com.springthought.Constants;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;

public class NameResolverVisitorImpl implements NameResolverVisitor {

    private Session session;
    private Long tenantId;
    private ArrayList<LabelValue> errorMessages = new ArrayList<>(0);

    @Override
    public boolean visit(Group group, Session session) {

        this.session = session;
        this.tenantId = group.getTenantId();
        this.errorMessages = new ArrayList<>(0);

        Category category = null;

        if (StringUtils.isNotBlank(group.getCategoryName())) {
            category = getCategory(group.getCategoryName(), Constants.CategoryType.MINISTRY);
            if (category != null) {
                group.setCategory(category);
            } else {
                addErrorMessages("importData.invalid.category.name", group.getCategoryName());
            }
        }
        return (category != null);
    }


    @Override
    public boolean visit(Contribution contribution, Session session) {

        this.session = session;
        this.tenantId = contribution.getTenantId();
        this.errorMessages = new ArrayList<>(0);

        Category category = null;
        Person person = null;

        if (StringUtils.isNotBlank(contribution.getCategoryName())) {
            category = getCategory(contribution.getCategoryName(), Constants.CategoryType.FUND);
            if (category != null) {
                contribution.setCategory(category);
            } else {
                addErrorMessages("importData.invalid.category.name", contribution.getCategoryName());
            }
        }

        if (contribution.getConnectId() != null || contribution.getConnectId() != 0) {
            person = getPerson(contribution.getConnectId());
            if (person != null) {
                contribution.setPerson(person);
            } else {
                addErrorMessages("importData.invalid.connect.id", contribution.getConnectId().toString());
            }

        }

        return (category != null && person != null);
    }

    private Person getPerson(Long connectId) {
        Person person = null;
        Criteria criteria = session.createCriteria(Person.class);
        Criterion personCriterion = Restrictions.eq("personId", connectId);
        Criterion tenantCriterion = Restrictions.eq("tenantId", tenantId);
        person = (Person) criteria.add(Restrictions.and(personCriterion, tenantCriterion)).uniqueResult();
        return person;
    }


    private Category getCategory(String groupName, Constants.CategoryType type) {

        Category category;
        Criteria criteria = session.createCriteria(Category.class);
        Criterion groupCriterion = Restrictions.eq("name", groupName).ignoreCase();
        Criterion tenantCriterion = Restrictions.eq("tenantId", tenantId);
        Criterion categoryTypeCriterion = Restrictions.eq("categoryType", type);
        category = (Category) criteria.add(Restrictions.and(groupCriterion, tenantCriterion, categoryTypeCriterion)).uniqueResult();
        return category;
    }

    public ArrayList<LabelValue> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(ArrayList<LabelValue> errorMessages) {
        this.errorMessages = errorMessages;
    }

    private void addErrorMessages(String label, String value) {
        errorMessages.add(new LabelValue(label, value));
    }

}
