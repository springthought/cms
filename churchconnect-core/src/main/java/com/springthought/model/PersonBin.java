package com.springthought.model;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Cabinet generated by hbm2java
 */
@Entity
@Table(name = "person_bin")
public class PersonBin implements java.io.Serializable, ILobValue {

    /**
     *
     */
    private static final long serialVersionUID = -2712638594298732814L;
    private Long person_bin_id;
    private String fileName;
    private long size;
    private byte[] contents;
    private String contentType;
    private Person person;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "person_bin_id", unique = true, nullable = false)
    public Long getPersonBinId() {
	return this.person_bin_id;
    }

    public void setPersonBinId(Long personBinId) {
	this.person_bin_id = personBinId;
    }

    @OneToOne(mappedBy = "personBin", fetch = FetchType.LAZY)
    public Person getPerson() {
	return person;
    }

    public void setPerson(Person person) {
	this.person = person;
    }

    /**
     * @return the fileName
     */
    @Override
    @Column(name = "file_name")
    public String getFileName() {
	return fileName;
    }

    @Transient
    @Override
    public List<String> getFileNames() {
        return null;
    }

    /**
     * @param fileName
     *            the fileName to set
     */
    public void setFileName(String fileName) {
	this.fileName = fileName;
    }

    /**
     * @return the size
     */
    @Override
    public long getSize() {
	return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(long size) {
	this.size = size;
    }

    /**
     * @return the contents
     */

    @Override
    @Lob
    public byte[] getContents() {
	return contents;
    }

    /**
     * @param contents
     *            the contents to set
     */
    public void setContents(byte[] contents) {
	this.contents = contents;
    }

    /**
     * @return the contentType
     */
    @Override
    @Column(name = "content_type")
    public String getContentType() {
	return contentType;
    }

    /**
     * @param contentType
     *            the contentType to set
     */
    public void setContentType(String contentType) {
	this.contentType = contentType;
    }

    @Override
    @Transient
    public InputStream getInputstream() throws IOException {
	return new ByteArrayInputStream(this.contents);
    }

    @Override
    @Transient
    public long getId() {
	return this.person_bin_id;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
	result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
	result = prime * result + ((person_bin_id == null) ? 0 : person_bin_id.hashCode());
	result = prime * result + (int) (size ^ (size >>> 32));
	return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PersonBin other = (PersonBin) obj;
	if (contentType == null) {
	    if (other.contentType != null)
		return false;
	} else if (!contentType.equals(other.contentType))
	    return false;
	if (fileName == null) {
	    if (other.fileName != null)
		return false;
	} else if (!fileName.equals(other.fileName))
	    return false;
	if (person_bin_id == null) {
	    if (other.person_bin_id != null)
		return false;
	} else if (!person_bin_id.equals(other.person_bin_id))
	    return false;
	if (size != other.size)
	    return false;
	return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "PersonBin [fileName=" + fileName + ", size=" + size + ", contentType=" + contentType + "]";
    }

    /*
     * (non-Javadoc)
     *
     * @see org.primefaces.model.UploadedFile#write(java.lang.String)
     */
    @Override
    public void write(String filePath) throws Exception {
	// TODO Auto-generated method stub

    }

}
