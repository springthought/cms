/**
 *
 */
package com.springthought.model;

/**
 * @author eairrick
 *
 */

public interface ITenant {

	public Long getTenantId();
	public void setTenantId(Long tenantId);

}
