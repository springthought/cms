/**
 * 
 */
package com.springthought.model;

import com.google.common.base.Objects;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author eairrick
 *
 */
@Entity
@Table(name = "aggregate")
public class Aggregate extends BaseObject implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4053200797911910651L;
	private BigDecimal value;
	private Long aggregateId;

	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "aggrgt_id", unique = true, nullable = false)
	public Long getAggregatedId() {
		return aggregateId;
	}

	public Aggregate(BigDecimal value) {
		this.value = value;
	}

	public void setAggregatedId(Long aggregateId) {
		this.aggregateId = aggregateId;
	}
	/**
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getValue());
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Aggregate) {
			Aggregate that = (Aggregate) object;
			return Objects.equal(this.getValue(), that.getValue());
		}
		return false;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("value", value)
				.toString();
	}


}
