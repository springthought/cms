package com.springthought.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;




/**
 * Entity implementation class for Entity: SystemMenuGroup
 *
 */
@Entity
@Table(name = "system_menu_group")

public class SystemMenuGroup extends BaseObject implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4112049988868441392L;
	private long systemMenuGroupId;
	private String name;
	private Integer item_order;
	private String descr;
	private String role;
	private String view;
	private String iconpath;
	private Set<SystemMenuItem> systemMenuItems = new HashSet<SystemMenuItem>(0);
	private SystemMenu systemMenu;
	private String mode;


	/**
	 *
	 */
	public SystemMenuGroup() {
		super();
	}

	/**
	 * @return the systemMenuGroupId
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "system_menu_group_id", unique = true, nullable = false)
	public long getSystemMenuGroupId() {
		return systemMenuGroupId;
	}

	/**
	 * @param systemMenuGroupId
	 *            the systemMenuGroupId to set
	 */
	public void setSystemMenuGroupId(long systemMenuGroupId) {
		this.systemMenuGroupId = systemMenuGroupId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "system_menu_id", nullable = false)
	public SystemMenu getSystemMenu() {
		return this.systemMenu;
	}


	public void setSystemMenu(SystemMenu systemMenu) {
		this.systemMenu = systemMenu;
	}

	/**
	 * @return the menu_group_name
	 */
	@Column(name = "name", length = 50)
	public String getName() {
		return name;
	}

	/**
	 * @param menu_group_name
	 *            the menu_group_name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the order
	 */
	@Column(name = "item_order")
	public Integer getItemOrder() {
		return item_order;
	}

	/**
	 * @param order
	 *            the order to set
	 */
	public void setItemOrder(Integer _order) {
		this.item_order = _order;
	}

	/**
	 * @return the descr
	 */
	@Column(name = "descr", length = 100)
	public String getDescr() {
		return descr;
	}

	/**
	 * @param descr
	 *            the descr to set
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

	/**
	 * @return the role
	 */
	@Column(name = "role", length = 100)
	public String getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the view
	 */
	@Column(name = "view", length = 50)
	public String getView() {
		return view;
	}

	/**
	 * @param view
	 *            the view to set
	 */
	public void setView(String view) {
		this.view = view;
	}

	/**
	 * @return the iconpath
	 */
	@Column(name = "iconpath", length = 100)
	public String getIconpath() {
		return iconpath;
	}

	/**
	 * @param iconpath
	 *            the iconpath to set
	 */
	public void setIconpath(String iconpath) {
		this.iconpath = iconpath;
	}

	public String getMode() {
	    return mode;
	}

	public void setMode(String mode) {
	    this.mode = mode;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "systemMenuGroup")
	@OrderBy("itemOrder")
	public Set<SystemMenuItem> getSystemMenuItems() {
		return this.systemMenuItems;
	}

	public void setSystemMenuItems(Set<SystemMenuItem> systemMenuItems) {
		this.systemMenuItems = systemMenuItems;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descr == null) ? 0 : descr.hashCode());
		result = prime * result
				+ ((iconpath == null) ? 0 : iconpath.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((item_order == null) ? 0 : item_order.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((view == null) ? 0 : view.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemMenuGroup other = (SystemMenuGroup) obj;
		if (descr == null) {
			if (other.descr != null)
				return false;
		} else if (!descr.equals(other.descr))
			return false;
		if (iconpath == null) {
			if (other.iconpath != null)
				return false;
		} else if (!iconpath.equals(other.iconpath))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (item_order == null) {
			if (other.item_order != null)
				return false;
		} else if (!item_order.equals(other.item_order))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (view == null) {
			if (other.view != null)
				return false;
		} else if (!view.equals(other.view))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SystemMenuGroup [name=" + name + ", order=" + item_order
				+ ", descr=" + descr + ", role=" + role + ", view=" + view
				+ ", iconpath=" + iconpath + ", systemMenuGroupId="
				+ systemMenuGroupId + "]";
	}

}

