package com.springthought.model;

import org.hibernate.Session;

import java.util.ArrayList;

public interface NameResolverVisitor {
    boolean visit(Group group, Session session);
    boolean visit(Contribution contribution, Session session);

    ArrayList<LabelValue> getErrorMessages();

    void setErrorMessages(ArrayList<LabelValue> errorMessages);
}
