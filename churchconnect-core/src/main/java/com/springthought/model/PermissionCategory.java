package com.springthought.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity
@Table(name = "permission_category", uniqueConstraints = {
	@UniqueConstraint(columnNames = { "name" }, name = "UQ_CAT_NAME") })
public class PermissionCategory extends BaseObject implements Serializable{

    private Long permCategoryId;
    private String name;
    private String descr;
    private Boolean active = true;
    private List<Permission> permissions = new ArrayList<Permission>(0);

    private static final long serialVersionUID = 1L;

    public PermissionCategory() {
	super();
    }

    public PermissionCategory(String name, String descr, Boolean active) {
	this.name = name;
	this.descr = descr;
	this.active = active;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "permcategory_id", unique = true, nullable = false)
    public Long getPermCategoryId() {
	return permCategoryId;
    }

    public void setPermCategoryId(Long permCategoryId) {
	this.permCategoryId = permCategoryId;
    }

    @Column(name = "", length = 25)
    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Column(name = "descr", length = 100)
    public String getDescr() {
	return this.descr;
    }

    public void setDescr(String descr) {
	this.descr = descr;
    }

    @OneToMany(mappedBy = "permissionCategory", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    public List<Permission> getPermissions() {
	return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
	this.permissions = permissions;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
	return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(Boolean active) {
	this.active = active;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((active == null) ? 0 : active.hashCode());
	result = prime * result + ((descr == null) ? 0 : descr.hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	PermissionCategory other = (PermissionCategory) obj;
	if (active == null) {
	    if (other.active != null) {
		return false;
	    }
	} else if (!active.equals(other.active)) {
	    return false;
	}
	if (descr == null) {
	    if (other.descr != null) {
		return false;
	    }
	} else if (!descr.equals(other.descr)) {
	    return false;
	}
	if (name == null) {
	    if (other.name != null) {
		return false;
	    }
	} else if (!name.equals(other.name)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "PermissionCategory [name=" + name + ", descr=" + descr + ", active=" + active + "]";
    }


}
