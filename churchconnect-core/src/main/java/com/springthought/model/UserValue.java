package com.springthought.model;

// Generated Oct 7, 2012 10:37:22 AM by Hibernate Tools 3.4.0.CR1


import com.springthought.Constants.UserValueType;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * UserValue generated by hbm2java
 */
@Entity
@Table(name = "user_value")
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long") )
@Filters({ @Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam") })
public class UserValue extends BaseObject implements java.io.Serializable, ITenant {

	/**
	 *
	 */
	private static final long serialVersionUID = -6334518015895931833L;
	private Long uservalueId;
	private Long tenantId;
	private String name;
	private String descr;
	private UserValueType value_type;

	public UserValue() {

	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "uservalue_id", unique = true, nullable = false)
	public Long getUservalueId() {
		return this.uservalueId;
	}

	public void setUservalueId(Long uservalueId) {
		this.uservalueId = uservalueId;
	}



	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "descr", length = 100)
	public String getDescr() {
		return this.descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	@Column(name = "value_type",  length = 20, nullable = false)
	@Enumerated(EnumType.STRING)
	public UserValueType getValue_Type() {
		return this.value_type;
	}

	public void setValue_Type(UserValueType type) {
		this.value_type = type;
	}


	@Override
	@Column(name = "tenant_id", nullable = false)
	@Index(name = "TENANTID_IDX")
	public Long getTenantId() {
		return tenantId;
	}

	@Override
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserValue [uservalueId=" + uservalueId + ", name=" + name
				+ ", descr=" + descr + ", value_type=" + value_type + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((value_type == null) ? 0 : value_type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserValue other = (UserValue) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value_type != other.value_type)
			return false;
		return true;
	}


}
