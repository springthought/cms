package com.springthought.model;

import java.io.Serializable;

/**
 * A simple JavaBean to represent label-value pairs. This is most commonly used
 * when constructing user interface elements which have a label to be displayed
 * to the user, and a corresponding value to be returned to the server. One
 * example is the <code>&lt;html:options&gt;</code> tag.
 * 
 * <p>Note: this class has a natural ordering that is inconsistent with equals.
 *
 * @see org.apache.struts.util.LabelValueBean
 */
public class LabelValue implements Comparable, Serializable, ILabelValue {

    private static final long serialVersionUID = 3689355407466181430L;

    

    // ----------------------------------------------------------- Constructors


    /**
     * Default constructor.
     */
    public LabelValue() {
        super();
    }

    /**
     * Construct an instance with the supplied property values.
     *
     * @param label The label to be displayed to the user.
     * @param value The value to be returned to the server.
     */
    public LabelValue(final String label, final String value) {
        this.label = label;
        this.value = value;
    }

    // ------------------------------------------------------------- Properties


    /**
     * The property which supplies the option label visible to the end user.
     */
    private String label;

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#getLabel()
	 */
    @Override
	public String getLabel() {
        return this.label;
    }

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#setLabel(java.lang.String)
	 */
    @Override
	public void setLabel(String label) {
        this.label = label;
    }


    /**
     * The property which supplies the value returned to the server.
     */
    private String value;

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#getValue()
	 */
    @Override
	public String getValue() {
        return this.value;
    }

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#setValue(java.lang.String)
	 */
    @Override
	public void setValue(String value) {
        this.value = value;
    }


    // --------------------------------------------------------- Public Methods

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#compareTo(java.lang.Object)
	 */
    @Override
	public int compareTo(Object o) {
        // Implicitly tests for the correct type, throwing
        // ClassCastException as required by interface
        String otherLabel = ((ILabelValue) o).getLabel();

        return this.getLabel().compareTo(otherLabel);
    }

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#toString()
	 */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer("LabelValue[");
        sb.append(this.label);
        sb.append(", ");
        sb.append(this.value);
        sb.append("]");
        return (sb.toString());
    }

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#equals(java.lang.Object)
	 */
    @Override
	public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof LabelValue)) {
            return false;
        }

        ILabelValue bean = (ILabelValue) obj;
        int nil = (this.getValue() == null) ? 1 : 0;
        nil += (bean.getValue() == null) ? 1 : 0;

        if (nil == 2) {
            return true;
        } else if (nil == 1) {
            return false;
        } else {
            return this.getValue().equals(bean.getValue());
        }

    }

    /* (non-Javadoc)
	 * @see com.springthought.model.ILabelValue#hashCode()
	 */
    @Override
	public int hashCode() {
        return (this.getValue() == null) ? 17 : this.getValue().hashCode();
    }
}