package com.springthought.model;

import javax.mail.internet.InternetAddress;

public class MessageTemplateSupplier {
    
    private Person person = null;
    private InternetAddress sender = null;
    private String applicationURL = null;
    private Message message;
    
   
    public Person getPerson() {
        return person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }
  
    public String getApplicationURL() {
        return applicationURL;
    }
    public void setApplicationURL(String applicationURL) {
        this.applicationURL = applicationURL;
    }
    public Message getMessage() {
        return message;
    }
    public void setMessage(Message message) {
        this.message = message;
    }
    
    public InternetAddress getSender() {
        return sender;
    }
    public void setSender(InternetAddress sender) {
        this.sender = sender;
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((applicationURL == null) ? 0 : applicationURL.hashCode());
	result = prime * result + ((sender == null) ? 0 : sender.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	MessageTemplateSupplier other = (MessageTemplateSupplier) obj;
	if (applicationURL == null) {
	    if (other.applicationURL != null)
		return false;
	} else if (!applicationURL.equals(other.applicationURL))
	    return false;
	if (sender == null) {
	    if (other.sender != null)
		return false;
	} else if (!sender.equals(other.sender))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "MessageTemplateSupplier [sender=" + sender + ", applicationURL=" + applicationURL + ", message="
		+ message + "]";
    }
 
	    

}
