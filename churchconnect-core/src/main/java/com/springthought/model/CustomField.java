/**
 *
 */
package com.springthought.model;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author eairrick
 *
 */
@Entity
@Table(name = "customfield")
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long") )
@Filters({ @Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam") })
public class CustomField extends BaseObject implements java.io.Serializable, ITenant {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long customFieldId;

    private CustomMeta customMeta;

    private Person person = new Person();
    private Household household = new Household();

    private String textValue = "";
    private Integer numberValue = 0;
    private Boolean booleanValue = false;
    private Date dateValue = new Date();
    private Integer seqno;
    private String UID = "";
    private Long tenantId;

    /**
     * @return the customFieldId
     */

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "customfield_id", unique = true, nullable = false)
    public Long getCustomFieldId() {
	return customFieldId;
    }

    /**
     * @param customFieldId
     *            the customFieldId to set
     */
    public void setCustomFieldId(Long customFieldId) {
	this.customFieldId = customFieldId;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
	return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
	this.tenantId = tenantId;

    }

    /**
     * @return the person
     */

    @ManyToOne
    @JoinColumn(name = "person_id", insertable = false, updatable = false, nullable = true)
    public Person getPerson() {
	return person;
    }

    /**
     * @param person
     *            the person to set
     */
    public void setPerson(Person person) {
	this.person = person;
    }

    /**
     * @return the household
     * @ManyToOne(fetch = FetchType.EAGER)
     * @JoinColumn(name = "household_id", nullable = true)
     * @ForeignKey(name = "FK_HSHLD_CFLDS") public Household getHousehold() {
     *                  return household; }
     *
     *
     *                  public void setHousehold(Household household) {
     *                  this.household = household; }
     */

    /**
     * @return the textValue
     */
    @Column(name = "txt_val", nullable = true, length = 50)
    public String getTextValue() {
	return textValue;
    }

    /**
     * @param textValue
     *            the textValue to set
     */
    public void setTextValue(String textValue) {
	this.textValue = textValue;
    }

    /**
     * @return the numberValue
     */
    @Column(name = "num_val", nullable = true)
    public Integer getNumberValue() {
	return numberValue;
    }

    /**
     * @param numberValue
     *            the numberValue to set
     */

    public void setNumberValue(Integer numberValue) {
	this.numberValue = numberValue;
    }

    /**
     * @return the booleanValue
     */
    @Column(name = "bln_val", nullable = true)
    public Boolean getBooleanValue() {
	return booleanValue;
    }

    /**
     * @param booleanValue
     *            the booleanValue to set
     */
    public void setBooleanValue(Boolean booleanValue) {
	this.booleanValue = booleanValue;
    }

    /**
     * @return the dateValue
     */
    @Column(name = "dte_val", nullable = true)
    public Date getDateValue() {
	return dateValue;
    }

    /**
     * @param dateValue
     *            the dateValue to set
     */
    public void setDateValue(Date dateValue) {
	this.dateValue = dateValue;
    }

    /**
     * @return the customMeta
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "custommeta_id", nullable = true)
    @ForeignKey(name = "FK_META_CFLDS")
    public CustomMeta getCustomMeta() {
	return customMeta;
    }

    /**
     * @param customMeta
     *            the customMeta to set
     */
    public void setCustomMeta(CustomMeta customMeta) {
	this.customMeta = customMeta;
    }

    /**
     * @return the uID
     */
    @Column(length = 36, name = "uid", nullable = false, unique = false)
    public String getUID() {
	return UID;
    }

    /**
     * @param uID
     *            the uID to set
     */
    public void setUID(String uID) {
	UID = uID;
    }

    /**
     * @param idx
     *            the idx to set
     */
    // public void setIdx(Integer idx) {
    // this.idx = idx;
    // }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "CustomField [customFieldId=" + customFieldId + ", textValue=" + textValue + ", numberValue="
		+ numberValue + ", booleanValue=" + booleanValue + ", dateValue=" + dateValue + "]";
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	CustomField other = (CustomField) obj;
	if (booleanValue == null) {
	    if (other.booleanValue != null) {
		return false;
	    }
	} else if (!booleanValue.equals(other.booleanValue)) {
	    return false;
	}
	if (customFieldId == null) {
	    if (other.customFieldId != null) {
		return false;
	    }
	} else if (!customFieldId.equals(other.customFieldId)) {
	    return false;
	}
	if (dateValue == null) {
	    if (other.dateValue != null) {
		return false;
	    }
	} else if (!dateValue.equals(other.dateValue)) {
	    return false;
	}
	if (numberValue == null) {
	    if (other.numberValue != null) {
		return false;
	    }
	} else if (!numberValue.equals(other.numberValue)) {
	    return false;
	}
	if (textValue == null) {
	    if (other.textValue != null) {
		return false;
	    }
	} else if (!textValue.equals(other.textValue)) {
	    return false;
	}
	return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((booleanValue == null) ? 0 : booleanValue.hashCode());
	result = prime * result + ((customFieldId == null) ? 0 : customFieldId.hashCode());
	result = prime * result + ((dateValue == null) ? 0 : dateValue.hashCode());
	result = prime * result + ((numberValue == null) ? 0 : numberValue.hashCode());
	result = prime * result + ((textValue == null) ? 0 : textValue.hashCode());
	return result;
    }

}
