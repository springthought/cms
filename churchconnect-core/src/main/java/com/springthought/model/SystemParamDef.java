/**
 *
 */
package com.springthought.model;

import com.google.common.base.Objects;
import com.springthought.Constants.DataServiceType;
import com.springthought.Constants.MetaDataType;
import org.hibernate.annotations.Index;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * @author eairrick
 *
 */
@Entity
@Table(name = "system_paramdef", uniqueConstraints = { @UniqueConstraint(columnNames = { "name" }, name = "UQ_NAME") })
//@Indexed
public class SystemParamDef extends BaseObject {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long paramDefID;
	private String name;
	private String descr;
	private MetaDataType dataType;
	private Boolean active = true;
	private Integer minValue = 0; // use for number type only
	private Integer maxValue = 9999; // use for number type only
	private Boolean required = false;
	private Set<SystemParamValue> systemParamValues = new HashSet<SystemParamValue>(
			0);

	private SystemReportDef systemReportDef;

	private String defaultValue = "";
	private Integer defaultValueNumber = 0;
	private Boolean defaultValueBoolean = false;
	private Date defaultValueDate = new Date();

	private String UID = java.util.UUID.randomUUID().toString();

	private String metaData = "";
	private DataServiceType dataService =  null;

	/**
	 * @param uID
	 *            the uID to set
	 */
	public void setUID(String uID) {
		UID = uID;
	}

	private CustomField customField;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "paramdef_id", unique = true, nullable = false)
	public Long getParamDefId() {
		return this.paramDefID;
	}

	public void setParamDefId(Long Id) {
		this.paramDefID = Id;
	}

	@Column(name = "name", length = 50, nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "dataService", length = 50, nullable = true)
	@Enumerated(EnumType.STRING)
	public DataServiceType getDataService() {
		return dataService;
	}

	public void setDataService(DataServiceType dataService) {
		this.dataService = dataService;
	}

	@Column(name = "descr", length = 100)
	public String getDescr() {
		return this.descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	/**
	 * @return the fieldType
	 */
	@Column(name = "data_type", nullable = false, length = 25)
	@Enumerated(EnumType.STRING)
	public MetaDataType getDataType() {
		return dataType;
	}

	/**
	 * @param fieldType
	 *            the fieldType to set
	 */
	public void setDataType(MetaDataType dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the systemParamValues
	 */

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "systemParamDef", orphanRemoval = true, cascade = CascadeType.ALL )
	public Set<SystemParamValue> getSystemParamValues() {
		return systemParamValues;
	}

	/**
	 * @param systemParamValues
	 *            the systemParamValues to set
	 */
	public void setSystemParamValues(Set<SystemParamValue> values) {
		this.systemParamValues = values;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reportdef_id", nullable = true)
	@Index(name = "rptdef_idx")
	public SystemReportDef getSystemReportDef() {
		return systemReportDef;
	}

	public void setSystemReportDef(SystemReportDef sysemReportDef) {
		this.systemReportDef = sysemReportDef;
	}

	/**
	 * @return the active
	 */
	@Column(name = "active", nullable = true)
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}



	/**
	 * @return the min_value
	 */
	@Column(name = "min_value", nullable = true)
	public Integer getMinValue() {
		return minValue;
	}

	/**
	 * @param min_value
	 *            the min_value to set
	 */
	public void setMinValue(Integer min_value) {
		this.minValue = min_value;
	}

	/**
	 * @return the max_value
	 */
	@Column(name = "max_value", nullable = true)
	public Integer getMaxValue() {
		return maxValue;
	}

	/**
	 * @param max_value
	 *            the max_value to set
	 */
	public void setMaxValue(Integer max_value) {
		this.maxValue = max_value;
	}

	/**
	 * @return the required
	 */
	public Boolean getRequired() {
		return required;
	}

	/**
	 * @param required
	 *            the required to set
	 */
	public void setRequired(Boolean required) {
		this.required = required;
	}

	/**
	 * @return the defalutValue
	 */

	@Column(name = "default_txt", nullable = true, length = 50)
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defalutValue
	 *            the defalutValue to set
	 */
	public void setDefaultValue(String defalutValue) {
		this.defaultValue = defalutValue;
	}

	/**
	 * @return the defalutValueNumber
	 */
	@Column(name = "default_nbr", nullable = true)
	public Integer getDefaultValueNumber() {
		return defaultValueNumber;
	}

	/**
	 * @param defalutValueNumber
	 *            the defalutValueNumber to set
	 */
	public void setDefaultValueNumber(Integer defalutValueNumber) {
		this.defaultValueNumber = defalutValueNumber;
	}

	/**
	 * @return the defalutValueBoolean
	 */
	@Column(name = "default_bln", nullable = true)
	public Boolean getDefaultValueBoolean() {
		return this.defaultValueBoolean;
	}

	/**
	 * @param defalutValueBoolean
	 *            the defalutValueBoolean to set
	 */
	public void setDefaultValueBoolean(Boolean defalutValueBoolean) {
		this.defaultValueBoolean = defalutValueBoolean;
	}

	/**
	 * @return the defalutValueDate
	 */
	@Column(name = "default_dte", nullable = true)
	public Date getDefaultValueDate() {
		return defaultValueDate;
	}

	/**
	 * @param defalutValueDate
	 *            the defalutValueDate to set
	 */
	public void setDefaultValueDate(Date defalutValueDate) {
		this.defaultValueDate = defalutValueDate;
	}

	/**
	 * @return the custmField
	 */
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "customMeta", orphanRemoval = true, cascade = CascadeType.ALL )
	public CustomField getCustomField() {
		return customField;
	}

	/**
	 * @param custmField
	 *            the custmField to set
	 */
	public void setCustomField(CustomField customField) {
		this.customField = customField;
	}

	/**
	 * @return the uID
	 */
	@Column(length = 36, name = "uid", nullable = false, unique = true)
	public String getUID() {
		return UID;
	}

	@Column(length = 30, name = "metadata", nullable = true, unique = false)
	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	@Override
	public String toString() {
		return "SystemParamDef [paramDefID=" + paramDefID + ", name=" + name
				+ ", descr=" + descr + ", dataType=" + dataType + ", active="
				+ active + ", customFieldType=" + ", minValue=" + minValue + ", maxValue=" + maxValue
				+ ", required=" + required + ", systemParamValues="
				+ systemParamValues + ", defaultValue=" + defaultValue
				+ ", defaultValueNumber=" + defaultValueNumber
				+ ", defaultValueBoolean=" + defaultValueBoolean
				+ ", defaultValueDate=" + defaultValueDate + ", UID=" + UID
				+ ", metaData=" + metaData + ", customField=" + customField
				+ "]";
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(paramDefID, getName(), getDescr(),
				getDataType(), getActive(), getMinValue(), getMaxValue(),
				getRequired(), getDefaultValue(), getDefaultValueNumber(),
				getDefaultValueBoolean(), getDefaultValueDate(), getUID(),
				getMetaData(), getCustomField());
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof SystemParamDef) {
			SystemParamDef that = (SystemParamDef) object;
			return Objects.equal(this.paramDefID, that.paramDefID)
					&& Objects.equal(this.getName(), that.getName())
					&& Objects.equal(this.getDescr(), that.getDescr())
					&& Objects.equal(this.getDataType(), that.getDataType())
					&& Objects.equal(this.getActive(), that.getActive())
					&& Objects.equal(this.getMinValue(), that.getMinValue())
					&& Objects.equal(this.getMaxValue(), that.getMaxValue())
					&& Objects.equal(this.getRequired(), that.getRequired())
					&& Objects.equal(this.getDefaultValue(),
							that.getDefaultValue())
					&& Objects.equal(this.getDefaultValueNumber(),
							that.getDefaultValueNumber())
					&& Objects.equal(this.getDefaultValueBoolean(),
							that.getDefaultValueBoolean())
					&& Objects.equal(this.getDefaultValueDate(),
							that.getDefaultValueDate())
					&& Objects.equal(this.getUID(), that.getUID())
					&& Objects.equal(this.getMetaData(), that.getMetaData())
					&& Objects.equal(this.getCustomField(),
							that.getCustomField());
		}
		return false;
	}

}
