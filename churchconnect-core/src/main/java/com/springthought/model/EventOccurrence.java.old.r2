package com.springthought.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "event_occur")
public class EventOccurrence extends Event implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date setStartDate;
	private Date setEndDate;
	private Event event;
	
	/**
	 * @TODO: remove parent field
	 */
	private boolean parent = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "setStartDate", length = 19)
	public Date getSetStartDate() {
		return setStartDate;
	}

	public void setSetStartDate(Date setStartDate) {
		this.setStartDate = setStartDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "setEndDate", length = 19)
	public Date getSetEndDate() {
		return setEndDate;
	}

	public void setSetEndDate(Date setEndDate) {
		this.setEndDate = setEndDate;
	}

	@Column(name = "parent", nullable = true)
	public boolean isParent() {
		return parent;
	}

	public void setParent(boolean parent) {
		this.parent = parent;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id", nullable = false)
	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

}
