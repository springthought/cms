package com.springthought.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Base class for Model objects. Child objects should implement toString(),
 * equals() and hashCode().
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@MappedSuperclass
public abstract class BaseObject implements Serializable {

    private static final long serialVersionUID = 6562191742274042053L;
	private Integer version;
	private String created_by_user;
	private String updated_by_user;
	private Date create_date;
	private Date last_update;
	protected final Logger log =LoggerFactory.getLogger(getClass());



	@Version
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * @return the created_by_user
	 */
	@Column(name = "created_by_user")
	public String getCreatedByUser() {
		return created_by_user;
	}

	/**
	 * @param created_by_user
	 *            the created_by_user to set
	 */
	public void setCreatedByUser(String created_by_user) {
		this.created_by_user = created_by_user;
	}

	/**
	 * @return the updated_by_user
	 */
	@Column(name = "updated_by_user")
	public String getUpdatedByUser() {
		return updated_by_user;
	}

	/**
	 * @param updated_by_user
	 *            the updated_by_user to set
	 */
	public void setUpdatedByUser(String updated_by_user) {
		this.updated_by_user = updated_by_user;
	}

	/**
	 * @return the create_date
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	public Date getCreateDate() {
		return create_date;
	}

	/**
	 * @param create_date
	 *            the create_date to set
	 */
	public void setCreateDate(Date create_date) {
		this.create_date = create_date;
	}

	/**
	 *
	 * @return last_update
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update", length = 19)
	public Date getLastUpdate() {
		return this.last_update;
	}

	/**
	 * @param lastupdate
	 *            the last update to set
	 */
	public void setLastUpdate(Date lastUpdate) {
		this.last_update = lastUpdate;
	}

	/**
	 * Returns a multi-line String with key=value pairs.
	 *
	 * @return a String representation of this class.
	 */
	public abstract String toString();

	/**
	 * Compares object equality. When using Hibernate, the primary key should
	 * not be a part of this comparison.
	 *
	 * @param o
	 *            object to compare to
	 * @return true/false based on equality tests
	 */
	public abstract boolean equals(Object o);

	/**
	 * When you override equals, you should override hashCode. See "Why are
	 * equals() and hashCode() importation" for more information:
	 * http://www.hibernate.org/109.html
	 *
	 * @return hashCode
	 */
	public abstract int hashCode();

}
