/**
 *
 */
package com.springthought.model;

import org.hibernate.annotations.Index;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author eairrick
 *
 */
@Entity
@Table(name = "custom_meta_value")
public class CustomMetaValue extends BaseObject implements ILabelValue {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Long valueListId;
	private String listLabel = "";
	private String descr = "";

	private CustomMeta customMeta;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "valuelist_id", unique = true, nullable = false)
	public Long getValueListId() {
		return this.valueListId;
	}

	public void setValueListId(Long valueListId) {
		this.valueListId = valueListId;
	}

	@Column(name = "list_label", length = 50)
	public String getListLabel() {
		return this.listLabel;
	}

	public void setListLabel(String listLabel) {
		this.listLabel = listLabel;
	}

	@Column(name = "descr", length = 100)
	public String getDescr() {
		return this.descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	/**
	 * @return the customMeta
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "custommeta_id", nullable = true)
	@Index(name = "CUSTOMMETAID_IDX")
	public CustomMeta getCustomMeta() {
		return customMeta;
	}

	/**
	 * @param customMeta
	 *            the customMeta to set
	 */
	public void setCustomMeta(CustomMeta customMeta) {
		this.customMeta = customMeta;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descr == null) ? 0 : descr.hashCode());
		result = prime * result
				+ ((listLabel == null) ? 0 : listLabel.hashCode());
		result = prime * result
				+ ((valueListId == null) ? 0 : valueListId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CustomMetaValue other = (CustomMetaValue) obj;
		if (descr == null) {
			if (other.descr != null) {
				return false;
			}
		} else if (!descr.equals(other.descr)) {
			return false;
		}
		if (listLabel == null) {
			if (other.listLabel != null) {
				return false;
			}
		} else if (!listLabel.equals(other.listLabel)) {
			return false;
		}
		if (valueListId == null) {
			if (other.valueListId != null) {
				return false;
			}
		} else if (!valueListId.equals(other.valueListId)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomMetaValue [valueListId=" + valueListId + ", listLabel="
				+ listLabel + ", descr=" + descr + "]";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.springthought.model.ILabelValue#getLabel()
	 */
	@Override
	@Transient
	public String getLabel() {
		return this.descr;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.springthought.model.ILabelValue#setLabel(java.lang.String)
	 */
	@Override
	public void setLabel(String label) {
		this.descr = label;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.springthought.model.ILabelValue#getValue()
	 */
	@Override
	@Transient
	public String getValue() {
		return this.listLabel;
	}

	/*
	 * (non-Javadoc)d
	 *
	 * @see com.springthought.model.ILabelValue#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.listLabel = value;

	}

	@Override
	public int compareTo(Object o) {
		// Implicitly tests for the correct type, throwing
		// ClassCastException as required by interface
		String otherLabel = ((ILabelValue) o).getLabel();
		return this.getLabel().compareTo(otherLabel);
	}

}
