/**
 *
 */
package com.springthought.model;

import com.springthought.Constants.CustomFieldType;
import com.springthought.util.Queries;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author eairrick
 *
 */
@Entity
@Table(name = "custom_meta", uniqueConstraints = {@UniqueConstraint(columnNames = {
        "tenant_id", "name"}, name = "UQ_TNNT_NAME")})
@Filters({@Filter(name = "teantFilter", condition = ":teantFilterParam = tenant_id")})
@NamedNativeQueries({
        @NamedNativeQuery(
                name = Queries.CUSTOMVALUES_FIND_BY_FIELD_ID,
                query = "select * from custom_meta_value c where c.custommeta_id = :custommeta_id", resultClass = CustomMetaValue.class
        ),
        @NamedNativeQuery(
                name = Queries.CUSTOMMETA_CHECK_EXISTS,
                query = "select * from custom_meta as cm where  ( cm.active = 1 and cm.customFieldType = :type) and cm.uid not in (select cf.uid from customfield cf where cf.person_id = :id ) order by name", resultClass = CustomMeta.class
        )})
@NamedQueries({
        @NamedQuery(name = Queries.CUSTOMMETA_FIND_BY_NAME, query = "from CustomMeta c where trim(lower(c.name)) = :name order by c.name")
})
public class CustomMeta extends BaseObject implements ITenant {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long customMetaId;
    private Long tenantId;
    private String name;
    private String descr;
    private String dataType = "DT";
    private Boolean active = true;
    private CustomFieldType customFieldType = CustomFieldType.PERSON; // H=ouseHold
    // P=erson
    private Integer minValue = 0; // use for number type only
    private Integer maxValue = 9999; // use for number type only
    private Boolean required = false;
    private Set<CustomMetaValue> customMetaValues = new HashSet<CustomMetaValue>(
            0);
    private String defaultValue = "";
    private Integer defaultValueNumber = 0;
    private Boolean defaultValueBoolean = false;
    private Date defaultValueDate = new Date();

    private String UID = java.util.UUID.randomUUID().toString();

    private String metaData = "";
    private CustomField customField;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "custommeta_id", unique = true, nullable = false)
    public Long getCustomMetaId() {
        return this.customMetaId;
    }

    public void setCustomMetaId(Long customMetaId) {
        this.customMetaId = customMetaId;
    }

    @Column(name = "name", length = 50, nullable = false)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "descr", length = 100)
    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;

    }

    /**
     * @return the fieldType
     */
    @Column(name = "data_type", nullable = false, length = 2)
    public String getDataType() {
        return dataType;
    }

    /**
     * @param fieldType
     *            the fieldType to set
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the customMetaValues
     */

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customMeta", orphanRemoval = true)
    @Cascade({CascadeType.ALL, CascadeType.DELETE})
    public Set<CustomMetaValue> getCustomMetaValues() {
        return customMetaValues;
    }

    /**
     * @param customMetaValues
     *            the customMetaValues to set
     */
    public void setCustomMetaValues(Set<CustomMetaValue> customMetaValues) {
        this.customMetaValues = customMetaValues;
    }

    /**
     * @return the active
     */
    @Column(name = "active", nullable = true)
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the customFieldType
     */
    @Column(name = "customFieldType", length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    public CustomFieldType getCustomFieldType() {
        return customFieldType;
    }

    /**
     * @param customFieldType
     *            the customFieldType to set
     */
    public void setCustomFieldType(CustomFieldType customFieldType) {
        this.customFieldType = customFieldType;
    }

    /**
     * @return the min_value
     */
    @Column(name = "min_value", nullable = true)
    public Integer getMinValue() {
        return minValue;
    }

    /**
     * @param min_value
     *            the min_value to set
     */
    public void setMinValue(Integer min_value) {
        this.minValue = min_value;
    }

    /**
     * @return the max_value
     */
    @Column(name = "max_value", nullable = true)
    public Integer getMaxValue() {
        return maxValue;
    }

    /**
     * @param max_value
     *            the max_value to set
     */
    public void setMaxValue(Integer max_value) {
        this.maxValue = max_value;
    }

    /**
     * @return the required
     */
    public Boolean getRequired() {
        return required;
    }

    /**
     * @param required
     *            the required to set
     */
    public void setRequired(Boolean required) {
        this.required = required;
    }

    /**
     * @return the defalutValue
     */

    @Column(name = "default_txt", nullable = true, length = 50)
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * @param defalutValue
     *            the defalutValue to set
     */
    public void setDefaultValue(String defalutValue) {
        this.defaultValue = defalutValue;
    }

    /**
     * @return the defalutValueNumber
     */
    @Column(name = "default_nbr", nullable = true)
    public Integer getDefaultValueNumber() {
        return defaultValueNumber;
    }

    /**
     * @param defalutValueNumber
     *            the defalutValueNumber to set
     */
    public void setDefaultValueNumber(Integer defalutValueNumber) {
        this.defaultValueNumber = defalutValueNumber;
    }

    /**
     * @return the defalutValueBoolean
     */
    @Column(name = "default_bln", nullable = true)
    public Boolean getDefaultValueBoolean() {
        return this.defaultValueBoolean;
    }

    /**
     * @param defalutValueBoolean
     *            the defalutValueBoolean to set
     */
    public void setDefaultValueBoolean(Boolean defalutValueBoolean) {
        this.defaultValueBoolean = defalutValueBoolean;
    }

    /**
     * @return the defalutValueDate
     */
    @Column(name = "default_dte", nullable = true)
    public Date getDefaultValueDate() {
        return defaultValueDate;
    }

    /**
     * @param defalutValueDate
     *            the defalutValueDate to set
     */
    public void setDefaultValueDate(Date defalutValueDate) {
        this.defaultValueDate = defalutValueDate;
    }

    /**
     * @return the custmField
     */
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "customMeta", orphanRemoval = true)
    @Cascade({CascadeType.ALL, CascadeType.DELETE})
    public CustomField getCustomField() {
        return customField;
    }

    /**
     * @param custmField
     *            the custmField to set
     */
    public void setCustomField(CustomField customField) {
        this.customField = customField;
    }

    /**
     * @return the uID
     */
    @Column(length = 36, name = "uid", nullable = false, unique = true)
    public String getUID() {
        return UID;
    }

    /**
     * @param uID
     *            the uID to set
     */
    public void setUID(String uID) {
        UID = uID;
    }

    @Column(length = 30, name = "metaData", nullable = true, unique = false)
    protected String getMetaData() {
        return metaData;
    }

    protected void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CustomMeta [customMetaId=" + customMetaId + ", tenantId="
                + tenantId + ", name=" + name + ", descr=" + descr
                + ", dataType=" + dataType + ", active=" + active + "]";
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CustomMeta other = (CustomMeta) obj;
        if (active == null) {
            if (other.active != null) {
                return false;
            }
        } else if (!active.equals(other.active)) {
            return false;
        }
        if (customMetaId == null) {
            if (other.customMetaId != null) {
                return false;
            }
        } else if (!customMetaId.equals(other.customMetaId)) {
            return false;
        }
        if (dataType == null) {
            if (other.dataType != null) {
                return false;
            }
        } else if (!dataType.equals(other.dataType)) {
            return false;
        }
        if (descr == null) {
            if (other.descr != null) {
                return false;
            }
        } else if (!descr.equals(other.descr)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (tenantId == null) {
            if (other.tenantId != null) {
                return false;
            }
        } else if (!tenantId.equals(other.tenantId)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result
                + ((customMetaId == null) ? 0 : customMetaId.hashCode());
        result = prime * result
                + ((dataType == null) ? 0 : dataType.hashCode());
        result = prime * result + ((descr == null) ? 0 : descr.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result
                + ((tenantId == null) ? 0 : tenantId.hashCode());
        return result;
    }

}
