package com.springthought.model;

// Generated Oct 7, 2012 10:37:22 AM by Hibernate Tools 3.4.0.CR1

import com.springthought.util.Queries;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

//import org.hibernate.annotations.Cascade;
//import org.hibernate.annotations.CascadeType;

/**
 * Attendance generated by hbm2java
 */
@Entity
@Table(name = "attendance")
@FilterDef(name = "tenantFilter", parameters = @ParamDef(name = "tenantFilterParam", type = "long"))
@Filters({ @Filter(name = "tenantFilter", condition = "tenant_id = :tenantFilterParam") })
@NamedQueries({
        @NamedQuery(name = Queries.ATTENDANCE_BY_PERSONID,query = "from Attendance")
})
public class Attendance extends BaseObject implements java.io.Serializable, ITenant {

    /**
     *
     */
    private static final long serialVersionUID = 6760574995199849515L;
    private Long attendanceId;
    private Long tenantId;
    private Event event;
    private List<Person> persons = new ArrayList<Person>(0);
    private Integer generalCount = new Integer(0);

    private String name;

    public Attendance() {
    }

    public Attendance(String name) {
	this.name = name;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "attendance_id", unique = true, nullable = false)
    public Long getAttendanceId() {
	return this.attendanceId;
    }

    public void setAttendanceId(Long attendanceId) {
	this.attendanceId = attendanceId;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "event_id", nullable = false)
    @ForeignKey(name = "FK_ATTNDN_HAS_EVNT")
    public Event getEvent() {
	return this.event;
    }

    public void setEvent(Event event) {
	this.event = event;
    }

    @Override
    @Column(name = "tenant_id", nullable = false)
    @Index(name = "TENANTID_IDX")
    public Long getTenantId() {
	return tenantId;
    }

    @Override
    public void setTenantId(Long tenantId) {
	this.tenantId = tenantId;

    }

    /**
     * @return the generalCount
     */
    @Column(name = "general_count", unique = false, nullable = true)
    public Integer getGeneralCount() {
	return generalCount;
    }

    /**
     * @param generalCount
     *                         the generalCount to set
     */
    public void setGeneralCount(Integer generalCount) {
	this.generalCount = generalCount;
    }

    @Column(name = "name", nullable = false, length = 50)
    // Event Name + Start Date
    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    // @ManyToMany(mappedBy = "attendances", fetch = FetchType.LAZY)

    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinTable(name = "attendance_person", joinColumns = @JoinColumn(name = "attendance_id"), inverseJoinColumns = @JoinColumn(name = "person_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
	    "attendance_id", "person_id" }, name = "uq_atd_prs"))
    @ForeignKey(name = "FK_ATTNDN_HAS_PRSN")
    public List<Person> getPersons() {
	return persons;
    }

    public void setPersons(List<Person> persons) {
	this.persons = persons;
    }

    @Override
    public String toString() {
	return "Attendance [attendanceId=" + attendanceId + "]";
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((attendanceId == null) ? 0 : attendanceId.hashCode());
	return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Attendance other = (Attendance) obj;
	if (attendanceId == null) {
	    if (other.attendanceId != null) {
		return false;
	    }
	} else if (!attendanceId.equals(other.attendanceId)) {
	    return false;
	}
	return true;
    }

    public void removeAttendancesFromPeople() {
	getPersons().forEach(p -> p.getAttendances().remove(this));
    }

}
