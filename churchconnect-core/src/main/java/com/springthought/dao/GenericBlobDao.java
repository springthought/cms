package com.springthought.dao;

import java.io.Serializable;


public interface GenericBlobDao<T, PK extends Serializable> {
	
	 T save(T object);
	 void remove(T object);
	 T get(PK id);

}
