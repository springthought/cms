package com.springthought.dao;

import com.springthought.Constants.CodeListType;
import com.springthought.model.Role;
import com.springthought.model.SystemValue;
import com.springthought.model.UserValue;

import java.util.List;

/**
 * Lookup Data Access Object (GenericDao) interface.  This is used to lookup values in
 * the database (i.e. for drop-downs).
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public interface LookupDao extends GenericDao {
    //~ Methods ================================================================

    /**
     * Returns all Roles ordered by name
     * @return populated list of roles
     */
    List<Role> getRoles();
    List<UserValue> getHouseHoldStatus();
    List<SystemValue> getSystemValueByCodeListType(CodeListType codelist);
    
}
 