package com.springthought.dao;

import com.springthought.model.ILobValue;
import org.springframework.jdbc.core.JdbcTemplate;

public interface BlobDao {

	/**
	 * @return the jdbcTemplate
	 */
	public abstract JdbcTemplate getJdbcTemplate();

	/**
	 * @param jdbcTemplate
	 *            the jdbcTemplate to set
	 */
	public abstract void setJdbcTemplate(JdbcTemplate jdbcTemplate);

	public abstract void save(ILobValue lv);

	public abstract ILobValue get(Long iD);

	public abstract void remove(ILobValue lv);

}
