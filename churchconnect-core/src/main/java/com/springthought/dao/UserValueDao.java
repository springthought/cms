package com.springthought.dao;

import com.springthought.Constants.UserValueType;
import com.springthought.model.UserValue;

import java.util.List;

public interface UserValueDao extends GenericDao<UserValue, Long> {

	public List<UserValue> getUserValueByType(UserValueType type);
}
