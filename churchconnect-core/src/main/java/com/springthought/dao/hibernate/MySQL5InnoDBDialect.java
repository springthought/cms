package com.springthought.dao.hibernate;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.spi.ViolatedConstraintNameExtracter;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MySQL5InnoDBDialect extends org.hibernate.dialect.MySQL5InnoDBDialect {

    /**
     * Pattern to extract violated constraint name from {@link SQLException}.
     */
    private static final Pattern PATTERN = Pattern.compile(".*constraint\\W+(\\w+).*", Pattern.CASE_INSENSITIVE);

    /**
     * Pattern to extract duplicate key constraint name
     */
    private final String DUPLICATE_CONSTRAINT_MSG = "Duplicate Entry";
    private final String DUPLICATE_CONSTRAINT_NAME = "UQ_DUPLICATE_ENTRY";

    private ViolatedConstraintNameExtracter constraintNameExtracter;

    public MySQL5InnoDBDialect() {
	constraintNameExtracter = new ConstraintNameExtractor();
    }

    @Override
    public ViolatedConstraintNameExtracter getViolatedConstraintNameExtracter() {
	return constraintNameExtracter;
    }

    private class ConstraintNameExtractor implements ViolatedConstraintNameExtracter {

	@Override
	public String extractConstraintName(SQLException sqle) {
	    final String msg = sqle.getMessage();
	    final Matcher matcher = PATTERN.matcher(msg);
	    String constraintName = null;
	    if (matcher.matches()) {
		constraintName = matcher.group(1);
	    } else if (StringUtils.containsIgnoreCase(msg, DUPLICATE_CONSTRAINT_MSG)) {
		constraintName = DUPLICATE_CONSTRAINT_NAME;
	    }

	    return constraintName;
	}

    }

}
