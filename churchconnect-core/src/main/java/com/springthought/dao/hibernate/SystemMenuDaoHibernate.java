package com.springthought.dao.hibernate;

import com.springthought.Constants.MenuType;
import com.springthought.dao.SystemMenuDao;
import com.springthought.model.SystemMenu;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("systemMenuDao")
public class SystemMenuDaoHibernate extends GenericDaoHibernate<SystemMenu, Long> implements SystemMenuDao {

	public SystemMenuDaoHibernate() {
		super(SystemMenu.class);
	}

	@SuppressWarnings("unchecked")
	public List<SystemMenu> findByMenuType(MenuType menuType) {
		Criteria crtr = getSession().createCriteria(SystemMenu.class);
		crtr.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		crtr.add(Restrictions.eq("menu_type", menuType));
		return crtr.list();

	}
}
