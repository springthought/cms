package com.springthought.dao.hibernate;

import com.springthought.model.BaseObject;
import com.springthought.model.ITenant;
import com.springthought.service.SecurityContextManager;
import com.springthought.service.impl.SecurityContextManagerImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Date;


/**
 * @author eairrick
 *
 */
public class TenantInterceptor extends EmptyInterceptor {

	private static final long serialVersionUID = 3366241194333006918L;
	protected final Log log = LogFactory.getLog(getClass());


	SecurityContextManager securityContext = new SecurityContextManagerImpl();



	/**
	 * @author eairrick
	 */
	public TenantInterceptor() {
		super();
	}

	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state,
			String[] propertyNames, Type[] types) {

		Boolean HasStateBeenModified = false;

		if (entity instanceof BaseObject) {
			for (int i = 0; 1 < propertyNames.length; i++) {
				if ("createDate".equals(propertyNames[i])) {
					state[i] = new Date();
					HasStateBeenModified =  true;
					break;
				}
			}
		}

		if (entity instanceof ITenant) {
			for (int i = 0; 1 < propertyNames.length; i++) {
				if ("tenantId".equals(propertyNames[i])) {
					state[i] = securityContext.retriveTenantContext();
					HasStateBeenModified =  true;
					break;
				}
			}
		}

		return HasStateBeenModified;
	}
}
