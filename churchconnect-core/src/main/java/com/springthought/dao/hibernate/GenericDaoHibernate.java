package com.springthought.dao.hibernate;

import com.springthought.dao.GenericDao;
import com.springthought.dao.SearchException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.orm.ObjectRetrievalFailureException;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;


/**
 * This class serves as the Base class for all other DAOs - namely to hold
 * common CRUD methods that they might all use. You should only need to extend
 * this class when your require custom CRUD logic.
 * <p/>
 * <p>
 * To register this class in your Spring context file, use the following XML.
 *
 * <pre>
 *      &lt;bean id="fooDao" class="com.springthought.dao.hibernate.GenericDaoHibernate"&gt;
 *          &lt;constructor-arg value="com.springthought.model.Foo"/&gt;
 *      &lt;/bean&gt;
 * </pre>
 *
 * @param <T>  a type variable
 * @param <PK> the primary key for that type
 * @author <a href="mailto:bwnoll@gmail.com">Bryan Noll</a> Updated by jgarcia:
 * update hibernate3 to hibernate4
 * @author jgarcia (update: added full text search + reindexing)
 */
public class GenericDaoHibernate<T, PK extends Serializable> implements GenericDao<T, PK> {
    /**
     * Log variable for all child classes. Uses LogFactory.getLog(getClass())
     * from Commons Logging
     */
    protected final Log log = LogFactory.getLog(getClass());
    private Class<T> persistentClass;

    @Resource
    private SessionFactory sessionFactory;
    //private Analyzer defaultAnalyzer;


    @Resource
    private Validator validator;

    /**
     * Constructor that takes in a class to see which type of entity to persist.
     * Use this constructor when subclassing.
     *
     * @param persistentClass the class type you'd like to persist
     */
    public GenericDaoHibernate(final Class<T> persistentClass) {
        this.persistentClass = persistentClass;
        //defaultAnalyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
    }

    /**
     * Constructor that takes in a class and sessionFactory for easy creation of
     * DAO.
     *
     * @param persistentClass the class type you'd like to persist
     * @param sessionFactory  the pre-configured Hibernate SessionFactory
     */
    public GenericDaoHibernate(final Class<T> persistentClass, SessionFactory sessionFactory) {
        this.persistentClass = persistentClass;
        this.sessionFactory = sessionFactory;
        //defaultAnalyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    public Session getSession() throws HibernateException {

        Session sess = getSessionFactory().getCurrentSession();
        if (sess == null) {
            sess = getSessionFactory().openSession();
        }

        return sess;
    }

    @Autowired
    @Required
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Autowired
    @Required
    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    @Override
    public Validator getValidator() {
        return validator;
    }


    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        Session sess = getSession();
        /**
         *
         * Add the DISTINCT_ROOT_ENTITY to override Hibernates default behavior
         * is that you get one entity for each row returned by the database. If
         * you don't want to change the eager fetching, but do want to remove
         * duplicates in your result, you need to use this ResultTransformer: I
         * thinking I could have use the getAllDistinct() instead. Both the new
         * Transformer implementation and AllDistinct() return the same rows see
         * http
         * ://stackoverflow.com/questions/12795921/hibernate-getting-too-many
         * -rows
         */
        // Criteria criteria =
        // sess.createCriteria(persistentClass).setResultTransformer(
        // Criteria.DISTINCT_ROOT_ENTITY);
        // return criteria.list();

        return sess.createCriteria(persistentClass).list();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<T> getAllDistinct() {
        Collection<T> result = new LinkedHashSet<T>(getAll());
        return new ArrayList<T>(result);
    }

    /**
     * {@inheritDoc}
     */
    public List<T> search(String searchTerm) throws SearchException {

	/*Session sess = getSession();
	FullTextSession txtSession = Search.getFullTextSession(sess);

	org.apache.lucene.search.Query qry;

	try {
	    qry = HibernateSearchTools.generateQuery(searchTerm, this.persistentClass, sess, defaultAnalyzer);
	} catch (ParseException ex) {
	    throw new SearchException(ex);
	}
	org.hibernate.search.FullTextQuery hibQuery = txtSession.createFullTextQuery(qry, this.persistentClass);
	return hibQuery.list();*/

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public T get(PK id) {
        Session sess = getSession();
        IdentifierLoadAccess byId = sess.byId(persistentClass);
        T entity = (T) byId.load(id);

        if (entity == null) {
            log.warn("Uh oh, '" + this.persistentClass + "' object with id '" + id + "' not found...");
            throw new ObjectRetrievalFailureException(this.persistentClass, id);
        }

        return entity;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public boolean exists(PK id) {
        Session sess = getSession();
        IdentifierLoadAccess byId = sess.byId(persistentClass);
        T entity = (T) byId.load(id);
        return entity != null;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public T save(T object) {
        Session sess = getSession();
        return (T) sess.merge(object);

    }

    /**
     * {@inheritDoc}
     */
    public void remove(T object) {
        Session sess = getSession();
        sess.delete(object);
    }

    /**
     * {@inheritDoc}
     */
    public void remove(PK id) {
        Session sess = getSession();
        IdentifierLoadAccess byId = sess.byId(persistentClass);
        T entity = (T) byId.load(id);
        sess.delete(entity);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<T> findByNamedQuery(String queryName, Map<String, Object> queryParams) {

        Session sess = getSession();

        Query namedQuery = sess.getNamedQuery(queryName);

        if (queryParams != null) {
            for (String s : queryParams.keySet()) {
                namedQuery.setParameter(s, queryParams.get(s));
            }
        }

        return namedQuery.list();
    }

    /**
     * {@inheritDoc}
     */
    public void reindex() {
        //HibernateSearchTools.reindex(persistentClass, getSessionFactory().getCurrentSession());
    }

    /**
     * {@inheritDoc}
     */
    public void reindexAll(boolean async) {
        //HibernateSearchTools.reindexAll(async, getSessionFactory().getCurrentSession());
    }

    public void doWork() throws SQLException {

        getSession().doWork(new Work() {

            public void execute(Connection conn) throws SQLException {

            }
        });

    }

    /**
     * @return the persistentClass
     */
    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    @Override
    public Set<ConstraintViolation<T>> validate(T object) {

        Set<ConstraintViolation<T>> constraintViolations =
                validator.validate(object);
        return constraintViolations;
    }



}
