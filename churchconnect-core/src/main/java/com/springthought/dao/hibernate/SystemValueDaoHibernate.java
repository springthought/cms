package com.springthought.dao.hibernate;

import com.springthought.Constants.CodeListType;
import com.springthought.dao.SystemValueDao;
import com.springthought.model.SystemValue;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("systemValueDao")
public class SystemValueDaoHibernate extends GenericDaoHibernate<SystemValue, Long>
		implements SystemValueDao {


	public SystemValueDaoHibernate() {
		super(SystemValue.class);
	}


	/* (non-Javadoc)
	 * @see com.springthought.dao.SystemValueDao#getSystemValueByCodeListType(com.springthought.Constants.CodeListType)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SystemValue> getSystemValueByCodeListType(CodeListType codelist) {
		Criteria crtr = getSession().createCriteria(SystemValue.class);
		crtr.add(Restrictions.eq("code_list", codelist));
		return crtr.list();
	}





}
