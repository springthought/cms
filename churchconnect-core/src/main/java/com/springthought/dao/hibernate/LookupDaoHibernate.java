package com.springthought.dao.hibernate;

import com.springthought.Constants.CodeListType;
import com.springthought.Constants.UserValueType;
import com.springthought.dao.LookupDao;
import com.springthought.dao.SearchException;
import com.springthought.model.Role;
import com.springthought.model.SystemValue;
import com.springthought.model.UserValue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Hibernate implementation of LookupDao.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 * jgarcia: updated to hibernate 4
 */
@Repository("lookupDao")
public class LookupDaoHibernate implements LookupDao {

    private Log log = LogFactory.getLog(LookupDaoHibernate.class);
    private final SessionFactory sessionFactory;


    /**
     * Initialize LookupDaoHibernate with Hibernate SessionFactory.
     *
     * @param sessionFactory
     */
    @Autowired
    public LookupDaoHibernate(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<Role> getRoles() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Role.class).list();
    }

    public List<UserValue> getHouseHoldStatus() {
        return this.getUserValueByType(UserValueType.STATUS);
    }

    @SuppressWarnings("unchecked")
    private List<UserValue> getUserValueByType(UserValueType userValuetype) {
        Criteria crtr = sessionFactory.getCurrentSession().createCriteria(
                UserValue.class);
        crtr.add(Restrictions.eq("value_Type", userValuetype));
        return crtr.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SystemValue> getSystemValueByCodeListType(CodeListType codelist) {
        Criteria crtr = sessionFactory.getCurrentSession().createCriteria(SystemValue.class);
        crtr.add(Restrictions.eq("code_list", codelist));
        return crtr.list();
    }

    /**
     * Generic method used to get all objects of a particular type. This
     * is the same as lookup up all rows in a table.
     *
     * @return List of populated objects
     */
    @Override
    public List getAll() {
        return null;
    }

    /**
     * Gets all records without duplicates.
     * <p>Note that if you use this method, it is imperative that your model
     * classes correctly implement the hashcode/equals methods</p>
     *
     * @return List of populated objects
     */
    @Override
    public List getAllDistinct() {
        return null;
    }

    /**
     * Gets all records that match a search term. "*" will get them all.
     *
     * @param searchTerm the term to search for
     * @return the matching records
     * @throws SearchException
     */
    @Override
    public List search(String searchTerm) throws SearchException {
        return null;
    }

    /**
     * Generic method to get an object based on class and identifier. An
     * ObjectRetrievalFailureException Runtime Exception is thrown if
     * nothing is found.
     *
     * @param id the identifier (primary key) of the object to get
     * @return a populated object
     * @see ObjectRetrievalFailureException
     */
    @Override
    public Object get(Serializable id) {
        return null;
    }

    /**
     * Checks for existence of an object of type T using the id arg.
     *
     * @param id the id of the entity
     * @return - true if it exists, false if it doesn't
     */
    @Override
    public boolean exists(Serializable id) {
        return false;
    }

    /**
     * Generic method to save an object - handles both update and insert.
     *
     * @param object the object to save
     * @return the persisted object
     */
    @Override
    public Object save(Object object) {
        return null;
    }

    /**
     * Generic method to delete an object
     *
     * @param object the object to remove
     */
    @Override
    public void remove(Object object) {

    }

    /**
     * Generic method to delete an object
     *
     * @param id the identifier (primary key) of the object to remove
     */
    @Override
    public void remove(Serializable id) {

    }

    /**
     * Generic method to regenerate full text index of the persistent class T
     */
    @Override
    public void reindex() {

    }

    /**
     * Generic method to regenerate full text index of all indexed classes
     *
     * @param async true to perform the reindexing asynchronously
     */
    @Override
    public void reindexAll(boolean async) {

    }

    /**
     * Generic method to execute custom SQL
     */
    @Override
    public void doWork() throws SQLException {

    }

    public Session getSession() throws HibernateException {

        Session sess = getSessionFactory().getCurrentSession();
        if (sess == null) {
            sess = getSessionFactory().openSession();
        }
        return sess;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    @Override
    public Class getPersistentClass() {
        return null;
    }

    @Override
    public Set<ConstraintViolation> validate(Object object) {
        return null;
    }

    @Override
    public Validator getValidator() {
        return null;
    }


    /**
     * Find a list of records by using a named query
     *
     * @param queryName   query name of the named query
     * @param queryParams a map of the query names and the values
     * @return a list of the records found
     */
    @Override
    public List findByNamedQuery(String queryName, Map queryParams) {
        return null;
    }
}
