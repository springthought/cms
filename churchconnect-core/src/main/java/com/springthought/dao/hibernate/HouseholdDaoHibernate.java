package com.springthought.dao.hibernate;

import com.springthought.dao.HouseholdDao;
import com.springthought.model.Household;
import org.springframework.stereotype.Repository;

@Repository("householdDao")
public class HouseholdDaoHibernate extends GenericDaoHibernate<Household, Long>
		implements HouseholdDao {

	public HouseholdDaoHibernate() {
		super(Household.class);
	}

	public Household saveHousehold(Household household) {
		if (log.isDebugEnabled()) {
			log.debug("Household's id: " + household.getHouseholdId());
		}
		getSession().saveOrUpdate(household);
		getSession().flush();
		return household;
	}

	@Override
	public Household save(Household household) {
		return this.saveHousehold(household);
	}


}
