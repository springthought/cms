package com.springthought.dao.hibernate;

import com.springthought.Constants.UserValueType;
import com.springthought.dao.UserValueDao;
import com.springthought.model.UserValue;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

;

@Repository("userValueDao")
public class UserValueDaoHibernate extends GenericDaoHibernate<UserValue, Long>
		implements UserValueDao {


	public UserValueDaoHibernate() {
		super(UserValue.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserValue> getUserValueByType(UserValueType userValuetype) {
		Criteria crtr = getSession().createCriteria(UserValue.class);
		crtr.add(Restrictions.eq("value_Type", userValuetype));
		return crtr.list();
	}





}
