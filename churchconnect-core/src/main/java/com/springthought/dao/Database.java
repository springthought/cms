/**
 *
 */
package com.springthought.dao;

import com.springthought.Constants;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {

	public static Connection getConnection() throws IOException,
			ClassNotFoundException, SQLException {

		Properties prop = new Properties();

		prop.load(Database.class.getClassLoader().getResourceAsStream(
				Constants.JDBC_PROPERTIES));

		Class.forName(prop.getProperty("jdbc.driverClassName"));

		Connection cxn = DriverManager.getConnection(
				prop.getProperty("jdbc.url"),
				prop.getProperty("jdbc.username"),
				prop.getProperty("jdbc.password"));

		return cxn;

	}

	public static void close(Connection cxn) throws SQLException {
		if (cxn != null && !cxn.isClosed()) {
			cxn.close();
		}

	}
}
