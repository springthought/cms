/**
 * 
 */
package com.springthought.dao;

import com.springthought.model.Person;

/**
 * @author eairrick
 *
 */
public interface PersonDao extends GenericDao<Person, Long> {

}
