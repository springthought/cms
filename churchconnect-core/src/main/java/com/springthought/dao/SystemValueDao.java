package com.springthought.dao;

import com.springthought.Constants.CodeListType;
import com.springthought.model.SystemValue;

import java.util.List;

;

public interface SystemValueDao extends GenericDao<SystemValue, Long> {

	public List<SystemValue> getSystemValueByCodeListType( CodeListType codelist );
}
