/**
 *
 */
package com.springthought.dao;

import com.springthought.Constants.MenuType;
import com.springthought.model.SystemMenu;

import java.util.List;

/**
 * @author kinseye
 *
 */
public interface SystemMenuDao extends GenericDao< SystemMenu, Long> {

	public List<SystemMenu> findByMenuType(MenuType menuType);
}
