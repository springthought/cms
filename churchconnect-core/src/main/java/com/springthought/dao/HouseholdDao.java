/**
 * 
 */
package com.springthought.dao;

import com.springthought.model.Household;

/**
 * @author eairrick
 *
 */
public interface HouseholdDao extends GenericDao<Household, Long> {
	
	public Household saveHousehold( Household household);

}
