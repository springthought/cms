/**
 *
 */
package com.springthought.util;

import junit.framework.TestCase;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author eairrick
 *
 */
public class TimePeriodTest extends TestCase {

	public void testTimePeriod(){

		ArrayList<Date> dateList = new ArrayList<Date>();


		DateTime  dt = new DateTime(2199, 1, 1, 0, 0, 0, 0);
		DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm a");

		for (int m = 0; m < 1440; m += 30) {

			DateTime adjDt = dt.plus(Period.minutes(m));
			String str = adjDt.toString(fmt);
			System.out.println("JoDa Time: " + str);
			dateList.add(adjDt.toDate());

		}

	}

}
