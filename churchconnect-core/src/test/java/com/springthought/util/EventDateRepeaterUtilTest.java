package com.springthought.util;

import com.google.common.collect.Lists;
import io.lamma.Date;
import io.lamma.Dates;
import io.lamma.DayOfWeek;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class EventDateRepeaterUtilTest extends TestCase {
	@Before
	public void setUp() {
		// ========== date creation =============
		// create date with constructor
		System.out.println(new Date(2014, 7, 5));
		// Output: Date(2014,7,5)

		// create date with helper method
		System.out.println(Dates.newDate(2014, 7, 5));
		// Output: Date(2014,7,5)

		// create date with ISO 8601 format
		System.out.println(Dates.newDate("2014-07-05"));
		// Output: Date(2014,7,5)

		// ========= compare dates =============
		System.out.println(Dates.newDate(2014, 7, 7).isBefore(
				Dates.newDate(2014, 7, 8)));
		// Output: true

		// also support isAfter, isOnOrBefore and isOnOrAfter
		System.out.println(Dates.newDate(2014, 7, 7).isOnOrAfter(
				Dates.newDate(2014, 7, 8)));
		// Output: false

		// ========== manipulate dates =============

		System.out.println(Dates.newDate(2014, 7, 5).plusDays(2)); // plus days
		// Output: Date(2014,7,7)

		System.out.println(Dates.newDate(2014, 7, 5).plusWeeks(2)); // plus
																	// weeks
		// Output: Date(2014,7,19)

		System.out.println(Dates.newDate(2014, 7, 5).plusMonths(5)); // plus
																		// months
		// Output: Date(2014,12,5)

		System.out.println(Dates.newDate(2014, 7, 5).plusYears(10)); // plus
																		// years
		// Output: Date(2024,7,5)

		System.out.println(Dates.newDate(2014, 7, 5).minusDays(5)); // minus
																	// will work
																	// similar
																	// as plus
		// Output: Date(2014,6,30)

		// minus two Dates will result day difference in int
		System.out.println(Dates.newDate(2014, 7, 10).minus(
				Dates.newDate(2014, 7, 3)));
		// Output: 7 (int)

		// ==== week operations ======

		// is(DayOfWeek)
		System.out.println(Dates.newDate(2014, 7, 7).is(DayOfWeek.MONDAY));
		// Output: true

		// find the next Friday excludes today
		System.out.println(Dates.newDate(2014, 7, 7).next(DayOfWeek.FRIDAY));
		// Output: Date(2014,7,11)

		// find the past Wednesday excludes today
		System.out.println(Dates.newDate(2014, 7, 7).previous(
				DayOfWeek.WEDNESDAY));
		// Output: Date(2014,7,2)

		// goto Monday of current week (a week starts with Monday)
		System.out.println(Dates.newDate(2014, 7, 7).withDayOfWeek(
				DayOfWeek.MONDAY));
		// Output: Date(2014,7,7)

		// goto Sunday of current week (a week starts with Monday)
		System.out.println(Dates.newDate(2014, 7, 7).withDayOfWeek(
				DayOfWeek.SUNDAY));
		// Output: Date(2014,7,13)

		// list of dates of this week
		for (Date d : Dates.newDate(2014, 7, 7).daysOfWeek4j()) {
			System.out.println(d);
		}
		// Output: from Date(2014,7,7) to Date(2014,7,13)

		// ======== month operations ======

		// lists of Dates of this month
		for (Date d : Dates.newDate(2014, 7, 7).daysOfMonth4j()) {
			System.out.println(d);
		}
		// Output: from Date(2014,7,1) to Date(2014,7,31)

		// max day of month, leap day is considered
		System.out.println(Dates.newDate(2016, 2, 5).maxDayOfMonth());
		// Output: 29

		// last day of this month, leap day is considered
		System.out.println(Dates.newDate(2016, 2, 5).maxDayOfMonth());
		// Output: Date(2016, 2, 29)

		// all Fridays in Feb, 2016
		for (Date d : Dates.newDate(2016, 2, 5).sameWeekdaysOfMonth4j()) {
			System.out.println(d);
		}
		// Output: Date(2016,2,5), Date(2016,2,12), Date(2016,2,19) and
		// Date(2016,2,26)

		// ======== year operations ======

		// max day of year, leap year is considered
		System.out.println(Dates.newDate(2016, 2, 5).maxDayOfYear());
		// Output: 366

		System.out.println(Dates.newDate(2016, 2, 5).dayOfYear()); // day of
																	// year
		// Output: 36 (2016-02-05 is the 36th day of 2016)

		// all Fridays in 2016
		for (Date d : Dates.newDate(2016, 2, 5).sameWeekdaysOfYear4j()) {
			System.out.println(d);
		}
		// Output: Date(2016,1,1), Date(2016,1,8), Date(2016,1,15), ...,
		// Date(2016,12,30)
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testDailyNoWeekEnds() throws Exception {

		List<Date> results = EventDateRepeaterUtil.daily(
				Dates.newDate(2014, 1, 1), Dates.newDate(2014, 1, 31), true, 1);

		assertTrue(results.size() == 23);
	}

	@Test
	public void testDaily() throws Exception {

		List<Date> results = EventDateRepeaterUtil.daily(
				Dates.newDate(2014, 2, 1), Dates.newDate(2014, 2, 28), false, 1);

		assertTrue(results.size() == 28);
	}

	@Test
	public void testWeekly() {

		List<Date> results = EventDateRepeaterUtil.weekly(
				Dates.newDate(2014, 1, 1), Dates.newDate(2014, 1, 31),1);

		assertTrue(results.size() == 5);
	}

	@Test
	public void testBiWeekly() {

		List<Date> results = EventDateRepeaterUtil.weekly(
				Dates.newDate(2014, 1, 1), Dates.newDate(2014, 1, 31),2);
		assertTrue(results.size() == 3);
	}

	@Test
	public void testDateMonthly() {

		List<Date> results = EventDateRepeaterUtil.monthly(
				Dates.newDate(2014, 1, 1), Dates.newDate(2014, 3, 31), 10, 1);

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 1, 10),
				Dates.newDate(2014, 2, 10), Dates.newDate(2014, 3, 10));

		assertEquals(results, expected);

	}

	@Test
	public void testFirstDayMonthly() {

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 8, 4),
				Dates.newDate(2014, 9, 1), Dates.newDate(2014, 10, 6));

		List<Date> results = EventDateRepeaterUtil.firstDayMonthly(
				Dates.newDate(2014, 8, 1), Dates.newDate(2014, 10, 31),
				DayOfWeek.MONDAY,1);

		assertEquals(results, expected);
	}

	@Test
	public void testLastDayMonthly() {

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 8, 25),
				Dates.newDate(2014, 9, 29), Dates.newDate(2014, 10, 27));

		List<Date> results = EventDateRepeaterUtil.lastDayMonthly(
				Dates.newDate(2014, 8, 1), Dates.newDate(2014, 10, 31),
				DayOfWeek.MONDAY, 1);

		assertEquals(results, expected);
	}

	@Test
	public void testNthMonthly() {

		List<Date> results = EventDateRepeaterUtil.nthMonthly(
				Dates.newDate(2014, 8, 1), Dates.newDate(2014, 10, 31), 2,
				DayOfWeek.MONDAY, 1);

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 8, 11),
				Dates.newDate(2014, 9, 8), Dates.newDate(2014, 10, 13));

		assertEquals(results, expected);

	}

	@Test
	public void testNthDayMonthly() {

		List<Date> results = EventDateRepeaterUtil.nthDayMonthly(
				Dates.newDate(2014, 1, 8), Dates.newDate(2014, 3, 30), 8 , 1);

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 1, 8),
				Dates.newDate(2014, 2, 8), Dates.newDate(2014, 3, 8));

		assertEquals(results, expected);

	}

	public void testYearly() {

		List<Date> results = EventDateRepeaterUtil.yearly(
				Dates.newDate(2014, 1, 10), Dates.newDate(2017, 1, 10), 1);

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 1, 10),
				Dates.newDate(2015, 1, 10), Dates.newDate(2016, 1, 10),
				Dates.newDate(2017, 1, 10));

		assertEquals(results, expected);

	}

	@Test
	public void testFirstDayYealy() {

		List<Date> expected = Lists.newArrayList(Dates.newDate(2015, 8, 3),
				Dates.newDate(2016, 8, 1));

		List<Date> results = EventDateRepeaterUtil.firstDayYearly(
				Dates.newDate(2014, 8, 20), Dates.newDate(2016, 8, 20),
				DayOfWeek.MONDAY, 1);

		assertEquals(results, expected);
	}

	@Test
	public void testLastDayYealy() {

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 8, 25),
				Dates.newDate(2015, 8, 31), Dates.newDate(2016, 8, 29));

		List<Date> results = EventDateRepeaterUtil.lastDayYearly(
				Dates.newDate(2014, 8, 1), Dates.newDate(2016, 8, 31),
				DayOfWeek.MONDAY,1);

		assertEquals(results, expected);
	}

	@Test
	public void testNthYealy() {

		List<Date> expected = Lists.newArrayList(Dates.newDate(2014, 6, 19),
				Dates.newDate(2015, 6, 18), Dates.newDate(2016, 6, 16));

		List<Date> results = EventDateRepeaterUtil.nthYearly(
				Dates.newDate(2014, 6, 1), Dates.newDate(2016, 6, 30), 3,
				DayOfWeek.THURSDAY,1);

		assertEquals(results, expected);
	}

}
