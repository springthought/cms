<#assign pojoNameLower = pojo.shortName.substring(0,1).toLowerCase()+pojo.shortName.substring(1)>
<ui:composition template="/templates/mainMenu.xhtml"
                xmlns:f="http://java.sun.com/jsf/core"
                xmlns:h="http://java.sun.com/jsf/html"
                xmlns:ui="http://java.sun.com/jsf/facelets"
                xmlns:p="http://primefaces.org/ui"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:shared="http://java.sun.com/jsf/composite/components/shared">

	<ui:define name="pageTitle">${'#'}{text['${pojoNameLower}Detail.title']} </ui:define>

	<ui:param name="menu" value="${pojoNameLower}" />
	<ui:param name="bodyId" value="${pojoNameLower}" />

	<ui:define name="body">

	<h:form id="mainFrm" enctype="multipart/form-data" prependId="false">

	<p:growl id="growl" severity="info,warn" autoUpdate="true"	life="2000" globalOnly="true" />

	<p:messages id="msgs" severity="error" autoUpdate="true" globalOnly="true" />

	<p:panel style="width: 100%; margin: 0 auto"header="${'#'}{text['${pojoNameLower}Detail.title']}">

	<shared:instructionText textHeader="${'#'}{text['${pojoNameLower}Detail.heading']}" textSummary="${'#'}{text['${pojoNameLower}List.message']}" />


    <p:panelGrid columns="3"  styleClass="norowline nocolline listbutton" id="${pojoNameLower}Pnl">
		<p:commandButton action="mainMenu" id="cmdCancel" immediate="true" ajax="false" icon="fa fa-home Fs14 white" />
   	  	<p:commandButton action="${'#'}{${pojoNameLower}Form.edit}" id="add" immediate="true" ajax="false" icon="fa fa-plus Fs14 white"/>
    	<p:commandButton id="cmdDelete" icon="fa fa-trash Fs14 white" oncomplete="${pojoNameLower}DlgWv.show();" disabled="${'#'}{!${pojoNameLower}List.checked}"	update="dtbl${pojo.shortName}"/>
	</p:panelGrid>

	<p:dataTable id="dtbl${pojo.shortName}" var="${pojoNameLower}"
			value="${'#'}{${pojoNameLower}List.${util.getPluralForWord(pojoNameLower)}}"
			editable="false" widgetVar="${util.getPluralForWord(pojoNameLower)}WV" paginator="true"
			paginatorPosition="bottom" rows="25"
			paginatorTemplate="{CurrentPageReport} {FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown}"
			rowsPerPageTemplate="25,50,100"  rowKey="${'#'}{${pojoNameLower}.name}"
			selection="${'#'}{${pojoNameLower}List.selected${pojo.shortName}}" >


  	<p:ajax event="rowSelectRadio"
				listener="${'#'}{${pojoNameLower}List.radioSelected}"
				update=":mainFrm:cmdDelete :dlgFrm:dlg${pojo.shortName}" />

  	<p:column selectionMode="single" style="width:2%" />



    <#foreach field in pojo.getAllPropertiesIterator()>
    	<p:column headerText="${'#'}{text['${pojoNameLower}.${field.name}']}" sortBy="${'#'}{${pojoNameLower}.${field.name}}" filterBy="${'#'}{${pojoNameLower}.${field.name}}">
    <#if field.equals(pojo.identifierProperty)>
            <h:commandLink action="${'#'}{${pojoNameLower}Form.edit}" value="${'#'}{${pojoNameLower}.name}" ajaX="false" styleClass="ui-link">
                <f:param name="${field.name}" value="${'#'}{${pojoNameLower}.${field.name}}"/>
                <f:param name="from" value="list"/>
            </h:commandLink>
    <#elseif !c2h.isCollection(field) && !c2h.isManyToOne(field) && !c2j.isComponent(field)>
        <#if field.value.typeName == "java.util.Date" || field.value.typeName == "date">
            <#lt/>    <h:outputText value="${'#'}{${pojoNameLower}.${field.name}}" escape="true"/>
        <#elseif field.value.typeName == "boolean">
            <#lt/>    <p:selectBooleanCheckbox value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}" id="${field.name}" disabled="disabled"/>
        <#else>
            <#lt/>    <h:outputText value="${'#'}{${pojoNameLower}.${field.name}}" escape="true"/>
        </#if>
    </#if>
        </p:column>

    </#foreach>
    </p:dataTable>
   </p:panel>
   </h:form>
   </ui:define>

   <ui:define name="confirmDialog">

   <p:confirmDialog id="dlg${pojo.shortName}"
   		header="${'#'}{text['dialog.deleteConfirmation']}"
		severity="alert" widgetVar="${pojoNameLower}DlgWv" showEffect="clip"
		hideEffect="fold">

		<f:facet name="message">
			<p:panelGrid columns="1">
				<h:outputFormat id="otplUserToDelete"
					value="${'#'}{text['dialog.areYouSure']}">
					<f:param value="${'#'}{${pojoNameLower}List.selected${pojo.shortName}.name}" />
					<f:param value="${'#'}{text['${pojoNameLower}List.${pojoNameLower}']}" />
				</h:outputFormat>
			</p:panelGrid>
		</f:facet>


		<p:commandButton id="cmdbtnConfirm" value="${'#'}{text['button.yes']}"
				oncomplete="${pojoNameLower}DlgWv.hide();"
				actionListener="${'#'}{${pojoNameLower}List.delete}"
				update=":mainFrm:dtbl${pojo.shortName} :mainFrm:growl :mainFrm:cmdDelete" />

		<p:commandButton id="decline" value="${'#'}{text['button.no']}"
				onclick="${pojoNameLower}DlgWv.hide();" type="button" />

	</p:confirmDialog>

	</ui:define>

</ui:composition>
