<#assign pojoNameLower = pojo.shortName.substring(0,1).toLowerCase()+pojo.shortName.substring(1)>
<#assign getIdMethodName = pojo.getGetterSignature(pojo.identifierProperty)>

package ${basepackage}.webapp.action;

import java.io.Serializable;
import java.util.List;

import ${appfusepackage}.dao.SearchException;
import ${pojo.packageName}.${pojo.shortName};
<#if genericcore>
import ${appfusepackage}.service.GenericManager;
<#else>
import ${basepackage}.service.${pojo.shortName}Manager;
</#if>
import ${basepackage}.webapp.action.BasePage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.primefaces.event.SelectEvent;
import javax.faces.event.ActionEvent;


@Component("${pojoNameLower}List")
@Scope("view")
public class ${pojo.shortName}List extends BasePage implements Serializable {
    private String query;
<#if genericcore>
    private GenericManager<${pojo.shortName}, ${pojo.getJavaTypeName(pojo.identifierProperty, jdk5)}> ${pojoNameLower}Manager;
<#else>
    private ${pojo.shortName}Manager ${pojoNameLower}Manager;
</#if>
	private ${pojo.shortName} selected${pojo.shortName} = new ${pojo.shortName}();
    private boolean checked;

    @Autowired
<#if genericcore>
    public void set${pojo.shortName}Manager(@Qualifier("${pojoNameLower}Manager") GenericManager<${pojo.shortName}, ${pojo.getJavaTypeName(pojo.identifierProperty, jdk5)}> ${pojoNameLower}Manager) {
<#else>
    public void set${pojo.shortName}Manager(${pojo.shortName}Manager ${pojoNameLower}Manager) {
</#if>
        this.${pojoNameLower}Manager = ${pojoNameLower}Manager;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public ${pojo.shortName}List() {
        setSortColumn("${pojo.identifierProperty.name}"); // sets the default sort column
    }

    public List<${pojo.shortName}> get${util.getPluralForWord(pojo.shortName)}() {
        try {
            return ${pojoNameLower}Manager.search(query, ${pojo.shortName}.class);
        } catch (SearchException se) {
            addError(se.getMessage());
            return sort(${pojoNameLower}Manager.getAll());
        }
    }

    public String search() {
        return "success";
    }
    
    public ${pojo.shortName} getSelected${pojo.shortName}() {
        return selected${pojo.shortName};
    }

    public void setSelected${pojo.shortName}(${pojo.shortName} ${pojoNameLower}) {
        this.selected${pojo.shortName} = ${pojoNameLower};
    }

	public void delete( ActionEvent event ){
    	${pojoNameLower}Manager.remove( selected${pojo.shortName}.${getIdMethodName}());
	  	addMessage("${pojoNameLower}.deleted");
	  	checked = false;
    }
    
        
    public void radioSelected(SelectEvent event) {checked = true;}

	public boolean isChecked() {return checked;}

	public void setChecked(boolean checked) {this.checked = checked;}
}