<#assign pojoNameLower = pojo.shortName.substring(0,1).toLowerCase()+pojo.shortName.substring(1)>
<ui:composition template="/templates/mainMenu.xhtml"
                xmlns:f="http://java.sun.com/jsf/core"
                xmlns:h="http://java.sun.com/jsf/html"
                xmlns:ui="http://java.sun.com/jsf/facelets"
                xmlns:p="http://primefaces.org/ui"
                xmlns:c="http://java.sun.com/jsp/jstl/core"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:shared="http://java.sun.com/jsf/composite/components/shared">


<ui:define name="pageTitle">${'#'}{text['${pojoNameLower}Detail.title']}</ui:define>

<ui:param name="menu" value="${pojoNameLower}" />
<ui:param name="bodyId" value="${pojoNameLower}" />

<ui:define name="body">

<h:form id="mainFrm" enctype="multipart/form-data" prependId="false">

<p:growl id="growl" severity="info,warn" autoUpdate="true"	life="2000" globalOnly="true" />
<p:messages id="msgs" severity="error" autoUpdate="true"	globalOnly="true" />

<p:panel style="width: 100%; margin: 0 auto"header="${'#'}{text['${pojoNameLower}Detail.title']}">
<shared:instructionText textHeader="${'#'}{text['${pojoNameLower}Detail.heading']}" textSummary="${'#'}{text['${pojoNameLower}Detail.message']}" />

<f:facet name="actions">
	<p:commandLink	styleClass="ui-panel-titlebar-icon ui-corner-all ui-state-default" ajax="true" oncomplete="dlgHelpWv.show();" update="@form">
		<h:outputText styleClass="ui-icon ui-icon-help" />
		<f:setPropertyActionListener target="${'#'}{${pojoNameLower}Form.helpKey}"	value="${pojoNameLower}.help" />
	</p:commandLink>
</f:facet>

<p:panelGrid columns="1"  layout="grid" styleClass="norowline nocolline" id="${pojoNameLower}Pnl">

<#rt/>
<#foreach field in pojo.getAllPropertiesIterator()>

<#if field.equals(pojo.identifierProperty)>
    <#foreach column in field.getColumnIterator()>
    <#assign idFieldName = field.name>
    <#if field.value.identifierGeneratorStrategy == "assigned">
    	<p:outputLabel styleClass="control-label" for="${field.name}" value="${'#'}{text['${pojoNameLower}.${field.name}']}"/>
        <p:inputText id="${field.name}" value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}" required="${(!column.nullable)?string}"/>
        <p:message for="${field.name}" styleClass="help-inline"/>
    <#else>
        <#lt/><h:inputHidden value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}" id="${field.name}"/>
		<p:spacer />
		<p:spacer />
    </#if>
    </#foreach>

<#elseif !c2h.isCollection(field) && !c2h.isManyToOne(field) && !c2j.isComponent(field)>
    				<p:outputLabel styleClass="control-label" for="${field.name}" value="${'#'}{text['${pojoNameLower}.${field.name}']}"/>
    <#foreach column in field.getColumnIterator()>
        <#if field.value.typeName == "java.util.Date" || field.value.typeName == "date" >
            <#lt/>	<p:calendar value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}" id="${field.name}" required="${(!column.nullable)?string}" showOn="button" />
        <#elseif field.value.typeName == "boolean" || field.value.typeName == "java.lang.Boolean">
            <#lt/> <p:selectBooleanCheckbox value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}" id="${field.name}"/>
        <#else>
            <#lt/>	<p:inputText id="${field.name}" value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}" required="${(!column.nullable)?string}"<#if (column.length > 0)> maxlength="${column.length?c}"</#if>/>
        </#if>
        <p:message for="${field.name}" styleClass="help-inline"/>
      </#foreach>
<#elseif c2h.isManyToOne(field)>
    <#foreach column in field.getColumnIterator()>

            <#lt/>        <!-- todo: change this to read the identifier field from the other pojo -->
            <#lt/>  <p:selectOneMenu value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}" id="${field.name}" required="${(!column.nullable)?string}" styleClass="select">
            <f:selectItems value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.${field.name}}"/>
        </p:selectOneMenu>
        <p:message for="${field.name}" styleClass="help-inline"/>

    </#foreach>
</#if>
</#foreach>

</p:panelGrid>

<f:facet name="footer">
			<h:panelGroup>
				<p:splitButton value="${'#'}{text['button.save']}" actionListener="${'#'}{${pojoNameLower}Form.save}"
            		id="save" styleClass="btn btn-primary" ajax="false" icon="fa fa-floppy-o Fs14 white" update="growl">
            			<p:menuitem value="${'#'}{text['button.saveReturn']}"
							action="${'#'}{${pojoNameLower}Form.save}" icon="ui-icon-newwin">
						</p:menuitem>
            	</p:splitButton>

        		<c:if test="${'$'}{not empty ${pojoNameLower}Form.${pojoNameLower}.${idFieldName}}">
            		<p:commandButton value="${'#'}{text['button.delete']}" 	id="delete"  icon="fa fa-trash Fs14 white" oncomplete="${pojoNameLower}DlgWv.show();" update="growl"	/>
        		</c:if>

        		<p:commandButton value="${'#'}{text['button.cancel']}" action="${util.getPluralForWord(pojoNameLower)}" immediate="true"
            		id="cancel" styleClass="btn" ajax="false" icon="fa fa-hand-o-left Fs14 white" />
            </h:panelGroup>
		</f:facet>

</p:panel>

	<shared:helpdialog dlgid="dlgHelp" title="${'#'}{text['${pojoNameLower}Detail.heading']}" widgetId="dlgHelpWv">
		<h:outputText value="${'#'}{${pojoNameLower}Form.helpMessage}" style="font-size: 120% !important;" />
	</shared:helpdialog>

   <p:confirmDialog id="dlg${pojo.shortName}"
   		header="${'#'}{text['dialog.deleteConfirmation']}"
		severity="alert" widgetVar="${pojoNameLower}DlgWv" showEffect="clip"
		hideEffect="fold">

		<f:facet name="message">
			<p:panelGrid columns="1">
				<h:outputFormat id="otplUserToDelete"
					value="${'#'}{text['dialog.areYouSure']}">
					<f:param value="${'#'}{${pojoNameLower}Form.${pojoNameLower}.name}" />
					<f:param value="${'#'}{text['${pojoNameLower}List.${pojoNameLower}']}" />
				</h:outputFormat>
			</p:panelGrid>
		</f:facet>


		<p:commandButton id="cmdbtnConfirm" value="${'#'}{text['button.yes']}"
				oncomplete="${pojoNameLower}DlgWv.hide();"
				action="${'#'}{${pojoNameLower}Form.delete}"
				update=":mainFrm:growl" ajax="false" />

		<p:commandButton id="decline" value="${'#'}{text['button.no']}"
				onclick="${pojoNameLower}DlgWv.hide();" type="button" />

	</p:confirmDialog>
   </h:form>
</ui:define>

</ui:composition>

